import React, { Component } from 'react';

import {
  Alert,
} from 'react-native';

import { Router, Scene } from 'react-native-router-flux'

import Login from './src/components/LoginScreen.js'
import ForgetPassword from './src/components/ForgetPassword.js'
import Welcome from './src/components/WelcomeScreen.js'
import Signup from './src/components/Signup.js'
import Home from './src/components/Home.js'
import ShoppingList from './src/components/ShoppingList.js'
import GoShopping from './src/components/GoShopping.js'
import MoneyManagement from './src/components/MoneyManagement.js'
import MyMoney from './src/components/MyMoney.js'
import Pantry from './src/components/Pantry.js'
import SetUp from './src/components/SetUp.js'


const Routes = () => (
   <Router>
      <Scene key = "root">
         <Scene key = "Welcome" component = {Welcome} title = "Welcome"  initial = {true}/>
         <Scene key = "Login" component = {Login} title = "Login"/>
         <Scene key = "ForgetPassword" component = {ForgetPassword} title = "ForgetPassword"/>
         <Scene key = "Signup" component = {Signup} title = "Signup"/>
         <Scene key = "Home" component = {Home} title = "Home"/>
         <Scene key = "ShoppingList" component = {ShoppingList} title = "ShoppingList"  />
         <Scene key = "GoShopping" component = {GoShopping} title = "GoShopping"  />
         <Scene key = "MoneyManagement" component = {MoneyManagement} title = "MoneyManagement" />
         <Scene key = "MyMoney" component = {MyMoney} title = "MyMoney" />
         <Scene key = "Pantry" component = {Pantry} title = "Pantry"  />
         <Scene key = "SetUp" component = {SetUp} title = "SetUp" />
      </Scene>
   </Router>
)
export default Routes
