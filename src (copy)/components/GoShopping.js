/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  Button,
  TouchableHighlight,
  Image,
  TextInput,
  Keyboard,
  Alert,
  ScrollView,
  Modal,
  AsyncStorage,
  ImageBackground,
  TouchableOpacity,
  PanResponder,
  Animated,
  Easing,
  Dimensions,
  ListView,
  ActivityIndicator
} from 'react-native';

import styles from './GlobalStyle.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import { Table, TableWrapper, Row, Cell , Rows } from 'react-native-table-component';
import ImagePicker from 'react-native-image-picker';
import * as Animatable from 'react-native-animatable';
import renderIf from './renderIf'
import GlobalVariables from './GlobalVariables.js';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import {default as Sound} from 'react-native-sound';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";

type Props = {};
let whoosh=null;  
export default class GoShopping extends Component<Props> {

  static navigationOptions = {
      header: null
  }



 constructor(props) {
    super(props);

   AsyncStorage.getItem('TOKEN').then((data) => { 
    
      if(!data){
          Actions.Login();
      }
            
    });

   var GVar = GlobalVariables.getApiUrl();

    this.dataDrag = [1];
    this.pan = this.dataDrag.map( () => new Animated.ValueXY() );

    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
   
    this.state = {
      user_id               : null,
      token                 : null,
      avatarSource          : null,
      profileimage          : null,
      profileimage2         : null,
      subscribe             : null,
      Localprofileimage     : null,
      Localprofileimage2    : null,
      tableHead             : ['ITEMS','QUANTITY / WEIGHT (in Kg)','EACH PRICE'],
      tableDataReplica      : [],
      tableData             : [],
      tableHeadinLine       : ['ITEMS','EACH PRICE'],
      tableDatainColumn     : [ ],
      tableDatainPrice      : [ ],
      tableDatainPriceLine  : [ ],
      price             :'0.00',
      weight            :'0.00',
      cost              :'0.00',
      spendmoney        : 0,
      modalVisible      : false,
      modalVisible1     : false,
      showcolumn        : false,
      totalmoney        : null,
      ImagePath         : GVar.ImagePath,
      showmodel1        : false,
      pantryaddedid     : null,
      Pantryuseradded   : null,
      latestpantry      :null,
      userdatasource    :null,
      showDraggable     : true,
      dropZoneValues    : null,
      dataSource        : [],
      pantryinbox       : [],
      cross             : false,
      pantryaddedPrice  : null,
      LowmoneyValue     : null,
      editprice         : false,
      dataeditprice     : '',
      indexeditprice    : '',   
      elementofreditprice   : '',
      dataeditQuantity      : '',
      indexcellQuantity     : '',   
      elementIndexQuantity  : '',
      ispricevisible        : true, 
      editquantity          : false, 
      pantryweighttype      : false,
      indexofweighted_data  : '',
      isLoading             : true,
    };


    AsyncStorage.getItem('LowmoneyValue').then((data) => { 

        if( JSON.parse(data) ){

            this.setState({LowmoneyValue: parseFloat(JSON.parse(data)).toFixed(2),});            
            //this.setState({AmountLeft: parseFloat(JSON.parse(0)).toFixed(2),});

        }else{

            this.setState({LowmoneyValue: parseFloat(0.00).toFixed(2)});            
            //this.setState({AmountLeft: parseFloat(0.00).toFixed(2)});
        }
                          
    });
    AsyncStorage.multiGet(["profileimage", "profileimage2"]).then(response => {
    
           this.setState({
                                    profileimage     : response[0][1],
                                    profileimage2    : response[1][1],
                                   
                          });   
                          Alert.alert(JSON.parse(profileimage));            
    });


    AsyncStorage.getItem('totalmoney').then((data) => { 

        if( JSON.parse(data) ){

            this.setState({totalmoney: parseFloat(JSON.parse(data)).toFixed(2),});            
            this.setState({AmountLeft: parseFloat(0.00).toFixed(2)});

        }else{

            this.setState({totalmoney: parseFloat(0.00).toFixed(2)});            
            this.setState({AmountLeft: parseFloat(0.00).toFixed(2)});
        }
              
                          
    });


      AsyncStorage.getItem('isSingle').then((data) => {
          
            console.log(data,"CHECKLANGUAGE")

            if( data == 'true'){

              this.setState({isSingle : true});
              this.setState({ShowtwoLanguageBox : false});
            }

            if( data == 'false'){

              this.setState({isSingle : false});
              this.setState({ShowtwoLanguageBox : true});
            }

            if( data == null ){
              this.setState({ShowtwoLanguageBox : true});
            }

            

      });  


 
    


    AsyncStorage.multiGet(["tableData", "tableDatainColumn","tableDataReplica","tableDatainPrice","tableDatainPriceLine","user_id","TOKEN","userdatasource","dataSource"]).then(response => {
              
          var tableData                     = response[0][1];
          var tableDatainColumn             = response[1][1];
          var tableDataReplica              = response[2][1];
          var tableDatainPrice              = response[3][1];
          var tableDatainPriceLine          = response[4][1];
          var user_id                       = response[5][1];
          var token                         = response[6][1];
          var userdatasource                = response[7][1];
          //var dataSource                    = response[8][1];

          
          this.setState({ isLoading              : false });

          console.log(tableData,"GoShoppingtableData");
          console.log(tableDatainColumn,"GoShoppingtableDatainColumn");
          console.log(tableDataReplica,"GoShoppingtableDataReplica");
          console.log(tableDatainPrice,"GoShoppingtableDatainPrice");
          console.log(tableDatainPriceLine,"GoShoppingtableDatainPriceLine");
          console.log(userdatasource,"GoShoppinguserdatasource");
         
         

         
          if( JSON.parse(tableData) ){
            this.setState({ tableData             : JSON.parse(tableData) });
           
          }else{
            this.setState({ tableData             : [] });
          }
         
          if(JSON.parse(tableDatainColumn) ){
            this.setState({ tableDatainColumn             : JSON.parse(tableDatainColumn) });
            
          }else{
            this.setState({ tableDatainColumn             : [] });
          }

          if( JSON.parse(tableDataReplica) ){
            this.setState({ tableDataReplica             : JSON.parse(tableDataReplica) });
          }else{
            this.setState({ tableDataReplica             : [] });
            
          }


          if( JSON.parse(tableDatainPrice) ){
            this.setState({ tableDatainPrice             : JSON.parse(tableDatainPrice) });
          }else{
            this.setState({ tableDatainPrice             : [] });
            
          }

          if( JSON.parse(tableDatainPriceLine) ){
             this.setState({ tableDatainPriceLine             : JSON.parse(tableDatainPriceLine) });
          }else{
            this.setState({ tableDatainPriceLine             : [] });
           
          }

       

            this.setState({ user_id               : JSON.parse(user_id) });
            this.setState({ token                 : token });
            this.setState({ userdatasource        : JSON.parse(userdatasource) });
           
       
    });

    AsyncStorage.multiGet(["Localprofileimage", "Localprofileimage2","cinema", "cinema1", "dancing","dancing1", "sports", "sports1", "nightout", "nightout1","AmountLeft","totalmoney","pantryinbox","tableDatainPrice","pantryaddedPrice"]).then(response => {
                
            var Localprofileimage             = response[0][1];
            var Localprofileimage2            = response[1][1];
            var cinema                        = response[2][1];
            var cinema1                       = response[3][1];
            var dancing                       = response[4][1];
            var dancing1                      = response[5][1];
            var sports                        = response[6][1];
            var sports1                       = response[7][1];
            var nightout                      = response[8][1];
            var nightout1                     = response[9][1];
            var AmountLeft                    = response[10][1];
            var totalmoney                    = response[11][1];
            var pantryinbox                   = response[12][1];
            var tableDatainPrice              = response[13][1];
            var pantryaddedPrice              = response[14][1];
            var tableDatainPrice              = this.state.tableDatainPrice;

           

            if( pantryaddedPrice === null || pantryaddedPrice < 0){
              pantryaddedPrice = 0;
            }else{
              pantryaddedPrice = JSON.parse(pantryaddedPrice);
            }


            if( pantryinbox === null ){
              this.setState({pantryinbox: []});
            }else{
              this.setState({pantryinbox: JSON.parse(pantryinbox)});
            }

            var pantryinboxData = this.state.pantryinbox;

            var pantryaddedPriceForShopping = 0;           

            for(var i = 0; i < pantryinboxData.length; i++) {
                
                var obj   = pantryinboxData[i];
                var index = this.getIndexOfK(tableDatainPrice, obj);

                if( index != -1 ){

                  cell1     = index[0];
                  cell2     = index[1];

               
                  
                  pantryaddedPriceForShopping = pantryaddedPriceForShopping + (JSON.parse(tableDatainPrice[cell1][2]) * JSON.parse(tableDatainPrice[cell1][1]));
                  console.log(JSON.parse(tableDatainPrice[cell1][1]),"pantryaddedPricepantryaddedPrice1");
                }else{

                  pantryaddedPriceForShopping = pantryaddedPriceForShopping + 0;
                  console.log("pantryaddedPricepantryaddedPrice2");
                }

                console.log("pantryaddedPricepantryaddedPrice3");
                                
            }

            pantryaddedPrice = pantryaddedPriceForShopping; 


            //console.log(pantryaddedPrice,"pantryaddedPricepantryaddedPrice");
            //console.log(pantryaddedPriceForShopping,"pantryaddedPriceForShopping");

            var c   = 0;
            var c1  = 0;
            var d   = 0;
            var d1  = 0;
            var s   = 0;
            var s1  = 0;
            var n   = 0;
            var n1  = 0;
            var amount      = 0;
            var lettotalmoney  = 0;



            if( JSON.parse(cinema) ){
              console.log(cinema,"cinema");
              this.setState({cinema : JSON.parse(cinema)});

              c = parseFloat(JSON.parse(cinema));

            }else{
              this.setState({cinema : 0});
              c =0;
            }

            if( JSON.parse(cinema1) ){
              this.setState({cinema1 : JSON.parse(cinema1)});
              c1 = parseFloat(JSON.parse(cinema1));
            }else{
              this.setState({cinema : 0});
              c1 =0;
            }

            if( JSON.parse(dancing) ){
              this.setState({dancing : JSON.parse(dancing)});
              d = parseFloat(JSON.parse(dancing));
            }else{
              this.setState({dancing : 0});
              d = 0;
            }

            if( JSON.parse(dancing1) ){
              this.setState({dancing1 : JSON.parse(dancing1)});
              d1 = parseFloat(JSON.parse(dancing1));
            }else{
              this.setState({dancing : 0});
              d1 =0;
            }

            if( JSON.parse(sports) ){
              this.setState({sports : JSON.parse(sports)});
              s = parseFloat(JSON.parse(sports));
            }else{
              this.setState({sports : 0});
              s =0;
            }

            if( JSON.parse(sports1) ){
              this.setState({sports1 : JSON.parse(sports1)});
              s1 = parseFloat(JSON.parse(sports1));
            }else{
              this.setState({sports1 : 0});
              s1 =0;
            }

            if( JSON.parse(nightout) ){
              this.setState({nightout : JSON.parse(nightout)});
              n = parseFloat(JSON.parse(nightout));
            }else{
              this.setState({nightout : 0});
              n = 0;
            }

            if( JSON.parse(nightout1) ){
              console.log(JSON.parse(nightout1),"nightout1")
              this.setState({nightout1 : JSON.parse(nightout1)});
              n1 = parseFloat(JSON.parse(nightout1));
            }else{
              this.setState({nightout1 : 0});
              n1 = 0;
            }

            if( JSON.parse(totalmoney) ){

              this.setState({totalmoney : JSON.parse(totalmoney)});
              lettotalmoney = parseFloat(JSON.parse(totalmoney));
              
            }else{

              this.setState({totalmoney : 0});
              lettotalmoney = 0;
            }

            if( JSON.parse(AmountLeft) ){
              amount = JSON.parse(AmountLeft);
            }else{
              amount = 0;
            }
           


            var total = c + c1 + d + d1 + n + n1 + s + s1 + pantryaddedPrice;
            var amountLeft = lettotalmoney - total;
            this.setState({AmountLeft : parseFloat(JSON.parse(amountLeft)).toFixed(2)});

            if( amountLeft < 0 ){

              this.setState({AmountLeft : JSON.parse(0)});

              Alert.alert("Warning message","Somthing went wrong please reset your money");
            }

            console.log(total,"TouchableHighlight");
            console.log(lettotalmoney,"lettotalmoney");
            console.log(amountLeft,"amountLeftamountLeft");
            console.log(pantryaddedPrice,"pantryaddedPrice");
    });

    AsyncStorage.getItem('PANTRYDATA').then((data) => {

     
            var pantry = JSON.parse(data);


            if( Array.isArray(pantry) && pantry.length > 0 ){

                this.setState({
                           isLoading : false,
                           dataSource: JSON.parse(data),
                         });
            }else{

              this.setState({
                           isLoading : false,
                           dataSource: [],
                         });
            }
            

    });
  
  }


    getPanResponder(index) {

    
        return PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove              : Animated.event([null,{
                dx  : this.pan[index].x,
                dy  : this.pan[index].y
            }]),
            onPanResponderRelease           : (e, gesture) => {
                if(this.isDropZone(e, gesture)){
                    /*this.setState({
                        showDraggable : false
                    });*/

                    Alert.alert("Please drag balance in shopping table.");
                    Animated.spring(
                        this.pan[index],
                        {toValue:{x:0,y:0}}
                    ).start();
                }else{
                  console.log(e.nativeEvent.changedTouches,"touchestouches");
                    this.setState({modalVisible: true});
                    Animated.spring(
                        this.pan[index],
                        {toValue:{x:0,y:0}}
                    ).start();
                }
            }
        });    //
    }

    isDropZone(e, gesture){

      //Alert.alert("sdfds")

        //this.setState({modalVisible: true});
        var dz = this.state.dropZoneValues;

        console.log(e.nativeEvent.layout,"instancetouches");
        console.log(dz,"dztouches");

        var dz = this.state.dropZoneValues;
        //Alert.alert(JSON.stringify(e.nativeEvent.changedTouches));
        console.log(e.nativeEvent.changedTouches,"touches");
        console.log(gesture,"gesturetouches");
        //console.log(JSON.stringify(gesture.moveY),"gesture");
        return gesture.moveY > dz.y && gesture.moveY < dz.y + dz.height;
    }

    setDropZoneValues(event){

        this.setState({
            dropZoneValues : event.nativeEvent.layout,
            //modalVisible   : true
        });

      
    }




  componentDidMount = async () => {

            this.setLanguages = this.setLanguages();

           AsyncStorage.multiGet(["Localprofileimage", "Localprofileimage2"]).then(response => {
                
            var Localprofileimage              = response[0][1];
            var Localprofileimage2             = response[1][1];
            //alert(Localprofileimage)


            this.setState({
                                    Localprofileimage     : JSON.parse(Localprofileimage),
                                    Localprofileimage2    : JSON.parse(Localprofileimage2),
                          });          
            });


            AsyncStorage.multiGet(["name", "user_id","LowMoneyWarningType","MoneymanagementAudioType", "MyMoneyAudioType", "ShoppingListAudioType", "ShoppingListType", "LanguageType", "Selfietype","GoShoppingAudioType","TOKEN","GoShoppingAudioType","LowmoneyStatus","LowmoneyTune","LowmoneyValue","PrimaryLanguage","SeconderyLanguage","LanguageTypestatus","LanguageType"]).then(response => {
            

                    var name                          = response[0][1];
                    var user_id                       = response[1][1];
                    var LowMoneyWarningType           = response[2][1];
                    var MoneymanagementAudioType      = response[3][1];
                    var MyMoneyAudioType              = response[4][1];
                    var ShoppingListAudioType         = response[5][1];
                    var ShoppingListType              = response[6][1];
                    var LanguageType                  = response[7][1];
                    var Selfietype                    = response[8][1];
                    var GoShoppingAudioType           = response[9][1];
                    var token                         = response[10][1];
                    var GoShoppingAudioType           = response[11][1];
                    var LowmoneyStatus                = response[12][1];
                    var LowmoneyTune                  = response[13][1];
                    var LowmoneyValue                 = response[14][1];
                    var PrimaryLanguage               = response[15][1];
                    var SeconderyLanguage             = response[16][1];
                    var LanguageTypestatus            = response[17][1];
                    var LanguageType1                  = response[18][1];

                   //Alert.alert(SeconderyLanguage);

                   if( ShoppingListType == 'column' ){
                      this.setState({showcolumn: true});
                     
                    }else{
                      this.setState({showcolumn: false});

                    }

                   this.setState({
                                    name                    : name,
                                    LowMoneyWarningType     : LowMoneyWarningType,
                                    MoneymanagementAudioType: MoneymanagementAudioType,
                                    MyMoneyAudioType        : MyMoneyAudioType,
                                    ShoppingListAudioType   : ShoppingListAudioType,
                                    ShoppingListType        : ShoppingListType,
                                    LanguageType            : LanguageType,
                                    Selfietype              : Selfietype,
                                    GoShoppingAudioType     : GoShoppingAudioType,
                                    LowmoneyStatus          : LowmoneyStatus,
                                    LowmoneyTune            : LowmoneyTune,
                                    LowmoneyValue           : LowmoneyValue,
                                    PrimaryLanguage         : PrimaryLanguage,
                                    SeconderyLanguage       : SeconderyLanguage,
                                    token                   : token,
                                    LanguageTypestatus      : LanguageTypestatus
                              });
                   
                   

              });    
  }




  onPressShoopingList(){
    Actions.ShoppingList();
  }

  onPressHome(){
    Actions.Home();
  }

  onPressPantry(){
    Actions.Pantry();
  }

  onPressMyMoney(){
     Actions.MyMoney();
  }

  onPressMoneyManagement(){
      Actions.MoneyManagement();
  }

  onPressSetUp(){

  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }



  setCamera1(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        Alert.alert("Please check Permition")
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
      

      let source = { uri: response.uri };

      //alert(source)

      
      var token = this.state.token;
      var user_id = this.state.user_id;
      var Selfietype = this.state.Selfietype;

      let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie1', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
        
        this.setState({ Localprofileimage : source});

        AsyncStorage.multiSet([
                                    ["Localprofileimage",          JSON.stringify(source)],
                                   
                                ]);

        //Alert.alert(Selfietype)
        if( Selfietype === "same"){

          this.setState({ Localprofileimage : source, Localprofileimage2: source });
          AsyncStorage.multiSet([
                                    ["Localprofileimage",          JSON.stringify(source)],
                                    ["Localprofileimage2",         JSON.stringify(source)],
                                ]);

        }

      }
    });
  }


  setCamera(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        

      //alert(source)

      
      var token = this.state.token;
      var user_id = this.state.user_id;
      var Selfietype = this.state.Selfietype;


      

      let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie2', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
        
        this.setState({ Localprofileimage2 : source});

        AsyncStorage.multiSet([
                                    ["Localprofileimage2",          JSON.stringify(source)],
                                   
                                ]);

        //Alert.alert(Selfietype)
        if( Selfietype === "same"){

          this.setState({ Localprofileimage : source, Localprofileimage2: source });
          AsyncStorage.multiSet([
                                    ["Localprofileimage",          JSON.stringify(source)],
                                    ["Localprofileimage2",         JSON.stringify(source)],
                                ]);

        }
      }
    });
  }

  onPressLogout(){

    //Alert.alert("fsd");
    AsyncStorage.removeItem('TOKEN');
    Actions.Login();

  }

  GetPantryImage(){

    
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        var arr   = [];

        
      var token = this.state.token;
      var user_id = this.state.user_id;
      var PrimaryLanguage = this.state.PrimaryLanguage;


      
      let formdata = new FormData();

      formdata.append('user_id', user_id );
      formdata.append('language_code', PrimaryLanguage );
      formdata.append('pantry_path', "" );
  
      formdata.append('pantry_path', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      console.log(formdata,"formdata");

      var GVar = GlobalVariables.getApiUrl();

      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.AddPantryImage,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                            this.setState({pantryaddedid: responseJson.id});
                                                
                                           });
              
                
             }catch (error) {


              if( error.line == 18228 ){
                


              }

            }

      }catch (error) {

       // Alert.alert('something went wrong please try after some time');

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }

        this.setState({
          //userdatasource    : arr,
          showmodel1         : true,
          modalVisible1      : true,
          latestpantry      : source,
        });


        // AsyncStorage.setItem('userdatasource', JSON.stringify(arr) );

       
      }
    });
  }


  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }


  closePantryModel(visible) {
            

            var GVar = GlobalVariables.getApiUrl();

            var user_id = this.state.user_id;
            var token = this.state.token;


            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('pantry_id', this.state.pantryaddedid );
            //formdata.append('pantry_id', '1' );
 
            try {
                  
                let response = fetch( GVar.BaseUrl + GVar.DeletePantry,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token,
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                             if( responseJson.status == 1 ){

                                                // Alert.alert(JSON.stringify(responseJson));
                                                             
                                             }
                                                
                                           });
                
               }catch (error) {

                if( error.line == 18228 ){
                  Alert.alert("Net is not connected when it will be conneted it will auto reconoige to secondary language");
                }

               
              }

              this.setState({modalVisible1: visible});
  }




   onChangePantryName = ( value ) => {



    this.setState({changedPrimaryPantryadded : value });

    AsyncStorage.multiGet(["name", "user_id","LanguageType","TOKEN","PrimaryLanguage","SeconderyLanguage",]).then(response => {
    
            var name                          = response[0][1];
            var user_id                       = response[1][1];
            var LanguageType                  = response[2][1];
            var token                         = response[3][1];
            var PrimaryLanguage               = response[4][1];
            var SeconderyLanguage             = response[5][1];

            var GVar = GlobalVariables.getApiUrl();

            AsyncStorage.multiSet([
                                  ["PrimaryPantryadded",          JSON.stringify(value)],
                              ]);


            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('primary', PrimaryLanguage);
            formdata.append('secondary', SeconderyLanguage);
            formdata.append('text', value );
 
            try {
                  
                let response = fetch( GVar.BaseUrl + GVar.TextChange,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                             if( responseJson.status == 1 ){

                                                 //Alert.alert(JSON.stringify(responseJson));
                                                 this.setState({ Pantryuseradded: responseJson.text,});

                                                  AsyncStorage.multiSet([
                                                                            ["Pantryuseradded",             JSON.stringify(responseJson.text)],
                                                                            ["PrimaryPantryadded",          JSON.stringify(value)],
                                                                        ]);
                                             }
                                                
                                           });
                
               }catch (error) {

                if( error.line == 18228 ){
                  Alert.alert("Net is not connected when it will be conneted it will auto reconoige to secondary language");
                }

               
              }
    }); 


    
  }


  setLanguages(){

  

    AsyncStorage.getItem('PrimaryLanguage').then((P_LANG) => {
      
      AsyncStorage.getItem('Are you sure to log out?').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforlogout : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforlogout : voicefeatureGS.fil})
                              }

                        });
          
          AsyncStorage.getItem('ok').then((data) => {

                              var ok = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({ok : ok.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({ok : ok.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({ok : ok.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({ok : ok.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({ok : ok.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({ok : ok.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({ok : ok.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({ok : ok.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({ok : ok.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({ok : ok.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({ok : ok.fil})
                              }

          });

          AsyncStorage.getItem('cancel').then((data) => {

                              var cancel = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({cancel : cancel.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({cancel : cancel.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({cancel : cancel.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({cancel : cancel.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({cancel : cancel.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({cancel : cancel.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({cancel : cancel.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({cancel : cancel.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({cancel : cancel.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({cancel : cancel.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({cancel : cancel.fil})
                              }

          });

          AsyncStorage.getItem('WELCOME PAGE').then((data) => {

                var WECOME = JSON.parse(data);

                if( P_LANG == 'en'){

                  this.setState({WECOME : WECOME.en})
                }

                if( P_LANG == 'el'){

                    this.setState({WECOME : WECOME.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({WECOME : WECOME.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({WECOME : WECOME.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({WECOME : WECOME.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({WECOME : WECOME.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({WECOME : WECOME.it})
                }

                if( P_LANG == 'es'){

                    this.setState({WECOME : WECOME.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({WECOME : WECOME.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({WECOME : WECOME.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({WECOME : WECOME.fil})
                }

          });

          AsyncStorage.getItem('SHOPPING LIST').then((data) => {

                var SHOPPINGLIST = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({SHOPPINGLIST : SHOPPINGLIST.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SHOPPINGLIST : SHOPPINGLIST.fil})
                }

          });

          AsyncStorage.getItem('LETS GO SHOPPING').then((data) => {

                var GOSHOPPING = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({GOSHOPPING : GOSHOPPING.en})
                }

                if( P_LANG == 'el'){

                    this.setState({GOSHOPPING : GOSHOPPING.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({GOSHOPPING : GOSHOPPING.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({GOSHOPPING : GOSHOPPING.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({GOSHOPPING : GOSHOPPING.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({GOSHOPPING : GOSHOPPING.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({GOSHOPPING : GOSHOPPING.it})
                }

                if( P_LANG == 'es'){

                    this.setState({GOSHOPPING : GOSHOPPING.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({GOSHOPPING : GOSHOPPING.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({GOSHOPPING : GOSHOPPING.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({GOSHOPPING : GOSHOPPING.fil})
                }

          });

          AsyncStorage.getItem('PANTRY').then((data) => {

                var PANTRY = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({PANTRY : PANTRY.en})
                }

                if( P_LANG == 'el'){

                    this.setState({PANTRY : PANTRY.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({PANTRY : PANTRY.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({PANTRY : PANTRY.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({PANTRY : PANTRY.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({PANTRY : PANTRY.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({PANTRY : PANTRY.it})
                }

                if( P_LANG == 'es'){

                    this.setState({PANTRY : PANTRY.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({PANTRY : PANTRY.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({PANTRY : PANTRY.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({PANTRY : PANTRY.fil})
                }

          });

          AsyncStorage.getItem('HOW MUCH MONEY DO I HAVE').then((data) => {

                var MONEYIHAVE = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYIHAVE : MONEYIHAVE.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYIHAVE : MONEYIHAVE.fil})
                }

          });

          AsyncStorage.getItem('MONEY MANAGEMENT').then((data) => {

                var MONEYMANAGEMENT = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.fil})
                }

          });

          AsyncStorage.getItem('SETUP').then((data) => {

                var SETUP = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({SETUP : SETUP.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SETUP : SETUP.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SETUP : SETUP.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SETUP : SETUP.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SETUP : SETUP.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SETUP : SETUP.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SETUP : SETUP.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SETUP : SETUP.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SETUP : SETUP.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SETUP : SETUP.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SETUP : SETUP.fil})
                }

          });

          AsyncStorage.getItem('LOGOUT').then((data) => {

                var LOGOUT = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({LOGOUT : LOGOUT.en})
                }

                if( P_LANG == 'el'){

                    this.setState({LOGOUT : LOGOUT.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({LOGOUT : LOGOUT.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({LOGOUT : LOGOUT.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({LOGOUT : LOGOUT.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({LOGOUT : LOGOUT.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({LOGOUT : LOGOUT.it})
                }

                if( P_LANG == 'es'){

                    this.setState({LOGOUT : LOGOUT.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({LOGOUT : LOGOUT.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({LOGOUT : LOGOUT.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({LOGOUT : LOGOUT.fil})
                }

          });

          AsyncStorage.getItem('ITEM').then((data) => {

                var ITEM = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({ITEM : ITEM.en})
                }

                if( P_LANG == 'el'){

                    this.setState({ITEM : ITEM.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({ITEM : ITEM.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({ITEM : ITEM.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({ITEM : ITEM.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({ITEM : ITEM.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({ITEM : ITEM.it})
                }

                if( P_LANG == 'es'){

                    this.setState({ITEM : ITEM.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({ITEM : ITEM.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({ITEM : ITEM.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({ITEM : ITEM.fil})
                }

          });

          AsyncStorage.getItem('PRICE').then((data) => {

                var PRICE = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({PRICE : PRICE.en})
                }

                if( P_LANG == 'el'){

                    this.setState({PRICE : PRICE.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({PRICE : PRICE.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({PRICE : PRICE.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({PRICE : PRICE.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({PRICE : PRICE.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({PRICE : PRICE.it})
                }

                if( P_LANG == 'es'){

                    this.setState({PRICE : PRICE.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({PRICE : PRICE.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({PRICE : PRICE.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({PRICE : PRICE.fil})
                }

          });

          AsyncStorage.getItem('QUANTITY / WEIGHT').then((data) => {

                var QUANTITY = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({QUANTITY : QUANTITY.en})
                }

                if( P_LANG == 'el'){

                    this.setState({QUANTITY : QUANTITY.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({QUANTITY : QUANTITY.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({QUANTITY : QUANTITY.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({QUANTITY : QUANTITY.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({QUANTITY : QUANTITY.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({QUANTITY : QUANTITY.it})
                }

                if( P_LANG == 'es'){

                    this.setState({QUANTITY : QUANTITY.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({QUANTITY : QUANTITY.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({QUANTITY : QUANTITY.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({QUANTITY : QUANTITY.fil})
                }

          });

          AsyncStorage.getItem('MONEY SPEND SO FAR').then((data) => {

                var MONEYSPEND = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYSPEND : MONEYSPEND.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYSPEND : MONEYSPEND.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYSPEND : MONEYSPEND.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYSPEND : MONEYSPEND.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYSPEND : MONEYSPEND.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYSPEND : MONEYSPEND.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYSPEND : MONEYSPEND.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYSPEND : MONEYSPEND.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYSPEND : MONEYSPEND.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYSPEND : MONEYSPEND.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYSPEND : MONEYSPEND.fil})
                }

          });

          AsyncStorage.getItem('You have low price to add this pantry').then((data) => {

                var YouHaveLowMoney = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({YouHaveLowMoney : YouHaveLowMoney.en})
                }

                if( P_LANG == 'el'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.it})
                }

                if( P_LANG == 'es'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({YouHaveLowMoney : YouHaveLowMoney.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({YouHaveLowMoney : YouHaveLowMoney.fil})
                }

          });

          AsyncStorage.getItem('Please update weight on click weight').then((data) => {

                var PleaseUpdateWeight = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({PleaseUpdateWeight : PleaseUpdateWeight.en})
                }

                if( P_LANG == 'el'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.it})
                }

                if( P_LANG == 'es'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({PleaseUpdateWeight : PleaseUpdateWeight.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({PleaseUpdateWeight : PleaseUpdateWeight.fil})
                }

          });

          AsyncStorage.getItem('Suggestion message').then((data) => {

                var Suggestion_message = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({Suggestion_message : Suggestion_message.en})
                }

                if( P_LANG == 'el'){

                    this.setState({Suggestion_message : Suggestion_message.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({Suggestion_message : Suggestion_message.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({Suggestion_message : Suggestion_message.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({Suggestion_message : Suggestion_message.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({Suggestion_message : Suggestion_message.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({Suggestion_message : Suggestion_message.it})
                }

                if( P_LANG == 'es'){

                    this.setState({Suggestion_message : Suggestion_message.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({Suggestion_message : Suggestion_message.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({Suggestion_message : Suggestion_message.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({Suggestion_message : Suggestion_message.fil})
                }

          });

          AsyncStorage.getItem('Please first remove pantry item from selected Go-Shopping then add quantity').then((data) => {

                var firstRemovepantry = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({firstRemovepantry : firstRemovepantry.en})
                }

                if( P_LANG == 'el'){

                    this.setState({firstRemovepantry : firstRemovepantry.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({firstRemovepantry : firstRemovepantry.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({firstRemovepantry : firstRemovepantry.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({firstRemovepantry : firstRemovepantry.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({firstRemovepantry : firstRemovepantry.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({firstRemovepantry : firstRemovepantry.it})
                }

                if( P_LANG == 'es'){

                    this.setState({firstRemovepantry : firstRemovepantry.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({firstRemovepantry : firstRemovepantry.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({firstRemovepantry : firstRemovepantry.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({firstRemovepantry : firstRemovepantry.fil})
                }

          });

          AsyncStorage.getItem('Price can not Zero').then((data) => {

                var priceCanTZero = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({priceCanTZero : priceCanTZero.en})
                }

                if( P_LANG == 'el'){

                    this.setState({priceCanTZero : priceCanTZero.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({priceCanTZero : priceCanTZero.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({priceCanTZero : priceCanTZero.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({priceCanTZero : priceCanTZero.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({priceCanTZero : priceCanTZero.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({priceCanTZero : priceCanTZero.it})
                }

                if( P_LANG == 'es'){

                    this.setState({priceCanTZero : priceCanTZero.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({priceCanTZero : priceCanTZero.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({priceCanTZero : priceCanTZero.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({priceCanTZero : priceCanTZero.fil})
                }

          });
    });
  }


 getIndex(value, arr, prop) {
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }

 onPressSavePantry(){

     AsyncStorage.multiGet(["Pantryuseradded", "PrimaryPantryadded","TOKEN","user_id", "PrimaryLanguage","PANTRY"]).then(response => {

            var Pantryuseradded               = response[0][1];
            //var PrimaryPantryadded            = response[1][1];
            var token                         = response[2][1];
            var user_id                       = response[3][1];
            var PrimaryLanguage               = response[4][1];
            var dataSource                    = JSON.parse(response[5][1]);
            var source                       = this.state.latestpantry;
            var userdatasource               = this.state.userdatasource;

          
            

            var PrimaryPantryadded             = this.state.changedPrimaryPantryadded;

            console.log(JSON.stringify(dataSource),"dataSource");
            console.log(PrimaryPantryadded,"PrimaryPantryadded");

            if( !PrimaryPantryadded ){
              Alert.alert("Message", "Please enter pantry name also");
            }else{


                var userdatasource = this.state.userdatasource;

                if( userdatasource == null ){
                  userdatasource = [];
                }

                var pan = PrimaryPantryadded;
                var capspantry = pan.charAt(0).toUpperCase() + pan.substr(1);

                var index = this.getIndex(capspantry, userdatasource, 'pantry_name');
                var index1 = this.getIndex(capspantry, dataSource, 'en_pantry');

                console.log(index1,"index1");
            
                if( index < 0 && index1 < 0 ){


                /*insert into table*/

                var tabledata               = this.state.tableData;
                var tableDatainColumn       = this.state.tableDatainColumn;
                var tableDatainPrice        = this.state.tableDatainPrice;
                var tableDatainPriceLine    = this.state.tableDatainPriceLine;
                var tableDataReplica        = this.state.tableDataReplica;

                

                tabledata.unshift(
                          [JSON.stringify(source)]
                );


                arr     = tabledata;

                /*code to Column*/

                tableDatainColumn.unshift(
                    [JSON.stringify(source), '0']
                );

                arrCol  = tableDatainColumn;

                /*code to Price*/

                tableDatainPrice.unshift(
                    [JSON.stringify(source), "0", 0.00]
                );

                arrPrice  = tableDatainPrice;

                /*code to checking*/

                tableDataReplica.unshift([JSON.stringify(source), this.state.pantryaddedid]); 
                arrReplica  = tableDataReplica;

                /*code to price in Line*/

                tableDatainPriceLine.unshift(
                    [JSON.stringify(source), 0.00]
                );

                arrPriceLine     = tableDatainPriceLine;

                console.log(this.state.tableData,"tabledata");
                console.log(this.state.tableDatainColumn,"tableDatainColumn");
                console.log(this.state.tableDatainPrice,"tableDatainPrice");
                console.log(this.state.tableDatainPriceLine,"tableDatainPriceLine");
                console.log(this.state.tableDataReplica,"tableDataReplica");

                 this.setState({
                    tableData : arr,
                    tableDatainColumn : arrCol,
                    tableDataReplica : arrReplica,
                    tableDatainPrice : arrPrice,
                    tableDatainPriceLine : arrPriceLine,
                  });

              

                AsyncStorage.multiSet([
                    ["tableData",                 JSON.stringify(this.state.tableData)],
                    ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                    ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                    ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                    ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                ]);
                  /*end*/


                var arr = [];

                if( JSON.stringify(userdatasource) != 'null'){

                  arr = userdatasource;
                  id  = userdatasource.length + 1; 
                }

                var pan = PrimaryPantryadded;
                var capspantry = pan.charAt(0).toUpperCase() + pan.substr(1);

                arr.push({
                    useraddedpantry_id    : this.state.pantryaddedid, 
                    pantry_image          : source, 
                    pantry_name           : capspantry,
                    pantry_secondryname   : JSON.parse(Pantryuseradded),
                    pantry_isweighted     : this.state.pantryweighttype,
                });

                console.log(JSON.stringify(arr),"newpantryadded");

                AsyncStorage.setItem('userdatasource', JSON.stringify(arr) );

                
               

                this.setState({
                  userdatasource    : arr,
                });


                var token = this.state.token;
                var user_id = this.state.user_id;
                var PrimaryLanguage = this.state.PrimaryLanguage;


                
                let formdata = new FormData();

                formdata.append('user_id', user_id );
                formdata.append('language_code', PrimaryLanguage );
                formdata.append('id', this.state.pantryaddedid );
                formdata.append('pantry_name', capspantry );
                

                var GVar = GlobalVariables.getApiUrl();

                  try {
                        

                      try {
                            let response = fetch( GVar.BaseUrl + GVar.UpdatePantry,{
                                              method: 'POST',
                                              headers:{
                                                          'Accept': 'application/json',
                                                          'token' : token,
                                                          'Content-Type' : 'multipart/form-data',
                                                      },
                                              body: formdata,        
                                              }).then((response) => response.json())
                                                       .then((responseJson) => {

                                                        //Alert.alert(JSON.stringify(responseJson));
                                                            
                                                       });
                          
                            
                         }catch (error) {

                          if( error.line == 18228 ){
                            


                          }

                        }

                  }catch (error) {

                      if( error.line == 18228 ){
                        //Alert.alert("Please connect to network");
                      }

                           
                  }


                

                  Alert.alert("Message","OK" );
                  this.setState({modalVisible1: false});

                }else{
                  Alert.alert("Message", "Pantry allready added please add another");
                }
   
            }

     });

  } 

  getIndexOfK(arr, k) {
      for (var i = 0; i < arr.length; i++) {
        var index = arr[i].indexOf(k);
        if (index > -1) {
          return [i, index];
        }
      }
    }


  deletePantry( item ) {

    var tabledata             = this.state.tableData;
    var tableDatainColumn     = this.state.tableDatainColumn;
    var tableDataReplica      = this.state.tableDataReplica;
    var tableDatainPrice      = this.state.tableDatainPrice;
    var tableDatainPriceLine  = this.state.tableDatainPriceLine;

    var result    = new Array();

    console.log(this.state.tableData,"tableData");


    var tableDataReplica  = this.state.tableDataReplica;
    var pantryinbox       = this.state.pantryinbox;

    if( pantryinbox === null ){
      pantryinbox  = [];
    }

    var istic = this.getIndexOfK(pantryinbox, item);

    if( Array.isArray(istic) && istic.length > 0 ){
               
         Alert.alert( this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message", this.state.firstRemovepantry ? this.state.firstRemovepantry : "Please first remove pantry item from selected Go-Shopping then add quantity")   

    }else{

   
           result        = this.getIndexOfK(this.state.tableDataReplica, item);


          console.log(result,"result");

      if( result === undefined){
          Alert.alert("Sorry something went wrong")
      }else{
          cell1  = result[0];
          cell2  = result[1];

          var newtableDatainColumnquantity    = parseInt(tableDatainColumn[cell1][1] - 1);


          if( newtableDatainColumnquantity < 0 || newtableDatainColumnquantity == 0 ){

              console.log(tabledata,"tabledata");
              console.log(tableDatainColumn,"tableDatainColumn");
              console.log(tableDataReplica,"tableDataReplica");
              console.log(tableDatainPrice,"tableDatainPrice");
              console.log(tableDatainPriceLine,"tableDatainPriceLine");

              tabledata.splice([cell1],1);
              tableDatainColumn.splice([cell1],1);
              tableDataReplica.splice([cell1],1);
              tableDatainPrice.splice([cell1],1);
              tableDatainPriceLine.splice([cell1],1);


              console.log(tabledata,"tabledata");
              console.log(tableDatainColumn,"tableDatainColumn");
              console.log(tableDataReplica,"tableDataReplica");
              console.log(tableDatainPrice,"tableDatainPrice");
              console.log(tableDatainPriceLine,"tableDatainPriceLine");

              this.setState({tabledata : tabledata, tableDatainColumn : tableDatainColumn, tableDataReplica : tableDataReplica, tableDatainPrice : tableDatainPrice, tableDatainPriceLine : tableDatainPriceLine });

      
              AsyncStorage.multiSet([
                        ["tableData",                 JSON.stringify(this.state.tableData)],
                        ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                        ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                        ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                        ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                    ]);

            
          }else{

            var dataSource    = this.state.dataSource;

            var pantryindex   = this.getIndex(item, dataSource, 'pantry_image');

            console.log(pantryindex,"pantryindex");

            var cost = 0.00;

            if( pantryindex > 0 ){

               cost          = JSON.parse(dataSource[pantryindex].pantry_cost);
            }

            

          

        /*code for tableDatainColumn column*/       

              tableDatainColumn[cell1][1] = newtableDatainColumnquantity

              console.log( tableDatainColumn,"newtableDatainColumnquantity");

              this.setState({tableDatainColumn : tableDatainColumn});

        /*end code for column*/


        /* code for data price column*/

              console.log(tableDatainPrice[cell1][0],"tableDatainPrice1");
              console.log(tableDatainPrice[cell1][1],"tableDatainPrice2");  // do minus quantity

              var newtableDatainPrice    = tableDatainPrice[cell1][1] - 1;

              
              //const cost        = JSON.parse(dataSource[pantryindex].pantry_cost) * newtableDatainPrice;

              tableDatainPrice[cell1][1] = newtableDatainPrice;
              tableDatainPrice[cell1][2] = cost;

              console.log( tableDatainPrice,"newtableDatainPrice");

              this.setState({tableDatainPrice : tableDatainPrice});
        /* end data price column*/

        /*code for line*/


             var tabledataArr             = tabledata[cell1][cell2].split("#");
             var tableDatainPriceLineArr  = tableDatainPriceLine[cell1][cell2].split("#");

             tabledataArr.splice(0, 1);
             tableDatainPriceLineArr.splice(0, 1);

             var t1 = tabledataArr.join('#');
             var t2 = tableDatainPriceLineArr.join('#');

             tabledata[cell1][0] = t1;
             tableDatainPriceLine[cell1][0] = t2;
            // tableDatainPriceLine[cell1][1] = cost;

              

             this.setState({tabledata : tabledata, tableDatainPriceLine : tableDatainPriceLine});

        /*end code for line*/
            
             
              AsyncStorage.multiSet([
                        ["tableData",                 JSON.stringify(this.state.tableData)],
                        ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                        ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                        ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                        ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                    ]);


          }
     }

   

    
    }   

    this.setState({cross : false }) 
   
  }

  _selectPantry(data, index, elementofr){

    console.log(data,"onSelectdata");
    console.log(index,"onSelectindex");
    console.log(elementofr,"onSelectelementofr");

    var pantrybox = this.state.pantryinbox;
    var pricedata = this.state.tableDatainPrice;
    var AmountLeft = this.state.AmountLeft;
    var pantryaddedPrice = this.state.pantryaddedPrice;
    var pricedataCol = this.state.tableDatainPrice;
    var LowMoneyWarningType = this.state.LowMoneyWarningType;

    console.log(LowMoneyWarningType,"LowMoneyWarningType");
    

    var LowmoneyValue = JSON.parse(this.state.LowmoneyValue);


    var LowmoneyValue2 = LowmoneyValue / 2;
    var LowmoneyValue3 = LowmoneyValue2 / 2;

    console.log(LowmoneyValue,"LowmoneyValue");
    console.log(LowmoneyValue2,"LowmoneyValue2");
    console.log(LowmoneyValue3,"LowmoneyValue3");
      
   
    if(pantryaddedPrice === null || pantryaddedPrice < 0){
      pantryaddedPrice = 0;

      console.log(pantryaddedPrice,"pantryaddedPricevalue")
    }

    if( pricedata == null ){
      pricedata = [];
    }

    var price         = this.getIndexOfK(pricedata, data);

    var r1 = price[0];
    var r2 = price[1];

    console.log(pricedata[r1][2],"price");
    console.log(AmountLeft,"totalmoney");
   // console.log(parseFloat(pricedata[r1][2]) < parseFloat(AmountLeft),"123");

    var check = pricedata[r1][2] < AmountLeft;

  
   
    console.log((parseInt(pricedata[r1][2] * pricedata[r1][1]) <= parseInt(AmountLeft)),"istick");
    console.log((parseInt(pricedata[r1][2] * pricedata[r1][1])),"pricedata");
    console.log((parseInt(AmountLeft)),"AmountLeft");

    var ispantrycheck         = this.getIndexOfK(pantrybox, data);
    console.log(ispantrycheck,"ispantrycheck");
    if( (parseFloat(pricedata[r1][2] * pricedata[r1][1]) <= parseFloat(AmountLeft)) || Array.isArray(ispantrycheck)){

      
      if( pantrybox == null ){
        pantrybox = [];
      } 

      var index         = this.getIndexOfK(pantrybox, data);

      console.log(index,"index");

      var leftAmount = 0;

      if( index === undefined ){

          pantrybox.push(data);



          leftAmount = parseFloat(AmountLeft) - parseFloat(pricedata[r1][2] * pricedata[r1][1]);
          pantryaddedPrice =  (parseFloat(pricedata[r1][2] * pricedata[r1][1])) + pantryaddedPrice;

         // Alert.alert(JSON.stringify(pantryaddedPrice));

        

        
         
          if( (leftAmount <= LowmoneyValue ) && (leftAmount > LowmoneyValue2)){

            
                  if( LowMoneyWarningType == 0 ){

                      AsyncStorage.getItem('currentAlert').then((data) => {

                        console.log(JSON.parse(data),"soundmusic3");

                        var tune = JSON.parse(data);


                        var Sound = require('react-native-sound');


                        Sound.setCategory('Playback');
                          if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.path, Sound.MAIN_BUNDLE, (error) => {
                              if (error) {
                                  console.log('failed to load the sound', error);
                              } else {
                                  whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);// have to put the call to play() in the onload callback
                              }
                          });

                      });
                  }else{

                     AsyncStorage.getItem('currentAlert').then((data) => {

                        console.log(JSON.parse(data),"soundmusic4");

                        var tune = JSON.parse(data);


                        var Sound = require('react-native-sound');


                        Sound.setCategory('Playback');

                            if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.url, Sound.MAIN_BUNDLE, (error) => {
                              if (error) {
                                  console.log('failed to load the sound', error);
                              } else {
                                  whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);// have to put the call to play() in the onload callback
                              }
                          });

                      });

                  }
            
            
          }

          if( (leftAmount <= LowmoneyValue2) && (leftAmount > LowmoneyValue3)) {
            
                  if( LowMoneyWarningType == 0 ){

                      AsyncStorage.getItem('currentAlert').then((data) => {

                        console.log(JSON.parse(data),"soundmusic5");

                        var tune = JSON.parse(data);


                        var Sound = require('react-native-sound');


                        Sound.setCategory('Playback');

                            if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.path, Sound.MAIN_BUNDLE, (error) => {
                              if (error) {
                                  console.log('failed to load the sound', error);
                              } else {
                                  whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);// have to put the call to play() in the onload callback
                              }
                          });

                      });
                  }else{

                     AsyncStorage.getItem('currentAlert').then((data) => {

                        console.log(JSON.parse(data),"soundmusic5");

                        var tune = JSON.parse(data);


                        var Sound = require('react-native-sound');


                        Sound.setCategory('Playback');

                            if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.url, Sound.MAIN_BUNDLE, (error) => {
                              if (error) {
                                  console.log('failed to load the sound', error);
                              } else {
                                  whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);// have to put the call to play() in the onload callback
                              }
                          });

                      });

                  }
          }


          if(leftAmount <= LowmoneyValue3){
            
                        if( LowMoneyWarningType == 0 ){

                            AsyncStorage.getItem('currentAlert').then((data) => {

                              console.log(JSON.parse(data),"soundmusic6");

                              var tune = JSON.parse(data);


                              var Sound = require('react-native-sound');


                              Sound.setCategory('Playback');

                                  if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.path, Sound.MAIN_BUNDLE, (error) => {
                                    if (error) {
                                        console.log('failed to load the sound', error);
                                    } else {
                                        whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);// have to put the call to play() in the onload callback
                                    }
                                });

                            });
                        }else{

                           AsyncStorage.getItem('currentAlert').then((data) => {

                              console.log(JSON.parse(data),"soundmusic7");

                              var tune = JSON.parse(data);


                              var Sound = require('react-native-sound');


                              Sound.setCategory('Playback');

                                  if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.url, Sound.MAIN_BUNDLE, (error) => {
                                    if (error) {
                                        console.log('failed to load the sound', error);
                                    } else {
                                        whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);// have to put the call to play() in the onload callback
                                    }
                                });

                            });

                        }
          }
          
          
      }else{

          cell1  = index[0];
          pantrybox.splice([cell1],1);

          leftAmount = parseFloat(AmountLeft) + parseFloat(pricedata[r1][2] * pricedata[r1][1]);
          pantryaddedPrice =  pantryaddedPrice - (parseFloat(pricedata[r1][1] * pricedataCol[r1][1]));
          //this.setState({AmountLeft:leftAmount});
          
      }

      AsyncStorage.setItem('pantryaddedPrice',JSON.stringify(pantryaddedPrice) );


      this.setState({pantryaddedPrice:pantryaddedPrice});
      this.setState({pantryinbox:pantrybox});
      this.setState({AmountLeft:leftAmount});

      AsyncStorage.setItem('pantryinbox', JSON.stringify(this.state.pantryinbox));
      AsyncStorage.setItem('AmountLeft',this.state.AmountLeft);
      AsyncStorage.setItem('pantryaddedPrice',this.state.pantryaddedPrice);
      
      console.log(pantryaddedPrice,"pantryaddedPrice");
      console.log(pantrybox,"pantrybox");
      console.log(leftAmount,"leftAmount");

    }else{



      if( LowMoneyWarningType == 0 ){

          AsyncStorage.getItem('currentAlert').then((data) => {

            console.log(JSON.parse(data),"soundmusic8");

            var tune = JSON.parse(data);


            var Sound = require('react-native-sound');


            Sound.setCategory('Playback');

                if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.path, Sound.MAIN_BUNDLE, (error) => {
                  if (error) {
                      console.log('failed to load the sound', error);
                      nextProps.music.backgroundMusic.play()
                      nextProps.music.backgroundMusic.setNumberOfLoops(-1)
                  } else {

                    whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);
                  }
              });

          });
      }else{

         AsyncStorage.getItem('currentAlert').then((data) => {

            console.log(JSON.parse(data),"soundmusic9");

            var tune = JSON.parse(data);


            var Sound = require('react-native-sound');


            Sound.setCategory('Playback');

                if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.url, Sound.MAIN_BUNDLE, (error) => {
                  if (error) {
                      console.log('failed to load the sound', error);
                  } else {
                      whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000); // have to put the call to play() in the onload callback
                  }
              });

          });

      }


      Alert.alert(this.state.YouHaveLowMoney);
      //Alert.alert("Warning Message",LowMoneyWarningType);
    }

    

  }

   handleTextChanged(text) {
    //Alert.alert(text);
    
  }

  _selectPantryLine(data, index, elementofr, i){

        var pantrybox = this.state.pantryinbox;
        var pricedata = this.state.tableDatainPrice;
        var pricedataCol = this.state.tableDatainPrice;
        var AmountLeft = this.state.AmountLeft;
        var pantryaddedPrice = this.state.pantryaddedPrice;
        var LowmoneyValue = this.state.LowmoneyValue;

        var LowMoneyWarningType = this.state.LowMoneyWarningType;

        console.log(LowMoneyWarningType,"LowMoneyWarningType")


        //Alert.alert(LowmoneyValue);

        if(pantryaddedPrice === null){
          pantryaddedPrice = 0;

          console.log(pantryaddedPrice,"pantryaddedPricevalue")
        }
       


        var image = data.split("#");
        console.log(image[0],"data");
        console.log(index,"index");
        console.log(elementofr,"elementofr");
        console.log(pricedata,"pricedata");
        console.log(i,"indexofimage");

        if(pricedata == null ){
          pricedata = [];
        }
    
        var resultindex         = this.getIndexOfK(pricedata, data);


        var r1 = resultindex[0];
        var r2 = resultindex[1];

        console.log(pricedata[r1][1],"price");
        console.log(pricedataCol[r1][1],"pricedataCol");
        console.log(AmountLeft,"totalmoney");
        console.log(parseFloat(pricedata[r1][1] * pricedataCol[r1][1]),"newpricedata");


        if( (parseFloat(pricedata[r1][1] * pricedataCol[r1][1]) < parseFloat(AmountLeft)) ){

          if( pantrybox == null ){
            pantrybox = [];
          }

          var index         = this.getIndexOfK(pantrybox, data);

          var leftAmount = 0;

          if( index === undefined ){

              pantrybox.push(data);

              pantryaddedPrice =  (parseFloat(pricedata[r1][1] * pricedataCol[r1][1])) + pantryaddedPrice;

              leftAmount = parseFloat(AmountLeft) - parseFloat(pricedata[r1][1] * pricedataCol[r1][1]);
              //this.setState({AmountLeft:leftAmount});

          }else{

              cell1  = index[0];
              pantrybox.splice([cell1],1);

              pantryaddedPrice =  pantryaddedPrice - (parseFloat(pricedata[r1][1] * pricedataCol[r1][1]));

              leftAmount = parseFloat(AmountLeft) + parseFloat(pricedata[r1][1] * pricedataCol[r1][1]);
              //this.setState({AmountLeft:leftAmount});
              
          }

          AsyncStorage.setItem('pantryaddedPrice',JSON.stringify(pantryaddedPrice) );


          this.setState({pantryaddedPrice:pantryaddedPrice});
          this.setState({pantryinbox:pantrybox});
          this.setState({AmountLeft:leftAmount});

          AsyncStorage.setItem('pantryinbox', JSON.stringify(this.state.pantryinbox));

          AsyncStorage.setItem('AmountLeft',this.state.AmountLeft);
          
          

          console.log(pantrybox,"pantryinbox");

          
        }else{


          if( LowMoneyWarningType == 0 ){

              AsyncStorage.getItem('currentAlert').then((data) => {

                console.log(JSON.parse(data),"soundmusic1");

                var tune = JSON.parse(data);


                var Sound = require('react-native-sound');


                Sound.setCategory('Playback');

                    if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.path, Sound.MAIN_BUNDLE, (error) => {
                      if (error) {
                          console.log('failed to load the sound', error);
                      } else {
                          whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);// have to put the call to play() in the onload callback
                      }
                  });

              });
          }else{

             AsyncStorage.getItem('currentAlert').then((data) => {

                console.log(JSON.parse(data),"soundmusic2");

                var tune = JSON.parse(data);


                var Sound = require('react-native-sound');


                Sound.setCategory('Playback');

                    if (whoosh!=null) {
                            whoosh.release();
                          }
                       whoosh = new Sound(tune.url, Sound.MAIN_BUNDLE, (error) => {
                      if (error) {
                          console.log('failed to load the sound', error);
                      } else {
                          whoosh.play(); // have to put the call to play() in the onload callback
                    setInterval( () => { 
                      whoosh.release();
                     }, 15*1000);// have to put the call to play() in the onload callback
                      }
                  });

              });

          }


          Alert.alert(this.state.YouHaveLowMoney);
        }

    

  }

  _clickPrice(data, index, elementofr){

   // Alert.alert("Pantry Price Each",data);
    if( data === ' '){
      data = 0;

    }

     Alert.alert(
        'Pantry price is '+data+'. Do you want to edit price?',
        '',
        [
          {text: this.state.cancel ? this.state.cancel : 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: this.state.ok ? this.state.ok : 'OK', onPress: () => {this.editableprice(data, index, elementofr)}},
        ],
        { cancelable: false }
      )
  }

  _clickQuantity(data, index, elementofr){

   // Alert.alert("Pantry Price Each",data);
    if( data === ' '){
      data = 0;

    }

    this.editableQuantity(data, index, elementofr)

     /*Alert.alert(
        'Pantry Quantity is '+data+'. Do you want to edit Quantity?',
        '',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => {this.editableQuantity(data, index, elementofr)}},
        ],
        { cancelable: false }
      )*/
  }


  _onChangeQuantity(data, index, indexofR){

    var pantrybox = this.state.pantryinbox;

    console.log(pantrybox,"pantrybox");
    console.log(indexofR,"elementofr");
    console.log(data,"data");
    console.log(index,"index");



    if( data != ''){

           if( data == 0 ){

              var tabledata             = this.state.tableData;
              var tableDatainColumn     = this.state.tableDatainColumn;
              var tableDataReplica      = this.state.tableDataReplica;
              var tableDatainPrice      = this.state.tableDatainPrice;
              var tableDatainPriceLine  = this.state.tableDatainPriceLine;

              tabledata.splice([indexofR],1);
              tableDatainColumn.splice([indexofR],1);
              tableDataReplica.splice([indexofR],1);
              tableDatainPrice.splice([indexofR],1);
              tableDatainPriceLine.splice([indexofR],1);


              this.setState({tabledata : tabledata, tableDatainColumn : tableDatainColumn, tableDataReplica : tableDataReplica, tableDatainPrice : tableDatainPrice, tableDatainPriceLine : tableDatainPriceLine });

      
              AsyncStorage.multiSet([
                        ["tableData",                 JSON.stringify(this.state.tableData)],
                        ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                        ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                        ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                        ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                    ]);
        }

        if( data > 0 ){

          

          var data                  = JSON.parse(data);


          var tabledata             = this.state.tableData;
          var tableDatainColumn     = this.state.tableDatainColumn;
          var tableDataReplica      = this.state.tableDataReplica;
          var tableDatainPrice      = this.state.tableDatainPrice;
          var tableDatainPriceLine  = this.state.tableDatainPriceLine;

          if( pantrybox === null ){
            pantrybox  = [];
          }

          var istic = this.getIndexOfK(pantrybox, tableDataReplica[indexofR][0]);


          console.log(tabledata[indexofR],"tabledata");
          console.log(tableDatainColumn[indexofR],"tableDatainColumn");
          console.log(tableDataReplica[indexofR],"tableDataReplica");
          console.log(tableDatainPrice[indexofR],"tableDatainPrice");
          console.log(tableDatainPriceLine[indexofR],"tableDatainPriceLine");

          if( Array.isArray(istic) && istic.length > 0 ){
               
               Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message",this.state.firstRemovepantry ? this.state.firstRemovepantry : "Please first remove pantry item from selected Go-Shopping then add quantity")   

          }else{

                  var dataSource    = this.state.dataSource;

                  if( dataSource == null ){
                    dataSource = [];
                  }

                  var pantryindex   = this.getIndex(tableDataReplica[indexofR][0], dataSource, 'pantry_image');

                  console.log(pantryindex,"pantryindex");
                  console.log(dataSource[pantryindex],"dataSource");



                  console.log(tableDatainColumn[indexofR][1],"quantity");
                  
                  var pantry_cost = 'pantry_cost';
                  //const price = dataSource[pantryindex].pantry_cost * data;
                 
                  var price = 0.00;

                  if( pantryindex > -1 ){
                    price = dataSource[pantryindex].pantry_cost;
                  }


                  var arr = [];

                  for (var i = 0; i < data; i++) {

                    arr.push(tableDataReplica[indexofR]);
                  }

                  var t1 = arr.join('#');

                  tableDatainColumn[indexofR][1]    = data;
                  tableDatainPrice[indexofR][1]     = data;
                  
                  tableDatainPriceLine[indexofR][0] = t1;
                  
                  tabledata[indexofR][0]            = t1;

                  if( tableDatainPriceLine[indexofR][1] > 0 ){


                      //tableDatainPrice[indexofR][2]     = price;
                      //tableDatainPriceLine[indexofR][1] = price;

                      console.log(tableDatainPrice[indexofR],"tableDatainPrice[indexofR]");
                      console.log(tableDatainPriceLine[indexofR],"tableDatainPriceLine[indexofR]");
                  }

                  

                  this.setState({tabledata : tabledata, tableDatainPriceLine : tableDatainPriceLine, tableDatainPrice : tableDatainPrice, tableDatainColumn : tableDatainColumn});

                 
                  AsyncStorage.multiSet([
                            ["tableData",                 JSON.stringify(this.state.tableData)],
                            ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                            ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                            ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                        ]);

                    

                }
          }
    }
  
 
  }


  editableQuantity(data, index, elementofr){

    var tableDataReplica  = this.state.tableDataReplica;
    var pantryinbox       = this.state.pantryinbox;

    if( pantryinbox === null ){
      pantryinbox  = [];
    }

    var istic = this.getIndexOfK(pantryinbox, tableDataReplica[elementofr]);


    if( Array.isArray(istic) && istic.length > 0 ){
               
        Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message",this.state.firstRemovepantry ? this.state.firstRemovepantry : "Please first remove pantry item from selected Go-Shopping then add quantity")   

    }else{

        this.setState({editquantity         : true});
        this.setState({dataeditQuantity     : data});
        this.setState({indexcellQuantity    : index});
        this.setState({elementIndexQuantity : elementofr});
    }
  }


  _oneditQuantity(quantity){

    try {

       if (/^\d+$/.test(JSON.parse(quantity.text)))
        {
   


      //var data          = this.state.dataeditQuantity;
      var data          = JSON.parse(quantity.text);
      var index         = this.state.indexcellQuantity;
      var indexofR      = this.state.elementIndexQuantity;
      
 
      var pantrybox = this.state.pantryinbox;

      console.log(pantrybox,"pantrybox");
      console.log(indexofR,"elementofr");
      console.log(data,"data");
      console.log(index,"index");

      if( data != ''){

           if( data == 0 ){

              var tabledata             = this.state.tableData;
              var tableDatainColumn     = this.state.tableDatainColumn;
              var tableDataReplica      = this.state.tableDataReplica;
              var tableDatainPrice      = this.state.tableDatainPrice;
              var tableDatainPriceLine  = this.state.tableDatainPriceLine;

              tabledata.splice([indexofR],1);
              tableDatainColumn.splice([indexofR],1);
              tableDataReplica.splice([indexofR],1);
              tableDatainPrice.splice([indexofR],1);
              tableDatainPriceLine.splice([indexofR],1);


              this.setState({tabledata : tabledata, tableDatainColumn : tableDatainColumn, tableDataReplica : tableDataReplica, tableDatainPrice : tableDatainPrice, tableDatainPriceLine : tableDatainPriceLine });

      
              AsyncStorage.multiSet([
                        ["tableData",                 JSON.stringify(this.state.tableData)],
                        ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                        ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                        ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                        ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                    ]);
        }

        if( data > 0 ){

          

          var data                  = JSON.parse(data);


          var tabledata             = this.state.tableData;
          var tableDatainColumn     = this.state.tableDatainColumn;
          var tableDataReplica      = this.state.tableDataReplica;
          var tableDatainPrice      = this.state.tableDatainPrice;
          var tableDatainPriceLine  = this.state.tableDatainPriceLine;

          if( pantrybox === null ){
            pantrybox  = [];
          }

          var istic = this.getIndexOfK(pantrybox, tableDataReplica[indexofR][0]);


          console.log(tabledata[indexofR],"tabledata");
          console.log(tableDatainColumn[indexofR],"tableDatainColumn");
          console.log(tableDataReplica[indexofR],"tableDataReplica");
          console.log(tableDatainPrice[indexofR],"tableDatainPrice");
          console.log(tableDatainPriceLine[indexofR],"tableDatainPriceLine");

          if( Array.isArray(istic) && istic.length > 0 ){
               
               Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message",this.state.firstRemovepantry ? this.state.firstRemovepantry : "Please first remove pantry item from selected Go-Shopping then add quantity")   

          }else{

                  var dataSource    = this.state.dataSource;

                  if( dataSource == null ){
                    dataSource = [];
                  }

                  var pantryindex   = this.getIndex(tableDataReplica[indexofR][0], dataSource, 'pantry_image');

                  console.log(pantryindex,"pantryindex");
                  console.log(dataSource[pantryindex],"dataSource");



                  console.log(tableDatainColumn[indexofR][1],"quantity");
                  
                  var pantry_cost = 'pantry_cost';
                  //const price = dataSource[pantryindex].pantry_cost * data;
                 
                  var price = 0.00;

                  if( pantryindex > -1 ){
                    price = dataSource[pantryindex].pantry_cost;
                  }


                  var arr = [];

                  for (var i = 0; i < data; i++) {

                    arr.push(tableDataReplica[indexofR]);
                  }

                  var t1 = arr.join('#');

                  tableDatainColumn[indexofR][1]    = data;
                  tableDatainPrice[indexofR][1]     = data;
                  
                  tableDatainPriceLine[indexofR][0] = t1;
                  
                  tabledata[indexofR][0]            = t1;

                  if( tableDatainPriceLine[indexofR][1] > 0 ){


                      //tableDatainPrice[indexofR][2]     = price;
                      //tableDatainPriceLine[indexofR][1] = price;

                      console.log(tableDatainPrice[indexofR],"tableDatainPrice[indexofR]");
                      console.log(tableDatainPriceLine[indexofR],"tableDatainPriceLine[indexofR]");
                  }

                  

                  this.setState({tabledata : tabledata, tableDatainPriceLine : tableDatainPriceLine, tableDatainPrice : tableDatainPrice, tableDatainColumn : tableDatainColumn});

                 
                  AsyncStorage.multiSet([
                            ["tableData",                 JSON.stringify(this.state.tableData)],
                            ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                            ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                            ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                        ]);

                    

                }
          }
    }

    }
  else{
    Alert.alert('Please enter a correct value');
  }

 } 
 catch(e) {
  Alert.alert("please enter a valid value"); // error in the above string (in this case, yes)!
 }
     

    
}


  editableprice(data, index, elementofr){

    var tableDataReplica  = this.state.tableDataReplica;
    var pantryinbox       = this.state.pantryinbox;

    if( pantryinbox === null ){
      pantryinbox  = [];
    }

    var istic = this.getIndexOfK(pantryinbox, tableDataReplica[elementofr]);


    if( Array.isArray(istic) && istic.length > 0 ){
               
        Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message",this.state.firstRemovepantry ? this.state.firstRemovepantry : "Please first remove pantry item from selected Go-Shopping then add quantity")   

    }else{

        this.setState({editprice : true});
        this.setState({ispricevisible : true});
        this.setState({dataeditprice : data});
        this.setState({indexeditprice : index});
        this.setState({elementofreditprice : elementofr});
        console.log(this.state.editprice,"editprice");
        console.log(index,"indexeditprice");
        console.log(elementofr,"elementindexeditprice");
        console.log(tableDataReplica[elementofr],"tableDataReplica");
        console.log(istic,"istic");

    }
  }

  handleViewRef = ref => this.view = ref;

  bounce = () =>{ 

    //this.view.rubberBand(800).then(endState => this.setState({ cross    : true }) );
    this.setState({ cross    : true });
   

  }




   _oneditprice( editpricevalue ){

      //alert(JSON.stringify(editpricevalue.text));

      var dataeditprice         = this.state.dataeditprice;
      var indexeditprice        = this.state.indexeditprice;
      var elementofreditprice   = this.state.elementofreditprice;
      var tableDatainPrice      = this.state.tableDatainPrice;
      var tableDatainPriceLine  = this.state.tableDatainPriceLine;
      var tableDataReplica      = this.state.tableDataReplica;

     
      console.log(dataeditprice,'dataeditprice');
      console.log(indexeditprice,"indexeditprice");
      console.log(elementofreditprice,"elementofreditprice");
      console.log(tableDatainPrice,"tableDatainPrice");
      console.log(tableDatainPriceLine,"tableDatainPriceLine");
      console.log(tableDataReplica,"tableDataReplica");



       var pantrybox = this.state.pantryinbox;

       var indexofR   = elementofreditprice;
       var data       = dataeditprice;
       var index      = indexeditprice;

    
        if( editpricevalue.text != ''){

            if( editpricevalue.text == 0 ){

                Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion Message", this.state.priceCanTZero ? this.state.priceCanTZero : "Price can not Zero");
                 
            }

            //DEEPAK SINGH
            /*code for checking item is ticked or not before chenging price value*/
              if( pantrybox === null ){
                  pantrybox  = [];
                }
              var istic = this.getIndexOfK(pantrybox, tableDataReplica[indexofR][0]);
             if( Array.isArray(istic) && istic.length > 0 ){
               
               Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message",this.state.firstRemovepantry ? this.state.firstRemovepantry : "Please first remove pantry item from selected Go-Shopping then add quantity")   

           }else{ 

            



              if( editpricevalue.text > 0 ){

                

                      var data                  = JSON.parse(data);


                      var tabledata             = this.state.tableData;
                      var tableDatainColumn     = this.state.tableDatainColumn;
                      var tableDataReplica      = this.state.tableDataReplica;
                      var tableDatainPrice      = this.state.tableDatainPrice;
                      var tableDatainPriceLine  = this.state.tableDatainPriceLine;

                     /* console.log(tabledata[indexofR],"tabledata");
                      console.log(tableDatainColumn[indexofR],"tableDatainColumn");
                      console.log(tableDataReplica[indexofR],"tableDataReplica");*/
                      console.log(tableDatainPrice[indexofR][2],"tableDatainPrice");
                      console.log(tableDatainPriceLine[indexofR][1],"tableDatainPriceLine");

                      tableDatainPrice[indexofR][2]       = parseFloat(editpricevalue.text).toFixed(2);
                      tableDatainPriceLine[indexofR][1]   = parseFloat(editpricevalue.text).toFixed(2);


                      this.setState({tableDatainPrice : tableDatainPrice, tableDatainPriceLine : tableDatainPriceLine});

                      AsyncStorage.multiSet([
                                      ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                                      ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                                  ]);

                    
                }

             }/*code end here for ticked/not ticked*/ 
        }


    }


  async createPDF() {

        AsyncStorage.getItem('tableDatainPrice').then((data) => { 


          var tableDatainPrice = [];

          if( data ){
            tableDatainPrice = JSON.parse(data);
          }else{
            tableDatainPrice = [];
          }

          var GVar = GlobalVariables.getApiUrl();

          var imagepath = GVar.ImagePath

          //console.log(GVar.ImagePath,"ImagePath");
        
          var html = '<strong>Shopping List Items</strong></p><table><tbody><tr><td>No</td><td>Image</td><td>Quantity</td><td>Price</td></tr>';                     

          

          for (var i = 0; i < tableDatainPrice.length; i++) {

              if( tableDatainPrice[i][0].includes("thumb")){
                   var image = tableDatainPrice[i][0];

                   var slr = i +1; 

                   console.log(image,"image")

                   html = html + '<tr><td>'+slr+'</td><td>'+'<img src="'+imagepath+image+'" alt="Smiley face" height="42" width="42">'+'</td><td>'+tableDatainPrice[i][1]+'</td><td>'+tableDatainPrice[i][2]+'</td></tr>';
              
              }else{

                var slr = i +1; 

                var image = JSON.parse(tableDatainPrice[i][0]);

                console.log(image.uri,"image1");
                
                html = html + '<tr><td>'+slr+'</td><td>'+'<img src="'+image.uri+'" alt="Smiley face" height="42" width="42">'+'</td><td>'+tableDatainPrice[i][1]+'</td><td>'+tableDatainPrice[i][2]+'</td></tr>';
              
              }
          }

          console.log(html,"html");
          
          var options = {
             
                        html: html, // HTML String

                      // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************                              
                        fileName: 'shoppinglist',          /* Optional: Custom Filename excluded extension
                                                    Default: Randomly generated
                                                  */

                        directory: 'docs',         /* Optional: 'docs' will save the file in the `Documents`
                                                    Default: Temp directory */

                        base64: true ,               


                        height: 800,                
                        width: 1056,               /* Optional: 1056 sets the width of the DOCUMENT that will produced
                                                    Default: 792
                                                  */
                        padding: 24,                /* Optional: 24 is the # of pixels between the outer paper edge and
                                                            corresponding content edge.  Example: width of 1056 - 2*padding
                                                            => content width of 1008
                                                    Default: 10
                                                  */
                  };

                  RNHTMLtoPDF.convert(options).then((data) => {
                  console.log(data.filePath,'filePath');
                  console.log(data.base64,'base64');
                 
                  /*Alert.alert(
                      'options filename' + options.fileName,
                      'data filePath=' + data.filePath
                  );*/

                  Alert.alert('Successfully downloaded at ',data.filePath );
              });


        });
  }

  resetPosition(e, rowData){

    Alert.alert("Terst");
  }

  _clickWeight(elementofr){

    //Alert.alert(JSON.stringify(elementofr));
    //Alert.alert(JSON.stringify(elementofr));

    if( elementofr > -1 ){
        var tableDatainPriceLine = this.state.tableDatainPriceLine;

        var price = '0.00';

        if( tableDatainPriceLine[elementofr][1] != ' '){
          price = tableDatainPriceLine[elementofr][1];
        }

        Alert.alert(
            'Do you want to edit weight for the pantry ?',
            '',
            [
              {text: this.state.cancel ? this.state.cancel : 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: this.state.ok ? this.state.ok : 'OK', onPress: () => { this.setState({indexofweighted_data : elementofr,modalVisible: true, price : price })}},
            ],
            { cancelable: false }
          )
        }

  }
 checkfornumber(value){
    if (/^\d+(\.\d{0,4})?$/.test(value)){
        this.setState({weight:value});
        this.setState({cost : parseFloat(this.state.price * value).toFixed(2)});
    }
    else{
          Alert.alert("please enter a valid value");
    }
 }
  

  onPressAddWeight(){

    var cost                          = this.state.cost;
    var weight                        = this.state.weight;
    var indexofweighted_data          = this.state.indexofweighted_data;

    var tableData                     = this.state.tableData;
    var tableDatainColumn             = this.state.tableDatainColumn;
    var tableDataReplica              = this.state.tableDataReplica;
    var tableDatainPrice              = this.state.tableDatainPrice;
    var tableDatainPriceLine          = this.state.tableDatainPriceLine;

    
    console.log(cost,"cost");
    console.log(weight,"weight");
    console.log(indexofweighted_data,"indexofweighted_data");

  
    if( indexofweighted_data > -1 && weight > 0.00 ){
      

      var item = tableDataReplica[indexofweighted_data][0];

      console.log(item,'item');
      console.log(tableData[indexofweighted_data],"tableData");
      console.log(tableDatainColumn[indexofweighted_data],"tableDatainColumn");
      console.log(tableDataReplica[indexofweighted_data],"tableDataReplica");
      console.log(tableDatainPrice[indexofweighted_data],"tableDatainPrice");
      console.log(tableDatainPriceLine[indexofweighted_data],"tableDatainPriceLine");

      
      tableDatainPriceLine[indexofweighted_data][0]   = item;
      tableData[0]                                    = item;

      tableDatainPrice[indexofweighted_data][1]       = weight;
      tableDatainColumn[indexofweighted_data][1]      = weight;


      this.setState({ tableData: tableData, tableDatainColumn:tableDatainColumn, tableDatainPrice:tableDatainPrice, tableDatainPriceLine:tableDatainPriceLine});
      
      this.setState({modalVisible: false, cost:'0.00', weight:'0.00', indexofweighted_data: ''});
      
             AsyncStorage.setItem('dataSource', JSON.stringify(this.state.dataSource) );
             AsyncStorage.setItem('tableDatainColumn', JSON.stringify(this.state.tableDatainColumn) );
             AsyncStorage.setItem('tableDatainPrice', JSON.stringify(this.state.tableDatainPrice) );
             AsyncStorage.setItem('tableDatainPriceLine', JSON.stringify(this.state.tableDatainPriceLine) );

      Alert.alert("Successfully added weight");
    }else{
      Alert.alert("Please enter some weight");
    }

    


  }
 

  render() {
    var  state = this.state;
    var inline = this.state.showcolumn ;
    var dismissKeyboard = require('dismissKeyboard');


    const element = (data, index, elementofr ) => {

        var rowindex = elementofr;

        if( index == 0 ){

            var views = [];
            var myArr = data.split("#");
            
            
            if( data.includes("thumb") ){

                  var pantrybox = this.state.pantryinbox;

                  if( pantrybox == null ){
                    pantrybox = [];
                  }


                  var index        = this.getIndexOfK(pantrybox, data);

                  var tic = <Text style={{ fontSize: 20,marginTop:5}}></Text>;

                  var isticimage = false;

                  if( index != undefined ){
                      isticimage = true;
                      var tic = <Text style={{color:'red', fontSize: 20,marginTop:5}}>√</Text>;
                  }

                  

                  var image = myArr[0];

                  return  <TouchableOpacity style={{flex:1, flexDirection:'row'}} onPress={() => this._selectPantry(data, index, elementofr)}> 
                          <Image source={{uri: this.state.ImagePath +image}} style={{width:wp('5%'),height:hp('10%'), marginTop:5, marginLeft:5, opacity: isticimage ? 0.5 : 1, }} />
                          {tic}
                          </TouchableOpacity>;

            }else{


                  var pantrybox = this.state.pantryinbox;

                  if( pantrybox == null ){
                    pantrybox = [];
                  }


                  var index        = this.getIndexOfK(pantrybox, data);

                  var tic = <Text style={{ fontSize: 20,marginTop:5}}></Text>;

                  var isticimage = false;

                  if( index != undefined ){
                      isticimage = true;
                      var tic = <Text style={{color:'red', fontSize: 20,marginTop:5}}>√</Text>;
                  }

                  var image = myArr[0];
                 
                  return  <TouchableOpacity style={{flex:1, flexDirection:'row'}} onPress={() => this._selectPantry(data, index, elementofr)}>
                          <Image source={JSON.parse(image)} style={{marginLeft:5,width:wp('5%'),height:hp('10%'),opacity: isticimage ? 0.5 : 1,}}/>
                          {tic}
                          </TouchableOpacity>;
            }

            views.push(<Text style={{color:'red', fontSize: 20,}}>√</Text>)

        }

        if( index == 1 ){

          var item_atposition  = this.state.tableDataReplica;
          var userdatasource   = this.state.userdatasource;
          var DataSource       = this.state.dataSource;

          if( item_atposition == null ){
            item_atposition = [];
          }

          if( userdatasource == null ){
            userdatasource = [];
          }

          if( DataSource == null ){
            DataSource = [];
          }
          

          
          var weightimage = <View/>;


          var imagedata        = '';
          var dataid           = '';

          if( item_atposition == null ){
            item_atposition = [];
            
          }
          
          if( Array.isArray(item_atposition) && item_atposition.length > 0 ){
           
            var imagedata         = item_atposition[elementofr][0];
            var dataid            = item_atposition[elementofr][1];

          }

        
          if( imagedata.includes("thumb") ){

              var DataSource       = this.state.dataSource;
              
              if(  DataSource == null ){
                DataSource = [];
              }


              var indexofimage_server = this.getIndex(imagedata, DataSource, 'pantry_image');

              console.log(indexofimage_server,"indexofimage_server");
              console.log(DataSource[indexofimage_server],"dataindexofimage_server");
              console.log(DataSource,"dataindexserver");


              if( indexofimage_server > -1 ){


                var isweight = DataSource[indexofimage_server]['pantry_isweighted'];

                console.log(isweight,"isweight12345678910");
               
                if( isweight == 1 ){

                  console.log("yes");
                  console.log(isweight,"isweight123456");
                  weightimage = <TouchableOpacity onPress={() => this._clickWeight(elementofr)} style={{}}>
                                    <Image source={require('../assets/images/shoppingimages/scale-big.png')} style={{height:40, width:40, right:0}}/>
                                </TouchableOpacity>;
                }else{

                  console.log("no");
                  weightimage = <View/>;
                }
              }
            
          }else{

              var image = JSON.parse(imagedata);
              var userdatasource   = this.state.userdatasource;

              if( userdatasource == null ){
                userdatasource = [];
              }
              console.log(userdatasource,'userdatasource');

              var indexofimage_local = this.getIndex(dataid, userdatasource, 'useraddedpantry_id');

              if( indexofimage_local > -1 ){
                  console.log(userdatasource,"userdatasourceuserdatasource");
                  console.log(image,"imagedataimagedata");
                  console.log(indexofimage_local,"indexofimage_local");
                  console.log(dataid,"dataid");

                  var isweight = userdatasource[indexofimage_local]['pantry_isweighted'];

                  console.log(isweight,"isweight12345");

                  if( isweight == 1 ){

                    weightimage = <TouchableOpacity onPress={() => this._clickWeight(elementofr)}>
                                    <Image source={require('../assets/images/shoppingimages/scale-big.png')} style={{height:40, width:40}}/>
                                </TouchableOpacity>;
                  }else{

                    weightimage = <View/>;
                  }

              }
          }

          return  <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}}>
                  <TouchableOpacity onPress={() => this._clickQuantity(data, index, elementofr)}>
                      <View style={{justifyContent:'center',width:wp('10%'), borderWidth:1, height:hp('10%')}}>
                            <Text style={{color:'#000000', fontSize: RF(3), alignSelf:'center'}} >{data}</Text>        
                      </View>
                  </TouchableOpacity>    
                  <View style={{justifyContent:'center',flex:1, marginLeft:wp('1%')}}>
                      {weightimage}
                  </View>
                  </View>;
           
        }


        if( index == 2 ){

          return  <TouchableOpacity onPress={() => this._clickPrice(data, index, elementofr)}>
                      <View style={{flex:1}}>
                          <Text style={{color:'#000000', fontSize: RF(3), alignSelf:'center'}} >{data}</Text>
                      </View>
                  </TouchableOpacity>;

        }

        

    }


   
    const element1 = (data, index, elementofr ) => {
      

        console.log(index,"indexing");


        if( index == 0 ){

        var views = [];
        var myArr = data.split("#");

      
        var item_atposition  = this.state.tableDataReplica;

        if( item_atposition == null ){
          item_atposition = [];
        }
        
        var DataSource       = this.state.dataSource;

        if( Array.isArray(item_atposition) && item_atposition.length > 0 ){

            var imagedata         = item_atposition[elementofr][0];
            var dataid            = item_atposition[elementofr][1];
          }
        //var imagedata        = item_atposition[elementofr][0];
        //var dataid           = item_atposition[elementofr][1];

        console.log(imagedata,"imagedata");
        console.log(dataid,"dataid");
        console.log(elementofr,"elementofrelementofr0");



        
        console.log(item_atposition[elementofr][0],"item_atposition");
        var weightimage = <View/>;

        if( imagedata.includes("thumb") ){

              console.log(DataSource,"DataSourceDataSource");
              if(  DataSource == null ){
                DataSource = [];
              }

              

              var indexofimage_server = this.getIndex(imagedata, DataSource, 'pantry_image');

              console.log(indexofimage_server,"indexofimage_server");

              if( indexofimage_server > -1 ){
                var isweight = DataSource[indexofimage_server]['pantry_isweighted'];

                console.log(isweight,"isweight12345");

                if( isweight == 1 ){

                  weightimage = <TouchableOpacity onPress={() => this._clickWeight(elementofr)} style={{}}>
                                    <Image source={require('../assets/images/shoppingimages/scale-big.png')} style={{height:40, width:40, right:0}}/>
                                </TouchableOpacity>;
                }else{

                  weightimage = <View/>;
                }
              }
            
          }else{

              var image = JSON.parse(imagedata);
              var userdatasource   = this.state.userdatasource;

              if( userdatasource == null ){
                userdatasource = [];
              }
              console.log(userdatasource,'userdatasource');

              var indexofimage_local = this.getIndex(dataid, userdatasource, 'useraddedpantry_id');

              if( indexofimage_local > -1 ){
                  console.log(userdatasource,"userdatasourceuserdatasource");
                  console.log(image,"imagedataimagedata");
                  console.log(indexofimage_local,"indexofimage_local");
                  console.log(dataid,"dataid");

                  var isweight = userdatasource[indexofimage_local]['pantry_isweighted'];

                  console.log(isweight,"isweight12345");

                  if( isweight == 1 ){

                    weightimage = <TouchableOpacity onPress={() => this._clickWeight(elementofr)}>
                                    <Image source={require('../assets/images/shoppingimages/scale-big.png')} style={{height:40, width:40}}/>
                                </TouchableOpacity>;
                  }else{

                    weightimage = <View/>;
                  }

              }
        }



        if( data.includes("thumb") ){

          var image = myArr[0];
           
          console.log(myArr.length,"length");

          var pantrybox = this.state.pantryinbox;

          if( pantrybox == null ){
            pantrybox = [];
          }

          var index        = this.getIndexOfK(pantrybox, image);
          if( pantrybox == null ){
            pantrybox = [];
          }


          var tic = <Text style={{ fontSize: 20,marginTop:5}}></Text>;

          console.log(index,'indexindexindex');

          if( index != undefined ){
              var tic = <Text style={{color:'red', fontSize: 20,marginTop:5}}>√</Text>;
          }

          for ( var i =0; i< myArr.length; i++){
              views.push(
                  <View key={i} style={{flexWrap: 'wrap', marginLeft:2}}>
                      <TouchableOpacity style={{flexWrap: 'wrap'}} onLongPress={this.bounce.bind()} onPress={() => this._selectPantry(image, index, elementofr, i)} >
                      <Animatable.View ref={this.handleViewRef}>

                      <ImageBackground style={{width: wp('5%'), height: hp('10%')}} source={{uri: this.state.ImagePath +image}}>
                          { renderIf(this.state.cross)(
                             <Text style={{marginTop:-2, color: 'red', fontSize: RF(3) }} onPress={() => {Alert.alert( 'Are you sure to delete pantry item',
                                                '',
                                                [
                                                  {text: this.state.cancel ? this.state.cancel : 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.ok ? this.state.ok : 'OK', onPress: () => {this.deletePantry(image)}},
                                                ],
                                                { cancelable: false }
                                              )
                                              }}>X</Text> 
                            ) }
                      </ImageBackground>
                      {tic}
                    </Animatable.View>
                    </TouchableOpacity>
                  </View>
              )
          }

          

        }else{

            //var image = myArr[0];
            
            var image1= myArr[0].split(',');   
            var image = image1[0];
            
            console.log(myArr.length,"length");

            var pantrybox = this.state.pantryinbox;

             if( pantrybox == null ){
              pantrybox = [];
            }

            var index        = this.getIndexOfK(pantrybox, image);




            var tic = <Text style={{ fontSize: 20,marginTop:5}}></Text>;

            if( index != undefined ){
                var tic = <Text style={{color:'red', fontSize: 20,marginTop:5}}>√</Text>;
            }

             for ( var i =0; i< myArr.length; i++){
              views.push(
                  <View key={i} style={{flexWrap: 'wrap', marginLeft:2}}>
                      <TouchableOpacity style={{flexWrap: 'wrap'}} onLongPress={this.bounce.bind()} onPress={() => this._selectPantry(image, index, elementofr, i)} >
                      <Animatable.View ref={this.handleViewRef}>
                     
                      <ImageBackground style={{width: wp('5%'), height: hp('10%')}} source={JSON.parse(image)}>
                          { renderIf(this.state.cross)(
                             <Text style={{marginTop:-2, color: 'red' }} onPress={() => {Alert.alert( 'Are you sure to delete pantry item',
                                                '',
                                                [
                                                  {text: this.state.cancel ? this.state.cancel : 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.ok ? this.state.oh : 'OK', onPress: () => {this.deletePantry(image)}},
                                                ],
                                                { cancelable: false }
                                              )
                                              }}>X</Text> 
                            ) }
                      </ImageBackground>

                      {tic}
                    </Animatable.View>
                    </TouchableOpacity>
                  </View>
              )
        }

          
        }

        return <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent:'space-between'}}>
                      {views}
                      {weightimage}
                  </View>;

        //return views;


        }

        if( index == 1 ){
          return <Text style={{color:'#000000', fontSize: RF(3),}} onPress={() => this._clickPrice(data, index, elementofr)}>{data}</Text>;

        }
        

    }



    return (
          <View style={styles.goshopping_container} >
          <StatusBar
           backgroundColor="#a84e3a"
           barStyle="light-content"/> 
          <ImageBackground source={require('../assets/images/shoppingimages/letsgobk.jpg')}
                  style={[
                 styles.base,{
                     width: "100%"
                 }, {
                     height: "100%"
                 }
             ]}> 

              
                <View style={{flex: 1}}>

                  <View style={{flex: 1,flexDirection: 'row',  justifyContent: 'space-between',marginLeft:hp('5%')}}>
                    
                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%', height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera1.bind(this)}>
                              <View style={{}}>
                              { 
                                this.state.Localprofileimage != null
                               ? <Image source={this.state.Localprofileimage} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>

                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>

                    <View style={{width: wp('62%'), height: hp('10%'),marginTop:hp('5%'),}} >
                          <View style={{flex: 1, alignSelf:'center', }}>
                          <Text style = {{fontFamily:'Comic Sans MS', color:'#FFFFFF',fontSize:hp('5%'),fontWeight: 'bold',alignSelf:'center'}}>{this.state.name}</Text>
                          <Text style = {styles.hometext}>{this.state.PrimaryLanguage ? this.state.GOSHOPPING : 'LETS GO SHOPPING'}</Text>
                          </View>
                    </View>

                     <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%'), marginRight:hp('5%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%',height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera.bind(this)}>
                              <View style={{}}>
                              { 
                                this.state.Localprofileimage2 != null
                               ? <Image source={this.state.Localprofileimage2} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage2!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage2}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>


                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>
                  </View>

                  <View style={{height:'45%', flexDirection: 'row',marginTop:hp('17%') }} >
                      
                        <View style={{flex:1.5}} >
                                <View style={{flexDirection: 'row', width:'70%',height:'50%', alignSelf:'center', justifyContent:'center'}}>

                                      <TouchableHighlight  underlayColor='#232A40' style={{justifyContent:'center'}}>
                                      <Image source={require('../assets/images/shoppingimages/bag.png')} style={{width: wp('12%'), height: hp('24%'),alignSelf:'center',marginTop:'2%'}}/>
                                      </TouchableHighlight>

                                </View>

                                <View style={{marginTop:hp('-12%')}}>
                                <View style={{flexDirection: 'row',backgroundColor:'#FFFFFF',position:'absolute', width:'40%', alignSelf:'center', justifyContent:'center', marginTop:hp('1%'), borderRadius:wp('.5%')}}>
                                          <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e'}}>$ </Text>
                                          <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#000000',}}>{this.state.totalmoney}</Text>
                                </View>
                                </View>

                                <View style={{marginTop:hp('15%')}}>
                                  <Text style={{color:'#FFFFFF', fontSize:hp('3%'), alignSelf:'center', fontWeight:'bold'}}>{this.state.PrimaryLanguage ? this.state.MONEYSPEND : 'MONEY SPEND SO FAR'}</Text>
                                </View>

                                <View style={{flexDirection: 'row',backgroundColor:'#FFFFFF', width:'60%', alignSelf:'center', justifyContent:'center', marginTop:hp('1%'),borderRadius:wp('.5%')}}>
                                          <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e'}}>$ </Text>
                                          <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#000000'}}>{this.state.AmountLeft}</Text>
                                </View>    


                        </View>

                        <View style={{flex:3, backgroundColor:'#FFFFFF', borderWidth:1, borderColor:'#CCCCCC'}} >

                            { !this.state.showcolumn ? <View style={{flexDirection:'row',justifyContent:'space-between', backgroundColor:'#e0e7ed'}}>
                              <View>
                                <Text style={{alignSelf:'center', fontSize:hp('4%'),color:'#000000', marginLeft:10}}>{this.state.PrimaryLanguage ? this.state.ITEM : 'ITEM'}</Text>
                                </View>

                                <View>
                                <Text style={{alignSelf:'center', fontSize:hp('4%'), alignSelf:'flex-end',color:'#000000', marginRight:10}}>{this.state.PrimaryLanguage ? this.state.QUANTITY : 'QUANTITY'}</Text>
                                </View>
                              
                              </View> :
                              <View style={{flexDirection:'row',justifyContent:'space-between', backgroundColor:'#e0e7ed'}}>
                                <View>
                                <Text style={{alignSelf:'center', fontSize:hp('4%'),color:'#000000', marginLeft:10}}>{this.state.PrimaryLanguage ? this.state.ITEM : 'ITEM'}</Text>
                                </View>

                                <View>
                                <Text style={{alignSelf:'center', fontSize:hp('4%'),color:'#000000', marginLeft:10}}>{this.state.PrimaryLanguage ? this.state.QUANTITY : 'QUANTITY'}</Text>
                                </View>

                                 <View>
                                <Text style={{alignSelf:'center', fontSize:hp('4%'),color:'#000000', marginRight:10}}>{this.state.PrimaryLanguage ? this.state.PRICE : 'PRICE'}</Text>
                                </View>

                            
                              </View>
                            }

                            <ScrollView 

                                onTouchStart={(ev) => { 
                                this.setState({enabled:false }); }}
                                onMomentumScrollEnd={(e) => { this.setState({ enabled:true }); }}
                                onScrollEndDrag={(e) => { this.setState({ enabled:true }); }}
                                >
                               { this.state.showcolumn ?
                                      <Table borderStyle={{borderWidth: 2, borderColor: '#CCCCCC',backgroundColor:'#FFFFFF', marginBottom:10}}>
                                        
                                        {
                                          state.tableDatainPrice.map((rowData, index) => (
                                            <TableWrapper key={index} style={styles.row}>
                                              {
                                                rowData.map((cellData, cellIndex) => (
                                                  <Cell key={cellIndex} data={cellIndex === 0 || cellIndex === 1 || cellIndex === 2? element(cellData, cellIndex, index) : cellData} textStyle={{}} 
                                                  onResponderRelease={(e) => this.resetPosition(e, rowData)}/>
                                                ))
                                              }
                                            </TableWrapper>
                                          ))
                                        }
                                      </Table>
                                      :
                                      <Table borderStyle={{borderWidth: 2, borderColor: '#CCCCCC',marginBottom:10}}>
                                    
                                      {
                                        state.tableDatainPriceLine.map((rowData, index) => (
                                          <TableWrapper key={index} style={styles.row}>
                                            {
                                              rowData.map((cellData, cellIndex) => (
                                                <Cell key={cellIndex} data={cellIndex === 0 || cellIndex === 1? element1(cellData,cellIndex, index) : cellData} textStyle={{}}/>
                                              ))
                                            }
                                          </TableWrapper>
                                        ))
                                      }
                                      </Table>
                                     
                              }
                            </ScrollView>
                        </View>

                        <View style={{flex:1.5}} >

                                <TouchableOpacity onPress={()=> {Alert.alert(this.state.PleaseUpdateWeight ? this.state.PleaseUpdateWeight : 'Please update weight on click weight')}}>
                                    <Image  source={require('../assets/images/shoppingimages/scale.png')} style={{height: hp('15%'), width:wp('18%'),alignSelf:'center'}}/>
                                </TouchableOpacity>
                                
                                <TouchableOpacity onPress={this.GetPantryImage.bind(this)}>
                                    <Image source={require('../assets/images/shoppingimages/camera-btn2.png')} style={{height: hp('10%'), width:wp('18%'),alignSelf:'center', marginTop:hp('2%')}}  />
                                </TouchableOpacity>

                      </View>

                  </View>

                  <View style={{flex: 1,  flexDirection: 'row',marginTop:hp('2%'),marginBottom:hp('.5%')}} >

                          <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressHome.bind(this)}  underlayColor='#232A40'>
                          <ImageBackground source={require('../assets/images/shoppingimages/welcome.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center'}}>
                           <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.WECOME : 'WECOME PAGE'}</Text>
                          </ImageBackground>
                          </TouchableOpacity>

                          <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressShoopingList.bind(this)}  underlayColor='#232A40'>
                          <ImageBackground source={require('../assets/images/shoppingimages/shopping-list.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center'}}>
                            <Text style={{alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5)}}>{this.state.PrimaryLanguage ? this.state.SHOPPINGLIST : 'SHOPPING LIST'}</Text>
                          </ImageBackground>
                          </TouchableOpacity>

                          <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressPantry.bind(this)}  underlayColor='#232A40'>
                          <ImageBackground source={require('../assets/images/shoppingimages/pantry.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                              <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.PANTRY : 'PANTRY'}</Text>
                          </ImageBackground>
                          </TouchableOpacity>

                          <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMyMoney.bind(this)}  underlayColor='#232A40'>
                          <ImageBackground source={require('../assets/images/shoppingimages/howmuch.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                              <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE : 'HOW MUCH MONEY I HAVE'}</Text>
                          </ImageBackground>
                          </TouchableOpacity>

                          <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMoneyManagement.bind(this)}  underlayColor='#232A40'>
                          <ImageBackground source={require('../assets/images/shoppingimages/money-management.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                              <Text style={{marginLeft:'5%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYMANAGEMENT : 'MONEY MANAGEMENT'}</Text>
                          </ImageBackground>
                          </TouchableOpacity>

                          
                          <TouchableOpacity style={styles.homebutton_style} onPress={() => {
                                              Alert.alert(
                                               this.state.PrimaryLanguage ? this.state.voicefeatureforlogout : 'Are you sure to Logout',
                                                '',
                                                [
                                                  {text: this.state.cancel ? this.state.cancel : 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.ok ? this.state.ok : 'OK', onPress: () => {this.onPressLogout(this)}},
                                                ],
                                                { cancelable: false }
                                              )
                                    }}>
                          <ImageBackground source={require('../assets/images/shoppingimages/logoutmain.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                            <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.LOGOUT : 'LOGOUT'}</Text>
                          </ImageBackground>
                          </TouchableOpacity>
                    
                  </View>

                </View>

                  {this.state.isLoading ?  <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>}
             
              
              
              </ImageBackground>

              <Modal
                    supportedOrientations={['portrait', 'landscape']}
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                      alert('Modal has been closed.');
                    }}>
                    
                    <ScrollView>
                    <View style={{flexDirection: 'row',justifyContent: 'center', alignItems: 'center',width: '60%', marginLeft:'20%',marginTop:-10, height:hp('100%')}}>
                        

                        <View style={{flex: 1,flexDirection: 'row',height: '60%', backgroundColor: '#028da0',borderRadius:5, borderColor:'#FFFFFF', borderWidth:10}} >

                            <View style={{flex: 2,flexDirection: 'column', justifyContent:'center'}}>

                              <View style={{}}>
                                    <TouchableHighlight style={{marginTop:5,}} onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                                        <Text style={{right:0, color:'red', fontSize: hp('5%'),marginLeft:10}}>X</Text>
                                    </TouchableHighlight>
                              </View>

                              <View style={{flex: 1,}}>
                              <Image source={require('../assets/images/shoppingimages/scale-big.png')} style={{height:'100%', width:'55%',  marginLeft:'25%'}}/>
                              </View> 

                              <View style={{width: wp('25%'),alignSelf:'center',marginBottom:10, height: hp('8%'),backgroundColor:'#000000', borderWidth: 1,justifyContent:'center'}} >
                                    <TouchableHighlight
                                        onPress={() => {
                                              Alert.alert(
                                                'Are you sure to Re-Set Scales',
                                                '',
                                                [
                                                  {text: this.state.cancel ? this.state.cancel : 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.ok ? this.state.ok : 'OK', onPress: () => {this.setState({ cost : '0.00'}); this.setState({ price : '0.00'}); this.setState({ weight : '0.00'});}},
                                                ],
                                                { cancelable: false }
                                              )
                                    }}>
                                    <Text style={{fontWeight: 'bold',color:'#FFFFFF',fontSize: hp('3%'),alignSelf:'center'}}>Re-Set Scales And Prices</Text>
                                    </TouchableHighlight>
                              </View>
                            </View>



                            <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',alignItems: 'center',marginRight:wp('2%')}}>
                              <View style={{width: '100%', height: 50,alignItems:'center', marginTop:0 }} >
                                 <Text style={{fontWeight: 'bold',fontSize: hp('3%'),color:'#FFFFFF'}}>Price Per Kilo</Text>
                                 <View style={{ width: '100%',marginRight:10, backgroundColor:'#FFFFFF',borderColor: '#FFFFFF',}} >
                                    <TextInput style={{
                                        alignSelf: 'center',
                                        textAlign:'center',
                                        width: "100%",
                                        // height:25,
                                        backgroundColor:'#FFFFFF',
                                        borderColor: '#FFFFFF',
                                        //borderWidth: 1,
                                        //borderRadius: 5,
                                        marginTop:-5,
                                        color    :'black',
                                        fontSize : hp('3%')
                                      }}
                                      value={this.state.price}
                                      onChangeText={(price) => this.setState({ price })}
                                      keyboardType='numeric'
                                      editable={false} 
                                  />
                                 </View> 
                              </View>

                              <View style={{width: '100%', height: 50,alignItems:'center',marginTop:5, }} >
                                <Text style={{fontWeight: 'bold',fontSize: hp('3%'),color:'#FFFFFF'}}>Weight(in Kg)</Text>
                                <View style={{ width: '100%',marginRight:10, backgroundColor:'#FFFFFF',borderColor: '#FFFFFF',}} >
                                    <TextInput style={{
                                        alignSelf: 'center',
                                        textAlign:'center',
                                        width: "100%",
                                        //height:25,
                                        backgroundColor:'#FFFFFF',
                                        borderColor: '#FFFFFF',
                                        //borderWidth: 1,
                                        //borderRadius: 5,
                                        marginTop:-5,
                                        fontSize : hp('3%')
                                      }}
                                      value={this.state.weight}
                                      onChangeText={this.checkfornumber.bind(this)}
                                      keyboardType='numeric'
                                  />
                                 </View> 
                              </View>

                              <View style={{width: '100%', height: 50,alignItems:'center',marginTop:5 }} >
                                 <Text style={{fontWeight: 'bold',fontSize: hp('3%'),color:'#FFFFFF'}}>Cost</Text>
                                 <View style={{ width: '100%',marginRight:10, backgroundColor:'#FFFFFF',borderColor: '#FFFFFF',}} >
                                    <TextInput style={{
                                        alignSelf: 'center',
                                        textAlign:'center',
                                        marginTop:-5,
                                        color    :'black',
                                        fontSize : hp('3%')
                                      }}
                                      placeholder = {JSON.stringify(this.state.cost)}
                                      value={this.state.cost}
                                      onChangeText={(cost) => this.setState({ cost })}
                                      keyboardType={''}
                                      keyboardType='numeric'
                                      editable={false} 
                                  />
                                 </View> 
                              </View>

                              <TouchableHighlight onPress={this.onPressAddWeight.bind(this)}>
                              <View style={{width: wp('20%'), height: hp('8%'),marginTop:5,marginBottom:10,backgroundColor:'#6dcaf6', borderWidth: 1,marginRight:10, justifyContent:'center', borderColor:'#6dcaf6'}} >
                                    <Text style={{fontSize: hp('4%'),alignSelf:'center'}}>OK</Text>
                              </View>
                              </TouchableHighlight>
                              


                              </View>
                            </View>

                        <View style={{}} />
                    </View>    
                    </ScrollView>
                  </Modal>




                  {this.state.showmodel1 ?  <Modal
                      supportedOrientations={['landscape']}
                      animationType="slide"
                      transparent={true}
                      visible={this.state.modalVisible1}
                      //visible={true}
                      onRequestClose={() => {
                        alert('Modal has been closed.');
                      }}>
                      <ScrollView>
                     
                      <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',alignItems: 'center', width: wp('80%'), marginTop:hp('25%'), marginLeft: wp('10%')}}>
                            

                            <View style={{flex: 1,flexDirection: 'column',height: hp('50%'), backgroundColor: '#00ffff',borderWidth:5,borderRadius:5, borderColor:'#FFFFFF'}} >

                               <TouchableHighlight style={{marginTop:hp('5%'),backgroundColor:'#FFFFFF', }} onPress={() => {this.closePantryModel(!this.state.modalVisible1);}}>
                                  <Text style={{marginRight:0,color:'red', fontSize:hp('3%')}}>Close</Text>
                              </TouchableHighlight>


                              <View style={{flex: 1,flexDirection  : 'row', justifyContent : 'space-between',borderWidth : 1,}}>
                               
                                <View style={{width: wp('20%'), height: hp('20%'), marginLeft:-5,}} >
                                    <Image style={{height:wp('20%'), width:hp('25%'),alignSelf:'center', marginTop:hp('2%'),}} source={this.state.latestpantry} />
                                </View>

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between',marginTop:hp('10%'),right:12}}>
                                
                                        <View>
                                            <Text style={{fontSize:hp('3'), fontWeight: 'bold',color:'#000000'}}>Pantry Weighted</Text>
                                        </View>
                                        
                                        <View style={{width: '70%', height: '20%',marginTop:-25 }}>

                                                  <RadioGroup
                                                        size={24}
                                                        thickness={2}
                                                        color='#9575b2'
                                                       // highlightColor='#ccc8b9'
                                                        selectedIndex ={ 0 } onSelect = {(index, value) => this.setState({pantryweighttype:value})} >
                                                        <RadioButton value={false} >
                                                        <View style={{flexDirection:'row'}}>
                                                          <Text style={styles.normaltext_small}>No</Text>
                                                          </View>
                                                        </RadioButton>
                                                 
                                                        <RadioButton value={true}>
                                                        <View style={{flexDirection:'row'}}>
                                                         <Text style={styles.normaltext_small}>Yes</Text>
                                                        </View>
                                                        </RadioButton>
                                               
                                                    </RadioGroup> 

                                        </View>

                                </View>

                                <View style={{flex:1, flexDirection:"column", width: 100, height: 100, right:30, marginTop:-5}} >

                                  {this.state.ShowtwoLanguageBox ? <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',marginLeft:5}}>
                                        <View style={{flex: 1,flexDirection: 'column', width: '100%', height: 50,}} >
                                                  <View style={{width: '100%', height: hp('10%'), borderWidth: 1, borderColor: '#000000',marginTop:10,justifyContent:'center'}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                          //backgroundColor: '#FFFFFF'
                                                        }}
                                                        value={this.state.PrimaryPantryadded}
                                                        onChangeText={this.onChangePantryName.bind(this.state.PrimaryPantryadded)}
                                                        keyboardType=''
                                                        placeholder={'Primary Language'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  </View>
                                                  
                                                  <View style={{width: '100%', height: hp('10%'),marginTop:5, borderWidth: 1, borderColor: '#000000',}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                          //backgroundColor: '#FFFFFF'
                                                        }}
                                                        value={this.state.Pantryuseradded}
                                                        keyboardType=''
                                                        placeholder={'Secondary Language'}
                                                        underlineColorAndroid="transparent"
                                                        editable={false}
                                                  />   
                                                  </View>
                                        </View>
                                        <View style={{width: '100%', height: hp('10%'), marginTop:hp('25%'),borderWidth: 1, borderColor: '#000000',backgroundColor:'#FF0000',justifyContent:'center'}} >
                                              <TouchableOpacity onPress={this.onPressSavePantry.bind(this)}>
                                                  <Text style = {{alignSelf:'center',fontWeight: 'bold',color:'#FFFFFF',fontSize:hp('3%')}}>ADD PANTRY</Text> 
                                              </TouchableOpacity>
                                        </View>
                                      </View>:
                                      <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',marginLeft:20}}>
                                        <View style={{flex: 1,flexDirection: 'row', width: '100%', height: 50,marginTop:hp('5%'),alignSelf:'center'  }} >
                                                  <View style={{width: wp('25%'), height: hp('10%'), borderWidth: 1, borderColor: '#000000',justifyContent:'center'}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                        }}
                                                        value={this.state.PrimaryPantryadded}
                                                        onChangeText={this.onChangePantryName.bind(this.state.PrimaryPantryadded)}
                                                        keyboardType=''
                                                        placeholder={'Primary Language'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  </View>
                                                  
                                        </View>
                                        <View style={{width: '85%', height: hp('10%'),marginTop:hp('20%'),borderWidth: 1, borderColor: '#000000',backgroundColor:'#FF0000',justifyContent:'center'}} >
                                              <TouchableOpacity onPress={this.onPressSavePantry.bind(this)}>
                                                  <Text style = {{alignSelf:'center',fontWeight: 'bold',color:'#FFFFFF',fontSize:hp('3%')}}>ADD PANTRY</Text> 
                                              </TouchableOpacity>
                                        </View>
                                      </View>

                                      
                                  }

                                    </View>

                              </View>
                             
                            </View>

                            <View style={{}} />
                          </View>
                          </ScrollView>
                      </Modal> : <View/> }


                { this.state.editprice ? 
                  <Modal
                    supportedOrientations={['portrait', 'landscape']}
                    animationType="slide"
                    transparent={true}
                    visible={this.state.ispricevisible}
                    onRequestClose={() => {
                      alert('Modal has been closed.');
                    }}>
                    
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '60%', 
                        marginLeft:'20%',
                      }}>
                        <View style={{}} />

                        <View style={{flex: 1,flexDirection: 'column',height: '60%', backgroundColor: '#00ffff',borderWidth:5,borderRadius:5, borderColor:'#FFFFFF'}} >

                           <TouchableHighlight style={{marginTop:20,backgroundColor:'#FFFFFF', }} onPress={() => {this.setState({ispricevisible: false});}}>
                              <Text style={{right:0}}>Close</Text>
                          </TouchableHighlight>
                          <View>
                            <TextInput
                                style={{height: 40}}
                                placeholder="Enter Price"
                                onChangeText={(text) => this._oneditprice({text})}
                                keyboardType="numeric"
                              />
                          </View>
                      </View>
                        <View style={{}} />
                      </View>
                  </Modal> : <View/>}


                  { this.state.editquantity ? 
                  <Modal
                    supportedOrientations={['portrait', 'landscape']}
                    animationType="slide"
                    transparent={true}
                    visible={this.state.editquantity}
                    onRequestClose={() => {
                      alert('Modal has been closed.');
                    }}>
                    
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '20%', 
                        marginLeft:'40%',
                      }}>
                        
                        <View style={{flex: 1,flexDirection: 'column',height: '40%', backgroundColor: '#00ffff',borderWidth:5,borderRadius:5, borderColor:'#FFFFFF'}} >

                           <TouchableHighlight style={{marginTop:20,backgroundColor:'#FFFFFF', }} onPress={() => {this.setState({editquantity: false});}}>
                              <Text style={{right:0, fontSize:RF(4), fontWeight: 'bold'}}>Close</Text>
                          </TouchableHighlight>
                          <View style={{justifyContent:'center', color:'#000000', borderWidth:2, marginTop:'10%',height:'30%', width:'80%', alignSelf:'center'}}>
                            <TextInput
                                style={{ fontSize: RF(3), color:'#000000'}}
                                placeholder="Enter Quantity"
                                onChangeText={(text) => this._oneditQuantity({text})}
                                keyboardType="numeric"
                                underlineColorAndroid="transparent"
                              />
                          </View>
                      </View>
                        
                      </View>
                  </Modal> : <View/>}

          </View> 
    );
  }
}
