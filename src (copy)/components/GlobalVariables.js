import React, { Component } from 'react';


export default class GlobalVariables extends Component<Props> {

    
    static Api = {

      BaseUrl                   :   'http://18.218.60.240/',
      ImagePath                 :   'http://18.218.60.240/assets/pantryLibrary/',
      Login                     :   'api/login',
      Registration              :   'api/signup',
      ForgetPassword            :   'api/forget-password',
      Profile                   :   'api/profile',
      Pantry                    :   'http://18.218.60.240/api/pantry',
      Setting                   :   'api/user-setting',
      UploadSelfie              :   'api/update-selfie',
      GoShoppingAudioType       :   'api/goshopping-audiotype',
      ShoppingListAudioType     :   'api/shopping-audiotype',
      ShoppingListType          :   'api/shopping-type',
      MyMoneyAudioType          :   'api/mymoney-audiotype',
      MoneymanagementAudioType  :   'api/moneymanagement-audiotype',
      LowMoneyWarningType       :   'api/lowmoneywarning-type',
      LanguageType              :   'api/language-type',
      UpdateName                :   'api/update-name',
      Selfietype                :   'api/selfie-type',
      LowMoneyValue             :   'api/update-lowmoney',
      Languages                 :   'api/languages',
      UserMoney                 :   'api/user-money',
      UpdateMoney               :   'api/update-money',
      ResetMoney                :   'api/reset-money',
      UpdateSetting             :   'api/update-setting',
      AddPantry                 :   'api/user-add-pantry-name',
      AddPantryImage            :   'api/user-add-pantry',
      UpdatePantry              :   'api/user-update-pantry',
      DeletePantry              :   'api/user-delete-pantry',
      AddShoppingList           :   'api/add-shoppinglist',
      UpdateShoppingList        :   'api/update-shoppinglist',
      DownloadShoppingList      :   'api/print-shoppinglist',
      TextChange                :   'api/change-text',
      DeleteAdminPantry         :   'api/delete-pantry',
      Translations              :   'api/translations',
      BaseImageURl              :   'http://18.218.60.240/assets/userLibrary/',
      UpdatePrimaryLanguage     :   'api/update-primary-language',

    }

    static getApiUrl() {
      return this.Api;
    } 
}

