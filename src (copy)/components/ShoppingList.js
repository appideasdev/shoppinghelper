/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  Button,
  TouchableHighlight,
  Image,
  ScrollView,
  ListView,
  Alert,
  ActivityIndicator,
  AsyncStorage,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  Modal,
  TextInput,
  List
} from 'react-native';

import styles from './GlobalStyle.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import Tts from 'react-native-tts';
import renderIf from './renderIf';
import * as Animatable from 'react-native-animatable';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";



//import PDFView from 'react-native-pdf-view';
//import { List, ListItem } from 'react-native-elements';


import { Table, TableWrapper, Row, Cell, Rows } from 'react-native-table-component';

import GlobalVariables from './GlobalVariables.js';


type Props = {};
var itemArray=[];


export default class ShoppingList extends Component<Props> {

  static navigationOptions = {
      header: null
  }

  constructor(props) {

    AsyncStorage.getItem('TOKEN').then((data) => { 

      if(!data){
          Actions.Login();
      }
            
    });
    
    super(props);
    var GVar = GlobalVariables.getApiUrl();

    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      user_id  : null,
      token    : null,
      pantryaddedid : null,
      tableHead: ['Item','Quantity/Weight(in Kg)'],
      tableDataReplica: [],
      tableData: [],
      tableHeadinLine: ['Item'],
      tableDatainColumn: [ ],
      tableDatainPrice: [ ],
      tableDatainPriceLine: [ ],
      ImagePath : GVar.ImagePath,
      isLoading : true,
      isLoadingPantry : true,
      name      : null,
      dataSource: ds.cloneWithRows([]),
      avatarSource: null,
      profileimage    : null,
      profileimage2   : null,
      subscribe       : null,
      Localprofileimage     : null,
      Localprofileimage2    : null,
      showmodel             : false,
      showcolumn            : false,
      ShowtwoLanguageBox    : true,
      LanguageType          : null,
      variable              : null,
      pantryinbox           : [],
      instancedata          : '',
      dataeditQuantity      : '',
      indexcellQuantity     : '',   
      elementIndexQuantity  : '',
      ispricevisible        : true, 
      editquantity          : false, 
      pantryweighttype      : false,
      rubberBand            : null,
      isSingle              : null,
    };

    var name = AsyncStorage.getItem('name');

    AsyncStorage.getItem('name').then((data) => { 
        
    this.setState({
               name: data,
               //isLoading : false,
             });
    });

    AsyncStorage.getItem('pantryinbox').then((data) => { 

          console.log(data,"pantryinbox");

          this.setState({pantryinbox: JSON.parse(data)});  

    });


    AsyncStorage.getItem('LANGUAGES').then((data) => { 

          console.log(data,"LANGUAGES");

          this.setState({LANGUAGES: JSON.parse(data)});  

    });

    AsyncStorage.multiGet(["profileimage", "profileimage2"]).then(response => {
    
           this.setState({
                                    profileimage     : response[0][1],
                                    profileimage2    : response[1][1],
                                   
                          });   
                          
                           
    });


    AsyncStorage.getItem('PrimaryLanguage').then((data) => {
          
          

              if( data == null ){

                this.setState({PrimaryLanguage : 'en' });
                
              }else{

                this.setState({PrimaryLanguage : data }); 
              }
              

    });

    

    AsyncStorage.multiGet(["tableData", "tableDatainColumn","tableDataReplica","tableDatainPrice","tableDatainPriceLine","totalmoney"]).then(response => {
              
          

          var tableData                     = response[0][1];
          var tableDatainColumn             = response[1][1];
          var tableDataReplica              = response[2][1];
          var tableDatainPrice              = response[3][1];
          var tableDatainPriceLine          = response[4][1];
          var totalmoney                    = response[5][1];

         
          /*this.setState({ tableData             : JSON.parse(tableData) }); 
          this.setState({ tableDatainColumn     : JSON.parse(tableDatainColumn) }); 
          this.setState({ tableDataReplica      : JSON.parse(tableDataReplica) }); 
          this.setState({ tableDatainPrice      : JSON.parse(tableDatainPrice) });
          this.setState({ tableDatainPriceLine  : JSON.parse(tableDatainPriceLine) });
          this.setState({ totalmoney            : JSON.parse(totalmoney) });*/

        

          console.log(JSON.parse(tableData),"tabledata");

          this.setState({isLoading:false})
          //Alert.alert("tabledata");
          
          if( JSON.parse(tableData) ){
            this.setState({ tableData             : JSON.parse(tableData) });
           
          }else{
            this.setState({ tableData             : [] });
          }
         
          if(JSON.parse(tableDatainColumn) ){
            this.setState({ tableDatainColumn             : JSON.parse(tableDatainColumn) });
            
          }else{
            this.setState({ tableDatainColumn             : [] });
          }

          if( JSON.parse(tableDataReplica) ){
            this.setState({ tableDataReplica             : JSON.parse(tableDataReplica) });
          }else{
            this.setState({ tableDataReplica             : [] });
            
          }


          if( JSON.parse(tableDatainPrice) ){
            this.setState({ tableDatainPrice             : JSON.parse(tableDatainPrice) });
          }else{
            this.setState({ tableDatainPrice             : [] });
            
          }

          if( JSON.parse(tableDatainPriceLine) ){
             this.setState({ tableDatainPriceLine             : JSON.parse(tableDatainPriceLine) });
          }else{
            this.setState({ tableDatainPriceLine             : [] });
           
          }

          if( JSON.parse(totalmoney)){
             this.setState({ totalmoney             : JSON.parse(totalmoney) });
          }else{
            this.setState({ totalmoney             : parseFloat(0.00).toFixed(0) });
           
          }

       
    });

    AsyncStorage.getItem('isSingle').then((data) => {
          
            console.log(data,"CHECKLANGUAGETYPE")

            if( JSON.parse(data) == 'true' || data == 'true' ){

              this.setState({isSingle : true});
              this.setState({ShowtwoLanguageBox : false});
            }

            if( JSON.parse(data) == 'false' || data == 'false' ){

              this.setState({isSingle : false});
              this.setState({ShowtwoLanguageBox : true});
            }

            if( JSON.parse(data) == null  || data == null){
              this.setState({ShowtwoLanguageBox : true});
            }

            

      });  


    
    this.handleViewRef = this.handleViewRef.bind(this);
    
  }

  componentDidMount = async () => {

          this.setLanguages = this.setLanguages();

          AsyncStorage.multiGet(["Localprofileimage", "Localprofileimage2","userdatasource"]).then(response => {
                
            var Localprofileimage              = response[0][1];
            var Localprofileimage2             = response[1][1];
            var userdatasource                 = response[2][1];

            //alert(Localprofileimage)


            this.setState({
                                    Localprofileimage     : JSON.parse(Localprofileimage),
                                    Localprofileimage2    : JSON.parse(Localprofileimage2),
                                    userdatasource        : JSON.parse(userdatasource),          
                          });  

                

            });


            AsyncStorage.multiGet(["name", "user_id","LowMoneyWarningType","MoneymanagementAudioType", "MyMoneyAudioType", "ShoppingListAudioType", "ShoppingListType", "LanguageType", "Selfietype","GoShoppingAudioType","TOKEN","GoShoppingAudioType","LowmoneyStatus","LowmoneyTune","LowmoneyValue","PrimaryLanguage","SeconderyLanguage","LanguageTypestatus","LanguageType"]).then(response => {
            

                    var name                          = response[0][1];
                    var user_id                       = response[1][1];
                    var LowMoneyWarningType           = response[2][1];
                    var MoneymanagementAudioType      = response[3][1];
                    var MyMoneyAudioType              = response[4][1];
                    var ShoppingListAudioType         = response[5][1];
                    var ShoppingListType              = response[6][1];
                    var LanguageType                  = response[7][1];
                    var Selfietype                    = response[8][1];
                    var GoShoppingAudioType           = response[9][1];
                    var token                         = response[10][1];
                    var GoShoppingAudioType           = response[11][1];
                    var LowmoneyStatus                = response[12][1];
                    var LowmoneyTune                  = response[13][1];
                    var LowmoneyValue                 = response[14][1];
                    var PrimaryLanguage               = response[15][1];
                    var SeconderyLanguage             = response[16][1];
                    var LanguageTypestatus            = response[17][1];
                    var LanguageType1                  = response[18][1];

                    //Alert.alert(ShoppingListType);

                    if( ShoppingListType == 'column' ){
                      this.setState({showcolumn: true});
                     
                    }else{
                      this.setState({showcolumn: false});

                    }

/*                    if( LanguageType =="dual" ){

                      this.setState({ShowtwoLanguageBox : true});
                    }else{

                      this.setState({ShowtwoLanguageBox : false});
                    }
*/
                  


                     this.setState({
                                    name                    : name.toUpperCase(),
                                    LowMoneyWarningType     : LowMoneyWarningType,
                                    MoneymanagementAudioType: MoneymanagementAudioType,
                                    MyMoneyAudioType        : MyMoneyAudioType,
                                    ShoppingListAudioType   : ShoppingListAudioType,
                                    ShoppingListType        : ShoppingListType,
                                    LanguageType            : LanguageType,
                                    Selfietype              : Selfietype,
                                    GoShoppingAudioType     : GoShoppingAudioType,
                                    LowmoneyStatus          : LowmoneyStatus,
                                    LowmoneyTune            : LowmoneyTune,
                                    LowmoneyValue           : LowmoneyValue,
                                    PrimaryLanguage         : PrimaryLanguage,
                                    SeconderyLanguage       : SeconderyLanguage,
                                    token                   : token,
                                    LanguageTypestatus      : LanguageTypestatus,
                                    user_id                 : user_id,
                              });

                 
            /*var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();
            
            if(PrimaryLanguage == null){
              PrimaryLanguage = 'en';
            }

            formdata.append('language', PrimaryLanguage );

            try {
                  let response = fetch(GVar.Pantry,{
                                    method: 'POST',
                                    headers:{
                                                'token' : token
                                            },
                                    body: formdata,   
                                    }).then((response) => response.json())
                                             .then((responseJson) => {
                                               this.setState({
                                                 isLoadingPantry: false,
                                                 dataSource: responseJson.pantry,
                                               }, function() {
                                                 //Alert.alert(JSON.stringify(responseJson));
                                               });
                                             });


                
               }catch (error) {

                if( error.line == 18228 ){
                  //Alert.alert("Please connect to network");
                }

               
              }*/


          }); 


           AsyncStorage.getItem('PANTRYDATA').then((data) => {

     
            var pantry = JSON.parse(data);


            if( Array.isArray(pantry) && pantry.length > 0 ){

                this.setState({
                           isLoading : false,
                           dataSource: JSON.parse(data),
                         });
            }else{

              this.setState({
                           isLoading : false,
                           dataSource: [],
                         });
            }
            

          });
    
   
          
   }

    getIndexOfK(arr, k) {
      for (var i = 0; i < arr.length; i++) {
        var index = arr[i].indexOf(k);
        if (index > -1) {
          return [i, index];
        }
      }
    }


    getIndex(value, arr, prop) {
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }

    



   GetGridViewItem (id) {

    
              

              var pantry_id     = id;

              var dataSource    = this.state.dataSource;

              if( dataSource == null ){
                dataSource = [];
              }

              var pantryindex   = this.getIndex(id, dataSource, 'id');
              var pantry        = dataSource[pantryindex];

              

      /**********************************************************/


              //AUDIO

              var isVoice                 = this.state.ShoppingListAudioType;
              var SeconderyLanguage       = this.state.SeconderyLanguage;
              var LanguageTypestatus      = this.state.LanguageTypestatus;
              var isSingle                = this.state.isSingle;
              var PrimaryLanguage         = this.state.PrimaryLanguage;
              var Language                = this.state.LANGUAGES;

              
              
              if( isVoice == 0 ){

                  
                  console.log(this.state.PrimaryLanguage,"PrimaryLanguage");        

                  Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

                        console.log(setDefaultLanguage,"setDefaultLanguage")
                        Tts.speak( pantry.pantry );

                  }).catch((error) => {

                      var index = this.getIndex( PrimaryLanguage, Language.languages, 'language_code'  );

                      console.log(Language.languages[index],"currentPrimaryLangauage");

                      var LanguageName = '';

                      if( index > -1 ){

                        LanguageName = Language.languages[index];
                      }

                      Alert.alert("Your device doesn't support "+ LanguageName.language +" primary language voice.");
                      console.log(error,"setDefaultLanguage");

                  });
                  
                  
                  


                  //Tts.speak(PrimaryLanguage);

                  //alert(PrimaryLanguage)

                  
                  console.log(isSingle,"isSingle")
                  if( isSingle === false ){
                    
                          Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                                  console.log(setDefaultLanguage,"setDefaultLanguageSecondary")
                                  
                                  var secondrayPantry = '';
                                  if( SeconderyLanguage == 'en' ){



                                      secondrayPantry = pantry.en_pantry
                                      Tts.speak( pantry.en_pantry );
                                  }

                                  if( SeconderyLanguage == 'el' ){

                                      secondrayPantry = pantry.el_pantry
                                      Tts.speak( secondrayPantry );
                                  }

                                  if( SeconderyLanguage == 'cmn' ){

                                      secondrayPantry = pantry.cmn_pantry
                                      Tts.speak( secondrayPantry );
                                  }

                                  if( SeconderyLanguage == 'zh' ){

                                      secondrayPantry = pantry.zh_pantry
                                      Tts.speak( secondrayPantry );
                                  }

                                  if( SeconderyLanguage == 'ar' ){

                                      secondrayPantry = pantry.ar_pantry
                                      Tts.speak( secondrayPantry );
                                  }

                                  if( SeconderyLanguage == 'vi' ){

                                      secondrayPantry = pantry.vi_pantry
                                      Tts.speak( secondrayPantry );
                                  }

                                  if( SeconderyLanguage == 'it' ){

                                      secondrayPantry = pantry.it_pantry
                                      Tts.speak( secondrayPantry );
                                  }

                                  if( SeconderyLanguage == 'es' ){

                                      secondrayPantry = pantry.es_pantry
                                      Tts.speak( pantry.es_pantry );
                                  }

                                 
                                  if( SeconderyLanguage == 'hi' ){

                                     
                                      secondrayPantry = pantry.hi_pantry
                                      Tts.speak( pantry.hi_pantry );
                                      
                                  }

                                  if( SeconderyLanguage == 'pa' ){

                                      secondrayPantry = pantry.pa_pantry
                                      Tts.speak( secondrayPantry );
                                  }

                                  if( SeconderyLanguage == 'fil' ){

                                      secondrayPantry = pantry.fil_pantry
                                      Tts.speak( secondrayPantry );
                                  }

                                  console.log(secondrayPantry,"secondrayPantry")
                                  console.log(isSingle,"isSingle")
                                  console.log(SeconderyLanguage,"SeconderyLanguage")

                          }).catch((error) => {

                              var index = this.getIndex( SeconderyLanguage, Language.languages, 'language_code'  );

                              console.log(Language.languages[index],"currentSecondaryLanguage");

                              var LanguageName = '';

                              if( index > -1 ){

                                LanguageName = Language.languages[index];
                              }


                              Alert.alert("Your device doesn't support "+LanguageName.language+" secondary language voice.");
                              
                          });

                          

                  }

                  
                   
                  
              }

      /************************************************************/

              var pantry_image  = 'pantry_image';

              var item          = pantry.pantry_image;
              //var item_isweighted = pantry.pantry_isweighted;
              var item_isweighted = 0;

              console.log(item_isweighted,"item_isweighted");
 
              var cost          = pantry.pantry_cost;

              AsyncStorage.multiGet(['tableData', 'tableDatainLine']).then((data) => {

                  var arr           = [];
                  var arrCol        = [];
                  var arrReplica    = [];
                  var arrPriceLine  = [];

                  

                  var tabledata               = this.state.tableData;
                  var tableDatainColumn       = this.state.tableDatainColumn;
                  var tableDatainPrice        = this.state.tableDatainPrice;
                  var tableDatainPriceLine    = this.state.tableDatainPriceLine;
                  var tableDataReplica        = this.state.tableDataReplica;

                  if( tableDataReplica == null ){

                    tableDataReplica = [];
                  }

                 
                  var result        = this.getIndexOfK(tableDataReplica, item);
                  
                  var quantity      = 1;
                  

                  var cell1         = 0;
                  var cell2         = 0;

                  if( JSON.stringify(tabledata) != ' '){

                    arr = [];
                    id  = tabledata.length + 1; 
                  }

              

              if( result != null){

                  var pantrybox = this.state.pantryinbox;

                  if( pantrybox === null ){
                    pantrybox  = [];
                  }

                  var istic = this.getIndexOfK(pantrybox, item);

                  console.log(result,"resultadded");
                  console.log(pantrybox,"pantrybox");
                  console.log(istic,"istic");

                  if( Array.isArray(istic) && istic.length > 0 ){
               
                      arr = this.state.tableData;
                      arrCol = this.state.tableDatainColumn;
                      arrReplica = this.state.tableDataReplica;
                      arrPrice = this.state.tableDatainPrice;
                      arrPriceLine = this.state.tableDatainPriceLine;

                      Alert.alert( this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message",this.state.Addquantityforpantry ? this.state.Addquantityforpantry : "The option for adding quantity of added pantry for shopping should be on the ‘Lets Go Shopping’ page.")   

                  }if( item_isweighted == '1'){

                      arr = this.state.tableData;
                      arrCol = this.state.tableDatainColumn;
                      arrReplica = this.state.tableDataReplica;
                      arrPrice = this.state.tableDatainPrice;
                      arrPriceLine = this.state.tableDatainPriceLine;

                      Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message :  "Suggestion message",this.state.Addquantityforpantry ? this.state.Addquantityforpantry : "The option for purchase by weight should be on the ‘Lets Go Shopping’ page.");   

                  }else{

                        cell1  = result[0];
                        cell2  = result[1];


                        console.log(tableDatainColumn[cell1][0],"pantryimage");
                        console.log(tableDatainColumn[cell1][1],"pantryquantity");

                        var image         = tableDatainColumn[cell1][0];
                        var quantity      = parseFloat(tableDatainColumn[cell1][1]).toFixed(0)

                        var itemquantity  = parseInt(tableDatainColumn[cell1][1]) +  parseInt("1");
                        var itemimage     = image;

                        var arr               = [];
                        

                        for (var i = 0; i < itemquantity; i++) {

                          arr.push(item);
                        }

                        var t1 = arr.join('#');





                        /*code to inline*/
                    
                        var newpantry      = tabledata[cell1, cell2];

                        
                        var newItem = item + '#' + newpantry[0];

                        //tabledata[cell1][0] =  item + '#' + newpantry[0];
                        tabledata[cell1][0] =  t1;
                        arr = tabledata;

                        this.setState({tabledata:arr});
                        AsyncStorage.setItem('tabledata', JSON.stringify(this.state.tabledata) );

                        console.log(tabledata+"tabledata");

                        arrReplica = this.state.tableDataReplica;

                        /*code to column*/

                        if( tableDatainColumn == null ){
                          tableDatainColumn = [];
                        }

                        var resultcol        = this.getIndexOfK(tableDatainColumn, item);
                        var col1  = resultcol[0];
                        var col2  = resultcol[1];

                        var newpantrycol      =   tableDatainColumn[col1, col2];

                        //tableDatainColumn[col1][1] = + parseInt(newpantrycol[1]) + + 1;
                        tableDatainColumn[col1][1] = itemquantity;

                       
                        console.log("newjson"+JSON.stringify(tableDatainColumn));

                        arrCol = tableDatainColumn;

                        this.setState({tableDatainColumn:arrCol});
                        AsyncStorage.setItem('tableDatainColumn', JSON.stringify(this.state.tableDatainColumn) );




                         /*code to Price*/

                         if( tableDatainPrice == null ){
                          tableDatainPrice = [];
                         }

                        var resultPrice        = this.getIndexOfK(tableDatainPrice, item);

                        var price1  = resultPrice[0];
                        var price2  = resultPrice[1];

                        var newpantryprice     =   tableDatainPrice[price1, price2];

                        //tableDatainPrice[col1][1] = + parseInt(newpantryprice[1]) + + 1;
                        tableDatainPrice[col1][1] = itemquantity;
                        //tableDatainPrice[col1][2] =  tableDatainPrice[col1][1] * cost;
                        
                        /*add cost*/
                        //tableDatainPrice[col1][2] =  cost;

                        arrPrice = tableDatainPrice;

                        console.log(arrPrice,"tableDatainPrice");

                        this.setState({tableDatainPrice:arrPrice});
                        AsyncStorage.setItem('tableDatainPrice', JSON.stringify(this.state.tableDatainPrice) );

                        /*code to Price in Line*/

                        

                        var resultPrice        = this.getIndexOfK(tableDatainPrice, item);

                        console.log(resultPrice,"resultPriceamit");

                        var priceLine1  = resultPrice[0];
                        var priceLine2  = resultPrice[1];

                        var newpantryLine      = tableDatainPriceLine[priceLine1, priceLine1];

                      
                        //tableDatainPriceLine[priceLine1][0] =  item + '#' + newpantryLine[0];
                        tableDatainPriceLine[priceLine1][0] = t1;
                        //tableDatainPriceLine[priceLine1][1] =  tableDatainPrice[col1][1] * cost;
                        
                        /*add cost*/
                        //tableDatainPriceLine[priceLine1][1] =  cost;
                        
                        console.log(tableDatainPriceLine,"tableDatainPriceLine1");
                        arrPriceLine = tableDatainPriceLine;

                        


                        this.setState({tableDatainPriceLine:arrPriceLine});
                        AsyncStorage.setItem('tableDatainPriceLine', JSON.stringify(this.state.tableDatainPriceLine) );

                        /*end*/

                        var GVar = GlobalVariables.getApiUrl();

                            

                            /*let formdata = new FormData();

                            var type = 'admin';

                            formdata.append('user_id', this.state.user_id);
                            formdata.append('type', type);
                            formdata.append('pantry_id', pantry_id);
                           
                            var token = this.state.token;

                            console.log(formdata,"formdata1");
                            console.log(token,"token1");


                            try {
                                var response = fetch(GVar.BaseUrl + GVar.UpdateShoppingList , {
                                                method: 'POST',
                                                headers:{
                                                            'token' : token
                                                        },
                                                body: formdata,
                                              }).then((response) => response.json())
                                                 .then((responseJson) => {

                                                    //Alert.alert(JSON.stringify(responseJson));
                                                    console.log(responseJson,"responseJson");
                                                     
                                                 });

                               }catch (error) {

                              }*/

                       
                        }

              }else{

                      console.log(pantry_id,"pantry_id"); 

                      /*code to Line*/

                      tabledata.unshift(
                          [item]
                      );


                      arr     = tabledata;

                      /*code to Column*/

                      tableDatainColumn.unshift(
                          [item, 1]
                      );

                      arrCol  = tableDatainColumn;

                      /*code to Price*/

                      tableDatainPrice.unshift(
                          [item, 1, cost]
                      );

                      arrPrice  = tableDatainPrice;

                      /*code to checking*/

                      tableDataReplica.unshift([item, pantry_id]); 
                      arrReplica  = tableDataReplica;

                      /*code to price in Line*/

                      tableDatainPriceLine.unshift(
                          [item, cost]
                      );

                      arrPriceLine     = tableDatainPriceLine;


                      /*var GVar = GlobalVariables.getApiUrl();

                      

                      let formdata = new FormData();

                      var type = 'admin';

                      formdata.append('user_id', this.state.user_id);
                      formdata.append('type', type);
                      formdata.append('pantry_id', pantry_id);
                     
                      var token = this.state.token;

                      console.log(formdata,"formdata");
                      console.log(token,"token");*/


                      /*try {
                          var response = fetch(GVar.BaseUrl + GVar.AddShoppingList , {
                                          method: 'POST',
                                          headers:{
                                                      'token' : token
                                                  },
                                          body: formdata,
                                        }).then((response) => response.json())
                                           .then((responseJson) => {

                                              //Alert.alert(JSON.stringify(responseJson));
                                              console.log(responseJson,"responseJsonInsert");
                                               
                                           });

                         }catch (error) {

                        }
*/
              }


              console.log(arr,"arr");
              console.log(arrCol,"arrCol");
              console.log(arrReplica,"arrReplica");
              console.log(arrPrice,"arrPrice");
              console.log(arrPriceLine,"arrPriceLine");


              this.setState({
                    tableData : arr,
                    tableDatainColumn : arrCol,
                    tableDataReplica : arrReplica,
                    tableDatainPrice : arrPrice,
                    tableDatainPriceLine : arrPriceLine,
                  });

              

               AsyncStorage.multiSet([
                  ["tableData",                 JSON.stringify(this.state.tableData)],
                  ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                  ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                  ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                  ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
              ]);

        });
     
        
   }


   GetGridViewItem1 (id) {

      var pantry_id         = id;
      var userdatasource    = this.state.userdatasource;

      var pantryindex       = this.getIndex(id, userdatasource, 'useraddedpantry_id');


      
      var pantry            = userdatasource[pantryindex];

      var pantry_image      = 'pantry_image';

      var item              = pantry.pantry_image;
      var item_isweighted   = pantry.pantry_isweighted;

      console.log(item_isweighted,"item_isweighted");

      AsyncStorage.multiGet(['tableData', 'tableDatainLine']).then((data) => {

        var arr           = [];
        var arrCol        = [];
        var arrReplica    = [];
        var arrPriceLine  = [];

        

        var tabledata               = this.state.tableData;
        var tableDatainColumn       = this.state.tableDatainColumn;
        var tableDatainPrice        = this.state.tableDatainPrice;
        var tableDatainPriceLine    = this.state.tableDatainPriceLine;
        var tableDataReplica        = this.state.tableDataReplica;


        if( tableDataReplica == null ){
          tableDataReplica = [];
        }
       
        var result        = this.getIndexOfK(tableDataReplica, JSON.stringify(item));
        
        var quantity      = 1;
        

        var cell1         = 0;
        var cell2         = 0;

        if( JSON.stringify(tabledata) != ' '){

          arr = [];
          id  = tabledata.length + 1; 
        }

              

              if( result != null){

                var pantrybox = this.state.pantryinbox;

                  if( pantrybox === null ){
                    pantrybox  = [];
                  }

                  var istic = this.getIndexOfK(pantrybox, JSON.stringify(item));

                  console.log(result,"resultadded");
                  console.log(pantrybox,"pantrybox");
                  console.log(istic,"istic");
                  console.log(item,"item");

                  if( Array.isArray(istic) && istic.length > 0 ){
               
                      Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message :  "Suggestion message",this.state.Addquantityforpantry ? this.state.Addquantityforpantry : "The option for adding quantity of added pantry for shopping should be on the ‘Lets Go Shopping’ page.")   

                  
                      arr = this.state.tableData;
                      arrCol = this.state.tableDatainColumn;
                      arrReplica = this.state.tableDataReplica;
                      arrPrice = this.state.tableDatainPrice;
                      arrPriceLine = this.state.tableDatainPriceLine;

                  }if(item_isweighted){

                      arr = this.state.tableData;
                      arrCol = this.state.tableDatainColumn;
                      arrReplica = this.state.tableDataReplica;
                      arrPrice = this.state.tableDatainPrice;
                      arrPriceLine = this.state.tableDatainPriceLine;

                      Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message :  "Suggestion message", this.state.pantryisweightedpleasegoshopping ? this.state.pantryisweightedpleasegoshopping : "This pantry is weighted please weighted while Go-Shopping.");

                  }else{

                            cell1  = result[0];
                            cell2  = result[1];



                            /*code to inline*/
                        
                            var newpantry      = tabledata[cell1, cell2];

                            
                            var newItem = item + '#' + newpantry[0];

                            tabledata[cell1][0] =  JSON.stringify(item) + '#' + newpantry[0];
                            arr = tabledata;

                            this.setState({tabledata:arr});
                            AsyncStorage.setItem('tabledata', JSON.stringify(this.state.tabledata) );

                            console.log(tabledata+"tabledata");

                            arrReplica = this.state.tableDataReplica;

                            /*code to column*/

                            if( tableDatainColumn == null ){
                              tableDatainColumn = [];
                            }

                            var resultcol        = this.getIndexOfK(tableDatainColumn, JSON.stringify(item));
                            var col1  = resultcol[0];
                            var col2  = resultcol[1];

                            var newpantrycol      =   tableDatainColumn[col1, col2];

                            tableDatainColumn[col1][1] = + parseInt(newpantrycol[1]) + + 1;

                           
                            console.log("newjson"+JSON.stringify(tableDatainColumn));

                            arrCol = tableDatainColumn;

                            this.setState({tableDatainColumn:arrCol});
                            AsyncStorage.setItem('tableDatainColumn', JSON.stringify(this.state.tableDatainColumn) );




                             /*code to Price*/

                            if( tableDatainPrice == null ){
                              tableDatainPrice = [];
                            }

                            var resultPrice        = this.getIndexOfK(tableDatainPrice, JSON.stringify(item));

                            var price1  = resultPrice[0];
                            var price2  = resultPrice[1];

                            var newpantryprice     =   tableDatainPrice[price1, price2];

                            tableDatainPrice[col1][1] = + parseInt(newpantryprice[1]) + + 1;

                            arrPrice = tableDatainPrice;

                            console.log(arrPrice,"tableDatainPrice");

                            this.setState({tableDatainPrice:arrPrice});
                            AsyncStorage.setItem('tableDatainPrice', JSON.stringify(this.state.tableDatainPrice) );

                            /*code to Price in Line*/

                            

                            var resultPrice        = this.getIndexOfK(tableDatainPrice, JSON.stringify(item));

                            var priceLine1  = resultPrice[0];
                            var priceLine2  = resultPrice[1];

                            var newpantryLine      = tableDatainPriceLine[priceLine1, priceLine1];

                          
                            tableDatainPriceLine[priceLine1][0] =  JSON.stringify(item) + '#' + newpantryLine[0];
                            
                            console.log(tableDatainPriceLine,"tableDatainPriceLine1");
                            arrPriceLine = tableDatainPriceLine;

                            


                            this.setState({tableDatainPriceLine:arrPriceLine});
                            AsyncStorage.setItem('tableDatainPriceLine', JSON.stringify(this.state.tableDatainPriceLine) );

                            /*end*/

                            var GVar = GlobalVariables.getApiUrl();

                                

                                /*let formdata = new FormData();

                                var type = 'admin';

                                formdata.append('user_id', this.state.user_id);
                                formdata.append('type', type);
                                formdata.append('pantry_id', pantry_id);
                               
                                var token = this.state.token;

                                console.log(formdata,"formdata1");
                                console.log(token,"token1");


                                try {
                                    var response = fetch(GVar.BaseUrl + GVar.UpdateShoppingList , {
                                                    method: 'POST',
                                                    headers:{
                                                                'token' : token
                                                            },
                                                    body: formdata,
                                                  }).then((response) => response.json())
                                                     .then((responseJson) => {

                                                        //Alert.alert(JSON.stringify(responseJson));
                                                        console.log(responseJson,"responseJson");
                                                         
                                                     });

                                   }catch (error) {

                                  }*/

                           
                            }

              }else{

                     console.log(pantry_id,"pantry_id"); 

                      /*code to Line*/

                      tabledata.unshift(
                          [JSON.stringify(item)]
                      );


                      arr     = tabledata;

                      /*code to Column*/

                      tableDatainColumn.unshift(
                          [JSON.stringify(item), quantity]
                      );

                      arrCol  = tableDatainColumn;

                      /*code to Price*/

                      tableDatainPrice.unshift(
                          [JSON.stringify(item), quantity, 0.00]
                      );

                      arrPrice  = tableDatainPrice;

                      /*code to checking*/

                      tableDataReplica.unshift([JSON.stringify(item), pantry_id]); 
                      arrReplica  = tableDataReplica;

                      /*code to price in Line*/

                      tableDatainPriceLine.unshift(
                          [JSON.stringify(item), 0.00]
                      );

                      arrPriceLine     = tableDatainPriceLine;


                      var GVar = GlobalVariables.getApiUrl();

                      

                      let formdata = new FormData();

                      var type = 'admin';

                      formdata.append('user_id', this.state.user_id);
                      formdata.append('type', type);
                      formdata.append('pantry_id', pantry_id);
                     
                      var token = this.state.token;

                      console.log(formdata,"formdata");
                      console.log(token,"token");


                      /*try {
                          var response = fetch(GVar.BaseUrl + GVar.AddShoppingList , {
                                          method: 'POST',
                                          headers:{
                                                      'token' : token
                                                  },
                                          body: formdata,
                                        }).then((response) => response.json())
                                           .then((responseJson) => {

                                              //Alert.alert(JSON.stringify(responseJson));
                                              console.log(responseJson,"responseJsonInsert");
                                               
                                           });

                         }catch (error) {

                        }
*/
              }


              console.log(arr,"arr");
              console.log(arrCol,"arrCol");
              console.log(arrReplica,"arrReplica");
              console.log(arrPrice,"arrPrice");
              console.log(arrPriceLine,"arrPriceLine");


              this.setState({
                    tableData : arr,
                    tableDatainColumn : arrCol,
                    tableDataReplica : arrReplica,
                    tableDatainPrice : arrPrice,
                    tableDatainPriceLine : arrPriceLine,
                  });

              

               AsyncStorage.multiSet([
                  ["tableData",                 JSON.stringify(this.state.tableData)],
                  ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                  ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                  ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                  ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
              ]);

        });

    
      
    
       
   }


  setLanguages(){

  

    AsyncStorage.getItem('PrimaryLanguage').then((P_LANG) => {
          
          

          AsyncStorage.getItem('WELCOME PAGE').then((data) => {

                var WECOME = JSON.parse(data);

                if( P_LANG == 'en'){

                  this.setState({WECOME : WECOME.en})
                }

                if( P_LANG == 'el'){

                    this.setState({WECOME : WECOME.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({WECOME : WECOME.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({WECOME : WECOME.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({WECOME : WECOME.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({WECOME : WECOME.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({WECOME : WECOME.it})
                }

                if( P_LANG == 'es'){

                    this.setState({WECOME : WECOME.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({WECOME : WECOME.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({WECOME : WECOME.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({WECOME : WECOME.fil})
                }

          });

          AsyncStorage.getItem('SHOPPING LIST').then((data) => {

                var SHOPPINGLIST = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({SHOPPINGLIST : SHOPPINGLIST.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SHOPPINGLIST : SHOPPINGLIST.fil})
                }

          });

          AsyncStorage.getItem('LETS GO SHOPPING').then((data) => {

                var GOSHOPPING = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({GOSHOPPING : GOSHOPPING.en})
                }

                if( P_LANG == 'el'){

                    this.setState({GOSHOPPING : GOSHOPPING.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({GOSHOPPING : GOSHOPPING.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({GOSHOPPING : GOSHOPPING.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({GOSHOPPING : GOSHOPPING.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({GOSHOPPING : GOSHOPPING.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({GOSHOPPING : GOSHOPPING.it})
                }

                if( P_LANG == 'es'){

                    this.setState({GOSHOPPING : GOSHOPPING.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({GOSHOPPING : GOSHOPPING.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({GOSHOPPING : GOSHOPPING.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({GOSHOPPING : GOSHOPPING.fil})
                }

          });

          AsyncStorage.getItem('PANTRY').then((data) => {

                var PANTRY = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({PANTRY : PANTRY.en})
                }

                if( P_LANG == 'el'){

                    this.setState({PANTRY : PANTRY.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({PANTRY : PANTRY.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({PANTRY : PANTRY.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({PANTRY : PANTRY.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({PANTRY : PANTRY.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({PANTRY : PANTRY.it})
                }

                if( P_LANG == 'es'){

                    this.setState({PANTRY : PANTRY.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({PANTRY : PANTRY.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({PANTRY : PANTRY.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({PANTRY : PANTRY.fil})
                }

          });

          AsyncStorage.getItem('HOW MUCH MONEY DO I HAVE').then((data) => {

                var MONEYIHAVE = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYIHAVE : MONEYIHAVE.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYIHAVE : MONEYIHAVE.fil})
                }

          });

          AsyncStorage.getItem('MONEY MANAGEMENT').then((data) => {

                var MONEYMANAGEMENT = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.fil})
                }

          });

          AsyncStorage.getItem('SETUP').then((data) => {

                var SETUP = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({SETUP : SETUP.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SETUP : SETUP.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SETUP : SETUP.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SETUP : SETUP.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SETUP : SETUP.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SETUP : SETUP.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SETUP : SETUP.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SETUP : SETUP.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SETUP : SETUP.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SETUP : SETUP.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SETUP : SETUP.fil})
                }

          });

          AsyncStorage.getItem('LOGOUT').then((data) => {

                var LOGOUT = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({LOGOUT : LOGOUT.en})
                }

                if( P_LANG == 'el'){

                    this.setState({LOGOUT : LOGOUT.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({LOGOUT : LOGOUT.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({LOGOUT : LOGOUT.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({LOGOUT : LOGOUT.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({LOGOUT : LOGOUT.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({LOGOUT : LOGOUT.it})
                }

                if( P_LANG == 'es'){

                    this.setState({LOGOUT : LOGOUT.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({LOGOUT : LOGOUT.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({LOGOUT : LOGOUT.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({LOGOUT : LOGOUT.fil})
                }

          });

          AsyncStorage.getItem('ITEM').then((data) => {

                var ITEM = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({ITEM : ITEM.en})
                }

                if( P_LANG == 'el'){

                    this.setState({ITEM : ITEM.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({ITEM : ITEM.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({ITEM : ITEM.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({ITEM : ITEM.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({ITEM : ITEM.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({ITEM : ITEM.it})
                }

                if( P_LANG == 'es'){

                    this.setState({ITEM : ITEM.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({ITEM : ITEM.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({ITEM : ITEM.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({ITEM : ITEM.fil})
                }

          });
        AsyncStorage.getItem('Are you sure to log out?').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforlogout : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforlogout : voicefeatureGS.fil})
                              }

                        });
                AsyncStorage.getItem('ok').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforok : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforok : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforok : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforok : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforok : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforok : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforok : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforok : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforok : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('cancel').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforcancel : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforcancel : voicefeatureGS.fil})
                              }

                        });

          AsyncStorage.getItem('QUANTITY / WEIGHT').then((data) => {

                var QUANTITY = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({QUANTITY : QUANTITY.en})
                }

                if( P_LANG == 'el'){

                    this.setState({QUANTITY : QUANTITY.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({QUANTITY : QUANTITY.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({QUANTITY : QUANTITY.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({QUANTITY : QUANTITY.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({QUANTITY : QUANTITY.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({QUANTITY : QUANTITY.it})
                }

                if( P_LANG == 'es'){

                    this.setState({QUANTITY : QUANTITY.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({QUANTITY : QUANTITY.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({QUANTITY : QUANTITY.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({QUANTITY : QUANTITY.fil})
                }

          });

          AsyncStorage.getItem('Suggestion message').then((data) => {

                var Suggestion_message = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({Suggestion_message : Suggestion_message.en})
                }

                if( P_LANG == 'el'){

                    this.setState({Suggestion_message : Suggestion_message.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({Suggestion_message : Suggestion_message.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({Suggestion_message : Suggestion_message.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({Suggestion_message : Suggestion_message.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({Suggestion_message : Suggestion_message.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({Suggestion_message : Suggestion_message.it})
                }

                if( P_LANG == 'es'){

                    this.setState({Suggestion_message : Suggestion_message.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({Suggestion_message : Suggestion_message.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({Suggestion_message : Suggestion_message.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({Suggestion_message : Suggestion_message.fil})
                }

          });

          AsyncStorage.getItem('The option for adding quantity of added pantry for shopping should be on the Lets Go Shopping page.').then((data) => {

                var Addquantityforpantry = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({Addquantityforpantry : Addquantityforpantry.en})
                }

                if( P_LANG == 'el'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.it})
                }

                if( P_LANG == 'es'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({Addquantityforpantry : Addquantityforpantry.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({Addquantityforpantry : Addquantityforpantry.fil})
                }

          });

          AsyncStorage.getItem('This pantry is weighted please weighted while Go-Shopping.').then((data) => {

                var pantryisweightedpleasegoshopping = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.en})
                }

                if( P_LANG == 'el'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.it})
                }

                if( P_LANG == 'es'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({pantryisweightedpleasegoshopping : pantryisweightedpleasegoshopping.fil})
                }

          });
    });
  }


  GetItem (flower_name) {
  
        Alert.alert("Pantry Name", flower_name);
        

  }


  onPressMoneyManagement(){
      Actions.MoneyManagement();
  }

  onPressHome(){
      Actions.Home();
  }

  onPressPantry(){
    Actions.Pantry();
  }

  onPressMyMoney(){
     Actions.MyMoney();
  }

  onPressGoShopping(){
    Actions.GoShopping();
  }

  onPressSetUp(){
      Actions.SetUp();
  }

  setCamera1(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        Alert.alert("Please check Permition")
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
      

      let source = { uri: response.uri };

      
      var token = this.state.token;
      var user_id = this.state.user_id;
      var Selfietype = this.state.Selfietype;


      
let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie1', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
        
        this.setState({ Localprofileimage : source});

        AsyncStorage.multiSet([
                                    ["Localprofileimage",          JSON.stringify(source)],
                                   
                                ]);

        //Alert.alert(Selfietype)
        if( Selfietype === "same"){

          this.setState({ Localprofileimage : source, Localprofileimage2: source });
          AsyncStorage.multiSet([
                                    ["Localprofileimage",          JSON.stringify(source)],
                                    ["Localprofileimage2",         JSON.stringify(source)],
                                ]);

        }

      }
    });
  }


  setCamera(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        

      //alert(source)

      
      var token = this.state.token;
      var user_id = this.state.user_id;
      var Selfietype = this.state.Selfietype;


      
let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie2', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
        
        this.setState({ Localprofileimage2 : source});

        AsyncStorage.multiSet([
                                    ["Localprofileimage2",          JSON.stringify(source)],
                                   
                                ]);

        //Alert.alert(Selfietype)
        if( Selfietype === "same"){

          this.setState({ Localprofileimage : source, Localprofileimage2: source });
          AsyncStorage.multiSet([
                                    ["Localprofileimage",          JSON.stringify(source)],
                                    ["Localprofileimage2",         JSON.stringify(source)],
                                ]);

        }
      }
    });
  }


GetPantryImage(){

    
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        var arr   = [];

        
      var token = this.state.token;
      var user_id = this.state.user_id;
      var PrimaryLanguage = this.state.PrimaryLanguage;


      
      let formdata = new FormData();

      formdata.append('user_id', user_id );
      formdata.append('language_code', PrimaryLanguage );
      formdata.append('pantry_path', "" );
  
      formdata.append('pantry_path', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();

      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.AddPantryImage,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                            this.setState({pantryaddedid: responseJson.id});
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

      }catch (error) {
         // Alert.alert('something went wrong please try after some time');

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }

        this.setState({
          //userdatasource    : arr,
          showmodel         : true,
          modalVisible      : true,
          latestpantry      : source,
        });


        // AsyncStorage.setItem('userdatasource', JSON.stringify(arr) );

       
      }
    });
  }


  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }


  closePantryModel(visible) {
            

            var GVar = GlobalVariables.getApiUrl();

            var user_id = this.state.user_id;
            var token = this.state.token;


            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('pantry_id', this.state.pantryaddedid );
            //formdata.append('pantry_id', '1' );
 
            try {
                  
                let response = fetch( GVar.BaseUrl + GVar.DeletePantry,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token,
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                             if( responseJson.status == 1 ){

                                                // Alert.alert(JSON.stringify(responseJson));
                                                             
                                             }
                                                
                                           });
                
               }catch (error) {

                if( error.line == 18228 ){
                  Alert.alert("Net is not connected when it will be conneted it will auto reconoige to secondary language");
                }

               
              }

              this.setState({modalVisible: visible});
  }

  onChangePantryName = ( value ) => {

    this.setState({changedPrimaryPantryadded : value })


    AsyncStorage.multiGet(["name", "user_id","LanguageType","TOKEN","PrimaryLanguage","SeconderyLanguage",]).then(response => {
    
            var name                          = response[0][1];
            var user_id                       = response[1][1];
            var LanguageType                  = response[2][1];
            var token                         = response[3][1];
            var PrimaryLanguage               = response[4][1];
            var SeconderyLanguage             = response[5][1];

            var GVar = GlobalVariables.getApiUrl();


            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('primary', PrimaryLanguage);
            formdata.append('secondary', SeconderyLanguage);
            formdata.append('text', value );
 
            try {
                  
                let response = fetch( GVar.BaseUrl + GVar.TextChange,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                             if( responseJson.status == 1 ){

                                                 // Alert.alert(JSON.stringify(responseJson));
                                                 this.setState({ Pantryuseradded: responseJson.text, });

                                                  AsyncStorage.multiSet([
                                                                            ["Pantryuseradded",             JSON.stringify(responseJson.text)],
                                                                            ["PrimaryPantryadded",          JSON.stringify(value)],
                                                                        ]);

                                             }
                                                
                                           });
                
               }catch (error) {

                if( error.line == 18228 ){
                  // if( this.state.ShoppingListAudioType == 1 ){
                  //     Tts.speak('Net is not connected when it will be conneted it will auto reconoige to secondary language');
                  // }
                  Alert.alert("Net is not connected when it will be conneted it will auto reconoige to secondary language");
                }

               
              }
    }); 
  } 


 onPressSavePantry(){

     AsyncStorage.multiGet(["Pantryuseradded", "PrimaryPantryadded","TOKEN","user_id", "PrimaryLanguage","dataSource"]).then(response => {

            var Pantryuseradded               = response[0][1];
            //var PrimaryPantryadded            = response[1][1];
            var token                         = response[2][1];
            var user_id                       = response[3][1];
            var PrimaryLanguage               = response[4][1];
            var dataSource                    = this.state.dataSource;
            var source                        = this.state.latestpantry;
            var userdatasource                = this.state.userdatasource;
            var isweighted                    = this.state.pantryweighttype;


            var PrimaryPantryadded             = this.state.changedPrimaryPantryadded;

          

            if( !PrimaryPantryadded ){
              Alert.alert("Message", "Please enter pantry name also");
            }else{


                var userdatasource = this.state.userdatasource;

                if( userdatasource == null ){
                  userdatasource = [];
                }

                var pan = PrimaryPantryadded;
                var capspantry = pan.charAt(0).toUpperCase() + pan.substr(1);

                var index = this.getIndex(capspantry, userdatasource, 'pantry_name');
                var index1 = this.getIndex(capspantry, dataSource, 'en_pantry');

                console.log(index1,"index1");
            
                if( index < 0 && index1 < 0 ){


                var arr = [];

                if( JSON.stringify(userdatasource) != 'null'){

                  arr = userdatasource;
                  id  = userdatasource.length + 1; 
                }

                var pan = PrimaryPantryadded;
                var capspantry = pan.charAt(0).toUpperCase() + pan.substr(1);

                arr.push({
                    useraddedpantry_id    : this.state.pantryaddedid, 
                    pantry_image          : source, 
                    pantry_name           : capspantry,
                    pantry_secondryname   : JSON.parse(Pantryuseradded),
                    pantry_isweighted     : this.state.pantryweighttype,
                });

                //alert(JSON.stringify(arr));

                AsyncStorage.setItem('userdatasource', JSON.stringify(arr) );

                
               

                this.setState({
                  userdatasource    : arr,
                });


                var token = this.state.token;
                var user_id = this.state.user_id;
                var PrimaryLanguage = this.state.PrimaryLanguage;


                
                let formdata = new FormData();

                formdata.append('user_id', user_id );
                formdata.append('language_code', PrimaryLanguage );
                formdata.append('id', this.state.pantryaddedid );
                formdata.append('pantry_name', capspantry );
                

                var GVar = GlobalVariables.getApiUrl();

                try {
                      

                    try {
                          let response = fetch( GVar.BaseUrl + GVar.UpdatePantry,{
                                            method: 'POST',
                                            headers:{
                                                        'Accept': 'application/json',
                                                        'token' : token,
                                                        'Content-Type' : 'multipart/form-data',
                                                    },
                                            body: formdata,        
                                            }).then((response) => response.json())
                                                     .then((responseJson) => {

                                                      //Alert.alert(JSON.stringify(responseJson));
                                                          
                                                     });
                        
                          
                       }catch (error) {

                        if( error.line == 18228 ){
                          


                        }

                      }

                }catch (error) {

                    if( error.line == 18228 ){
                      //Alert.alert("Please connect to network");
                    }

                         
                }

                  Alert.alert("Message","Ok" );
                  this.setState({modalVisible: false});

                }else{
                  Alert.alert("Message", "Pantry allready added please add another");
                }
   
            }

     });

  } 

  onPressLogout(){

    //Alert.alert("fsd");
    AsyncStorage.clear();
    AsyncStorage.removeItem('TOKEN');
    Actions.Login();

  }

  _alertIndex(index) {
    Alert.alert(`This is row ${index + 1}`);
  }

  downloadPdf(){

      var token = this.state.token;
      var user_id = this.state.user_id;
     
      let formdata = new FormData();

      formdata.append('user_id', user_id );
     
      var GVar = GlobalVariables.getApiUrl();

      console.log("downloadPdf1")

      try {
              let response = fetch( GVar.BaseUrl + GVar.DownloadShoppingList,{
                                  method: 'POST',
                                  body: formdata,  
                                  headers:{
                                      'token' :token,
                                  },      
                                  }).then((response) => response.json())
                                 .then((responseJson) => {
                                 
                                      Alert.alert(JSON.stringify(responseJson))
                                 });


      }catch (error) {

              if( error.line == 18228 ){
                
                Alert.alert("Message","Please to internet");
                
              }

      }


  }

 

  deletePantry( item ) {

    var tabledata             = this.state.tableData;
    var tableDatainColumn     = this.state.tableDatainColumn;
    var tableDataReplica      = this.state.tableDataReplica;
    var tableDatainPrice      = this.state.tableDatainPrice;
    var tableDatainPriceLine  = this.state.tableDatainPriceLine;

    var result    = new Array();

    console.log(this.state.tableData,"tableData");


    var tableDataReplica  = this.state.tableDataReplica;
    var pantryinbox       = this.state.pantryinbox;

    if( pantryinbox === null ){
      pantryinbox  = [];
    }

    var istic = this.getIndexOfK(pantryinbox, item);

    if( Array.isArray(istic) && istic.length > 0 ){
               
         Alert.alert(this.state.Suggestion_message ? this.state.Suggestion_message :"Suggestion message", this.state.Addquantityforpantry ? this.state.Addquantityforpantry : "The option for adding quantity of added pantry for shopping should be on the ‘Lets Go Shopping’ page.")   

    }else{

   
           result        = this.getIndexOfK(this.state.tableDataReplica, item);


          console.log(result,"result");

      if( result === undefined){
          Alert.alert("Sorry something went wrong")
      }else{
          cell1  = result[0];
          cell2  = result[1];

          var newtableDatainColumnquantity    = parseInt(tableDatainColumn[cell1][1] - 1);

          console.log(newtableDatainColumnquantity,"newtableDatainColumnquantity1");


          if( newtableDatainColumnquantity < 0 || newtableDatainColumnquantity == 0 ){

              console.log(tabledata,"tabledata");
              console.log(tableDatainColumn,"tableDatainColumn");
              console.log(tableDataReplica,"tableDataReplica");
              console.log(tableDatainPrice,"tableDatainPrice");
              console.log(tableDatainPriceLine,"tableDatainPriceLine");

              tabledata.splice([cell1],1);
              tableDatainColumn.splice([cell1],1);
              tableDataReplica.splice([cell1],1);
              tableDatainPrice.splice([cell1],1);
              tableDatainPriceLine.splice([cell1],1);


              console.log(tabledata,"tabledata");
              console.log(tableDatainColumn,"tableDatainColumn");
              console.log(tableDataReplica,"tableDataReplica");
              console.log(tableDatainPrice,"tableDatainPrice");
              console.log(tableDatainPriceLine,"tableDatainPriceLine");

              this.setState({tabledata : tabledata, tableDatainColumn : tableDatainColumn, tableDataReplica : tableDataReplica, tableDatainPrice : tableDatainPrice, tableDatainPriceLine : tableDatainPriceLine });

      
              AsyncStorage.multiSet([
                        ["tableData",                 JSON.stringify(this.state.tableData)],
                        ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                        ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                        ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                        ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                    ]);

            
          }else{

            var dataSource    = this.state.dataSource;

            var pantryindex   = this.getIndex(item, dataSource, 'pantry_image');

            console.log(pantryindex,"pantryindex");

            var cost = 0.00;

            if( pantryindex > 0 ){

               cost          = JSON.parse(dataSource[pantryindex].pantry_cost);
            }

            

          

        /*code for tableDatainColumn column*/       

              tableDatainColumn[cell1][1] = newtableDatainColumnquantity

              console.log( tableDatainColumn,"newtableDatainColumnquantity");

              this.setState({tableDatainColumn : tableDatainColumn});

        /*end code for column*/


        /* code for data price column*/

              console.log(tableDatainPrice[cell1][0],"tableDatainPrice1");
              console.log(tableDatainPrice[cell1][1],"tableDatainPrice2");  // do minus quantity

              var newtableDatainPrice    = tableDatainPrice[cell1][1] - 1;

              
              //const cost        = JSON.parse(dataSource[pantryindex].pantry_cost) * newtableDatainPrice;

              tableDatainPrice[cell1][1] = newtableDatainPrice;
              tableDatainPrice[cell1][2] = cost;

              console.log( tableDatainPrice,"newtableDatainPrice");

              this.setState({tableDatainPrice : tableDatainPrice});
        /* end data price column*/

        /*code for line*/


             var tabledataArr             = tabledata[cell1][cell2].split("#");
             var tableDatainPriceLineArr  = tableDatainPriceLine[cell1][cell2].split("#");

             tabledataArr.splice(0, 1);
             tableDatainPriceLineArr.splice(0, 1);

             var t1 = tabledataArr.join('#');
             var t2 = tableDatainPriceLineArr.join('#');

             tabledata[cell1][0] = t1;
             tableDatainPriceLine[cell1][0] = t2;
            // tableDatainPriceLine[cell1][1] = cost;

              

             this.setState({tabledata : tabledata, tableDatainPriceLine : tableDatainPriceLine});

        /*end code for line*/
            
             
              AsyncStorage.multiSet([
                        ["tableData",                 JSON.stringify(this.state.tableData)],
                        ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                        ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                        ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                        ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                    ]);


          }

     }

   

    
    }

    this.setState({ cross  : false })

  }


  _onChangeQuantity(data, index, indexofR){

    this.setState({instancedata : data})

    var pantrybox = this.state.pantryinbox;

    console.log(pantrybox,"pantrybox");
    console.log(indexofR,"elementofr");
    console.log(data,"data");
    console.log(index,"index");



    if( data != '' ){

     // console.log("DeletePantryDeletePantryDeletePantry")

           if( data == 0 ){



              var tabledata             = this.state.tableData;
              var tableDatainColumn     = this.state.tableDatainColumn;
              var tableDataReplica      = this.state.tableDataReplica;
              var tableDatainPrice      = this.state.tableDatainPrice;
              var tableDatainPriceLine  = this.state.tableDatainPriceLine;

              tabledata.splice([indexofR],1);
              tableDatainColumn.splice([indexofR],1);
              tableDataReplica.splice([indexofR],1);
              tableDatainPrice.splice([indexofR],1);
              tableDatainPriceLine.splice([indexofR],1);


              this.setState({tabledata : tabledata, tableDatainColumn : tableDatainColumn, tableDataReplica : tableDataReplica, tableDatainPrice : tableDatainPrice, tableDatainPriceLine : tableDatainPriceLine });

      
              AsyncStorage.multiSet([
                        ["tableData",                 JSON.stringify(this.state.tableData)],
                        ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                        ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                        ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                        ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                    ]);
        }

        if( data > 0 ){

          

          var data                  = JSON.parse(data);


          var tabledata             = this.state.tableData;
          var tableDatainColumn     = this.state.tableDatainColumn;
          var tableDataReplica      = this.state.tableDataReplica;
          var tableDatainPrice      = this.state.tableDatainPrice;
          var tableDatainPriceLine  = this.state.tableDatainPriceLine;

          if( pantrybox === null ){
            pantrybox  = [];
          }

          var istic = this.getIndexOfK(pantrybox, tableDataReplica[indexofR][0]);


          console.log(tabledata[indexofR],"tabledata");
          console.log(tableDatainColumn[indexofR],"tableDatainColumn");
          console.log(tableDataReplica[indexofR],"tableDataReplica");
          console.log(tableDatainPrice[indexofR],"tableDatainPrice");
          console.log(tableDatainPriceLine[indexofR],"tableDatainPriceLine");

          if( Array.isArray(istic) && istic.length > 0 ){
               
               Alert.alert( this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message", this.state.Addquantityforpantry ? this.state.Addquantityforpantry :"The option for adding quantity of added pantry for shopping should be on the ‘Lets Go Shopping’ page.")   

          }else{

                  var dataSource    = this.state.dataSource;

                  var pantryindex   = this.getIndex(tableDataReplica[indexofR][0], dataSource, 'pantry_image');

                  console.log(pantryindex,"pantryindex");
                  console.log(dataSource[pantryindex],"dataSource");



                  //console.log(tableDatainColumn[indexofR][1],"quantity");
                  
                  var pantry_cost = 'pantry_cost';
                  //const price = dataSource[pantryindex].pantry_cost * data;
                  var price = 0.00;

                  if( pantryindex > -1 ){
                    price = dataSource[pantryindex].pantry_cost;
                  }
                  
                  console.log(price,"price");


                  var arr = [];

                  for (var i = 0; i < data; i++) {

                    arr.push(tableDataReplica[indexofR]);
                  }

                  var t1 = arr.join('#');

                  tableDatainColumn[indexofR][1]    = data;
                  tableDatainPrice[indexofR][1]     = data;
                  
                  tableDatainPriceLine[indexofR][0] = t1;
                  
                  tabledata[indexofR][0]            = t1;

                  if( tableDatainPriceLine[indexofR][1] > 0 ){


                      //tableDatainPrice[indexofR][2]     = price;
                      //tableDatainPriceLine[indexofR][1] = price;

                      console.log(tableDatainPrice[indexofR],"tableDatainPrice[indexofR]");
                      console.log(tableDatainPriceLine[indexofR],"tableDatainPriceLine[indexofR]");
                  }

                  

                  this.setState({tabledata : tabledata, tableDatainPriceLine : tableDatainPriceLine, tableDatainPrice : tableDatainPrice, tableDatainColumn : tableDatainColumn});

                 
                  AsyncStorage.multiSet([
                            ["tableData",                 JSON.stringify(this.state.tableData)],
                            ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                            ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                            ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                        ]);

                    

                }
          }
    }
  
 
  }



  handleViewRef = ref =>{ this.view = ref; 

  }

  //bounce = () => this.view.bounce(800).then(endState => console.log(endState.finished ? this.setState({ cross    : true }) : 'bounce cancelled'));
  

  bounce = () =>{ 

    //this.view.stopAnimation();
    //this.view.bounce(800).then(endState => console.log(endState.finished ? this.setState({ cross    : true }) : 'bounce cancelled'))
    //this.view.rubberBand(800).then(endState => {endState.finished });
    //this.view = this.handleViewRef;  
    this.setState({ cross    : true })

  }

  async createPDF() {

        AsyncStorage.getItem('tableDatainPrice').then((data) => { 


          var tableDatainPrice = [];

          if( data ){
            tableDatainPrice = JSON.parse(data);
          }else{
            tableDatainPrice = [];
          }

          var GVar = GlobalVariables.getApiUrl();

          var imagepath = GVar.ImagePath

          //console.log(GVar.ImagePath,"ImagePath");
        
          var html = '<strong>Shopping List Items</strong></p><table><tbody><tr><td>No</td><td>Image</td><td>Quantity</td><td>Price</td></tr>';                     

          

          for (var i = 0; i < tableDatainPrice.length; i++) {

              if( tableDatainPrice[i][0].includes("thumb")){
                   var image = tableDatainPrice[i][0];

                   var slr = i +1; 

                   console.log(image,"image")

                   html = html + '<tr><td>'+slr+'</td><td>'+'<img src="'+imagepath+image+'" alt="Smiley face" height="42" width="42">'+'</td><td>'+tableDatainPrice[i][1]+'</td><td>'+tableDatainPrice[i][2]+'</td></tr>';
              
              }else{

                var slr = i +1; 

                var image = JSON.parse(tableDatainPrice[i][0]);

                console.log(image.uri,"image1");
                
                html = html + '<tr><td>'+slr+'</td><td>'+'<img src="'+image.uri+'" alt="Smiley face" height="42" width="42">'+'</td><td>'+tableDatainPrice[i][1]+'</td><td>'+tableDatainPrice[i][2]+'</td></tr>';
              
              }
          }

          console.log(html,"html");
          
          var options = {
             
                        html: html, // HTML String

                      // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************                              
                        fileName: 'shoppinglist',          /* Optional: Custom Filename excluded extension
                                                    Default: Randomly generated
                                                  */

                        directory: 'docs',         /* Optional: 'docs' will save the file in the `Documents`
                                                    Default: Temp directory */

                        base64: true ,               


                        height: 800,                
                        width: 1056,               /* Optional: 1056 sets the width of the DOCUMENT that will produced
                                                    Default: 792
                                                  */
                        padding: 24,                /* Optional: 24 is the # of pixels between the outer paper edge and
                                                            corresponding content edge.  Example: width of 1056 - 2*padding
                                                            => content width of 1008
                                                    Default: 10
                                                  */
                  };

                  RNHTMLtoPDF.convert(options).then((data) => {
                  console.log(data.filePath,'filePath');
                  console.log(data.base64,'base64');
                 
                  /*Alert.alert(
                      'options filename' + options.fileName,
                      'data filePath=' + data.filePath
                  );*/

                  Alert.alert('Successfully downloaded at ',data.filePath );
                 
              });


        });
  }


  onLikePost({ item, index }){

    //Alert.alert(JSON.stringify(item));
   // Alert.alert(index);

    let { dataSource } = this.state;
    let targetPost = dataSource[index];

    targetPost.status = !targetPost.status;

    console.log(targetPost,"targetPost")

   


    //targetPost.status = !targetPost.status;

   
    //this.setState({ dataSource : targetPost});

  } 


  _clickQuantity(data, index, elementofr){

    console.log(data,"data");
    console.log(index,"index");
    console.log(elementofr,"elementofr");

   // Alert.alert("Pantry Price Each",data);
    if( data === ' '){
      data = 0;

    }
/*
     Alert.alert(
        'Pantry Quantity is '+data+'. Do you want to edit Quantity?',
        '',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => {this.editableQuantity(data, index, elementofr)}},
        ],
        { cancelable: false }
      )*/

      this.editableQuantity(data, index, elementofr)
  }

  editableQuantity(data, index, elementofr){

    var tableDataReplica  = this.state.tableDataReplica;
    var pantryinbox       = this.state.pantryinbox;

    if( pantryinbox === null ){
      pantryinbox  = [];
    }

    var istic = this.getIndexOfK(pantryinbox, tableDataReplica[elementofr]);


    if( Array.isArray(istic) && istic.length > 0 ){
               
        Alert.alert( this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message", this.state.Addquantityforpantry ? this.state.Addquantityforpantry : "The option for adding quantity of added pantry for shopping should be on the ‘Lets Go Shopping’ page.")   

    }else{

        this.setState({editquantity         : true});
        this.setState({dataeditQuantity     : data});
        this.setState({indexcellQuantity    : index});
        this.setState({elementIndexQuantity : elementofr});
    }
  }



    _oneditQuantity(quantity){

        try {

       if (/^\d+$/.test(JSON.parse(quantity.text)))
        {
   
      



      //var data          = this.state.dataeditQuantity;
      var data          = JSON.parse(quantity.text);
      var index         = this.state.indexcellQuantity;
      var indexofR      = this.state.elementIndexQuantity;
      
 
      var pantrybox = this.state.pantryinbox;

      console.log(pantrybox,"pantrybox");
      console.log(indexofR,"elementofr");
      console.log(data,"data");
      console.log(index,"index");

      if( data != ''){

            console.log(data,"datadata123456");

           if( data == 0 ){

            console.log(data,"datadatadata");

              var tabledata             = this.state.tableData;
              var tableDatainColumn     = this.state.tableDatainColumn;
              var tableDataReplica      = this.state.tableDataReplica;
              var tableDatainPrice      = this.state.tableDatainPrice;
              var tableDatainPriceLine  = this.state.tableDatainPriceLine;

              tabledata.splice([indexofR],1);
              tableDatainColumn.splice([indexofR],1);
              tableDataReplica.splice([indexofR],1);
              tableDatainPrice.splice([indexofR],1);
              tableDatainPriceLine.splice([indexofR],1);


              this.setState({tabledata : tabledata, tableDatainColumn : tableDatainColumn, tableDataReplica : tableDataReplica, tableDatainPrice : tableDatainPrice, tableDatainPriceLine : tableDatainPriceLine });

      
              AsyncStorage.multiSet([
                        ["tableData",                 JSON.stringify(this.state.tableData)],
                        ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                        ["tableDataReplica",          JSON.stringify(this.state.tableDataReplica)],
                        ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                        ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                    ]);
        }

        if( data > 0 ){

          

          var data                  = JSON.parse(data);


          var tabledata             = this.state.tableData;
          var tableDatainColumn     = this.state.tableDatainColumn;
          var tableDataReplica      = this.state.tableDataReplica;
          var tableDatainPrice      = this.state.tableDatainPrice;
          var tableDatainPriceLine  = this.state.tableDatainPriceLine;

          if( pantrybox === null ){
            pantrybox  = [];
          }

          var istic = this.getIndexOfK(pantrybox, tableDataReplica[indexofR][0]);


          console.log(tabledata[indexofR],"tabledata");
          console.log(tableDatainColumn[indexofR],"tableDatainColumn");
          console.log(tableDataReplica[indexofR],"tableDataReplica");
          console.log(tableDatainPrice[indexofR],"tableDatainPrice");
          console.log(tableDatainPriceLine[indexofR],"tableDatainPriceLine");

          if( Array.isArray(istic) && istic.length > 0 ){
               
               Alert.alert( this.state.Suggestion_message ? this.state.Suggestion_message : "Suggestion message", this.state.Addquantityforpantry ? this.state.Addquantityforpantry : "The option for adding quantity of added pantry for shopping should be on the ‘Lets Go Shopping’ page.")   

          }else{

                  var dataSource    = this.state.dataSource;

                  if( dataSource == null ){
                    dataSource = [];
                  }

                  var pantryindex   = this.getIndex(tableDataReplica[indexofR][0], dataSource, 'pantry_image');

                  console.log(pantryindex,"pantryindex");
                  console.log(dataSource[pantryindex],"dataSource");



                  console.log(tableDatainColumn[indexofR][1],"quantity");
                  
                  var pantry_cost = 'pantry_cost';
                  //const price = dataSource[pantryindex].pantry_cost * data;
                 
                  var price = 0.00;

                  if( pantryindex > -1 ){
                    price = dataSource[pantryindex].pantry_cost;
                  }


                  var arr = [];

                  for (var i = 0; i < data; i++) {

                    arr.push(tableDataReplica[indexofR]);
                  }

                  var t1 = arr.join('#');

                  tableDatainColumn[indexofR][1]    = data;
                  tableDatainPrice[indexofR][1]     = data;
                  
                  tableDatainPriceLine[indexofR][0] = t1;
                  
                  tabledata[indexofR][0]            = t1;

                  if( tableDatainPriceLine[indexofR][1] > 0 ){


                      //tableDatainPrice[indexofR][2]     = price;
                      //tableDatainPriceLine[indexofR][1] = price;

                      console.log(tableDatainPrice[indexofR],"tableDatainPrice[indexofR]");
                      console.log(tableDatainPriceLine[indexofR],"tableDatainPriceLine[indexofR]");
                  }

                  

                  this.setState({tabledata : tabledata, tableDatainPriceLine : tableDatainPriceLine, tableDatainPrice : tableDatainPrice, tableDatainColumn : tableDatainColumn});

                 
                  AsyncStorage.multiSet([
                            ["tableData",                 JSON.stringify(this.state.tableData)],
                            ["tableDatainColumn",         JSON.stringify(this.state.tableDatainColumn)],
                            ["tableDatainPrice",          JSON.stringify(this.state.tableDatainPrice)],
                            ["tableDatainPriceLine",      JSON.stringify(this.state.tableDatainPriceLine)],
                        ]);

                    

                }
          }
    }
  }
  else{
    Alert.alert('Please enter a correct value');
  }

 } catch(e) {
        Alert.alert("please enter a valid value"); // error in the above string (in this case, yes)!
 }
     

    
 }



  render() {
    var state = this.state;
    var showmodel     = this.state.showmodel; 


    const element = (data, index, elementofr ) => {

        

        if( index == 0 ){

            var  views = [];
            var myArr = data.split("#");
            
            
            if( data.includes("thumb") ){

                var image = myArr[0];

                return <Image source={{uri: this.state.ImagePath +image}} style={{width:wp('8%'),height:hp('10%')}}/>;
     
            }else{
                var image = myArr[0];
                
                return <Image source={JSON.parse(image)} style={{width:wp('8%'),height:hp('10%')}}/>;
                
            }

          

        }

        if( index == 1 ){

          

         var views = [];

          
          return <TouchableOpacity  onPress={() => this._clickQuantity(data, index, elementofr)}>
                     <View style={{justifyContent:'center', borderWidth:1, height: hp('10%') }}>
                     <Text style={{color:'#000000', fontSize: RF(3),alignSelf:'center'}}>{data}</Text>
                     </View>
                 </TouchableOpacity>        


        }

        

    }


    const element1 = (data, index ) => {

        var views = [];
        var myArr = data.split("#");

        console.log(data+'mydatadata');
        console.log(myArr+'mydata');
        console.log(myArr.length+'mydatalength');

        //Alert.alert(JSON.stringify(myArr.length));

        if( data.includes("thumb") ){

            var image = myArr[0];
            for ( var i =0; i< myArr.length; i++){
              views.push(
                  <View key={i} style={{flexWrap: 'wrap', marginLeft:2}}>

                      <Animatable.View ref={this.handleViewRef}>

                      <TouchableOpacity style={{flexWrap: 'wrap'}} onLongPress={this.bounce.bind()} >
                      <ImageBackground style={{width: wp('5%'), height: hp('10%'),}} source={{uri: this.state.ImagePath +image}}>
                          { renderIf(this.state.cross)(
                             <Text style={{marginTop:-2, color: 'red', fontSize: RF(3.5) }} onPress={() => {Alert.alert( 'Delete',
                                                '',
                                                [
                                                  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: 'OK', onPress: () => {this.deletePantry(image)}},
                                                ],
                                                { cancelable: false }
                                              )
                                              }}>X</Text> 
                            ) }
                      </ImageBackground>
                      </TouchableOpacity>

                    </Animatable.View>
                    
                  </View>
              )
          }

          return <View style={{ flexDirection: 'row', flexWrap: 'wrap',}}>
                      {views}
                  </View>;


        }else{

          //var image = myArr[0];
          var image1= myArr[0].split(',');   
          var image = image1[0];
          for ( var i =0; i< myArr.length; i++){
              views.push(
                  <View key={i} style={{flexWrap: 'wrap', marginLeft:2}}>
                      
                    <Animatable.View ref={this.handleViewRef}>
                      <TouchableOpacity style={{flexWrap: 'wrap'}} onLongPress={this.bounce.bind()} >
                      <ImageBackground style={{width: wp('5%'), height: hp('10%')}}  source={JSON.parse(image)}>
                          { renderIf(this.state.cross)(
                             <Text style={{marginTop:-2, color: 'red',fontSize: RF(3.5) }} onPress={() => {Alert.alert( 'Delete',
                                                '',
                                                [
                                                  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: 'OK', onPress: () => {this.deletePantry(image)}},
                                                ],
                                                { cancelable: false }
                                              )
                                              }}>X</Text> 
                            ) }
                      </ImageBackground>
                      </TouchableOpacity>
                    </Animatable.View>
                    
                  </View>
              )
          }

          return <View style={{ flexDirection: 'row', flexWrap: 'wrap',}}>
                      {views}
                  </View>;
        }

        

       }



    return (
          <View style={styles.shoppingList_container}>
           <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>
          <ImageBackground source={require('../assets/images/shoppingimages/shopping-bg.jpg')}
                    style={[
                   styles.base,{
                       width: "100%"
                   }, {
                       height: "100%"
                   }
               ]}>
            
              <View style={{flex: 1,justifyContent: 'space-between'}}>

                <View style={{flex: 1,flexDirection: 'row', height: hp('30%'), justifyContent: 'space-between',marginLeft:hp('5%')}}>
                    
                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%', height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera1.bind(this)}>
                              <View style={{}}>
                              { 
                                this.state.Localprofileimage != null
                               ? <Image source={this.state.Localprofileimage} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>

                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>


                    <View style={{width: wp('62%'),marginTop:hp('4%'),}} >
                          <View style={{flex: 1, alignSelf:'center', }}>
                          <Text style = {{fontFamily:'Comic Sans MS', color:'#FFFFFF',fontSize:hp('5%'),fontWeight: 'bold',alignSelf:'center'}}>{this.state.name}'S</Text>
                          <Text style = {styles.hometext}>{this.state.PrimaryLanguage ?  this.state.SHOPPINGLIST : 'SHOPPING LIST'}</Text>
                          </View>
                    </View>

                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%'), marginRight:hp('5%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%',height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera.bind(this)}>
                              <View style={{}}>
                               { 
                                this.state.Localprofileimage2 != null
                               ? <Image source={this.state.Localprofileimage2} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage2!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage2}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>


                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>
                  </View>


                <View style={{height:'45%', flexDirection: 'row',alignSelf:'center', justifyContent:'center', width:'98%',marginTop:hp('14%'),}} >
                      <View style={{flex:2, justifyContent:'center'}} >
                        <View style={{  flex: 1,width: "90%", backgroundColor:'#FFFFFF',alignSelf:'center', borderWidth:2, borderColor:'#CCCCCC'}} >
                          <ScrollView 
                          onTouchStart={(ev) => { 
                          this.setState({enabled:false }); }}
                          onMomentumScrollEnd={(e) => { this.setState({ enabled:true }); }}
                          onScrollEndDrag={(e) => { this.setState({ enabled:true }); }}
                          >
                               <View style={{flex:1, justifyContent:'center'}} >
                                
                                  <FlatList
                                      extraData={this.state}
                                      data={ this.state.dataSource }
                                      keyExtractor={(item, index) => item.status}
                                      renderItem={({item, index}) =>
                                          <View style={{flex:1}}>
                                          <TouchableOpacity onPress={this.GetGridViewItem.bind(this, item.id)}>
                                          <View style={{flexDirection:'row',marginBottom:10,backgroundColor: Array.isArray(this.getIndexOfK(this.state.tableDataReplica, item.pantry_image)) ? 'gray' : 'white', height:hp('10%')}}>
                                          
                                            <View style={{width: wp('10%'), height: hp('10%'),justifyContent: 'center' }}>
                                              <Image
                                                    style={{width: wp('6%'), height: hp('8%'), marginLeft:5, alignSelf:'center'}}
                                                    source={{uri: this.state.ImagePath +item.pantry_image}}
                                                  />
                                            </View>

                                            <View style={{width: wp('10%'), height: hp('10%'),justifyContent: 'center' }}>
                                                <Text style={{alignSelf:'center', fontSize: hp('3%')}}>{item.pantry} </Text>
                                            </View>

                                            <View style={{width: wp('5%'), height: hp('10%'),justifyContent: 'center' }}>
                                                <Image source={require('../assets/images/right_arrow.png')} style={{width: 20, height: 20,alignSelf:'center',}}/>
                                        
                                            </View>
                                          
                                          </View>

                                        </TouchableOpacity> 
                                        <View style={styles.Hline}/>
                                        </View>
                                      }
                                      numColumns={1}
                                    />
                                </View>
                               
                                <View style={{flex:1}} >
                                    <FlatList
                                      data={ this.state.userdatasource }
                                      renderItem={({item}) =>
                                    
                                      <TouchableOpacity onPress={this.GetGridViewItem1.bind(this, item.useraddedpantry_id)}>
                                        <View style={{flexDirection:'row', justifyContent:'center', marginBottom:10,backgroundColor: Array.isArray(this.getIndexOfK(this.state.tableDataReplica, JSON.stringify(item.pantry_image))) ? 'gray' : 'white' , height:hp('10%')}}>
                                        
                                        <View style={{width: wp('10%'), height: hp('10%'),justifyContent: 'center' }}>
                                              <Image style={{width: wp('6%'), height: hp('8%'),marginLeft: wp('-3%'), alignSelf:'center'}} source={item.pantry_image} />

                                        </View>
                                        
                                        <View style={{width: wp('10%'), height: hp('10%'),justifyContent: 'center' }}>
                                                <Text style={{alignSelf:'center', fontSize: hp('3%'),marginLeft: wp('-3%')}}>{item.pantry_name} </Text>
                                            </View>

                                            <View style={{width: wp('5%'), height: hp('10%'),justifyContent: 'center' }}>
                                                <Image source={require('../assets/images/right_arrow.png')} style={{width: 20, height: 20,alignSelf:'center',marginLeft: wp('-3%')}}/>
                                        
                                            </View>
                                        </View>
                                      </TouchableOpacity>  }
                                        
                                        numColumns={1}
                                    />
                                    <View style={styles.Hline}/>
                                 </View>   
                          </ScrollView>
                        </View>
                        
                        <View style={{width: '100%',}} >
                              <TouchableOpacity onPress={this.GetPantryImage.bind(this)}>
                                  <Image source={require('../assets/images/shoppingimages/camera-btn2.png')} style={{height:hp('10%'), width:wp('30%'),alignSelf:'center' ,marginBottom:10, marginTop:10}}  />
                              </TouchableOpacity>
                        </View>

                      </View>

                      <View style={{flex:4, backgroundColor:'#FFFFFF', width:'95%', alignSelf:'center',borderWidth:2, borderColor:'#CCCCCC'}} >

                          { this.state.showcolumn ? <View style={{flexDirection:'row',justifyContent:'space-between', backgroundColor:'#e0e7ed'}}>
                            <View>
                            <Text style={{alignSelf:'center', fontSize:hp('4%'),color:'#000000', marginLeft:10}}>{this.state.PrimaryLanguage ? this.state.ITEM : "ITEM"}</Text>
                            </View>

                            <View>
                            <Text style={{alignSelf:'center', fontSize:hp('4%'), alignSelf:'flex-end',color:'#000000', marginRight:10}}>{ this.state.PrimaryLanguage ? this.state.QUANTITY : "QUANTITY / WEIGHT"}</Text>
                            </View>
                          
                              </View> :
                              <View style={{flexDirection:'row',justifyContent:'space-between', backgroundColor:'#e0e7ed'}}>
                                <View>
                                <Text style={{alignSelf:'center', fontSize:hp('4%'),color:'#000000', marginLeft:10}}>{this.state.PrimaryLanguage ? this.state.ITEM : "ITEM"}</Text>
                                </View>

                            
                              </View>
                            }

                      <ScrollView 
                          onTouchStart={(ev) => { 
                          this.setState({enabled:false }); }}
                          onMomentumScrollEnd={(e) => { this.setState({ enabled:true }); }}
                          onScrollEndDrag={(e) => { this.setState({ enabled:true }); }}
                          style={{}} >
                         { this.state.showcolumn ?
                                <Table borderStyle={{borderWidth: 2, borderColor: '#CCCCCC',backgroundColor:'#FFFFFF', marginBottom:10,}}>
                                  
                                  {
                                    state.tableDatainColumn.map((rowData, index) => (
                                      <TableWrapper key={index} style={styles.row}>
                                        {
                                          rowData.map((cellData, cellIndex) => (
                                            <Cell key={cellIndex} data={cellIndex === 0 || cellIndex === 1 || cellIndex === 2? element(cellData, cellIndex, index) : cellData} textStyle={{}} 
                                            onResponderRelease={(e) => this.resetPosition(e, rowData)}/>
                                          ))
                                        }
                                      </TableWrapper>
                                    ))
                                  }
                                </Table>
                                :
                                <Table borderStyle={{borderWidth: 1, borderColor: '#000000',marginBottom:10}}>
                              
                                {
                                  state.tableData.map((rowData, index) => (
                                    <TableWrapper key={index} style={styles.row}>
                                      {
                                        rowData.map((cellData, cellIndex) => (
                                          <Cell key={cellIndex} data={cellIndex === 0 || cellIndex === 1? element1(cellData,cellIndex, index) : cellData} textStyle={{}}/>
                                        ))
                                      }
                                    </TableWrapper>
                                  ))
                                }
                                </Table>
                               
                        }
                      </ScrollView>
                      </View>
                      
                </View>  
                

                <View style={{flex: 1,  flexDirection: 'row',marginTop:hp('2%'), marginBottom:hp('.5%')}} >
                            <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressHome.bind(this)}  underlayColor='#232A40'>
                            <ImageBackground source={require('../assets/images/shoppingimages/welcome.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center'}}>
                             <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.WECOME : 'WECOME PAGE'}</Text>
                            </ImageBackground>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressGoShopping.bind(this)}  underlayColor='#232A40'>
                            <ImageBackground source={require('../assets/images/shoppingimages/letsgo.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center',}}>
                                <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.GOSHOPPING : 'LETS GO SHOPPING'}</Text>
                            </ImageBackground>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressPantry.bind(this)}  underlayColor='#232A40'>
                            <ImageBackground source={require('../assets/images/shoppingimages/pantry.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                                <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.PANTRY : 'PANTRY'}</Text>
                            </ImageBackground>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMyMoney.bind(this)}  underlayColor='#232A40'>
                            <ImageBackground source={require('../assets/images/shoppingimages/howmuch.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                                <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE : 'HOW MUCH MONEY I HAVE'}</Text>
                            </ImageBackground>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMoneyManagement.bind(this)}  underlayColor='#232A40'>
                            <ImageBackground source={require('../assets/images/shoppingimages/money-management.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                                <Text style={{marginLeft:'5%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYMANAGEMENT : 'MONEY MANAGEMENT'}</Text>
                            </ImageBackground>
                            </TouchableOpacity>

                           
                             <TouchableOpacity style={styles.homebutton_style} onPress={() => {
                                              Alert.alert(
                                               this.state.PrimaryLanguage ? this.state.voicefeatureforlogout : 'Are you sure to Logout',
                                                '',
                                                [
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforcancel : 'cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforok : 'Ok', onPress: () => {this.onPressLogout(this)}},
                                                ],
                                                { cancelable: false }
                                              )
                                    }}>
                            <ImageBackground source={require('../assets/images/shoppingimages/logoutmain.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                              <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.LOGOUT : 'LOGOUT'}</Text>
                            </ImageBackground>
                            </TouchableOpacity>
                </View>
              </View>
               {this.state.isLoading ?  <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>}

              
             
             
              </ImageBackground>
              

                  {this.state.showmodel ?  <Modal
                      supportedOrientations={['landscape']}
                      animationType="slide"
                      transparent={true}
                      visible={this.state.modalVisible}
                      //visible={true}
                      onRequestClose={() => {
                        alert('Modal has been closed.');
                      }}>
                      <ScrollView>
                     
                      <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',alignItems: 'center', width: wp('80%'), marginTop:hp('25%'), marginLeft: wp('10%')}}>
                            

                            <View style={{flex: 1,flexDirection: 'column',height: hp('50%'), backgroundColor: '#00ffff',borderWidth:5,borderRadius:5, borderColor:'#FFFFFF'}} >

                               <TouchableOpacity style={{marginTop:hp('5%'),backgroundColor:'#FFFFFF', }} onPress={() => {this.closePantryModel(!this.state.modalVisible);}}>
                                  <Text style={{marginRight:0,color:'red', fontSize:hp('3%')}}>Close</Text>
                              </TouchableOpacity>


                              <View style={{flex: 1,flexDirection  : 'row', justifyContent : 'space-between',borderWidth : 1,}}>
                               
                                <View style={{width: wp('20%'), height: hp('20%'), marginLeft:-5,}} >
                                    <Image style={{height:wp('20%'), width:hp('25%'),alignSelf:'center', marginTop:hp('2%'),}} source={this.state.latestpantry} />
                                </View>

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between',marginTop:hp('10%'),right:12}}>
                                
                                        <View>
                                            <Text style={{fontSize:hp('3'), fontWeight: 'bold',color:'#000000'}}>Pantry Weighted</Text>
                                        </View>
                                        
                                        <View style={{width: '70%', height: '20%',marginTop:-25 }}>

                                                  <RadioGroup
                                                        size={24}
                                                        thickness={2}
                                                        color='#9575b2'
                                                       // highlightColor='#ccc8b9'
                                                        selectedIndex ={ 0 } onSelect = {(index, value) => this.setState({pantryweighttype:value})} >
                                                        <RadioButton value={false} >
                                                        <View style={{flexDirection:'row'}}>
                                                          <Text style={styles.normaltext_small}>No</Text>
                                                          </View>
                                                        </RadioButton>
                                                 
                                                        <RadioButton value={true}>
                                                        <View style={{flexDirection:'row'}}>
                                                         <Text style={styles.normaltext_small}>Yes</Text>
                                                        </View>
                                                        </RadioButton>
                                               
                                                    </RadioGroup> 

                                        </View>

                                </View>

                                <View style={{flex:1, flexDirection:"column", width: 100, height: 100, right:30, marginTop:-5}} >

                                  {this.state.ShowtwoLanguageBox ? <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',marginLeft:5}}>
                                        <View style={{flex: 1,flexDirection: 'column', width: '100%', height: 50,}} >
                                                  <View style={{width: '100%', height: hp('10%'), borderWidth: 1, borderColor: '#000000',marginTop:10,justifyContent:'center'}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                          //backgroundColor: '#FFFFFF'
                                                        }}
                                                        value={this.state.PrimaryPantryadded}
                                                        onChangeText={this.onChangePantryName.bind(this.state.PrimaryPantryadded)}
                                                        keyboardType=''
                                                        placeholder={'Primary Language'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  </View>
                                                  
                                                  <View style={{width: '100%', height: hp('10%'),marginTop:5, borderWidth: 1, borderColor: '#000000',}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                          //backgroundColor: '#FFFFFF'
                                                        }}
                                                        value={this.state.Pantryuseradded}
                                                        keyboardType=''
                                                        placeholder={'Secondary Language'}
                                                        underlineColorAndroid="transparent"
                                                        editable={false}
                                                  />   
                                                  </View>
                                        </View>
                                        <View style={{width: '100%', height: hp('10%'), marginTop:hp('25%'),borderWidth: 1, borderColor: '#000000',backgroundColor:'#FF0000',justifyContent:'center'}} >
                                              <TouchableOpacity onPress={this.onPressSavePantry.bind(this)}>
                                                  <Text style = {{alignSelf:'center',fontWeight: 'bold',color:'#FFFFFF',fontSize:hp('3%')}}>ADD PANTRY</Text> 
                                              </TouchableOpacity>
                                        </View>
                                      </View>:
                                      <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',marginLeft:20}}>
                                        <View style={{flex: 1,flexDirection: 'row', width: '100%', height: 50,marginTop:hp('5%'),alignSelf:'center'  }} >
                                                  <View style={{width: wp('25%'), height: hp('10%'), borderWidth: 1, borderColor: '#000000',justifyContent:'center'}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                        }}
                                                        value={this.state.PrimaryPantryadded}
                                                        onChangeText={this.onChangePantryName.bind(this.state.PrimaryPantryadded)}
                                                        keyboardType=''
                                                        placeholder={'Primary Language'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  </View>
                                                  
                                        </View>
                                        <View style={{width: '85%', height: hp('10%'),marginTop:hp('20%'),borderWidth: 1, borderColor: '#000000',backgroundColor:'#FF0000',justifyContent:'center'}} >
                                              <TouchableOpacity onPress={this.onPressSavePantry.bind(this)}>
                                                  <Text style = {{alignSelf:'center',fontWeight: 'bold',color:'#FFFFFF',fontSize:hp('3%')}}>ADD PANTRY</Text> 
                                              </TouchableOpacity>
                                        </View>
                                      </View>

                                      
                                  }

                                    </View>

                              </View>
                             
                            </View>

                            <View style={{}} />
                          </View>
                          </ScrollView>
                      </Modal> : <View/> }


                { this.state.editquantity ? 
                  <Modal
                    supportedOrientations={['portrait', 'landscape']}
                    animationType="slide"
                    transparent={true}
                    visible={this.state.editquantity}
                    onRequestClose={() => {
                      alert('Modal has been closed.');
                    }}>
                    
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '20%', 
                        marginLeft:'40%',
                      }}>
                        
                        <View style={{flex: 1,flexDirection: 'column',height: '40%', backgroundColor: '#00ffff',borderWidth:5,borderRadius:5, borderColor:'#FFFFFF'}} >

                           <TouchableHighlight style={{marginTop:20,backgroundColor:'#FFFFFF', }} onPress={() => {this.setState({editquantity: false});}}>
                              <Text style={{right:0, fontSize:RF(4), fontWeight: 'bold'}}>Close</Text>
                          </TouchableHighlight>
                          <View style={{justifyContent:'center', color:'#000000', borderWidth:2, marginTop:'10%',height:'30%', width:'80%', alignSelf:'center'}}>
                            <TextInput
                                style={{ fontSize: RF(3), color:'#000000'}}
                                placeholder="Enter Quantity"
                                onChangeText={(text) => this._oneditQuantity({text})}
                                keyboardType="numeric"
                                underlineColorAndroid="transparent"
                              />
                          </View>
                      </View>
                        
                      </View>
                  </Modal> : <View/>}
          </View> 
    );
  }
}


