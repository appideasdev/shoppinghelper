/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  Button,
  TouchableHighlight,
  Image,
  TextInput,
  Keyboard,
  Alert,
  FlatList,
  ScrollView,
  AsyncStorage,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';

import styles from './GlobalStyle.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import { Table, Row, Rows } from 'react-native-table-component';
import GlobalVariables from './GlobalVariables.js';
import ImagePicker from 'react-native-image-picker';
import Tts from 'react-native-tts';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";




type Props = {};

export default class GoShopping extends Component<Props> {

      static navigationOptions = {
          header: null
      }


     constructor(props)
     {

              AsyncStorage.getItem('TOKEN').then((data) => { 

                if(!data){
                    Actions.Login();
                }
                      
              }); 



       super(props);

           this.state = { 
            isLoading     : true,
            coin5cent     : 0,
            value5cent    : 0,
            coin10cent    : 0,
            value10cent   : 0,
            coin20cent    : 0,
            value20cent   : 0,
            coin50cent    : 0,
            value50cent   : 0,
            coin1dollar   : 0,
            value1dollar  : 0,
            coin2dollar   : 0,
            value2dollar  : 0,
            note5dollar   : 0,
            value5dollar  : 0,
            note10dollar  : 0,
            value10dollar : 0,
            note20dollar  : 0,
            value20dollar : 0,
            note50dollar  : 0,
            value50dollar : 0,
            note100dollar : 0,
            value100dollar : 0, 
            totalmoney    : 0,
            avatarSource: null,
            profileimage    : null,
            profileimage2   : null,
            subscribe       : null,
            Localprofileimage     : null,
            Localprofileimage2    : null,
           }



          AsyncStorage.multiGet(["profileimage", "profileimage2"]).then(response => {
    
             this.setState({
                                    profileimage     : response[0][1],
                                    profileimage2    : response[1][1],
                                   
                          });   
                           
                       });          
    




          AsyncStorage.getItem('PrimaryLanguage').then((data) => {
            
            

                if( data == null ){

                  this.setState({PrimaryLanguage : 'en' });
                  
                }else{

                  this.setState({PrimaryLanguage : data }); 
                }
                

          });

          AsyncStorage.getItem('isSingle').then((data) => {
          
              console.log(data,"CHECKLANGUAGETYPE")

              if( data == 'true'){

                this.setState({isSingle : true});
                this.setState({ShowtwoLanguageBox : false});
              }

              if( data == 'false'){

                this.setState({isSingle : false});
                this.setState({ShowtwoLanguageBox : true});
              }

              if( data == null ){
                this.setState({ShowtwoLanguageBox : true});
              }

              

          });  

      
          AsyncStorage.getItem('SeconderyLanguage').then((data) => {
          
              if( data == null ){
                this.setState({SeconderyLanguage : 'en'});
              }else{
                this.setState({SeconderyLanguage : data});
              }

              

          });  

          AsyncStorage.multiGet(["coin5cent", "coin10cent","coin20cent","coin50cent", "coin1dollar", "coin2dollar", "note5dollar", "note10dollar", "note20dollar", "note50dollar", "note100dollar", "totalmoney","user_id", "TOKEN","MyMoneyAudioType"]).then(response => {
            
            var coin5cent       = response[0][1];
            var coin10cent      = response[1][1];
            var coin20cent      = response[2][1];
            var coin50cent      = response[3][1];
            var coin1dollar     = response[4][1];
            var coin2dollar     = response[5][1];
            var note5dollar     = response[6][1];
            var note10dollar    = response[7][1];
            var note20dollar    = response[8][1];
            var note50dollar    = response[9][1];
            var note100dollar   = response[10][1];
            var totalmoney      = response[11][1];
            var user_id         = response[12][1];
            var token           = response[13][1];
            var MyMoneyAudioType           = response[14][1];


            var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();

            formdata.append('user_id',        JSON.parse(user_id) );
            formdata.append('money',          JSON.parse(totalmoney) );
            formdata.append('5centcoin',      JSON.parse(coin5cent) );
            formdata.append('10centcoin',     JSON.parse(coin10cent) );
            formdata.append('20centcoin',     JSON.parse(coin20cent) );
            formdata.append('50centcoin',     JSON.parse(coin50cent) );
            formdata.append('1dollarcoin',    JSON.parse(coin1dollar) );
            formdata.append('2dollarcoin',    JSON.parse(coin2dollar) );
            formdata.append('5dollarnote',    JSON.parse(note5dollar) );
            formdata.append('10dollarnote',   JSON.parse(note10dollar) );
            formdata.append('20dollarnote',   JSON.parse(note20dollar) );
            formdata.append('50dollarnote',   JSON.parse(note50dollar) );
            formdata.append('100dollarnote',  JSON.parse(note100dollar) );

           //Alert.alert( coin5cent +" "+  coin10cent  +" "+coin20cent  +" "+coin50cent +" "+coin1dollar+" "+coin2dollar+" "+note5dollar +" "+note10dollar+" "+note20dollar+" "+note50dollar+" "+note100dollar +" "+ totalmoney)


           console.log(formdata,"formdata");       

          try {
              let response = fetch( GVar.BaseUrl + GVar.UpdateMoney,{
                                method: 'POST',
                                headers:{
                                            'token' : token
                                        },
                                body: formdata,        
                                }).then((response) => response.json())
                               .then((responseJson) => {

                                //Alert.alert(JSON.stringify(responseJson));

                                if( responseJson.status == '1'){

                                
                                  
                                }else if( responseJson.status == '0'){

                                 //Alert.alert(responseJson.message);
                                  
                                }
                                    
                               });
          }catch (error) {

            if( error.line == 18228 ){
              
              
            }

          }

          if( coin5cent == null ){
                this.setState({
                       coin5cent            : parseFloat(0.00).toFixed(0),
                       value5cent           : parseFloat(( 5 / 100 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       coin5cent            : parseFloat(JSON.parse(coin5cent)).toFixed(0),
                       value5cent           : parseFloat(( 5 / 100 ) * JSON.parse(coin5cent)).toFixed(2),
                     });

            }


            if( coin10cent == null ){
                this.setState({
                       coin10cent            : parseFloat(0.00).toFixed(0),
                       value10cent           : parseFloat(( 10 / 100 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       coin10cent            : parseFloat(JSON.parse(coin10cent)).toFixed(0),
                       value10cent           : parseFloat(( 10 / 100 ) * JSON.parse(coin10cent)).toFixed(2),
                     });

            }


            if( coin20cent == null ){
                this.setState({
                       coin20cent            : parseFloat(0.00).toFixed(0),
                       value20cent           : parseFloat(( 20 / 100 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       coin20cent            : parseFloat(JSON.parse(coin20cent)).toFixed(0),
                       value20cent           : parseFloat(( 20 / 100 ) * JSON.parse(coin20cent)).toFixed(2),
                     });

            }


            if( coin50cent == null ){
                this.setState({
                       coin50cent            : parseFloat(0.00).toFixed(0),
                       value50cent           : parseFloat(( 50 / 100 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       coin50cent            : parseFloat(JSON.parse(coin50cent)).toFixed(0),
                       value50cent           : parseFloat(( 50 / 100 ) * JSON.parse(coin50cent)).toFixed(2),
                     });

            }

       
            if( coin1dollar == null ){
                this.setState({
                       coin1dollar            : parseFloat(0.00).toFixed(0),
                       value1dollar           : parseFloat((1 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       coin1dollar            : parseFloat(JSON.parse(coin1dollar)).toFixed(0),
                       value1dollar           : parseFloat(( 1 ) * JSON.parse(coin1dollar)).toFixed(2),
                     });

            }
          

            if( coin2dollar == null ){
                this.setState({
                       coin2dollar            : parseFloat(0.00).toFixed(0),
                       value2dollar           : parseFloat(( 2 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       coin2dollar            : parseFloat(JSON.parse(coin2dollar)).toFixed(0),
                       value2dollar           : parseFloat(( 2 ) * JSON.parse(coin2dollar)).toFixed(2),
                     });

            }


            if( note5dollar == null ){
                this.setState({
                       note5dollar            : parseFloat(0.00).toFixed(0),
                       value5dollar           : parseFloat(( 5 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       note5dollar            : parseFloat(JSON.parse(note5dollar)).toFixed(0),
                       value5dollar           : parseFloat(( 5 ) * JSON.parse(note5dollar)).toFixed(2),
                     });

            }



            if( note10dollar == null ){
                this.setState({
                       note10dollar            : parseFloat(0.00).toFixed(0),
                       value10dollar           : parseFloat(( 10 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       note10dollar            : parseFloat(JSON.parse(note10dollar)).toFixed(0),
                       value10dollar           : parseFloat(( 10 ) * JSON.parse(note10dollar)).toFixed(2),
                     });

            }


            if( note20dollar == null ){
                this.setState({
                       note20dollar            : parseFloat(0.00).toFixed(0),
                       value20dollar           : parseFloat(( 20 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       note20dollar            : parseFloat(JSON.parse(note20dollar)).toFixed(0),
                       value20dollar           : parseFloat(( 20 ) * JSON.parse(note20dollar)).toFixed(2),
                     });

            }


            if( note50dollar == null ){
                this.setState({
                       note50dollar            : parseFloat(0.00).toFixed(0),
                       value50dollar           : parseFloat(( 50 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       note50dollar            : parseFloat(JSON.parse(note50dollar)).toFixed(0),
                       value50dollar           : parseFloat(( 50 ) * JSON.parse(note50dollar)).toFixed(2),
                     });

            }


            if( note100dollar == null ){
                this.setState({
                       note100dollar            : parseFloat(0.00).toFixed(0),
                       value100dollar          : parseFloat(( 100 ) * JSON.parse(0)).toFixed(2),
                     });
              
            }else{

               this.setState({
                       note100dollar            : parseFloat(JSON.parse(note100dollar)).toFixed(0),
                       value100dollar           : parseFloat(( 100 ) * JSON.parse(note100dollar)).toFixed(2),
                     });

            }


            if( totalmoney == null){
                this.setState({
                       totalmoney            : parseFloat(0.00).toFixed(0),
                     });
              
            }else{

               this.setState({
                       totalmoney           : parseFloat( JSON.parse(totalmoney)).toFixed(2),
                     });

            }


            this.setState({isLoading:false})

           
          });


     }

    componentDidMount = async () => {

          this.setLanguages = this.setLanguages();

          AsyncStorage.multiGet(["Localprofileimage", "Localprofileimage2"]).then(response => {
                
            var Localprofileimage              = response[0][1];
            var Localprofileimage2             = response[1][1];

            //alert(Localprofileimage)


            this.setState({
                                    Localprofileimage     : JSON.parse(Localprofileimage),
                                    Localprofileimage2    : JSON.parse(Localprofileimage2),
                          });          
            });


            AsyncStorage.multiGet(["name", "user_id","LowMoneyWarningType","MoneymanagementAudioType", "MyMoneyAudioType", "ShoppingListAudioType", "ShoppingListType", "LanguageType", "Selfietype","GoShoppingAudioType","TOKEN","GoShoppingAudioType","LowmoneyStatus","LowmoneyTune","LowmoneyValue","PrimaryLanguage","SeconderyLanguage","LanguageTypestatus","LanguageType"]).then(response => {
            

                    var name                          = response[0][1];
                    var user_id                       = response[1][1];
                    var LowMoneyWarningType           = response[2][1];
                    var MoneymanagementAudioType      = response[3][1];
                    var MyMoneyAudioType              = response[4][1];
                    var ShoppingListAudioType         = response[5][1];
                    var ShoppingListType              = response[6][1];
                    var LanguageType                  = response[7][1];
                    var Selfietype                    = response[8][1];
                    var GoShoppingAudioType           = response[9][1];
                    var token                         = response[10][1];
                    var GoShoppingAudioType           = response[11][1];
                    var LowmoneyStatus                = response[12][1];
                    var LowmoneyTune                  = response[13][1];
                    var LowmoneyValue                 = response[14][1];
                    var PrimaryLanguage               = response[15][1];
                    var SeconderyLanguage             = response[16][1];
                    var LanguageTypestatus            = response[17][1];
                    var LanguageType1                  = response[18][1];

                   //Alert.alert(SeconderyLanguage);
 

                   this.setState({
                                    name                    : name.toUpperCase(),
                                    LowMoneyWarningType     : LowMoneyWarningType,
                                    MoneymanagementAudioType: MoneymanagementAudioType,
                                    MyMoneyAudioType        : MyMoneyAudioType,
                                    ShoppingListAudioType   : ShoppingListAudioType,
                                    ShoppingListType        : ShoppingListType,
                                    LanguageType            : LanguageType,
                                    Selfietype              : Selfietype,
                                    GoShoppingAudioType     : GoShoppingAudioType,
                                    LowmoneyStatus          : LowmoneyStatus,
                                    LowmoneyTune            : LowmoneyTune,
                                    LowmoneyValue           : LowmoneyValue,
                                    PrimaryLanguage         : PrimaryLanguage,
                                    SeconderyLanguage       : SeconderyLanguage,
                                    token                   : token,
                                    LanguageTypestatus      : LanguageTypestatus
                              });
              }); 



    }


    setLanguages(){

  

        AsyncStorage.getItem('PrimaryLanguage').then((P_LANG) => {
              
              

              AsyncStorage.getItem('WELCOME PAGE').then((data) => {

                    var WECOME = JSON.parse(data);

                    if( P_LANG == 'en'){

                      this.setState({WECOME : WECOME.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({WECOME : WECOME.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({WECOME : WECOME.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({WECOME : WECOME.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({WECOME : WECOME.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({WECOME : WECOME.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({WECOME : WECOME.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({WECOME : WECOME.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({WECOME : WECOME.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({WECOME : WECOME.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({WECOME : WECOME.fil})
                    }

              });

              AsyncStorage.getItem('SHOPPING LIST').then((data) => {

                    var SHOPPINGLIST = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({SHOPPINGLIST : SHOPPINGLIST.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({SHOPPINGLIST : SHOPPINGLIST.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({SHOPPINGLIST : SHOPPINGLIST.fil})
                    }

              });

              AsyncStorage.getItem('LETS GO SHOPPING').then((data) => {

                    var GOSHOPPING = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({GOSHOPPING : GOSHOPPING.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({GOSHOPPING : GOSHOPPING.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({GOSHOPPING : GOSHOPPING.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({GOSHOPPING : GOSHOPPING.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({GOSHOPPING : GOSHOPPING.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({GOSHOPPING : GOSHOPPING.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({GOSHOPPING : GOSHOPPING.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({GOSHOPPING : GOSHOPPING.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({GOSHOPPING : GOSHOPPING.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({GOSHOPPING : GOSHOPPING.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({GOSHOPPING : GOSHOPPING.fil})
                    }

              });

              AsyncStorage.getItem('PANTRY').then((data) => {

                    var PANTRY = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({PANTRY : PANTRY.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({PANTRY : PANTRY.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({PANTRY : PANTRY.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({PANTRY : PANTRY.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({PANTRY : PANTRY.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({PANTRY : PANTRY.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({PANTRY : PANTRY.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({PANTRY : PANTRY.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({PANTRY : PANTRY.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({PANTRY : PANTRY.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({PANTRY : PANTRY.fil})
                    }

              });

              AsyncStorage.getItem('HOW MUCH MONEY DO I HAVE').then((data) => {

                    var MONEYIHAVE = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({MONEYIHAVE : MONEYIHAVE.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({MONEYIHAVE : MONEYIHAVE.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({MONEYIHAVE : MONEYIHAVE.fil})
                    }

              });

              AsyncStorage.getItem('MONEY MANAGEMENT').then((data) => {

                    var MONEYMANAGEMENT = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.fil})
                    }

              });

              AsyncStorage.getItem('SETUP').then((data) => {

                    var SETUP = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({SETUP : SETUP.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({SETUP : SETUP.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({SETUP : SETUP.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({SETUP : SETUP.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({SETUP : SETUP.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({SETUP : SETUP.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({SETUP : SETUP.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({SETUP : SETUP.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({SETUP : SETUP.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({SETUP : SETUP.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({SETUP : SETUP.fil})
                    }

              });

              AsyncStorage.getItem('LOGOUT').then((data) => {

                    var LOGOUT = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({LOGOUT : LOGOUT.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({LOGOUT : LOGOUT.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({LOGOUT : LOGOUT.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({LOGOUT : LOGOUT.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({LOGOUT : LOGOUT.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({LOGOUT : LOGOUT.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({LOGOUT : LOGOUT.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({LOGOUT : LOGOUT.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({LOGOUT : LOGOUT.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({LOGOUT : LOGOUT.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({LOGOUT : LOGOUT.fil})
                    }

              });

              AsyncStorage.getItem('ADD AN IMAGE INTO PANTRY').then((data) => {

                    var ADDIMAGE = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({ADDIMAGE : ADDIMAGE.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({ADDIMAGE : ADDIMAGE.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({ADDIMAGE : ADDIMAGE.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({ADDIMAGE : ADDIMAGE.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({ADDIMAGE : ADDIMAGE.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({ADDIMAGE : ADDIMAGE.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({ADDIMAGE : ADDIMAGE.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({ADDIMAGE : ADDIMAGE.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({ADDIMAGE : ADDIMAGE.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({ADDIMAGE : ADDIMAGE.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({ADDIMAGE : ADDIMAGE.fil})
                    }

              });

              AsyncStorage.getItem('NUMBER OF COINS').then((data) => {

                    var NCOINS = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({NCOINS : NCOINS.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({NCOINS : NCOINS.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({NCOINS : NCOINS.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({NCOINS : NCOINS.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({NCOINS : NCOINS.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({NCOINS : NCOINS.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({NCOINS : NCOINS.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({NCOINS : NCOINS.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({NCOINS : NCOINS.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({NCOINS : NCOINS.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({NCOINS : NCOINS.fil})
                    }

              });


              AsyncStorage.getItem('TOTAL').then((data) => {

                    var TOTAL = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({TOTAL : TOTAL.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({TOTAL : TOTAL.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({TOTAL : TOTAL.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({TOTAL : TOTAL.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({TOTAL : TOTAL.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({TOTAL : TOTAL.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({TOTAL : TOTAL.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({TOTAL : TOTAL.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({TOTAL : TOTAL.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({TOTAL : TOTAL.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({TOTAL : TOTAL.fil})
                    }

              });

              AsyncStorage.getItem('RESET ALL COUNTS').then((data) => {

                    var RESET = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({RESET : RESET.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({RESET : RESET.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({RESET : RESET.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({RESET : RESET.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({RESET : RESET.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({RESET : RESET.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({RESET : RESET.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({RESET : RESET.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({RESET : RESET.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({RESET : RESET.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({RESET : RESET.fil})
                    }

              });

              AsyncStorage.getItem('I HAVE').then((data) => {

                    var IHAVE = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({IHAVE : IHAVE.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({IHAVE : IHAVE.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({IHAVE : IHAVE.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({IHAVE : IHAVE.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({IHAVE : IHAVE.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({IHAVE : IHAVE.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({IHAVE : IHAVE.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({IHAVE : IHAVE.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({IHAVE : IHAVE.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({IHAVE : IHAVE.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({IHAVE : IHAVE.fil})
                    }

              });

              AsyncStorage.getItem('USE FOR MONEY MANAGEMENT').then((data) => {

                    var USEMONEYMANAGEMENT = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({USEMONEYMANAGEMENT : USEMONEYMANAGEMENT.fil})
                    }

              });

              AsyncStorage.getItem('USE FOR SHOPPING').then((data) => {

                    var USESHOPPING = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({USESHOPPING : USESHOPPING.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({USESHOPPING : USESHOPPING.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({USESHOPPING : USESHOPPING.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({USESHOPPING : USESHOPPING.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({USESHOPPING : USESHOPPING.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({USESHOPPING : USESHOPPING.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({USESHOPPING : USESHOPPING.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({USESHOPPING : USESHOPPING.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({USESHOPPING : USESHOPPING.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({USESHOPPING : USESHOPPING.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({USESHOPPING : USESHOPPING.fil})
                    }

              });

              AsyncStorage.getItem('Do you want to reset all money?').then((data) => {

                    var RESETALLMONEY = JSON.parse(data);


                    if( P_LANG == 'en'){

                      this.setState({RESETALLMONEY : RESETALLMONEY.en})
                    }

                    if( P_LANG == 'el'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.el})
                    }

                    if( P_LANG == 'cmn'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.cmn})
                    }

                    if( P_LANG == 'zh'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.zh})
                    }

                    if( P_LANG == 'ar'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.ar})
                    }

                    if( P_LANG == 'vi'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.vi})
                    }

                    if( P_LANG == 'it'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.it})
                    }

                    if( P_LANG == 'es'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.es})
                    }

                    if( P_LANG == 'hi'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.hi})
                    }

                    if( P_LANG == 'pa'){

                        this.setState({RESETALLMONEY : RESETALLMONEY.pa})
                    }

                    if( P_LANG == 'fil'){

                       this.setState({RESETALLMONEY : RESETALLMONEY.fil})
                    }

              });

              AsyncStorage.getItem('cents').then((data) => {

                              var cents = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({cents : cents.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({cents : cents.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({cents : cents.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({cents : cents.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({cents : cents.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({cents : cents.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({cents : cents.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({cents : cents.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({cents : cents.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({cents : cents.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({cents : cents.fil})
                              }

              });

       AsyncStorage.getItem('Are you sure to log out?').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforlogout : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforlogout : voicefeatureGS.fil})
                              }

                        });
                AsyncStorage.getItem('ok').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforok : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforok : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforok : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforok : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforok : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforok : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforok : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforok : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforok : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('cancel').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforcancel : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforcancel : voicefeatureGS.fil})
                              }

                        });


              AsyncStorage.getItem('dollar').then((data) => {

                              var dollar = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({dollar : dollar.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({dollar : dollar.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({dollar : dollar.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({dollar : dollar.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({dollar : dollar.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({dollar : dollar.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({dollar : dollar.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({dollar : dollar.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({dollar : dollar.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({dollar : dollar.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({dollar : dollar.fil})
                              }

              });

              AsyncStorage.getItem('You have no more').then((data) => {

                              var nomore = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({nomore : nomore.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({nomore : nomore.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({nomore : nomore.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({nomore : nomore.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({nomore : nomore.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({nomore : nomore.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({nomore : nomore.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({nomore : nomore.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({nomore : nomore.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({nomore : nomore.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({nomore : nomore.fil})
                              }

              });

              AsyncStorage.getItem('Your all amount now reset please add again').then((data) => {

                              var allmountresetnow = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({allmountresetnow : allmountresetnow.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({allmountresetnow : allmountresetnow.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({allmountresetnow : allmountresetnow.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({allmountresetnow : allmountresetnow.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({allmountresetnow : allmountresetnow.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({allmountresetnow : allmountresetnow.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({allmountresetnow : allmountresetnow.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({allmountresetnow : allmountresetnow.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({allmountresetnow : allmountresetnow.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({allmountresetnow : allmountresetnow.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({allmountresetnow : allmountresetnow.fil})
                              }

              });

              AsyncStorage.getItem('Please insert a valid value').then((data) => {

                              var PleaseinsertvalidValue = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({PleaseinsertvalidValue : PleaseinsertvalidValue.fil})
                              }

              });
              
        });

        AsyncStorage.getItem('SeconderyLanguage').then((P_LANG) => { 


            AsyncStorage.getItem('cents').then((data) => {

                              var Scents = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({Scents : Scents.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({Scents : Scents.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({Scents : Scents.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({Scents : Scents.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({Scents : Scents.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({Scents : Scents.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({Scents : Scents.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({Scents : Scents.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({Scents : Scents.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({Scents : Scents.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({Scents : Scents.fil})
                              }

              });


              AsyncStorage.getItem('dollar').then((data) => {

                              var Sdollar = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({Sdollar : Sdollar.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({Sdollar : Sdollar.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({Sdollar : Sdollar.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({Sdollar : Sdollar.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({Sdollar : Sdollar.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({Sdollar : Sdollar.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({Sdollar : Sdollar.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({Sdollar : Sdollar.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({Sdollar : Sdollar.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({Sdollar : Sdollar.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({Sdollar : Sdollar.fil})
                              }

              });
        });
    }

     GetGridViewItem (item) {
      
     Alert.alert(item);

     }

     onPressMoney( money ){

        
      

        //Tts.setDefaultLanguage(this.state.PrimaryLanguage);

        var res = money.split(" ");        

          if( res[1] === 'cent'){

            //Alert.alert(res[0] +" "+ this.state.cents );

            if(this.state.MyMoneyAudioType == 0 ){

              Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

                    console.log(setDefaultLanguage,"setDefaultPrimaryLanguage")
                    if( setDefaultLanguage == 'success'){
                      
                      Tts.speak(res[0]);
                      Tts.speak(this.state.cents);
                      //Tts.speak("cents");
                    }

              }).catch((error) => {

                  Alert.alert("Your device doesn't support primary language voice.");
                  
              });
                
                
            }

          }else{

           // Alert.alert(res[0] +" "+ this.state.dollar );
            if( this.state.MyMoneyAudioType == 0 ){

              Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

                  console.log(setDefaultLanguage,"setDefaultPrimaryLanguage")
                  if( setDefaultLanguage == 'success'){
                      
                     Tts.speak(res[0]);
                     Tts.speak(this.state.dollar);
                      //Tts.speak("cents");
                  }

              }).catch((error) => {

                  Alert.alert("Your device doesn't support primary language voice.");
                  
              });

              
            }
          }

        if(!this.state.isSingle){

          //Tts.setDefaultLanguage(this.state.SeconderyLanguage);
          if( res[1] === 'cent'){

           
            if( this.state.MyMoneyAudioType == 0 ){

              Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                    console.log(setDefaultLanguage,"setDefaultsecondaryLanguage")
                    if( setDefaultLanguage == 'success'){
                      
                      Tts.speak(res[0]);
                      Tts.speak(this.state.Scents);
                      //Tts.speak("cents");
                    }
                    
              }).catch((error) => {

                  Alert.alert("Your device doesn't support secondary language voice.");
                  
              });
                
                
            }

          }else{

            
            if( this.state.MyMoneyAudioType == 0 ){


              Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                    console.log(setDefaultLanguage,"setDefaultsecondaryLanguage")
                    if( setDefaultLanguage == 'success'){
                      
                      Tts.speak(res[0]);
                      Tts.speak(this.state.Sdollar);
                    }
                    
              }).catch((error) => {

                  Alert.alert("Your device doesn't support secondary language voice.");
                  
              });

              
            }
          }
        }  
        
       
       
     }

    

      onPressMoneyManagement(){
        Actions.MoneyManagement();
      }


      onPressShoopingList(){
        Actions.ShoppingList();
      }

      onPressHome(){
        Actions.Home();
      }

      onPressGoShopping(){
        Actions.GoShopping();
      }

      onPressMyMoney(){
         Actions.MyMoney();
      }

      onPressPantry(){
          Actions.Pantry();
      }

      _keyboardDidHide() {
       
        Keyboard.dismiss()
      }

      onPressResetAmount(){

      }

  onPress5coinadd(){

    var totalcent   = parseInt(this.state.coin5cent) + 1;
    var value5cent  = parseFloat(( 5 / 100 ) * totalcent).toFixed(2);


    this.setState({ value5cent: value5cent, coin5cent: parseFloat(parseFloat(this.state.coin5cent) + 1).toFixed(0)});
    AsyncStorage.setItem('coin5cent', this.checknull(parseFloat(this.state.coin5cent).toFixed(2) + 1));


    var total       = parseFloat(parseFloat(this.state.totalmoney) + (( 5 / 100 ) )).toFixed(2);
    
    this.setState({ totalmoney: total });
    AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

    if( this.state.MyMoneyAudioType == 0 ){
     // Tts.setDefaultLanguage(this.state.PrimaryLanguage);

      Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

          console.log(setDefaultLanguage,"setDefaultPrimaryLanguage")
          if( setDefaultLanguage == 'success'){
              
              Tts.speak( (parseInt(this.state.coin5cent) + 1) + ' 5 '+ this.state.cents);
          }

      }).catch((error) => {

          Alert.alert("Your device doesn't support primary language voice.");
          
      });



      //Tts.speak( (parseInt(this.state.coin5cent) + 1) + ' 5 '+ this.state.cents);

      if( !this.state.isSingle ){

          //Tts.setDefaultLanguage(this.state.SeconderyLanguage);
          //Tts.speak( (parseInt(this.state.coin5cent) + 1) + ' 5 '+ this.state.Scents);

          Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                console.log(setDefaultLanguage,"setDefaultsecondaryLanguage")
                if( setDefaultLanguage == 'success'){
                  
                  Tts.speak( (parseInt(this.state.coin5cent) + 1) + ' 5 '+ this.state.Scents);
                }
                
          }).catch((error) => {

              Alert.alert("Your device doesn't support secondary language voice.");
              
          });
      } 
    }
    
    
  }

  onPress5coinminus(){

        if( this.state.coin5cent == 0 ){

          Alert.alert(this.state.nomore);

          if( this.state.MyMoneyAudioType == 0 ){
              Tts.speak(this.state.nomore);
          }
        }else{

          
          this.setState({ coin5cent: parseFloat(parseFloat(this.state.coin5cent) - 1).toFixed(0)});
      
          var totalcent   = parseFloat(this.state.coin5cent).toFixed(0) - 1;



          


          if( this.state.MyMoneyAudioType == 0 ){
             /*Tts.setDefaultLanguage(this.state.PrimaryLanguage);
             Tts.speak( (parseInt(this.state.coin5cent) - 1) +' "5" '+ this.state.cents);*/

              Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

                  console.log(setDefaultLanguage,"setDefaultPrimaryLanguage")
                  if( setDefaultLanguage == 'success'){
                      
                      Tts.speak( (parseInt(this.state.coin5cent) - 1) + ' 5 '+ this.state.cents);
                  }

              }).catch((error) => {

                  Alert.alert("Your device doesn't support primary language voice.");
                  
              });



              if( !this.state.isSingle ){

                  //Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                  //Tts.speak( (parseInt(this.state.coin5cent) - 1) +' "5" '+ this.state.Scents);
                  Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                      console.log(setDefaultLanguage,"setDefaultsecondaryLanguage")
                      if( setDefaultLanguage == 'success'){
                        
                        Tts.speak( (parseInt(this.state.coin5cent) - 1) +' "5" '+ this.state.Scents);
                      }
                      
                  }).catch((error) => {

                      Alert.alert("Your device doesn't support secondary language voice.");
                      
                  });
              }
          }

          var value5cent        = parseFloat(( 5 / 100 ) * totalcent).toFixed(2);

          this.setState({ value5cent: value5cent });
          var total       = parseFloat(parseFloat(this.state.totalmoney) - ( 5 / 100 )).toFixed(2);
          this.setState({ totalmoney: total });

          AsyncStorage.multiSet([
                                    ["value5cent",         this.checknull(JSON.stringify(value5cent))],
                                    ["coin5cent",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.coin5cent) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);
          
        }

  }


      onPress10coinadd(){

              var totalcent   = parseFloat(this.state.coin10cent) + 1;
              var value10cent  = ( 10 / 100 ) * totalcent;

              this.setState({ value10cent: parseFloat(value10cent).toFixed(2), coin10cent: parseFloat(parseFloat(this.state.coin10cent) + 1).toFixed(0)});
              AsyncStorage.setItem('coin10cent', this.checknull(JSON.stringify(parseFloat(this.state.coin10cent) + 1) ));


              var total       = parseFloat(parseFloat(this.state.totalmoney) + (( 10 / 100 ) )).toFixed(2);
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
                /*Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                Tts.speak( (parseInt(this.state.coin10cent) + 1) + ' 10 ' + this.state.cents);*/

                Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

                    console.log(setDefaultLanguage,"setDefaultPrimaryLanguage")
                    if( setDefaultLanguage == 'success'){
                        
                        Tts.speak( (parseInt(this.state.coin10cent) + 1) + ' 10 ' + this.state.cents);
                    }

                }).catch((error) => {

                    Alert.alert("Your device doesn't support primary language voice.");
                    
                });


                if( !this.state.isSingle ){

                    /*Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak( (parseInt(this.state.coin10cent) + 1) + ' 10 ' + this.state.Scents);*/

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                        console.log(setDefaultLanguage,"setDefaultsecondaryLanguage")
                        if( setDefaultLanguage == 'success'){
                          
                          Tts.speak( (parseInt(this.state.coin10cent) + 1) + ' 10 ' + this.state.Scents);
                        }
                        
                    }).catch((error) => {

                        Alert.alert("Your device doesn't support secondary language voice.");
                        
                    });
                }
              }
      }

      onPress10coinminus(){

            if( this.state.coin10cent == 0 ){

             Alert.alert(this.state.nomore);

              if( this.state.MyMoneyAudioType == 0 ){
                  Tts.speak(this.state.nomore);
              }

            }else{
              this.setState({ coin10cent: parseFloat(parseFloat(this.state.coin10cent) - 1).toFixed(0)});

              var totalcent   = parseFloat(this.state.coin10cent).toFixed(0) - 1;

              var value10cent        = parseFloat(( 10 / 100 ) * totalcent).toFixed(2);

              this.setState({ value10cent: value10cent });
              var total       = parseFloat(parseFloat(this.state.totalmoney) - ( 10 / 100 )).toFixed(2);
              this.setState({ totalmoney: total });

              AsyncStorage.multiSet([
                                    ["value10cent",         this.checknull(JSON.stringify(value10cent))],
                                    ["coin10cent",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.coin10cent) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);

              if( this.state.MyMoneyAudioType == 0 ){
                 /*Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                 Tts.speak(totalcent +' 10 '+ this.state.cents);*/

                  Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

                      console.log(setDefaultLanguage,"setDefaultPrimaryLanguage")
                      if( setDefaultLanguage == 'success'){
                          
                          Tts.speak(totalcent +' 10 '+ this.state.cents);
                      }

                  }).catch((error) => {

                      Alert.alert("Your device doesn't support primary language voice.");
                      
                  });


                 if( !this.state.isSingle ){

                    /*Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak(totalcent +' 10 '+ this.state.Scents);*/

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                        console.log(setDefaultLanguage,"setDefaultsecondaryLanguage")
                        if( setDefaultLanguage == 'success'){
                          
                           Tts.speak(totalcent +' 10 '+ this.state.Scents);
                        }
                        
                    }).catch((error) => {

                        Alert.alert("Your device doesn't support secondary language voice.");
                        
                    });
                }
              }
            }

      }


      onPress20coinadd(){

              var totalcent   = parseFloat(this.state.coin20cent) + 1;
              var value20cent  = ( 20 / 100 ) * totalcent;

              this.setState({ value20cent: parseFloat(value20cent).toFixed(2), coin20cent: parseFloat(parseFloat(this.state.coin20cent) + 1).toFixed(0)});
              AsyncStorage.setItem('coin20cent', this.checknull(JSON.stringify(parseFloat(this.state.coin20cent) + 1) ));


              var total       = parseFloat(parseFloat(this.state.totalmoney) + (20 / 100 )).toFixed(2);
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
                /*Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                Tts.speak( (parseInt(this.state.coin20cent) + 1) + ' 20 '+ this.state.cents);*/

                Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

                    console.log(setDefaultLanguage,"setDefaultPrimaryLanguage")
                    if( setDefaultLanguage == 'success'){
                        
                        Tts.speak( (parseInt(this.state.coin20cent) + 1) + ' 20 '+ this.state.cents);
                    }

                }).catch((error) => {

                    Alert.alert("Your device doesn't support primary language voice.");
                    
                });


                if( !this.state.isSingle ){

                    /*Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak( (parseInt(this.state.coin20cent) + 1) + ' 20 '+ this.state.Scents);*/

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                        console.log(setDefaultLanguage,"setDefaultsecondaryLanguage")
                        if( setDefaultLanguage == 'success'){
                          
                          Tts.speak( (parseInt(this.state.coin20cent) + 1) + ' 20 '+ this.state.Scents);
                        }
                        
                    }).catch((error) => {

                        Alert.alert("Your device doesn't support secondary language voice.");
                        
                    });

                }
              }

      }

    

    onPress20coinminus(){


            if( this.state.coin20cent == 0 ){

                  Alert.alert(this.state.nomore);

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

            }else{
                  this.setState({ coin20cent: parseFloat(parseFloat(parseFloat(this.state.coin20cent)) - 1).toFixed(0)});

                  var totalcent   = parseFloat(this.state.coin20cent) - 1;

                  var value20cent        = parseFloat(( 20 / 100 ) * totalcent).toFixed(2);

                  this.setState({ value20cent: value20cent });
                  var total       = parseFloat(parseFloat(this.state.totalmoney) - ( 20 / 100 )).toFixed(2);
                  this.setState({ totalmoney: total });

                  AsyncStorage.multiSet([
                                    ["value20cent",         this.checknull(JSON.stringify(value20cent))],
                                    ["coin20cent",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.coin20cent) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);


                  if( this.state.MyMoneyAudioType == 0 ){
                      /*Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                      Tts.speak( (parseInt(this.state.coin20cent) - 1) + ' 20 '+this.state.cents);*/

                      Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

                          console.log(setDefaultLanguage,"setDefaultPrimaryLanguage")
                          if( setDefaultLanguage == 'success'){
                              
                              Tts.speak( (parseInt(this.state.coin20cent) - 1) + ' 20 '+this.state.cents);
                          }

                      }).catch((error) => {

                          Alert.alert("Your device doesn't support primary language voice.");
                          
                      });


                      if( !this.state.isSingle ){

                          /*Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak( (parseInt(this.state.coin20cent) - 1) + ' 20 '+this.state.Scents);*/

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage).then(setDefaultLanguage => { 

                              console.log(setDefaultLanguage,"setDefaultsecondaryLanguage")
                              if( setDefaultLanguage == 'success'){
                                
                                Tts.speak( (parseInt(this.state.coin20cent) - 1) + ' 20 '+this.state.Scents);
                              }
                              
                          }).catch((error) => {

                              Alert.alert("Your device doesn't support secondary language voice.");
                              
                          });
                      }
                  }
            }
    }

    onPress50coinadd(){

              var totalcent   = parseFloat(this.state.coin50cent) + 1;
              var value50cent  = ( 50 / 100 ) * totalcent;

              this.setState({ value50cent: value50cent, coin50cent: parseFloat(parseFloat(this.state.coin50cent) + 1).toFixed(0)});
              AsyncStorage.setItem('coin50cent', this.checknull(JSON.stringify(parseFloat(this.state.coin50cent) + 1) ));


              var total       =parseFloat(parseFloat(this.state.totalmoney) + ( 50 / 100 ) ).toFixed(2);
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
                Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                Tts.speak( (parseInt(this.state.coin50cent) + 1) + ' 50 '+this.state.cents);

                if( !this.state.isSingle ){

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak( (parseInt(this.state.coin50cent) + 1) + ' 50 '+this.state.Scents);
                }
              }
    }

    onPress50coinminus(){

            if( this.state.coin50cent == 0 ){

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

                  Alert.alert(this.state.nomore);

            }else{
                  this.setState({ coin50cent: parseFloat(parseFloat(this.state.coin50cent) - 1).toFixed(0)});

                  var totalcent   = parseFloat(this.state.coin50cent) - 1;

                  var value50cent        = parseFloat(( 50 / 100 ) * totalcent).toFixed(2);

                  this.setState({ value50cent: value50cent });
                  var total       = parseFloat(parseFloat(this.state.totalmoney) - ( 50 / 100 )).toFixed(2);
                  this.setState({ totalmoney: total });

                 AsyncStorage.multiSet([
                                    ["value50cent",         this.checknull(JSON.stringify(value50cent))],
                                    ["coin50cent",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.coin50cent) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]); 

                  if( this.state.MyMoneyAudioType == 0 ){
                     Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                     Tts.speak( (parseInt(this.state.coin50cent) - 1) + ' 50 '+this.state.cents);

                    if( !this.state.isSingle ){

                        Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                        Tts.speak( (parseInt(this.state.coin50cent) - 1) + ' 50 '+this.state.Scents);
                    }
                  }
            }

    }


    onPress1dollaradd(){

              var totalcent     = parseFloat(this.state.coin1dollar) + 1;
              var value1dollar  = totalcent;

              this.setState({ value1dollar: value1dollar, coin1dollar: parseFloat(parseFloat(this.state.coin1dollar) + 1).toFixed(0)});
              AsyncStorage.setItem('coin1dollar', this.checknull(JSON.stringify(parseFloat(this.state.coin1dollar) + 1) ));


              var total       = parseFloat(parseFloat(this.state.totalmoney) + 1).toFixed(2) ;
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
                Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                Tts.speak( (parseInt(this.state.coin1dollar) + 1) + ' 1 '+this.state.dollar);

                if( !this.state.isSingle ){

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak( (parseInt(this.state.coin1dollar) + 1) + ' 1 '+this.state.Sdollar);
                }
              }
    }

    

    onPress1dollarminus(){


            if( this.state.coin1dollar == 0 ){

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

                  Alert.alert(this.state.nomore);

            }else{
                  this.setState({ coin1dollar: parseFloat(parseFloat(this.state.coin1dollar) - 1).toFixed(0)});

                  var totalcent   = parseFloat(this.state.coin1dollar) - 1;

                  var value1dollar        = parseFloat(totalcent).toFixed(2);

                  this.setState({ value1dollar: value1dollar });
                  var total       = parseFloat(parseFloat(this.state.totalmoney) - 1).toFixed(2);
                  this.setState({ totalmoney: total });

                  AsyncStorage.multiSet([
                                    ["value1dollar",         this.checknull(JSON.stringify(value1dollar))],
                                    ["coin1dollar",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.coin1dollar) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                      Tts.speak( (parseInt(this.state.coin1dollar) - 1) + ' 1 '+this.state.dollar);

                      if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak( (parseInt(this.state.coin1dollar) - 1) + ' 1 '+this.state.Sdollar);
                      }
                  }
            }
    }


    onPress2dollaradd(){


              var totalcent     = parseFloat(this.state.coin2dollar) + 1;
              var value2dollar  = totalcent * 2;

              this.setState({ value2dollar: value2dollar, coin2dollar: parseFloat(parseFloat(this.state.coin2dollar) + 1).toFixed(0)});
              AsyncStorage.setItem('coin2dollar', this.checknull(JSON.stringify(parseFloat(this.state.coin2dollar) + 1) ));


              var total       =   parseFloat(parseFloat(this.state.totalmoney) + 2).toFixed(2) ;
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
                Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                Tts.speak( (parseInt(this.state.coin2dollar) + 1) + ' 2 '+this.state.dollar);

                if( !this.state.isSingle ){

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak( (parseInt(this.state.coin2dollar) + 1) + ' 2 '+this.state.Sdollar);
                }
              }
    }



    onPress2dollarminus(){


            if( this.state.coin2dollar == 0 ){

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

                  Alert.alert(this.state.nomore);

            }else{
                  this.setState({ coin2dollar: parseFloat(parseFloat(this.state.coin2dollar) - 1).toFixed(0)});

                  var totalcent   = parseFloat(this.state.coin2dollar) - 1;

                  var value2dollar        = parseFloat(2 * totalcent).toFixed(2);

                  this.setState({ value2dollar: value2dollar });
                  var total       = parseFloat(parseFloat(this.state.totalmoney) - 2).toFixed(2);
                  this.setState({ totalmoney: total });


                  AsyncStorage.multiSet([
                                    ["value2dollar",         this.checknull(JSON.stringify(value2dollar))],
                                    ["coin2dollar",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.coin2dollar) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);


                  if( this.state.MyMoneyAudioType == 0 ){
                     Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                     Tts.speak( (parseInt(this.state.coin2dollar) - 1) + ' 2 '+this.state.dollar);

                      if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak( (parseInt(this.state.coin2dollar) - 1) + ' 2 '+this.state.Sdollar);
                      }
                  }
            }
    }


    onPress5dollaradd(){


              var totalcent     = parseFloat(this.state.note5dollar) + 1;
              var value5dollar  = totalcent * 5;

              this.setState({ value5dollar: value5dollar, note5dollar: parseFloat(parseFloat(this.state.note5dollar) + 1).toFixed(0)});
              AsyncStorage.setItem('note5dollar', this.checknull(JSON.stringify(parseFloat(this.state.note5dollar) + 1) ));


              var total       =  parseFloat(this.state.totalmoney) + 5 ;
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
                Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                Tts.speak( (parseInt(this.state.note5dollar) + 1) + ' 5 '+this.state.dollar);

                if( !this.state.isSingle ){

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak( (parseInt(this.state.note5dollar) + 1) + ' 5 '+this.state.Sdollar);
                }
              }
    }

    

    onPress5dollarminus(){

          if( this.state.note5dollar == 0 ){

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

                  Alert.alert(this.state.nomore);

            }else{
                  this.setState({ note5dollar: parseFloat(parseFloat(this.state.note5dollar) - 1).toFixed(0)});

                  var totalcent   = parseFloat(this.state.note5dollar) - 1;

                  var value5dollar        = parseFloat(5 * totalcent);

                  this.setState({ value5dollar: value5dollar });
                  var total       = parseFloat(this.state.totalmoney) - 5;
                  this.setState({ totalmoney: total });

                  AsyncStorage.multiSet([
                                    ["value5dollar",         this.checknull(JSON.stringify(value5dollar))],
                                    ["note5dollar",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.note5dollar) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);

                  if( this.state.MyMoneyAudioType == 0 ){
                     Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                     Tts.speak( (parseInt(this.state.note5dollar) - 1) + ' 5 '+this.state.dollar);

                      if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak( (parseInt(this.state.note5dollar) - 1) + ' 5 '+this.state.Sdollar);
                      }
                  }
            }
    }


      onPress10dollaradd(){


              var totalcent     = parseFloat(this.state.note10dollar) + 1;
              var value10dollar  = totalcent * 10;

              this.setState({ value10dollar: value10dollar, note10dollar: parseFloat(parseFloat(this.state.note10dollar) + 1).toFixed(0)});
              AsyncStorage.setItem('note10dollar', this.checknull(JSON.stringify(parseFloat(this.state.note10dollar) + 1) ));


              var total       = parseFloat(parseFloat(this.state.totalmoney) + 10).toFixed(2) ;
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
                Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                Tts.speak( (parseInt(this.state.note10dollar) + 1) + ' 10 '+this.state.dollar);

                if( !this.state.isSingle ){

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak( (parseInt(this.state.note10dollar) + 1) + ' 10 '+this.state.Sdollar);
                }
              }
      }



      onPress10dollarminus(){



          if( this.state.note10dollar == 0 ){

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

                  Alert.alert(this.state.nomore);

            }else{
                  this.setState({ note10dollar: parseFloat(parseFloat(this.state.note10dollar) - 1).toFixed(0)});

                  var totalcent   = parseFloat(this.state.note10dollar) - 1;

                  var value10dollar        = parseFloat(10 * totalcent);

                  this.setState({ value10dollar: value10dollar });
                  var total       = parseFloat(parseFloat(this.state.totalmoney) - 10).toFixed(2);
                  this.setState({ totalmoney: total });


                  AsyncStorage.multiSet([
                                    ["value10dollar",         this.checknull(JSON.stringify(value10dollar))],
                                    ["note10dollar",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.note10dollar) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);

                  if( this.state.MyMoneyAudioType == 0 ){
                     Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                     Tts.speak( (parseInt(this.state.note10dollar) - 1) + ' 10 '+this.state.dollar);

                      if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak( (parseInt(this.state.note10dollar) - 1) + ' 10 '+this.state.Sdollar);
                      }
                  }
            }
      }



      onPress20dollaradd(){

              var totalcent   = parseFloat(this.state.note20dollar) + 1;
              var value20dollar  = ( 20 ) * totalcent;

              this.setState({ value20dollar: value20dollar, note20dollar: parseFloat(parseFloat(this.state.note20dollar) + 1).toFixed(0)});
              AsyncStorage.setItem('note20dollar', this.checknull(JSON.stringify(parseFloat(this.state.note20dollar) + 1) ));


              var total       = parseFloat(this.state.totalmoney) + ( 20 );
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
                Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                Tts.speak( (parseInt(this.state.note20dollar) + 1) + ' 20 '+this.state.dollar);

                if( !this.state.isSingle ){

                    Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                    Tts.speak( (parseInt(this.state.note20dollar) + 1) + ' 20 '+this.state.Sdollar);
                }
              }
      }

      onPress20dollarminus(){


            if( this.state.note20dollar == 0 ){

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

                  Alert.alert(this.state.nomore);

            }else{
                  this.setState({ note20dollar: parseFloat(parseFloat(this.state.note20dollar) - 1).toFixed(0)});

                  var totalcent   = parseFloat(this.state.note20dollar) - 1;

                  var value20dollar        = parseFloat( 20  * totalcent).toFixed(2);

                  this.setState({ value20dollar: value20dollar });
                  var total       = parseFloat(this.state.totalmoney) - ( 20 );
                  this.setState({ totalmoney: total });


                  AsyncStorage.multiSet([
                                    ["value20dollar",         this.checknull(JSON.stringify(value20dollar))],
                                    ["note20dollar",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.note20dollar) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);

                  if( this.state.MyMoneyAudioType == 0 ){
                     Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                     Tts.speak( (parseInt(this.state.note20dollar) - 1) + ' 20 '+this.state.dollar);

                      if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak( (parseInt(this.state.note20dollar) - 1) + ' 20 '+this.state.Sdollar);
                      }
                  }
            }
      }



      onPress50dollaradd(){


              var totalcent   = parseFloat(this.state.note50dollar) + 1;
              var value50dollar  = ( 50 ) * totalcent;

              this.setState({ value50dollar: value50dollar, note50dollar: parseFloat(parseFloat(this.state.note50dollar) + 1).toFixed(0)});
              AsyncStorage.setItem('note50dollar', this.checknull(JSON.stringify(parseFloat(this.state.note50dollar) + 1) ));


              var total       = parseFloat(this.state.totalmoney) + ( 50 );
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

               if( this.state.MyMoneyAudioType == 0 ){
                      Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                      Tts.speak( (parseInt(this.state.note50dollar) + 1) + ' 50 '+this.state.dollar);

                      if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                           Tts.speak( (parseInt(this.state.note50dollar) + 1) + ' 50 '+this.state.Sdollar);

                      }
                }
      }

  

      onPress50dollarminus(){


            if( this.state.note50dollar == 0 ){

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

                  Alert.alert(this.state.nomore);

            }else{
                  this.setState({ note50dollar: parseFloat(parseFloat(this.state.note50dollar) - 1).toFixed(0)});

                  var totalcent   = parseFloat(this.state.note50dollar) - 1;

                  var value50dollar        = parseFloat( 50  * totalcent);

                  this.setState({ value50dollar: value50dollar });
                  var total       = parseFloat(this.state.totalmoney) - ( 50 );
                  this.setState({ totalmoney: total });


                  AsyncStorage.multiSet([
                                    ["value50dollar",         this.checknull(JSON.stringify(value50dollar))],
                                    ["note50dollar",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.note50dollar) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);


                  if( this.state.MyMoneyAudioType == 0 ){
                     Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                     Tts.speak( (parseInt(this.state.note50dollar) - 1) + ' 50 '+this.state.dollar);

                      if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                           Tts.speak( (parseInt(this.state.note50dollar) - 1) + ' 50 '+this.state.Sdollar);

                      }
                  }
            }
      }



      onPress100dollaradd(){


              var totalcent   = parseFloat(this.state.note100dollar) + 1;
              var value100dollar  = ( 100 ) * totalcent;

              this.setState({ value100dollar: value100dollar, note100dollar: parseFloat(parseFloat(this.state.note100dollar) + 1).toFixed(0)});
              AsyncStorage.setItem('note100dollar', this.checknull(JSON.stringify(parseFloat(this.state.note100dollar) + 1) ));


              var total       = parseFloat(this.state.totalmoney) + (100);
              
              this.setState({ totalmoney: total });
              AsyncStorage.setItem('totalmoney', this.checknull(JSON.stringify(parseFloat(total).toFixed(2)) ));

              if( this.state.MyMoneyAudioType == 0 ){
               // Tts.speak( (parseInt(this.state.note100dollar)  + 1) + ' 100 dollar');
               Tts.setDefaultLanguage(this.state.PrimaryLanguage);
               Tts.speak(JSON.stringify(parseFloat(this.state.note100dollar) + 1));
               Tts.speak( ' 100 '+this.state.dollar);

                      if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(JSON.stringify(parseFloat(this.state.note100dollar) + 1));
                          Tts.speak( ' 100 '+this.state.Sdollar);

                      }

              }
      }

  

      onPress100dollarminus(){


            if( this.state.note100dollar == 0 ){

                  if( this.state.MyMoneyAudioType == 0 ){
                      Tts.speak(this.state.nomore);
                  }

                  Alert.alert(this.state.nomore);

            }else{
                  this.setState({ note100dollar: parseFloat(parseFloat(this.state.note100dollar) - 1).toFixed(0)});
                   AsyncStorage.setItem('note100dollar', this.checknull(JSON.stringify(parseFloat(this.state.note100dollar) - 1) ));

                  var totalcent   = parseFloat(this.state.note100dollar) - 1;

                  var value100dollar        = parseFloat( 100  * totalcent).toFixed(0);

                  this.setState({ value100dollar: value100dollar });
                  var total       = parseFloat(this.state.totalmoney) - ( 100 );
                  this.setState({ totalmoney: total });


                  AsyncStorage.multiSet([
                                    ["value100dollar",         this.checknull(JSON.stringify(value100dollar))],
                                    ["note100dollar",          this.checknull(JSON.stringify(parseFloat(parseFloat(this.state.note100dollar) - 1).toFixed(0)))],
                                    ["totalmoney",          this.checknull(JSON.stringify(total))],
                                ]);

                  if( this.state.MyMoneyAudioType ==  0){
                     //Tts.speak((parseInt(this.state.note100dollar) - 1) +' 100 dollar');
                     Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                     Tts.speak(JSON.stringify(parseFloat(this.state.note100dollar) - 1));
                     Tts.speak(' 100 '+this.state.dollar);

                     if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(JSON.stringify(parseFloat(this.state.note100dollar) - 1));
                          Tts.speak(' 100 '+this.state.Sdollar);

                      }
                  }
            }
      }

      

      onPressResetMoney(){

            if( this.state.MyMoneyAudioType == 0 ){
                 Tts.speak(this.state.allmountresetnow);
            }

            this.setState({
                           totalmoney   : 0,
                           coin5cent    : 0,
                           value5cent   : 0,
                           coin10cent   : 0,
                           value10cent  : 0,
                           coin20cent   : 0,
                           value20cent  : 0,
                           coin50cent   : 0,
                           value50cent  : 0,
                           coin1dollar  : 0, 
                           value1dollar : 0, 
                           coin2dollar  : 0,
                           value2dollar : 0,
                           note5dollar  : 0,
                           value5dollar : 0,
                           note10dollar : 0,
                           value10dollar : 0,
                           note20dollar  : 0,
                           value20dollar : 0,
                           note50dollar  : 0,
                           value50dollar : 0,
                           note100dollar : 0,

                         });

           this.setState({ value100dollar: 0 });

           AsyncStorage.multiSet([
                                  ["coin5cent",       this.checknull(JSON.stringify(0))],
                                  ["coin10cent",      this.checknull(JSON.stringify(0))],
                                  ["coin20cent",      this.checknull(JSON.stringify(0))],
                                  ["coin50cent",      this.checknull(JSON.stringify(0))],
                                  ["coin1dollar",     this.checknull(JSON.stringify(0))],
                                  ["coin2dollar",     this.checknull(JSON.stringify(0))],
                                  ["note5dollar",     this.checknull(JSON.stringify(0))],
                                  ["note10dollar",    this.checknull(JSON.stringify(0))],
                                  ["note20dollar",    this.checknull(JSON.stringify(0))],
                                  ["note50dollar",    this.checknull(JSON.stringify(0))],
                                  ["note100dollar",   this.checknull(JSON.stringify(0))],
                                  ["totalmoney",      this.checknull(JSON.stringify(0))],
                                  ["pantryinbox",     this.checknull(JSON.stringify([]))],
                                  ["pantryaddedPrice",this.checknull(JSON.stringify(0))],

                              ]);

          AsyncStorage.multiSet([
                                ["cinema",          this.checknull(JSON.stringify('0'))],
                                ["cinema",          this.checknull(JSON.stringify('0'))],
                                ["cinema1",          this.checknull(JSON.stringify('0'))],
                                ["dancing",          this.checknull(JSON.stringify('0'))],
                                ["dancing1",          this.checknull(JSON.stringify('0'))],
                                ["sports",          this.checknull(JSON.stringify('0'))],
                                ["sports1",          this.checknull(JSON.stringify('0'))],
                                ["nightout",          this.checknull(JSON.stringify('0'))],
                                ["nightout1",          this.checknull(JSON.stringify('0'))],
                                ["AmountLeft",         this.checknull(JSON.stringify(this.state.totalmoney))],
                            ]);



           
            var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();

            formdata.append('user_id', this.state.user_id );

            try {
                let response = fetch( GVar.BaseUrl + GVar.ResetMoney,{
                                  method: 'POST',
                                  headers:{
                                              'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiQW1pdCBLdW1hciIsImVtYWlsIjoiYW1pdC5rdW1hckBhcHBpZGVhc2luYy5jb20iLCJkb2IiOiIxNTMxNDc3NTk5In0.NOcppFh3ep4AEf7Gr7SfX3du82uuSTMonGIM5ZNJNvs'

                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert("Message",responseJson);

                                            if( responseJson.status == '1'){

                                              this.setState({
                                                   totalmoney   : '0',
                                                   coin5cent    : '0',
                                                   value5cent   : '0',
                                                   coin10cent   : '0',
                                                   value10cent  : '0',
                                                   coin20cent   : '0',
                                                   value20cent  : '0',
                                                   coin50cent   : '0',
                                                   value50cent  : '0',
                                                   coin1dollar  : '0',
                                                   value1dollar : '0',
                                                   coin2dollar  : '0',
                                                   value2dollar : '0',
                                                   note5dollar  : '0',
                                                   value5dollar : '0',
                                                   note10dollar : '0',
                                                   value10dollar : '0',
                                                   note20dollar  : '0',
                                                   value20dollar : '0',
                                                   note50dollar  : '0',
                                                   value50dollar : '0',
                                                   note100dollar : '0',
                                                 });

                                                 this.setState({ value100dollar: '0' });

                                                 AsyncStorage.multiSet([
                                                                        ["coin5cent",       '0'],
                                                                        ["coin10cent",      '0'],
                                                                        ["coin20cent",      '0'],
                                                                        ["coin50cent",      '0'],
                                                                        ["coin1dollar",     '0'],
                                                                        ["coin2dollar",     '0'],
                                                                        ["note5dollar",     '0'],
                                                                        ["note10dollar",    '0'],
                                                                        ["note20dollar",    '0'],
                                                                        ["note50dollar",    '0'],
                                                                        ["note100dollar",   '0'],
                                                                        ["totalmoney",      '0'],
                                                                    ]);

                                              Actions.MyMoney();

                                              Alert.alert("Message",responseJson.message);

                                              if( this.state.MyMoneyAudioType == 0 ){
                                                 Tts.speak('Reset All money successfull');
                                              }




                                            }else{

                                              this.setState({
                                                   totalmoney   : '0',
                                                   coin5cent    : '0',
                                                   value5cent   : '0',
                                                   coin10cent   : '0',
                                                   value10cent  : '0',
                                                   coin20cent   : '0',
                                                   value20cent  : '0',
                                                   coin50cent   : '0',
                                                   value50cent  : '0',
                                                   coin1dollar  : '0',
                                                   value1dollar : '0',
                                                   coin2dollar  : '0',
                                                   value2dollar : '0',
                                                   note5dollar  : '0',
                                                   value5dollar : '0',
                                                   note10dollar : '0',
                                                   value10dollar : '0',
                                                   note20dollar  : '0',
                                                   value20dollar : '0',
                                                   note50dollar  : '0',
                                                   value50dollar : '0',
                                                   note100dollar : '0',
                                                 });

                                                 this.setState({ value100dollar: '0' });

                                                 AsyncStorage.multiSet([
                                                                        ["coin5cent",       0],
                                                                        ["coin10cent",      0],
                                                                        ["coin20cent",      0],
                                                                        ["coin50cent",      0],
                                                                        ["coin1dollar",     0],
                                                                        ["coin2dollar",     0],
                                                                        ["note5dollar",     0],
                                                                        ["note10dollar",    0],
                                                                        ["note20dollar",    0],
                                                                        ["note50dollar",    0],
                                                                        ["note100dollar",   0],
                                                                        ["totalmoney",      0],
                                                                    ]);
                                                                      
                                            }


                                                
                                           });
              
                
             }catch (error) {

              
            }


          }



      onChangeValue = ( value , name) => {


            var user_id           = this.state.user_id;
            var coin5cent         = this.state.coin5cent;
            var coin10cent        = this.state.coin10cent;
            var coin20cent        = this.state.coin20cent;
            var coin50cent        = this.state.coin50cent;
            var coin1dollar       = this.state.coin1dollar;
            var coin2dollar       = this.state.coin2dollar;
            var note5dollar       = this.state.note5dollar;
            var note10dollar      = this.state.note10dollar;
            var note20dollar      = this.state.note20dollar;
            var note50dollar      = this.state.note50dollar;
            var note100dollar     = this.state.note100dollar;
            var totalmoney        = this.state.totalmoney;


            var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();

            formdata.append('user_id',        user_id );
            formdata.append('money',          totalmoney );
            formdata.append('5centcoin',      coin5cent );
            formdata.append('10centcoin',     coin10cent );
            formdata.append('20centcoin',     coin20cent );
            formdata.append('50centcoin',     coin50cent );
            formdata.append('1dollarcoin',    coin1dollar );
            formdata.append('2dollarcoin',    coin2dollar );
            formdata.append('5dollarnote',    note5dollar );
            formdata.append('10dollarnote',   note10dollar );
            formdata.append('20dollarnote',   note20dollar );
            formdata.append('50dollarnote',   note50dollar );
            formdata.append('100dollarnote',  note100dollar );

          // Alert.alert( coin5cent +" "+  coin10cent  +" "+coin20cent  +" "+coin50cent +" "+coin1dollar+" "+coin2dollar+" "+note5dollar +" "+note10dollar+" "+note20dollar+" "+note50dollar+" "+note100dollar +" "+ totalmoney)


            if( totalmoney > 0 ){

                    try {
                      let response = fetch( GVar.BaseUrl + GVar.UpdateMoney,{
                                        method: 'POST',
                                        headers:{
                                                    'token' : token
                                                },
                                        body: formdata,        
                                        }).then((response) => response.json())
                                       .then((responseJson) => {

                                      // Alert.alert(JSON.stringify(responseJson));

                                        if( responseJson.status == '1'){

                                        
                                          
                                        }else if( responseJson.status == '0'){

                                         //Alert.alert(responseJson.message);
                                          
                                        }
                                            
                                       });
                   }catch (error) {

                    if( error.line == 18228 ){
                      
                      
                    }

                  }

            }

            if( value == '5cent'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseFloat(Valueinserted[1]).toFixed(2);
                    var actualValue_100         =  parseFloat(Valueinserted[1]).toFixed(2) * 100;
                    var new_coin5cent           =  parseFloat(actualValue_100 / 5).toFixed(0) ;
                    var isDivide                =  actualValue_100 % 5;


                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total               = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_5centvalue          = parseFloat((this.state.coin5cent) * ( 5/ 100));

                        //Alert.alert(JSON.stringify(old_5centvalue));

                        var addValue  = parseFloat( + parseFloat(old_total) +  + parseFloat(actualValue).toFixed(2)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(addValue) - old_5centvalue).toFixed(2);

                        this.setState({ coin5cent : new_coin5cent  , totalmoney: totalmoney, value5cent : actualValue});
                        AsyncStorage.multiSet([
                                                ["coin5cent",       this.checknull(new_coin5cent)],
                                                ["value5cent",      this.checknull(actualValue)],
                                                ["totalmoney",      this.checknull(totalmoney)],
                                            ]);

                        if( this.state.MyMoneyAudioType == 0 ){
                           //Tts.speak(actualValue+'*'+new_coin5cent+'='+totalmoney);
                           Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                            Tts.speak(new_coin5cent + " 5 cent" + " makes " + actualValue+" " +this.state.dollar );

                            if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_coin5cent + " 5 cent" + " makes " + actualValue+" " +this.state.Sdollar );

                            }
                        }


                    }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);

                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                    }

            }

            if( value == '10cent'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseFloat(Valueinserted[1]).toFixed(2);
                    var actualValue_100         =  parseFloat(Valueinserted[1]).toFixed(2) * 100;
                    var new_coin10cent          =  parseFloat(actualValue_100 / 10).toFixed(0) ;
                    var isDivide                =  actualValue_100 % 10;


                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total               = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_5centvalue          = parseFloat((this.state.coin10cent) * ( 10 / 100));

                        

                        var addValue  = parseFloat( + parseFloat(old_total) +  + parseFloat(actualValue).toFixed(2)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(addValue) - old_5centvalue).toFixed(2);

                        //Alert.alert("Message",+ parseFloat(old_total) +  + parseFloat(actualValue).toFixed(2));
                        //Alert.alert(addValue);

                        this.setState({ coin10cent : new_coin10cent  , totalmoney: totalmoney, value10cent : actualValue});
                        AsyncStorage.multiSet([
                                                ["coin10cent",       this.checknull(JSON.stringify(new_coin10cent))],
                                                ["value10cent",      this.checknull(JSON.stringify(actualValue))],
                                                ["totalmoney",      this.checknull(JSON.stringify(totalmoney))],
                                            ]);

                        if( this.state.MyMoneyAudioType == 0 ){
                           Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                           Tts.speak(new_coin10cent + " 10 cent" + " makes " + actualValue +" "+this.state.dollar);

                           if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_coin10cent + " 10 cent" + " makes " + actualValue +" "+this.state.Sdollar);

                            }
                        }

                    }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                    }
            }

            if( value == '20cent'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseFloat(Valueinserted[1]).toFixed(2);
                    var actualValue_100         =  parseFloat(Valueinserted[1]).toFixed(2) * 100;
                    var new_coin20cent          =  parseFloat(actualValue_100 / 20).toFixed(0) ;
                    var isDivide                =  actualValue_100 % 20;


                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total               = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_5centvalue          = parseFloat((this.state.coin20cent) * ( 20 / 100));

                        

                        var addValue    = parseFloat( + parseFloat(old_total) +  + parseFloat(actualValue).toFixed(2)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(addValue) - old_5centvalue).toFixed(2);

                        //Alert.alert("Message",+ parseFloat(old_total) +  + parseFloat(actualValue).toFixed(2));
                        //Alert.alert(JSON.stringify(old_5centvalue));

                        this.setState({ coin20cent : new_coin20cent  , totalmoney: totalmoney, value20cent : actualValue});

                        AsyncStorage.multiSet([
                                                ["coin20cent",       this.checknull(JSON.stringify(new_coin20cent))],
                                                ["value20cent",      this.checknull(JSON.stringify(actualValue))],
                                                ["totalmoney",       this.checknull(JSON.stringify(totalmoney))],
                                            ]);

                        if( this.state.MyMoneyAudioType == 0 ){

                           Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                           Tts.speak(new_coin20cent + " 20 cent" + " makes " + actualValue+" "+this.state.dollar );

                           if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_coin20cent + " 20 cent" + " makes " + actualValue+" "+this.state.Sdollar );

                            }
                        }

                    }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                    }
            }


            if( value == '50cent'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseFloat(Valueinserted[1]).toFixed(2);
                    var actualValue_100         =  parseFloat(Valueinserted[1]).toFixed(2) * 100;
                    var new_coin50cent          =  parseFloat(actualValue_100 / 50).toFixed(0) ;
                    var isDivide                =  actualValue_100 % 50;


                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total               = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_5centvalue          = parseFloat((this.state.coin50cent) * ( 50 / 100));

                        

                        var addValue    = parseFloat( + parseFloat(old_total) +  + parseFloat(actualValue).toFixed(2)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(addValue) - old_5centvalue).toFixed(2);

                        //Alert.alert("Message",+ parseFloat(old_total) +  + parseFloat(actualValue).toFixed(2));
                        //Alert.alert(JSON.stringify(old_5centvalue));

                        this.setState({ coin50cent : new_coin50cent  , totalmoney: totalmoney, value50cent : actualValue});

                        AsyncStorage.multiSet([
                                                ["coin50cent",       this.checknull(JSON.stringify(new_coin50cent))],
                                                ["value50cent",      this.checknull(JSON.stringify(actualValue))],
                                                ["totalmoney",       this.checknull(JSON.stringify(totalmoney))],
                                            ]);

                        if( this.state.MyMoneyAudioType == 0 ){

                           Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                           Tts.speak(new_coin50cent + " 50 cent" + " makes " + actualValue+" "+this.state.dollar );

                           if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_coin50cent + " 50 cent" + " makes " + actualValue+" "+this.state.Sdollar );

                            }
                        }

                    }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }

                    }
            }

            if( value == '1dollar'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseInt(Valueinserted[1]);
                    var new_note1dollar         =  parseFloat(parseInt(Valueinserted[1]) / 1).toFixed(0) ;
                    var isDivide                =  actualValue % 1;

                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_1     = parseInt(this.state.coin1dollar) * 1 ;

                        var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_1)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                        
                        this.setState({ coin1dollar : new_note1dollar  , totalmoney: totalmoney, value1dollar : actualValue});

                        AsyncStorage.multiSet([
                                                ["coin1dollar",       this.checknull(JSON.stringify(new_note1dollar))],
                                                ["value1dollar",       this.checknull(SON.stringify(actualValue))],
                                                ["totalmoney",        this.checknull(JSON.stringify(totalmoney))],
                                            ]);

                        if( this.state.MyMoneyAudioType == 0 ){

                           Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                           Tts.speak(new_note1dollar + " 1 dollar" + " makes " + actualValue+" "+this.state.dollar );

                           if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_note1dollar + " 1 dollar" + " makes " + actualValue+" "+this.state.Sdollar );


                            }
                        }

                    }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                    }
            }

            if( value == '2dollar'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseInt(Valueinserted[1]);
                    var new_note2dollar       =  parseFloat(parseInt(Valueinserted[1]) / 2).toFixed(0) ;
                    var isDivide                =  actualValue % 2;

                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_2     = parseInt(this.state.coin2dollar) * 2 ;

                        var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_2)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                        
                        this.setState({ coin2dollar : new_note2dollar  , totalmoney: totalmoney, value2dollar : actualValue});

                        AsyncStorage.multiSet([
                                                ["coin2dollar",        this.checknull(JSON.stringify(new_note2dollar))],
                                                ["value2dollar",       this.checknull(JSON.stringify(actualValue))],
                                                ["totalmoney",         this.checknull(JSON.stringify(totalmoney))],
                                            ]);

                        if( this.state.MyMoneyAudioType == 0 ){

                          Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                          Tts.speak(new_note2dollar + " 2 dollar" + " makes " + actualValue+" "+ this.state.dollar);

                          if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_note2dollar + " 2 dollar" + " makes " + actualValue+" "+ this.state.Sdollar);


                            }
                        }

                    }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                    }
            }

            if( value == '5dollar'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseInt(Valueinserted[1]);
                    var new_note5dollar         =  parseFloat(parseInt(Valueinserted[1]) / 5).toFixed(0) ;
                    var isDivide                =  actualValue % 5;

                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_5       = parseInt(this.state.note5dollar) * 5 ;

                        var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_5)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                        
                        this.setState({ note5dollar : new_note5dollar  , totalmoney: totalmoney, value5dollar : actualValue});

                       AsyncStorage.multiSet([
                                              ["note5dollar",        this.checknull(JSON.stringify(new_note5dollar))],
                                              ["value5dollar",       this.checknull(JSON.stringify(actualValue))],
                                              ["totalmoney",         this.checknull(JSON.stringify(totalmoney))],
                                          ]);

                       if( this.state.MyMoneyAudioType == 0 ){

                           Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                           Tts.speak(new_note5dollar + " 5 dollar" + " makes " + actualValue+" "+this.state.dollar );

                           if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_note5dollar + " 5 dollar" + " makes " + actualValue+" "+this.state.Sdollar );


                            }
                        }

                    }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                    }
            }

            if( value == '10dollar'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseInt(Valueinserted[1]);

                    var new_note10dollar       =  parseFloat(parseInt(Valueinserted[1]) / 10).toFixed(0) ;
                    var isDivide                =  actualValue % 10;

                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_10      = parseInt(this.state.note10dollar) * 10 ;

                        var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_10)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                        
                        this.setState({ note10dollar : new_note10dollar  , totalmoney: totalmoney, value10dollar : actualValue});

                        AsyncStorage.multiSet([
                                              ["note10dollar",        this.checknull(JSON.stringify(new_note10dollar))],
                                              ["value10dollar",      this.checknull(JSON.stringify(actualValue))],
                                              ["totalmoney",          this.checknull(JSON.stringify(totalmoney))],
                                          ]);

                        if( this.state.MyMoneyAudioType == 0 ){

                           Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                           Tts.speak(new_note10dollar + " 10 dollar" + " makes " + actualValue+" "+this.state.dollar );

                           if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_note10dollar + " 10 dollar" + " makes " + actualValue+" "+this.state.Sdollar );


                            }
                        }


                    }else{

                        if( !isNaN( parseInt(Valueinserted[1] * 10 ))) {
                          var actualValue             =  parseInt(Valueinserted[1] * 10 );

                          var new_note10dollar       =  parseFloat(actualValue / 10).toFixed(0) ;

                          var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                          var old_10      = parseInt(this.state.note10dollar) * 10 ;

                          var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_10)).toFixed(2);
                          var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                          
                          this.setState({ note10dollar : new_note10dollar  , totalmoney: totalmoney, value10dollar : actualValue});

                          AsyncStorage.multiSet([
                                                ["note10dollar",        this.checknull(JSON.stringify(new_note10dollar))],
                                                ["value10dollar",       this.checknull(JSON.stringify(actualValue))],
                                                ["totalmoney",          this.checknull(JSON.stringify(totalmoney))],
                                            ]);

                          if( this.state.MyMoneyAudioType == 0 ){

                            Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                            Tts.speak(new_note10dollar + " 10 dollar" + " makes " + actualValue+" "+this.state.dollar );

                            if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_note10dollar + " 10 dollar" + " makes " + actualValue+" "+this.state.Sdollar );


                            }
                          }
                        }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                        }
                        
                          
                    }
            }


            if( value == '20dollar'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseInt(Valueinserted[1]);
                    var new_note20dollar        =  parseFloat(parseInt(Valueinserted[1]) / 20).toFixed(0) ;
                    var isDivide                =  actualValue % 20;

                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_20     = parseInt(this.state.note20dollar) * 20 ;

                        var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_20)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                        
                        this.setState({ note20dollar : new_note20dollar  , totalmoney: totalmoney, value20dollar : actualValue});

                        AsyncStorage.multiSet([
                                              ["note20dollar",        this.checknull(JSON.stringify(new_note20dollar))],
                                              ["value20dollar",       this.checknull(JSON.stringify(actualValue))],
                                              ["totalmoney",          this.checknull(JSON.stringify(totalmoney))],
                                          ]);

                        if( this.state.MyMoneyAudioType == 0 ){

                           Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                           Tts.speak(new_note20dollar + " 20 dollar" + " makes " + actualValue+" "+this.state.dollar );

                            if( !this.state.isSingle ){

                                Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                Tts.speak(new_note20dollar + " 20 dollar" + " makes " + actualValue+" "+this.state.Sdollar );



                            }
                        }

                    }else{

                      alert(parseInt(Valueinserted[1] * 20 ))
                        if( !isNaN(parseInt(Valueinserted[1] * 20 ))) {

                            var actualValue             =  parseInt(Valueinserted[1] * 20 );
                            var new_note20dollar        =  parseFloat(parseInt(Valueinserted[1]) / 20).toFixed(0) ;
                    
                      

                            var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                            var old_20     = parseInt(this.state.note20dollar) * 20 ;

                            var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_20)).toFixed(2);
                            var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                            
                            this.setState({ note20dollar : new_note20dollar  , totalmoney: totalmoney, value20dollar : actualValue});

                            AsyncStorage.multiSet([
                                                  ["note20dollar",        this.checknull(JSON.stringify(new_note20dollar))],
                                                  ["value20dollar",       this.checknull(JSON.stringify(actualValue))],
                                                  ["totalmoney",          this.checknull(JSON.stringify(totalmoney))],
                                              ]);

                            if( this.state.MyMoneyAudioType == 0 ){

                              Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                              Tts.speak(new_note20dollar + " 20 dollar" + " makes " + actualValue+" "+this.state.dollar );

                              if( !this.state.isSingle ){

                                  Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                  Tts.speak(new_note20dollar + " 20 dollar" + " makes " + actualValue+" "+this.state.Sdollar );

                              }
                            }
                        }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                        }
                       
                    }
            }

            if( value == '50dollar'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseInt(Valueinserted[1]);
                    var new_note50dollar        =  parseFloat(parseInt(Valueinserted[1]) / 50).toFixed(0) ;
                    var isDivide                =  actualValue % 50;

                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_50     = parseInt(this.state.note50dollar) * 50 ;

                        var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_50)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                        
                        this.setState({ note50dollar : new_note50dollar  , totalmoney: totalmoney, value50dollar : actualValue});

                        AsyncStorage.multiSet([
                                              ["note50dollar",        this.checknull(JSON.stringify(new_note50dollar))],
                                              ["value50dollar",       this.checknull(JSON.stringify(actualValue))],
                                              ["totalmoney",          this.checknull(JSON.stringify(totalmoney))],
                                          ]);

                        if( this.state.MyMoneyAudioType == 0 ){

                            Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                            Tts.speak(new_note50dollar + " 50 dollar" + " makes " + actualValue+" "+this.state.dollar );

                            if( !this.state.isSingle ){

                                  Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                  Tts.speak(new_note50dollar + " 50 dollar" + " makes " + actualValue+" "+this.state.Sdollar );


                            }
                        }

                    }else{

                        if( !isNaN(parseInt(Valueinserted[1] * 50 ))) {

                          var actualValue             =  parseInt(Valueinserted[1] * 50 );
                          var new_note50dollar        =  parseFloat(actualValue / 50).toFixed(0) ;
                      

                          var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                          var old_50     = parseInt(this.state.note50dollar) * 50 ;

                          var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_50)).toFixed(2);
                          var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                          
                          this.setState({ note50dollar : new_note50dollar  , totalmoney: totalmoney, value50dollar : actualValue});

                          AsyncStorage.multiSet([
                                                ["note50dollar",        this.checknull(JSON.stringify(new_note50dollar))],
                                                ["value50dollar",       this.checknull(JSON.stringify(actualValue))],
                                                ["totalmoney",          this.checknull(JSON.stringify(totalmoney))],
                                            ]);

                          if( this.state.MyMoneyAudioType == 0 ){

                              Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                              Tts.speak(new_note50dollar + " 50 dollar" + " makes " + actualValue+" "+this.state.dollar );

                              if( !this.state.isSingle ){

                                  Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                  Tts.speak(new_note50dollar + " 50 dollar" + " makes " + actualValue+" "+this.state.Sdollar );

                              }
                          }
                        }else{

                          Alert.alert(this.state.PleaseinsertvalidValue);
                          if( this.state.MyMoneyAudioType == 0 ){
                           Tts.speak(this.state.PleaseinsertvalidValue);
                          }
                        }


                        
                    }
            }

            if( value == '100dollar'){

                    var Valueinserted  = name.split("$");

                    var actualValue             =  parseInt(Valueinserted[1]);
                    var new_note100dollar       =  parseFloat(parseInt(Valueinserted[1]) / 100).toFixed(0) ;
                    var isDivide                =  actualValue % 100;

                    if( JSON.stringify(isDivide) == 0 ){

                        var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                        var old_100     = parseInt(this.state.note100dollar) * 100 ;

                        var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_100)).toFixed(2);
                        var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                        
                        this.setState({ note100dollar : new_note100dollar  , totalmoney: totalmoney, value100dollar : actualValue});

                         AsyncStorage.multiSet([
                                              ["note100dollar",         this.checknull(JSON.stringify(new_note100dollar))],
                                              ["value100dollar",        this.checknull(JSON.stringify(actualValue))],
                                              ["totalmoney",            this.checknull(JSON.stringify(totalmoney))],
                                          ]);

                         if( this.state.MyMoneyAudioType == 0 ){

                            Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                            Tts.speak(new_note100dollar + " 100 dollar" + " makes " + actualValue+" "+this.state.dollar );
                            
                            if( !this.state.isSingle ){

                                  Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                  Tts.speak(new_note100dollar + " 100 dollar" + " makes " + actualValue+" "+this.state.Sdollar );

                            }
                        }

                    }else{

                         if( !isNaN(parseInt(Valueinserted[1] * 100 ))) {


                            var actualValue             =  parseInt(Valueinserted[1] * 100 );
                            var new_note100dollar       =  parseFloat(actualValue / 100).toFixed(0) ;
                        

                            var old_total   = parseFloat(this.state.totalmoney).toFixed(2);
                            var old_100     = parseInt(this.state.note100dollar) * 100 ;

                            var minusValue  = parseFloat(parseFloat(old_total) - parseInt(old_100)).toFixed(2);
                            var totalmoney  = parseFloat(parseFloat(minusValue) + actualValue).toFixed(2);
                            
                            this.setState({ note100dollar : new_note100dollar  , totalmoney: totalmoney, value100dollar : actualValue});

                             AsyncStorage.multiSet([
                                                  ["note100dollar",         this.checknull(JSON.stringify(new_note100dollar))],
                                                  ["value100dollar",        this.checknull(JSON.stringify(actualValue))],
                                                  ["totalmoney",            this.checknull(JSON.stringify(totalmoney))],
                                              ]);

                             if( this.state.MyMoneyAudioType == 0 ){

                                Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                                Tts.speak(new_note100dollar + " 100 dollar" + " makes " + actualValue+" "+this.state.dollar);
                               
                                if( !this.state.isSingle ){

                                      Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                                       Tts.speak(new_note100dollar + " 100 dollar" + " makes " + actualValue+" "+this.state.Sdollar);

                                }
                            }

                         }else{

                            Alert.alert(this.state.PleaseinsertvalidValue);
                            if( this.state.MyMoneyAudioType == 0 ){
                             Tts.speak(this.state.PleaseinsertvalidValue);
                            }
                        }

                    }
            }

      }

  checknull(value){
  if(value){
    return value;
  }
  else {
    return '';
  }
}

      onChangeCurrency = ( value , name) => {



        if (/^\d+$/.test(name))
          {

             val = parseFloat(name);

            console.log(name,"validnumber")
            console.log(value,"value")

            if( isNaN(val) || Number.isInteger(name) ){

              //if( name === null || name === '' || name === 0 || !name ){

                    if( value === '5cent'){
                     this.setState({ coin5cent :  ' '});
                    }

                    if( value === '10cent' ){

                      this.setState({coin10cent : ''});
                    }

                    if( value === '20cent' ){

                      this.setState({ coin20cent : ''});
                    }

                    if( value === '50cent' ){

                      this.setState({ coin50cent : ''});
                    }

                    if( value === '1dollar' ){

                      this.setState({ coin1dollar : ''});
                    }

                    if( value === '2dollar' ){

                      this.setState({ coin2dollar : ''});
                    }

                    if( value === '5dollar' ){

                      this.setState({ note5dollar : ''});
                    }

                    if( value === '10dollar' ){

                      this.setState({ note10dollar : ''});
                    }

                    if( value === '20dollar' ){

                      this.setState({ note20dollar : ''});
                    }

                    if( value === '50dollar' ){

                      this.setState({ note50dollar : ''});
                    }

                    if( value === '100dollar' ){

                      this.setState({ note100dollar : ''});
                    }

              //}else{

                //alert("Invalid Money");
              //}

            }else{

              var user_id           = this.state.user_id;
              var coin5cent         = this.state.coin5cent;
              var coin10cent        = this.state.coin10cent;
              var coin20cent        = this.state.coin20cent;
              var coin50cent        = this.state.coin50cent;
              var coin1dollar       = this.state.coin1dollar;
              var coin2dollar       = this.state.coin2dollar;
              var note5dollar       = this.state.note5dollar;
              var note10dollar      = this.state.note10dollar;
              var note20dollar      = this.state.note20dollar;
              var note50dollar      = this.state.note50dollar;
              var note100dollar     = this.state.note100dollar;
              var totalmoney        = this.state.totalmoney;


              var GVar = GlobalVariables.getApiUrl();

              let formdata = new FormData();

              formdata.append('user_id',        user_id );
              formdata.append('money',          totalmoney );
              formdata.append('5centcoin',      coin5cent );
              formdata.append('10centcoin',     coin10cent );
              formdata.append('20centcoin',     coin20cent );
              formdata.append('50centcoin',     coin50cent );
              formdata.append('1dollarcoin',    coin1dollar );
              formdata.append('2dollarcoin',    coin2dollar );
              formdata.append('5dollarnote',    note5dollar );
              formdata.append('10dollarnote',   note10dollar );
              formdata.append('20dollarnote',   note20dollar );
              formdata.append('50dollarnote',   note50dollar );
              formdata.append('100dollarnote',  note100dollar );

            // Alert.alert( coin5cent +" "+  coin10cent  +" "+coin20cent  +" "+coin50cent +" "+coin1dollar+" "+coin2dollar+" "+note5dollar +" "+note10dollar+" "+note20dollar+" "+note50dollar+" "+note100dollar +" "+ totalmoney)


              if( totalmoney > 0 ){

                      try {
                        let response = fetch( GVar.BaseUrl + GVar.UpdateMoney,{
                                          method: 'POST',
                                          headers:{
                                                      'token' : token
                                                  },
                                          body: formdata,        
                                          }).then((response) => response.json())
                                         .then((responseJson) => {

                                        // Alert.alert(JSON.stringify(responseJson));

                                          if( responseJson.status == '1'){

                                          
                                            
                                          }else if( responseJson.status == '0'){

                                           //Alert.alert(responseJson.message);
                                            
                                          }
                                              
                                         });
                     }catch (error) {

                      if( error.line == 18228 ){
                        
                        
                      }

                    }

              }

              if( value == '5cent'){

                var value5cent = parseFloat(this.state.value5cent).toFixed(2);
                var totalmoney = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value5cent) + (name * (5 / 100))).toFixed(2) ;

                this.setState({ value5cent: parseFloat((5 / 100 ) * name).toFixed(2) , totalmoney: newtotalmoney, coin5cent: name  });

                AsyncStorage.multiSet([
                                                ["value5cent",        this.checknull(JSON.stringify( parseFloat((5 / 100 ) * name).toFixed(2)))],
                                                ["totalmoney",         this.checknull(JSON.stringify(newtotalmoney))],
                                                ["coin5cent",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){

                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    
                    Tts.speak(name+' 5 '+this.state.cents);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 5 '+this.state.Scents);

                    }


                }
                
              }

              if( value == '10cent'){

                var value10cent = parseFloat(this.state.value10cent).toFixed(2);
                var totalmoney = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value10cent) + (name * (10 / 100))).toFixed(2) ;

                this.setState({ value10cent: parseFloat((10 / 100 ) * name).toFixed(2) , totalmoney: newtotalmoney, coin10cent: name   });
                

                AsyncStorage.multiSet([
                                                ["value10cent",        this.checknull(JSON.stringify( parseFloat((10 / 100 ) * name).toFixed(2)))],
                                                ["totalmoney",          this.checknull(JSON.stringify(newtotalmoney))],
                                                ["coin10cent",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){
                    
                     Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                     Tts.speak(name+' 10 '+ this.state.cents);

                     if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 10 '+ this.state.Scents);


                    }
                }
              }

              if( value == '20cent'){

                var value20cent = parseFloat(this.state.value20cent).toFixed(2);
                var totalmoney = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value20cent) + (name * (20 / 100))).toFixed(2) ;

                this.setState({ value20cent: parseFloat((20 / 100 ) * name).toFixed(2), totalmoney: newtotalmoney, coin20cent: name  });
               
                AsyncStorage.multiSet([
                                                ["value20cent",         this.checknull(JSON.stringify(parseFloat((20 / 100 ) * name).toFixed(2)))],
                                                ["totalmoney",          this.checknull(JSON.stringify(newtotalmoney))],
                                                ["coin20cent",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name+' 20 '+this.state.cents);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 20 '+ this.state.Scents);


                    }
                }
              }


              if( value == '50cent'){

                var value50cent = parseFloat(this.state.value50cent).toFixed(2);
                var totalmoney = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value50cent) + (name * (50 / 100))).toFixed(2) ;

                this.setState({ value50cent: parseFloat((50 / 100 ) * name).toFixed(2), totalmoney: newtotalmoney , coin50cent: name  });
                
                AsyncStorage.multiSet([
                                                ["value50cent",         this.checknull(JSON.stringify(parseFloat((50 / 100 ) * name).toFixed(2)))],
                                                ["totalmoney",          this.checknull(JSON.stringify(newtotalmoney))],
                                                ["coin50cent",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name+' 50 '+this.state.cents);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 50 '+ this.state.Scents);


                    }
                }

              }

              if( value == '1dollar'){

                var value1dollar = parseFloat(this.state.value1dollar).toFixed(2);
                var totalmoney = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value1dollar) + (name * 1)).toFixed(2) ;

                this.setState({ value1dollar: parseFloat(1 * name).toFixed(2) , totalmoney: newtotalmoney, coin1dollar: name  });
                
                AsyncStorage.multiSet([
                                                ["value1dollar",         this.checknull(JSON.stringify(parseFloat(1 * name).toFixed(2)))],
                                                ["totalmoney",           this.checknull(JSON.stringify(newtotalmoney))],
                                                ["coin1dollar",          this.checknull(JSON.stringify(name))],
                                            ]);

                if(this.state.MyMoneyAudioType == 0 ){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name+' 1 '+this.state.dollar);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 1 '+this.state.Sdollar);



                    }
                }

              }

              if( value == '2dollar'){

                var value2dollar = parseFloat(this.state.value2dollar).toFixed(2);
                var totalmoney = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value2dollar) + (name * 2)).toFixed(2) ;

                this.setState({ value2dollar: parseFloat(parseFloat(2 * name).toFixed(2)).toFixed(2), totalmoney: newtotalmoney, coin2dollar: name  });
                
                AsyncStorage.multiSet([
                                                ["value2dollar",        this.checknull(JSON.stringify( parseFloat(2 * name).toFixed(2)))],
                                                ["totalmoney",           this.checknull(JSON.stringify(newtotalmoney))],
                                                ["coin1dollar",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name+' 2 '+this.state.dollar);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 2 '+this.state.Sdollar);



                    }
                }
              }

              if( value == '5dollar'){

                var value5dollar  = parseFloat(this.state.value5dollar).toFixed(2);
                var totalmoney    = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value5dollar) + (name * 5)).toFixed(2) ;

                this.setState({ value5dollar: parseFloat(parseFloat(5 * name).toFixed(2)).toFixed(2), totalmoney: newtotalmoney, note5dollar: name  });
             
                AsyncStorage.multiSet([
                                                ["value5dollar",         this.checknull(parseFloat(5 * name).toFixed(2))],
                                                ["totalmoney",           this.checknull(newtotalmoney)],
                                                ["note5dollar",          this.checknull(name)],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name+' 5 '+this.state.dollar);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 5 '+this.state.Sdollar);



                    }
                }
              }

              if( value == '10dollar'){

                var value10dollar  = parseFloat(this.state.value10dollar).toFixed(2);
                var totalmoney    = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value10dollar) + (name * 10)).toFixed(2) ;

                this.setState({ value10dollar: parseFloat(parseFloat(10 * name).toFixed(2)).toFixed(2), totalmoney: newtotalmoney,  note10dollar: name });
              
                AsyncStorage.multiSet([
                                                ["value10dollar",        this.checknull(JSON.stringify( parseFloat( 10 * name ).toFixed(2)))],
                                                ["totalmoney",            this.checknull(JSON.stringify(newtotalmoney))],
                                                ["note10dollar",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType ==  0){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name+' 10 '+this.state.dollar);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 10 '+this.state.Sdollar);



                    }
                }
              }


              if( value == '20dollar'){

                var value20dollar  = parseFloat(this.state.value20dollar).toFixed(2);
                var totalmoney    = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value20dollar) + (name * 20)).toFixed(2) ;

                this.setState({ value20dollar: parseFloat(parseFloat(20 * name).toFixed(2)).toFixed(2), totalmoney: newtotalmoney, note20dollar: name   });
               

                AsyncStorage.multiSet([
                                                ["value20dollar",         this.checknull(JSON.stringify(parseFloat( 20 * name ).toFixed(2)))],
                                                ["totalmoney",            this.checknull(JSON.stringify(newtotalmoney))],
                                                ["note20dollar",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name+' 20 '+this.state.dollar);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 20 '+this.state.Sdollar);



                    }
                }
              }

              if( value == '50dollar'){

                var value50dollar  = parseFloat(this.state.value50dollar).toFixed(2);
                var totalmoney    = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value50dollar) + (name * 50)).toFixed(2) ;

                this.setState({ value50dollar: parseFloat(parseFloat(50 * name).toFixed(2)).toFixed(2), totalmoney: newtotalmoney, note50dollar: name  });
               
                AsyncStorage.multiSet([
                                                ["value50dollar",         this.checknull(JSON.stringify(parseFloat( 50 * name ).toFixed(2)))],
                                                ["totalmoney",            this.checknull(JSON.stringify(newtotalmoney))],
                                                ["note50dollar",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name+' 50 '+this.state.dollar);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name+' 50 '+this.state.Sdollar);



                    }
                }
              }

              if( value == '100dollar'){

                var value100dollar  = parseFloat(this.state.value100dollar).toFixed(2);
                var totalmoney    = parseFloat(this.state.totalmoney).toFixed(2);

                var newtotalmoney   =  parseFloat((totalmoney - value100dollar) + (name * 100)).toFixed(2) ;

                this.setState({ value100dollar : parseFloat(parseFloat(100 * name).toFixed(2)).toFixed(2), totalmoney: newtotalmoney, note100dollar: name  });

                AsyncStorage.multiSet([
                                                ["value100dollar",         this.checknull(JSON.stringify(parseFloat( 100 * name ).toFixed(2)))],
                                                ["totalmoney",             this.checknull(JSON.stringify(newtotalmoney))],
                                                ["note100dollar",          this.checknull(JSON.stringify(name))],
                                            ]);

                if( this.state.MyMoneyAudioType == 0 ){
                    Tts.setDefaultLanguage(this.state.PrimaryLanguage);
                    Tts.speak(name);
                    Tts.speak(' 100 '+this.state.dollar);

                    if( !this.state.isSingle ){

                          Tts.setDefaultLanguage(this.state.SeconderyLanguage);
                          Tts.speak(name);
                          Tts.speak(' 100 '+this.state.Sdollar);



                    }
                }
              }


            }

          }
          else{

              alert("please use numeric");
          }

      }
    


  setCamera1(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
      

      let source = { uri: response.uri };

      //alert(source)

      
      var token = this.state.token;
      var user_id = this.state.user_id;
      var Selfietype = this.state.Selfietype;


      
let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie1', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
        
        this.setState({ Localprofileimage : source});

        AsyncStorage.multiSet([
                                    ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                   
                                ]);

        //Alert.alert(Selfietype)
        if( Selfietype === "same"){

          this.setState({ Localprofileimage : source, Localprofileimage2: source });
          AsyncStorage.multiSet([
                                    ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                    ["Localprofileimage2",         this.checknull(JSON.stringify(source))],
                                ]);

        }

      }
    });
  }


  setCamera(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        

      //alert(source)

      
      var token = this.state.token;
      var user_id = this.state.user_id;
      var Selfietype = this.state.Selfietype;


      
let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie2', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
        this.setState({ Localprofileimage2 : source});

        AsyncStorage.multiSet([
                                    ["Localprofileimage2",          this.checknull(JSON.stringify(source))],
                                   
                                ]);

        //Alert.alert(Selfietype)
        if( Selfietype === "same"){

          this.setState({ Localprofileimage : source, Localprofileimage2: source });
          AsyncStorage.multiSet([
                                    ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                    ["Localprofileimage2",         this.checknull(JSON.stringify(source))],
                                ]);

        }
      }
    });
  }

  onPressLogout(){

    //Alert.alert("fsd");
    AsyncStorage.removeItem('TOKEN');
    Actions.Login();

  }



  render() {
    var state = this.state;
    var inline = 0;
    var dismissKeyboard = require('dismissKeyboard');

    var Loading = this.state.isLoading;

    var type = 'currency';
    return (
          <View style={styles.moneymanagement_container} >
          <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>  
           <ImageBackground source={require('../assets/images/shoppingimages/welcome-bg.jpg')}
                    style={[
                   styles.base,{
                       width: "100%"
                   }, {
                       height: "100%"
                   }
               ]}> 
           <ScrollView>
              <View style={{flex: 1,}} >
                  
                  <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between',marginLeft:hp('5%')}}>
                    
                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%', height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera1.bind(this)}>
                              <View style={{}}>
                               { 
                                this.state.Localprofileimage != null
                               ? <Image source={this.state.Localprofileimage} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>

                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>

                    <View style={{width: wp('62%'), height: hp('10%'),marginTop:hp('7%'),}} >
                          <View style={{flex: 1, alignSelf:'center', }}>
                          <Text style = {styles.hometext}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE :'HOW MUCH MONEY DO I HAVE'}</Text>
                          </View>
                    </View>

                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%'), marginRight:hp('5%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%',height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera.bind(this)}>
                              <View style={{}}>
                               { 
                                this.state.Localprofileimage2 != null
                               ? <Image source={this.state.Localprofileimage2} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage2!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage2}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>


                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>

                  </View>


                  <View style={{flex:3,flexDirection: 'column',backgroundColor:'#FFFFFF' , marginLeft:0, width:wp('99%'), marginTop:hp('12%'), alignSelf:'center'}}>

                      <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between',}}>
                          
                          <View style={{width: wp('50%'),height:hp('70%')}}>


                              <View style={{height:hp('10%') ,flexDirection: 'row',justifyContent: 'space-between',}}>
                                    
                                    
                                    <View style={{flex:3, marginLeft:'15%'}}>
                                        <Text style={{fontSize:hp('3%'),color:'#000000',alignSelf:'center'}}>{this.state.PrimaryLanguage ? this.state.NCOINS : "NUMBER OF COINS"}</Text>
                                    </View>
                                    
                                    
                                    <View style={{flex:1, marginRight:'6%'}}>
                                        <Text style={{fontSize:hp('3%'),color:'#000000',alignSelf:'center',}}>{this.state.PrimaryLanguage ? this.state.TOTAL : "TOTAL"}</Text>
                                    </View>
                              </View>

                              <View style={{height:hp('10%') ,flexDirection: 'row',justifyContent: 'space-between', marginTop:-10, left:10, marginRight:35,}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("5 cent")}>
                                    <View style={{justifyContent:'center',height:hp('9%'), width:wp('6.2%')}}>
                                        <Image source={require('../assets/images/currencyicons/5coin.png')} style={{alignSelf:'center', marginLeft:'30%', height:'100%',width:'100%'}}/>
                                     </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'space-between',marginTop:hp('1%'),}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}}  onPress={this.onPress5coinadd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center',}}  onPress={this.onPress5coinminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')}/>
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={this.state.coin5cent}
                                      onChangeText={this.onChangeCurrency.bind(this.state.coin5cent, '5cent')}
                                      keyboardType='numeric'
                                      placeholder={''}
                                      underlineColorAndroid="transparent"

                                    />   
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={'$' + this.state.value5cent}
                                      onChangeText={this.onChangeValue.bind(this , '5cent')}
                                      keyboardType='numeric'
                                      placeholder={'$ 0.00'}
                                      underlineColorAndroid="transparent"
                                      editable={false}
                                  />   
                                </View>
                              </View>

                              

                              <View style={{height:hp('10%') , flexDirection: 'row',justifyContent: 'space-between',left:10, marginRight:35}}>
                                    
                                    <TouchableOpacity onPress={() => this.onPressMoney("10 cent")}>
                                    <View style={{justifyContent:'center',height:hp('9%'), width:wp('6.2%'),}}>
                                        <Image source={require('../assets/images/currencyicons/10coin.png')}  style={{alignSelf:'center', marginLeft:'25%',height:'100%',width:'100%'}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'space-between',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}}  onPress={this.onPress10coinadd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center',}}  onPress={this.onPress10coinminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                   <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={this.state.coin10cent}
                                      onChangeText={this.onChangeCurrency.bind(this.state.coin10cent , '10cent')}
                                      keyboardType='numeric'
                                      placeholder={''}
                                      underlineColorAndroid="transparent"

                                    />   
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a',  fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                        <TextInput style={{
                                                          justifyContent:'center',
                                                          alignSelf:'center',
                                                          color :'black',
                                                          fontSize:hp('3.5%')
                                          }}
                                        value={'$' + this.state.value10cent}
                                        onChangeText={this.onChangeValue.bind(this , '10cent')}
                                        keyboardType='numeric'
                                        placeholder={'$ 0.00'}
                                        underlineColorAndroid="transparent"
                                        editable={false}

                                    />    
                                </View>
                              </View>

                              

                              <View style={{height:hp('10%')  ,flexDirection: 'row',justifyContent: 'space-between',left:10, marginRight:35}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("20 cent")} >
                                    <View style={{justifyContent:'center',height:hp('9%'), width:wp('6.2%'), alignSelf:'center'}}>
                                        <Image source={require('../assets/images/currencyicons/20coin.png')} style={{height:'100%',width:'100%'}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'space-between',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}}  onPress={this.onPress20coinadd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center',}}  onPress={this.onPress20coinminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={this.state.coin20cent}
                                      onChangeText={this.onChangeCurrency.bind(this.state.coin20cent , '20cent')}
                                      keyboardType='numeric'
                                      placeholder={''}
                                      underlineColorAndroid="transparent"

                                    />   
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a',  fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                        <TextInput style={{
                                                        justifyContent:'center',
                                                        alignSelf:'center',
                                                        color :'black',
                                                        fontSize:hp('3.5%')
                                        }}
                                        value={'$' + this.state.value20cent}
                                        onChangeText={this.onChangeValue.bind(this , '20cent')}
                                        keyboardType='numeric'
                                        placeholder={'$ 0.00'}
                                        underlineColorAndroid="transparent"
                                        editable={false}
                                    />   
                                </View>
                              </View>

                              

                              <View style={{height:hp('10%') ,flexDirection: 'row',justifyContent: 'space-between',left:10, marginRight:35}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("50 cent")} >
                                    <View style={{justifyContent:'center',height:hp('9%'), width:wp('6.2%'), alignSelf:'center',}}>
                                        <Image source={require('../assets/images/currencyicons/50coin.png')} style={{height:'100%', width:'100%'}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'space-between',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}}  onPress={this.onPress50coinadd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center',}}  onPress={this.onPress50coinminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={this.state.coin50cent}
                                      onChangeText={this.onChangeCurrency.bind(this.state.coin50cent , '50cent')}
                                      keyboardType='numeric'
                                      placeholder={''}
                                      underlineColorAndroid="transparent"

                                    />   
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                   <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                      <TextInput style={{
                                                        justifyContent:'center',
                                                        alignSelf:'center',
                                                        color :'black',
                                                        fontSize:hp('3.5%')
                                        }}
                                        value={'$' + this.state.value50cent}
                                        onChangeText={this.onChangeValue.bind(this , '50cent')}
                                        keyboardType='numeric'
                                        placeholder={'$ 0.00'}
                                        underlineColorAndroid="transparent"
                                        editable={false}
                                    />  
                                </View>
                              </View>  

                              

                              <View style={{height:hp('10%') ,flexDirection: 'row',justifyContent: 'space-between',left:10, marginRight:35}}>
                                   <TouchableOpacity onPress={() => this.onPressMoney("1 dollar")}>
                                    <View style={{justifyContent:'center',height:hp('9%'), width:wp('6.2%'),alignSelf:"center", marginTop:'1%', marginLeft:'.5%'}}>
                                        <Image source={require('../assets/images/currencyicons/1dollarcoin.png')} style={{height:'100%', width:'100%'}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'space-between',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}}  onPress={this.onPress1dollaradd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center',}}  onPress={this.onPress1dollarminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={this.state.coin1dollar}
                                      onChangeText={this.onChangeCurrency.bind(this.state.coin1dollar , '1dollar')}
                                      keyboardType='numeric'
                                      placeholder={''}
                                      underlineColorAndroid="transparent"

                                    />   
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={'$' + this.state.value1dollar}
                                      onChangeText={this.onChangeValue.bind(this , '1dollar')}
                                      keyboardType='numeric'
                                      placeholder={'$ 0.00'}
                                      underlineColorAndroid="transparent"
                                      editable={false}
                                  />   
                                </View>
                              </View>  
                              
                              

                              <View style={{height:hp('10%') ,flexDirection: 'row',justifyContent: 'space-between',left:10, marginRight:35}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("2 dollar")}>
                                    <View style={{flexDirection: 'column',justifyContent:'center',height:hp('9%'), width:wp('6.2%'),alignSelf:'center', alignItem:"center", marginLeft:'2%',marginTop:'1%'}}>
                                        <Image source={require('../assets/images/currencyicons/2dollarcoin.png')} style={{height:'100%', width:'99%'}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'space-between',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}}  onPress={this.onPress2dollaradd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center',}}  onPress={this.onPress2dollarminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={this.state.coin2dollar}
                                      onChangeText={this.onChangeCurrency.bind(this.state.coin1dollar , '2dollar')}
                                      keyboardType='numeric'
                                      placeholder={''}
                                      underlineColorAndroid="transparent"

                                    />   
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={'$' + this.state.value2dollar}
                                      onChangeText={this.onChangeValue.bind(this , '2dollar')}
                                      keyboardType='numeric'
                                      placeholder={'$ 0.00'}
                                      underlineColorAndroid="transparent"
                                      editable={false}

                                  />   
                                </View>
                              </View>
                                                          
                          </View>

                          

                          <View style={{width: wp('50%'),height:hp('70%')}}>

                              <View style={{height:hp('10%') ,flexDirection: 'row',justifyContent: 'space-between',}}>
                                    
                                    
                                    <View style={{flex:3, marginLeft:'15%'}}>
                                        <Text style={{fontSize:hp('3%'),color:'#000000',alignSelf:'center'}}>{this.state.PrimaryLanguage ? this.state.NCOINS : "NUMBER OF COINS"}</Text>
                                    </View>
                                    
                                    
                                    <View style={{flex:1, marginRight:'6%'}}>
                                        <Text style={{fontSize:hp('3%'),color:'#000000',alignSelf:'center',}}>{this.state.PrimaryLanguage ? this.state.TOTAL : "TOTAL"}</Text>
                                    </View>
                              </View>


                              <View style={{height:hp('10%'),flexDirection: 'row',justifyContent: 'space-between', marginTop:-10, left:0, marginRight:35}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("5 dollar")}>
                                    <View style={{justifyContent:'center'}}>
                                        <Image source={require('../assets/images/currencyicons/5dollar.png')} style={{height:hp('7%'), width:wp('10%'),}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'space-between',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}}  onPress={this.onPress5dollaradd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center',}}  onPress={this.onPress5dollarminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                      }}
                                      value={this.state.note5dollar}
                                      onChangeText={this.onChangeCurrency.bind(this.state.note5dollar , '5dollar')}
                                      keyboardType='numeric'
                                      placeholder={''}
                                      underlineColorAndroid="transparent"

                                    />   
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                        }}
                                        value={'$' + this.state.value5dollar}
                                        onChangeText={this.onChangeValue.bind(this , '5dollar')}                                    
                                        keyboardType='numeric'
                                        placeholder={'$ 0.00'}
                                        underlineColorAndroid="transparent"
                                        editable={false}
                                    />   
                                </View>
                              </View>

                              

                              <View style={{height:hp('10%'),flexDirection: 'row',justifyContent: 'space-between',left:0, marginRight:35}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("10 dollar")}>
                                    <View style={{justifyContent:'center'}}>
                                        <Image source={require('../assets/images/currencyicons/10dollar.png')} style={{height:hp('7%'), width:wp('10%'),}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'center',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}} onPress={this.onPress10dollaradd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center'}} onPress={this.onPress10dollarminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                   <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                                }}
                                                value={this.state.note10dollar}
                                                onChangeText={this.onChangeCurrency.bind(this.state.note10dollar , '10dollar')}
                                                keyboardType='numeric'
                                                placeholder={''}
                                                underlineColorAndroid="transparent"

                                            />
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                        }}
                                        value={'$' + this.state.value10dollar}
                                        onChangeText={this.onChangeValue.bind(this , '10dollar')}                                     
                                        keyboardType='numeric'
                                        placeholder={'$ 0.00'}
                                        underlineColorAndroid="transparent"
                                        editable={false}

                                    />   
                                  </View>
                              </View>

                              
                              <View style={{height:hp('10%'),flexDirection: 'row',justifyContent: 'space-between',left:0, marginRight:35}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("20 dollar")}>
                                    <View style={{justifyContent:'center'}}>
                                        <Image source={require('../assets/images/currencyicons/20dollar.png')} style={{height:hp('7%'), width:wp('10%'),}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'center',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}} onPress={this.onPress20dollaradd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center'}} onPress={this.onPress20dollarminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                                }}
                                                value={this.state.note20dollar}
                                                onChangeText={this.onChangeCurrency.bind(this.state.note20dollar , '20dollar')}
                                                keyboardType='numeric'
                                                placeholder={''}
                                               underlineColorAndroid="transparent"

                                            />
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                                }}
                                                value={'$ ' +this.state.value20dollar}
                                                oonChangeText={this.onChangeValue.bind(this, '20dollar')}
                                                keyboardType='numeric'
                                                placeholder={'$ 0.00'}
                                                underlineColorAndroid="transparent"
                                                editable={false}
                                            />  
                                </View>
                              </View>

                              

                              <View style={{height:hp('10%'),flexDirection: 'row',justifyContent: 'space-between',left:0, marginRight:35}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("50 dollar")}>
                                    <View style={{justifyContent:'center'}}>
                                        <Image source={require('../assets/images/currencyicons/50dollar.png')} style={{height:hp('7%'), width:wp('10%'),}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'center',marginTop:3,}}>
                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center', marginRight:10}} onPress={this.onPress50dollaradd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center'}} onPress={this.onPress50dollarminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                                }}
                                                value={this.state.note50dollar}
                                                onChangeText={this.onChangeCurrency.bind(this.state.note50dollar , '50dollar')}
                                                keyboardType='numeric'
                                                placeholder={''}
                                                underlineColorAndroid="transparent"
                                            /> 
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                   <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                                }}
                                                value={'$ ' +this.state.value50dollar}
                                                onChangeText={this.onChangeValue.bind(this , '50dollar')}
                                                keyboardType='numeric'
                                                placeholder={'$ 0.00'}
                                                underlineColorAndroid="transparent"
                                                editable={false}
                                            /> 
                                </View>
                              </View>  

                              

                              <View style={{height:hp('10%'),flexDirection: 'row',justifyContent: 'space-between',left:0, marginRight:35,}}>
                                    <TouchableOpacity onPress={() => this.onPressMoney("100 dollar")}>
                                    <View style={{justifyContent:'center'}}>
                                        <Image source={require('../assets/images/currencyicons/100dollar.png')} style={{height:hp('7%'), width:wp('10%'),}}/>
                                    </View>
                                    </TouchableOpacity>
                                    
                                    <View style={{flexDirection: 'row',justifyContent: 'center',}}>
                                        <TouchableOpacity style={{height:25, width:25, marginRight:10, alignSelf:'center'}} onPress={this.onPress100dollaradd.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/add.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height:25, width:25, alignSelf:'center'}} onPress={this.onPress100dollarminus.bind(this)}  underlayColor='#232A40'>
                                        <Image source={require('../assets/images/currencyicons/subtract.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                                }}
                                                value={this.state.note100dollar}
                                                onChangeText={this.onChangeCurrency.bind(this.state.note100dollar , '100dollar')}
                                                keyboardType='numeric'
                                                placeholder={''}
                                                 underlineColorAndroid="transparent"

                                            /> 
                                    </View>
                                    
                                    <View>
                                        <Text style={{marginRight:10,color:'#e36a2a', fontSize:hp('5%')}}>=</Text>
                                    </View>
                                    
                                    <View style={{width: '25%',backgroundColor: '#FFFFFF',justifyContent:'center',borderWidth: 1, borderColor: '#000000',marginRight:10, height: RF(10)}} >
                                    
                                      <TextInput style={{
                                                      justifyContent:'center',
                                                      alignSelf:'center',
                                                      color :'black',
                                                      fontSize:hp('3.5%')
                                                    }}
                                                    value={'$ ' + this.state.value100dollar}
                                                    onChangeText={this.onChangeValue.bind(this , '100dollar')}
                                                    keyboardType='numeric'
                                                    placeholder={'$ 0.00'}
                                                    underlineColorAndroid="transparent"
                                                    editable={false}
                                                />    
                                </View>
                              </View>  
                              
                              

                              <View style={{height:hp('10%'),flexDirection: 'row',justifyContent: 'space-between',left:20, marginRight:35, marginTop:hp('.5%')}}>
                                  <View style={{marginLeft:'40%', justifyContent:'center'}}>
                                      <Text style={{fontSize:hp('5%'),fontWeight: 'bold',color:'#000000', alignSelf:'center',}}>{this.state.PrimaryLanguage ? this.state.IHAVE : 'I HAVE'}</Text>
                                  </View>
                                  <View style={{width: '30%',  backgroundColor: '#FF4500', borderWidth:1, borderRadius:5, borderColor:'#FF4500', right:20, justifyContent:'center'}}>
                                  <TextInput style={{
                                                  alignSelf: 'center',
                                                  textAlign:'center',
                                                  color :'#FFFFFF',
                                                  fontSize: hp('3%')
                                                }}
                                                value={'$ ' + parseFloat(this.state.totalmoney).toFixed(2)}
                                                onChangeText={(totalmoney) => this.setState({ totalmoney })}
                                                keyboardType='numeric'
                                                placeholder={'$ 0.00'}
                                                underlineColorAndroid="transparent"
                                                editable={false}
                                            /> 
                                  </View>
                              </View>


                                
                          </View>


                      </View>   
                          
                  </View> 

                  <View style={{flex:1,flexDirection: 'column',backgroundColor:'#FFFFFF' ,  width:'99%', alignSelf:'center'}}>

                          <View style={{flex: 3,flexDirection: 'row',justifyContent: 'space-between',}}>
                            <View style={{width: wp('50%'), height: hp('8%'), backgroundColor: '#FFFFFF',justifyContent:'center'}} >

                                    <View style={{flex: 1,flexDirection: 'row',justifyContent: 'flex-start',}}>
                                      <View style={{width: wp('25%'),justifyContent: 'flex-start', height: hp('7%'), backgroundColor: '#000000', borderWidth:1, marginLeft:30,justifyContent:'center'}}>
                                      <TouchableOpacity 
                                        onPress={() => {
                                              Alert.alert(
                                                this.state.PrimaryLanguage ? this.state.RESETALLMONEY : 'Do you want to reset all money ?',
                                                '',
                                                [
                                                  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: 'OK', onPress: () => {this.onPressResetMoney()}},
                                                ],
                                                { cancelable: false }
                                              )
                                            }}>
                                        <Text style={{alignSelf:'center', color:'#FFFFFF', fontSize:hp('3%')}}>{this.state.PrimaryLanguage ? this.state.RESET : 'RESET ALL AMOUNT'}</Text>
                                        </TouchableOpacity>
                                      </View>
                            </View>

                          </View>

                          <View style={{flex: 3, flexDirection:'row', justifyContent:'space-between'}}>
                                  
                                  <View style={{width: '55%', height: 50, backgroundColor: '#1a74f5',  borderWidth:1, right:5,borderColor:'#3354FF', justifyContent:'center'}}>
                                  
                                          <TouchableOpacity onPress={this.onPressMoneyManagement.bind(this)}  underlayColor='#232A40' style={{width:'100%',height:50, justifyContent:'center'}}>
                                          <Text style={{alignSelf:'center', color:'#FFFFFF', fontSize:hp('3%')}}>{this.state.PrimaryLanguage ? this.state.USEMONEYMANAGEMENT : 'USE FOR MONEY MANAGEMENT'}</Text>
                                          </TouchableOpacity>
                                  
                                  </View>
                                  
                                  <View style={{width: '40%', height: 50, backgroundColor: '#1a74f5',  borderWidth:1, right:5,borderColor:'#3354FF', justifyContent:'center'}}>
                                  
                                        <TouchableOpacity onPress={this.onPressGoShopping.bind(this)}  underlayColor='#232A40'  style={{width:'100%',height:50, justifyContent:'center'}}>
                                        <Text style={{alignSelf:'center', color:'#FFFFFF',fontSize:hp('3%')}}>{this.state.PrimaryLanguage ? this.state.USESHOPPING : 'USE FOR SHOPPING'}</Text>
                                        </TouchableOpacity>

                                  </View>
                          </View>

                        </View>

                  </View>



                  <View style={{flex: 1,flexDirection: 'row', alignSelf:'center', position:'relative', marginTop:5}} >


                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressHome.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/welcome.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center'}}>
                     <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.WECOME : 'WECOME PAGE'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressShoopingList.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/shopping-list.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center'}}>
                      <Text style={{alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5)}}>{this.state.PrimaryLanguage ? this.state.SHOPPINGLIST : 'SHOPPING LIST'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>


                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressGoShopping.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/letsgo.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center',}}>
                        <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.GOSHOPPING : 'LETS GO SHOPPING'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressPantry.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/pantry.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                        <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.PANTRY : 'PANTRY'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={() => {
                                              Alert.alert(
                                               this.state.PrimaryLanguage ? this.state.voicefeatureforlogout : 'Are you sure to Logout',
                                                '',
                                                [
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforcancel : 'cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforok : 'Ok', onPress: () => {this.onPressLogout(this)}},
                                                ],
                                                { cancelable: false }
                                              )
                                    }}>
                    <ImageBackground source={require('../assets/images/shoppingimages/logoutmain.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                      <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.LOGOUT : 'LOGOUT'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                  </View>


              </View>
              { this.state.isLoading ?<ActivityIndicator
                        animating     ={true}
                        transparent   ={true}
                        visible       ={false}
                        style         ={styles.indicator}
                        size          ="large"
                      />  : <View/>}    
              </ScrollView>
          </ImageBackground>    
          </View> 
    );
  }
}


