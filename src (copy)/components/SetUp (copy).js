/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  PermissionsAndroid,
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  TouchableHighlight,
  Image,
  TextInput,
  Keyboard,
  Alert,
  ScrollView,
  Modal,
  Picker,
  Radio,
  AsyncStorage,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  FlatList
} from 'react-native';



import styles from './GlobalStyle.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import { Table, Row, Rows } from 'react-native-table-component';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import GlobalVariables from './GlobalVariables.js';
import Tts from 'react-native-tts';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import FlipToggle from 'react-native-flip-toggle-button';
import RF from "react-native-responsive-fontsize";


import {default as Sound} from 'react-native-sound';
import MusicFiles from 'react-native-get-music-files';

type Props = {};


const Button = ({title, onPress}) => (
  <TouchableOpacity onPress={onPress}>
    <Text style={styles.button}>{title}</Text>
  </TouchableOpacity>
);

const Header = ({children, style}) => <Text style={[styles.header, style]}>{children}</Text>;

const Feature = ({title, onPress, description, buttonLabel = 'Select', status}) => (
  <View style={styles.feature}>
    <Header style={{flex: 1, color:'#FFFFFF',fontSize: RF(2.5)}}>{title ? title : 'unknown'}</Header>
    {status ? <Text style={{padding: 5}}>{resultIcons[status] || ''}</Text> : null}
    <Button title={buttonLabel} onPress={onPress} />
  </View>
);



const resultIcons = {
  '': '',
  pending: '\u274C',
  playing: '\u25B6',
  win: '\u2713',
  fail: '\u274C',
};

const audioTests = [
  {
    id   : 1,
    title: 'Alert Tune Wave',
    url: 'https://raw.githubusercontent.com/zmxv/react-native-sound-demo/master/pew2.aac',
  },
  {
    id   : 2,  
    title: 'Alert Tune Birds Chirp',
    url: 'https://raw.githubusercontent.com/zmxv/react-native-sound-demo/master/frog.wav',
  },
  {
    id   : 3,  
    title: 'Alert Tune Goat Baa',
    url: 'http://www.slspencer.com/Sounds/Chewbacca/Chewie3.mp3',
  },
  {
    id   : 4,  
    title: 'Incoming Alert',
    url: 'http://18.218.60.240/assets/ringtones/IncomingMsg.mp3',
  },
  {
    id   : 5,  
    title: 'Bells Alert',
    url: 'http://18.218.60.240/assets/ringtones/BellsSms.mp3',
  },
  {
    id   : 6,  
    title: 'Thickness Alert',
    url: 'http://18.218.60.240/assets/ringtones/FunnySms.mp3',
  },
  {
    id   : 7,  
    title: 'Pleasant Sound Alert',
    url: 'http://18.218.60.240/assets/ringtones/PleasantSoundSms.mp3',
  },
  {
    id   : 8,  
    title: 'Samsung Alert',
    url: 'http://18.218.60.240/assets/ringtones/SamsungAndroidSms.mp3',
  },
  {
    id   : 9,  
    title: 'Alert 9',
    url: 'http://18.218.60.240/assets/ringtones/Sms.mp3',
  },
  {
    id   : 10,  
    title: 'Alert 10',
    url: 'http://18.218.60.240/assets/ringtones/VirusMassageTone.mp3',
  },
  {
    id   : 11,  
    title: 'Alert 11',
    url: 'http://18.218.60.240/assets/ringtones/WaterEffectSMSTONE.mp3',
  },
  {
    id   : 12,  
    title: 'Alert 12',
    url: 'http://18.218.60.240/assets/ringtones/slowspringboard.wav',
  },
];





let whoosh=null;



function playSound(testInfo, component) {


  setTestState(testInfo, component, 'pending');

  const callback = (error, sound) => {
    if (error) {
      Alert.alert('error', error.message);
      setTestState(testInfo, component, 'fail');
      return;
    }
    setTestState(testInfo, component, 'playing');
    // Run optional pre-play callback
    testInfo.onPrepared && testInfo.onPrepared(sound, component);
    sound.play(() => {
      // Success counts as getting to the end
      setTestState(testInfo, component, 'win');
      // Release when it's done so we're not using up resources
      sound.release();
    });
  };

  // If the audio is a 'require' then the second parameter must be the callback.
  if (testInfo.isRequire) {
    const sound = new Sound(testInfo.url, error => callback(error, sound));
  } else {
    const sound = new Sound(testInfo.url, testInfo.basePath, error => callback(error, sound));
  }
}


function setTestState(testInfo, component, status) {
  component.setState({tests: {...component.state.tests, [testInfo.title]: status}});

  //console.log(testInfo,component, status,"componentcomponent")
}

function playDeviceSound(testInfo, component) {
  Sound.setCategory('Playback', false);
  // sound.release();
  

  setTestState(testInfo, component, 'pending');

  const callback = (error, sound) => {
    if (error) {
      Alert.alert('error', error.message);
      setTestState(testInfo, component, 'fail');
      return;
    }
    setTestState(testInfo, component, 'playing');
    // Run optional pre-play callback
    testInfo.onPrepared && testInfo.onPrepared(sound, component);
    
    sound.play(() => {
      // Success counts as getting to the end
      setTestState(testInfo, component, 'win');
      // Release when it's done so we're not using up resources
      sound.reset();
    });
  };

  // If the audio is a 'require' then the second parameter must be the callback.
  if (testInfo.isRequire) {
    const sound = new Sound(testInfo.path, error => callback(error, sound));
  } else {
    const sound = new Sound(testInfo.path, testInfo.basePath, error => callback(error, sound));
  }
}

export default class GoShopping extends Component<Props> {

  static navigationOptions = {
      header: null
  }


  


 constructor(props) {


    

    Sound.setCategory('Playback', false);


    var GVar = GlobalVariables.getApiUrl();



    AsyncStorage.getItem('TOKEN').then((data) => { 

      

      try {
                  let response = fetch(GVar.BaseUrl + GVar.Profile,{
                                    method: 'GET',
                                    headers:{
                                                'token' : data
                                            },
                                    }).then((response) => response.json())
                                             .then((responseJson) => {

                                              if( responseJson.status == '1'){

                                                  var profile = responseJson.profile;

                                                 
                                                  AsyncStorage.setItem('USER', this.checknull(JSON.stringify(profile) ));

                                                  var name = profile.name;

                                                  this.setState({
                                                     isLoading      : true,
                                                     name           : name.toUpperCase(),
                                                     user_id        : profile.user_id,
                                                     profileimage   : profile.profile_image,
                                                     profileimage2  : profile.profile_image1,
                                                     password       : profile.password,
                                                     subscribe      : profile.subscribe,
                                                     dob            : profile.dob,
                                                     email          : profile.email,
                                                     created_on     : profile.created_on,
                                                  });
                                                    
                                                 AsyncStorage.multiSet([
                                                                    ["name",                this.checknull(this.state.name)],
                                                                    ["user_id",             this.checknull(this.state.user_id)],
                                                                    ["profileimage",        this.checknull(this.state.profileimage)],
                                                                    ["profileimage2",       this.checknull(this.state.profileimage2)],
                                                                    ["password",            this.checknull(this.state.password)],
                                                                    ["status",              this.checknull(JSON.stringify(profile.status))],
                                                                    ["subscribe",           this.checknull(JSON.stringify(profile.subscribe))],
                                                                    ["dob",                 this.checknull(this.state.dob)],
                                                                    ["email",               this.checknull(this.state.email)],
                                                                    ["created_on",          this.checknull(this.state.created_on)],
                                                                ]);
                                              }else{
                                                
                                              }
                                                  
                                             });

              
                  
               }catch (error) {

                if( error.line == 18228 ){
                      
                    
                }

              }

      if(!data){
          Actions.Login();
      }
            
    });

    super(props);

    this.setLanguages = this.setLanguages();
    this.state={
        isLoading                 : true,
        LANGUAGES                 : null,
        ALLSECONDARYLANGUAGES     : null,
        PickerValueHolder         : null,
        name                      : null,
        user_id                   : null,
        LowMoneyWarningType       : null,
        MoneymanagementAudioType  : null, 
        pre_lan                   :null,
        baseimageurl              :'http://18.218.60.240/assets/userLibrary/',
        MoneymanagementAudioTypestatus  : null, 
        ShoppingListAudioType     : null,
        ShoppingListAudioTypestatus     : null,
        MyMoneyAudioType          : null,
        voicefeatureforON         :null,
        MyMoneyAudioTypestatus          : null,
        GoShoppingAudioType       : null,
        GoShoppingAudioTypestatus       : null,
        ShoppingListType          : null,
        LanguageType              : null,
        Selfietype                : null,
        price                     : null,
        token                     : null,
        setModalVisibletune       : false,
        modalVisible              : false,
        modalVisibleMusic         : false,
        devicemusic               : false,
        LanguageTypestatus        : null,
        showsinglelangage         : false,
        showsduallangage          : false,
        setModalVisiblelanguage   : false,
        setModalVisibleSLanguage  : false,
        ismusic                   : false,
        animating                 : false,
        songs                     : [],
        currentAlert              : [],
        ALL_SecondryLANGUAGES     : [],
        NAME                      : 'NAME',
        LANGUAGE                  : 'LANGUAGE',
        DUAL                      : 'DUAL',
        SINGLE                    : 'SINGLE',
        
    }





   
    Sound.setCategory('Playback', false); // true = mixWithOthers

    // Special case for stopping
    this.stopSoundLooped = () => {
      if (!this.state.loopingSound) {
        return;
      }

      this.state.loopingSound.stop().release();
      this.setState({loopingSound: null, tests: {...this.state.tests, ['mp3 in bundle (looped)']: 'win'}});
    };

    this.state = {
      loopingSound: undefined,
      tests: {},
    };




   
    AsyncStorage.getItem('LANGUAGES').then((data) => {

      var languages = JSON.parse(data);

      this.setState({
         LANGUAGES: languages,
        
       });

      
    });


    

    AsyncStorage.getItem('isSingle').then((data) => {
          
          

          if( data == 'true'){

            this.setState({isSingle : true});
          }

          if( data == 'false'){

            this.setState({isSingle : false});
          }

          

    });

    AsyncStorage.getItem('MMoneyAudioType').then((data) => {
          
          

          if( data == 'true'){

            this.setState({MMoneyAudioType : true});
          }

          if( data == 'false'){

            this.setState({MMoneyAudioType : false});
          }

    });


    AsyncStorage.getItem('HMMoneyAudioType').then((data) => {
          
          

          if( data == 'true'){

            this.setState({HMMoneyAudioType : true});
          }

          if( data == 'false'){

            this.setState({HMMoneyAudioType : false});
          }

    });

    AsyncStorage.getItem('SLAudioType').then((data) => {
          
          

          if( data == 'true'){

            this.setState({SLAudioType : true});
          }

          if( data == 'false'){

            this.setState({SLAudioType : false});
          }

    });


    AsyncStorage.getItem('LGSAudioType').then((data) => {
          
          

          if( data == 'true'){

            this.setState({LGSAudioType : true});
          }

          if( data == 'false'){

            this.setState({LGSAudioType : false});
          }

    });


    
    

    
  }


  setLanguages(){

  
    AsyncStorage.getItem('PrimaryLanguage').then((P_LANG) => {

                        
                        AsyncStorage.getItem('WELCOME PAGE').then((data) => {

                                            var WECOME = JSON.parse(data);

                                            if( P_LANG == 'en'){

                                              this.setState({WECOME : WECOME.en})
                                              
                                            }

                                            if( P_LANG == 'el'){

                                                this.setState({WECOME : WECOME.el})
                                            }

                                            if( P_LANG == 'cmn'){

                                                this.setState({WECOME : WECOME.cmn})
                                            }

                                            if( P_LANG == 'zh'){

                                                this.setState({WECOME : WECOME.zh})
                                            }

                                            if( P_LANG == 'ar'){

                                                this.setState({WECOME : WECOME.ar})
                                            }

                                            if( P_LANG == 'vi'){

                                                this.setState({WECOME : WECOME.vi})
                                            }

                                            if( P_LANG == 'it'){

                                                this.setState({WECOME : WECOME.it})
                                            }

                                            if( P_LANG == 'es'){

                                                this.setState({WECOME : WECOME.es})
                                            }

                                            if( P_LANG == 'hi'){

                                                this.setState({WECOME : WECOME.hi})
                                            }

                                            if( P_LANG == 'pa'){

                                                this.setState({WECOME : WECOME.pa})
                                            }

                                            if( P_LANG == 'fil'){

                                               this.setState({WECOME : WECOME.fil})
                                            }

                        });

                        AsyncStorage.getItem('SHOPPING LIST').then((data) => {

                              var SHOPPINGLIST = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SHOPPINGLIST : SHOPPINGLIST.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SHOPPINGLIST : SHOPPINGLIST.fil})
                              }

                        });

                        AsyncStorage.getItem('LETS GO SHOPPING').then((data) => {

                              var GOSHOPPING = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({GOSHOPPING : GOSHOPPING.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({GOSHOPPING : GOSHOPPING.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({GOSHOPPING : GOSHOPPING.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({GOSHOPPING : GOSHOPPING.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({GOSHOPPING : GOSHOPPING.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({GOSHOPPING : GOSHOPPING.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({GOSHOPPING : GOSHOPPING.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({GOSHOPPING : GOSHOPPING.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({GOSHOPPING : GOSHOPPING.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({GOSHOPPING : GOSHOPPING.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({GOSHOPPING : GOSHOPPING.fil})
                              }

                        });

              
                        AsyncStorage.getItem('HOW MUCH MONEY DO I HAVE').then((data) => {

                              var MONEYIHAVE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({MONEYIHAVE : MONEYIHAVE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({MONEYIHAVE : MONEYIHAVE.fil})
                              }

                        });

                        AsyncStorage.getItem('MONEY MANAGEMENT').then((data) => {

                              var MONEYMANAGEMENT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.fil})
                              }

                        });

                        AsyncStorage.getItem('LOGOUT').then((data) => {

                              var LOGOUT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({LOGOUT : LOGOUT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({LOGOUT : LOGOUT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({LOGOUT : LOGOUT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({LOGOUT : LOGOUT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({LOGOUT : LOGOUT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({LOGOUT : LOGOUT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({LOGOUT : LOGOUT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({LOGOUT : LOGOUT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({LOGOUT : LOGOUT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({LOGOUT : LOGOUT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({LOGOUT : LOGOUT.fil})
                              }

                        });


                        AsyncStorage.getItem('NAME').then((data) => {

                              var NAME = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({NAME : NAME.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({NAME : NAME.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({NAME : NAME.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({NAME : NAME.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({NAME : NAME.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({NAME : NAME.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({NAME : NAME.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({NAME : NAME.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({NAME : NAME.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({NAME : NAME.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({NAME : NAME.fil})
                              }

                        });

                        AsyncStorage.getItem('LANGUAGE').then((data) => {

                              var LANGUAGE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({LANGUAGE : LANGUAGE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({LANGUAGE : LANGUAGE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({LANGUAGE : LANGUAGE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({LANGUAGE : LANGUAGE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({LANGUAGE : LANGUAGE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({LANGUAGE : LANGUAGE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({LANGUAGE : LANGUAGE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({LANGUAGE : LANGUAGE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({LANGUAGE : LANGUAGE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({LANGUAGE : LANGUAGE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({LANGUAGE : LANGUAGE.fil})
                              }

                        });

                        AsyncStorage.getItem('DUAL').then((data) => {

                              var DUAL = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({DUAL : DUAL.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({DUAL : DUAL.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({DUAL : DUAL.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({DUAL : DUAL.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({DUAL : DUAL.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({DUAL : DUAL.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({DUAL : DUAL.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({DUAL : DUAL.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({DUAL : DUAL.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({DUAL : DUAL.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({DUAL : DUAL.fil})
                              }

                        });

                        AsyncStorage.getItem('SINGLE').then((data) => {

                              var SINGLE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SINGLE : SINGLE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SINGLE : SINGLE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SINGLE : SINGLE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SINGLE : SINGLE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SINGLE : SINGLE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SINGLE : SINGLE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SINGLE : SINGLE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SINGLE : SINGLE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SINGLE : SINGLE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SINGLE : SINGLE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SINGLE : SINGLE.fil})
                              }

                        });

                        AsyncStorage.getItem('LOW MONEY WARNING').then((data) => {

                              var MONEYWARNING = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({MONEYWARNING : MONEYWARNING.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({MONEYWARNING : MONEYWARNING.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({MONEYWARNING : MONEYWARNING.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({MONEYWARNING : MONEYWARNING.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({MONEYWARNING : MONEYWARNING.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({MONEYWARNING : MONEYWARNING.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({MONEYWARNING : MONEYWARNING.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({MONEYWARNING : MONEYWARNING.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({MONEYWARNING : MONEYWARNING.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({MONEYWARNING : MONEYWARNING.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({MONEYWARNING : MONEYWARNING.fil})
                              }

                        });


                        AsyncStorage.getItem('PLAY A TUNE').then((data) => {

                              var PLAYATUNE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({PLAYATUNE : PLAYATUNE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({PLAYATUNE : PLAYATUNE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({PLAYATUNE : PLAYATUNE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({PLAYATUNE : PLAYATUNE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({PLAYATUNE : PLAYATUNE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({PLAYATUNE : PLAYATUNE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({PLAYATUNE : PLAYATUNE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({PLAYATUNE : PLAYATUNE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({PLAYATUNE : PLAYATUNE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({PLAYATUNE : PLAYATUNE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({PLAYATUNE : PLAYATUNE.fil})
                              }

                        });


                        AsyncStorage.getItem('ALARM').then((data) => {

                              var ALARM = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({ALARM : ALARM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({ALARM : ALARM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({ALARM : ALARM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({ALARM : ALARM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({ALARM : ALARM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({ALARM : ALARM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({ALARM : ALARM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({ALARM : ALARM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({ALARM : ALARM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({ALARM : ALARM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({ALARM : ALARM.fil})
                              }

                        });

                        AsyncStorage.getItem('ALL SELFIES INDEPENDENT').then((data) => {

                              var SELFIESINDEPENDENT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.fil})
                              }

                        });

                        AsyncStorage.getItem('SELFIES ALL SAME').then((data) => {

                              var SELFIESALLSAME = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SELFIESALLSAME : SELFIESALLSAME.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SELFIESALLSAME : SELFIESALLSAME.fil})
                              }

                        });

                        AsyncStorage.getItem('SELFIES ALL SAME').then((data) => {

                              var SELFIESALLSAME = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SELFIESALLSAME : SELFIESALLSAME.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SELFIESALLSAME : SELFIESALLSAME.fil})
                              }

                        });

                        AsyncStorage.getItem('OPTIONS').then((data) => {

                              var OPTIONS = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({OPTIONS : OPTIONS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({OPTIONS : OPTIONS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({OPTIONS : OPTIONS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({OPTIONS : OPTIONS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({OPTIONS : OPTIONS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({OPTIONS : OPTIONS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({OPTIONS : OPTIONS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({OPTIONS : OPTIONS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({OPTIONS : OPTIONS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({OPTIONS : OPTIONS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({OPTIONS : OPTIONS.fil})
                              }

                        });


                        AsyncStorage.getItem('AUDIO ON TAP').then((data) => {

                              var AUDIOONTAP = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({AUDIOONTAP : AUDIOONTAP.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({AUDIOONTAP : AUDIOONTAP.fil})
                              }

                        });

                        AsyncStorage.getItem('REPEATED IN LINE').then((data) => {

                              var REPEATEDINLINE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({REPEATEDINLINE : REPEATEDINLINE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({REPEATEDINLINE : REPEATEDINLINE.fil})
                              }

                        });

                        AsyncStorage.getItem('NUMBERED COLUMN').then((data) => {

                              var NUMBEREDCOLUMN = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.fil})
                              }

                        });

                          AsyncStorage.getItem('ON').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforON : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforON : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforON : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforON : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforON : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforON : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforON : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforON : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforON : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforON : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforON : voicefeatureGS.fil})
                              }

                        });

                          AsyncStorage.getItem('OFF').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforOff : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforOff : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('Are you sure to log out?').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforlogout : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforlogout : voicefeatureGS.fil})
                              }

                        });
                AsyncStorage.getItem('ok').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforok : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforok : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforok : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforok : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforok : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforok : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforok : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforok : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforok : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('cancel').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforcancel : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforcancel : voicefeatureGS.fil})
                              }

                        });


                        AsyncStorage.getItem('Your voice feature is active for money management').then((data) => {

                         

                              var voicefeatureMM = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureMM : voicefeatureMM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureMM : voicefeatureMM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureMM : voicefeatureMM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureMM : voicefeatureMM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureMM : voicefeatureMM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureMM : voicefeatureMM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureMM : voicefeatureMM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureMM : voicefeatureMM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureMM : voicefeatureMM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureMM : voicefeatureMM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureMM : voicefeatureMM.fil})
                              }

                        });

                        AsyncStorage.getItem('Your voice feature is active for my money').then((data) => {

                         

                              var voicefeatureMyM = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureMyM : voicefeatureMyM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureMyM : voicefeatureMyM.fil})
                              }

                        });

                        AsyncStorage.getItem('Your voice feature is active for shopping list').then((data) => {

                         

                              var voicefeatureSL = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureSL : voicefeatureSL.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureSL : voicefeatureSL.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureSL : voicefeatureSL.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureSL : voicefeatureSL.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureSL : voicefeatureSL.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureSL : voicefeatureSL.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureSL : voicefeatureSL.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureSL : voicefeatureSL.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureSL : voicefeatureSL.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureSL : voicefeatureSL.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureSL : voicefeatureSL.fil})
                              }

                        });


                        AsyncStorage.getItem('Your voice feature is active for go shopping').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureGS : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureGS : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureGS : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureGS : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureGS : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureGS : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureGS : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureGS : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureGS : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureGS : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureGS : voicefeatureGS.fil})
                              }

                        });
    });


    AsyncStorage.getItem('SeconderyLanguage').then((P_LANG) => {

        AsyncStorage.getItem('Your voice feature is active for money management').then((data) => {

                         

                              var SvoicefeatureMM = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({SvoicefeatureMM : SvoicefeatureMM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SvoicefeatureMM : SvoicefeatureMM.fil})
                              }

                        });

                        AsyncStorage.getItem('Your voice feature is active for my money').then((data) => {

                         

                              var SvoicefeatureMyM = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({SvoicefeatureMyM : SvoicefeatureMyM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SvoicefeatureMyM : SvoicefeatureMyM.fil})
                              }

                        });

                        AsyncStorage.getItem('Your voice feature is active for shopping list').then((data) => {

                         

                              var SvoicefeatureSL = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({SvoicefeatureSL : SvoicefeatureSL.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SvoicefeatureSL : SvoicefeatureSL.fil})
                              }

                        });


                        AsyncStorage.getItem('Your voice feature is active for go shopping').then((data) => {

                         

                              var SvoicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({SvoicefeatureGS : SvoicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SvoicefeatureGS : SvoicefeatureGS.fil})
                              }

                        });

    });

    
  }


  setModalVisible(visible) {

      this.setState({modalVisible: visible});

      if (whoosh!=null) {
        whoosh.release();
      }
  }

  setModalVisiblelanguage(visible) {
    this.setState({setModalVisibleLanguage: false});
  }

  setModalVisibleSlanguage(visible) {
    this.setState({setModalVisibleSLanguage: false});
  }

  setModalVisibleDevice(visible) {

    if (whoosh!=null) {
    whoosh.release();
    }
    this.setState({devicemusic: false});

    
  }

checknull(value){
  if(value){
    return value;
  }
  else {
    return '';
  }
}


  componentDidMount = async () => {



      var ismusic = false;



        MusicFiles.getAll({
            id : true,
            blured : false,
            artist : true,
            duration : true, //default : true
            cover : true, //default : true,
            title : true,
            cover : true,
            batchNumber : 0, //get 5 songs per batch
            minimumSongDuration : 10000, //in miliseconds,
            fields : ['title','artwork','duration','artist','genre','lyrics','albumTitle']
        }).then(tracks => {


            if(tracks == 'Something get wrong with musicCursor'){
              ismusic = false;

            }else if( tracks == "You dont' have any songs" ){
              ismusic = false;
            }else{
              ismusic = true;

            }

          

            this.setState({songs : tracks, ismusic : ismusic });

            console.log(this.state.ismusic,"ismusic");
            console.log(this.state.songs,"songs");
        })
        

      var GVar = GlobalVariables.getApiUrl();

      AsyncStorage.getItem('TOKEN').then((data) => {
        

              fetch( GVar.BaseUrl + GVar.Translations,{
                                  method: 'POST',
                                  headers:{
                                              'token' : data
                                          },
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            if( responseJson.status == 1 ){

                                                var translation = responseJson.translation;
                                                console.log("translation",translation.length);
                                                
                                                for(var i = 0; i < translation.length; i++) {
                                                    
                                                    var translationdata = translation[i];

                                                    console.log(translationdata.en,"translationtranslation");

                                                    AsyncStorage.setItem( translationdata.en , this.checknull(JSON.stringify(translationdata) ));
                                                }
                                            }
                                                
                                  });                    
      });




      AsyncStorage.multiGet(["name", "user_id","LowMoneyWarningType","MoneymanagementAudioType", "MyMoneyAudioType", "ShoppingListAudioType", "ShoppingListType", "LanguageType", "Selfietype","GoShoppingAudioType","TOKEN","GoShoppingAudioType","LowmoneyStatus","LowmoneyTune","LowmoneyValue","PrimaryLanguage","SeconderyLanguage","LanguageTypestatus","LanguageType"]).then(response => {
    
           

            var name                          = response[0][1];
            var user_id                       = response[1][1];
            var LowMoneyWarningType           = response[2][1];
            var MoneymanagementAudioType      = response[3][1];
            var MyMoneyAudioType              = response[4][1];
            var ShoppingListAudioType         = response[5][1];
            var ShoppingListType              = response[6][1];
            var LanguageType                  = response[7][1];
            var Selfietype                    = response[8][1];
            var GoShoppingAudioType           = response[9][1];
            var token                         = response[10][1];
            var GoShoppingAudioType           = response[11][1];
            var LowmoneyStatus                = response[12][1];
            var LowmoneyTune                  = response[13][1];
            var LowmoneyValue                 = response[14][1];
            var PrimaryLanguage               = response[15][1];
            var SeconderyLanguage             = response[16][1];
            var LanguageTypestatus            = response[17][1];
            var LanguageType1                  = response[18][1];
          


            var GVar = GlobalVariables.getApiUrl();

            fetch( GVar.BaseUrl + GVar.Profile,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            if( responseJson.status == 1 ){

                                                var profile = responseJson.profile;
                                                var name = profile.name;
                                                user_id  = profile.user_id;

                                                console.log("PROFILE",responseJson.profile)

                                                this.setState({
                                                     isLoading       : false,
                                                     name           : name.toUpperCase(),
                                                     user_id        : profile.user_id,
                                                     profileimage   : profile.profile_image,
                                                     profileimage2  : profile.profile_image1,
                                                     password       : profile.password,
                                                     subscribe      : profile.subscribe,
                                                     dob            : profile.dob,
                                                     email          : profile.email,
                                                     created_on     : profile.created_on,
                                                });
                                                    
                                                AsyncStorage.multiSet([
                                                                    ["name",                this.checknull(this.state.name)],
                                                                    ["user_id",             this.checknull(this.state.user_id)],
                                                                    ["profileimage",        this.checknull(this.state.profileimage)],
                                                                    ["profileimage2",       this.checknull(this.state.profileimage2)],
                                                                    ["password",            this.checknull(this.state.password)],
                                                                    ["status",              this.checknull(JSON.stringify(profile.status))],
                                                                    ["subscribe",           this.checknull(JSON.stringify(profile.subscribe))],
                                                                    ["dob",                 this.checknull(this.state.dob)],
                                                                    ["email",               this.checknull(this.state.email)],
                                                                    ["created_on",          this.checknull(this.state.created_on)],
                                                                ]);
                                            }
                                                
                                  });

         
            


// update setting

            if( LowmoneyStatus == null ){

              LowmoneyStatus = 'alarm';
            }

            if( MoneymanagementAudioType == null ){

              MoneymanagementAudioType = '0'; 
            }

            if( MoneymanagementAudioType == 0 ){

              MoneymanagementAudioType = '1';
            }else if( MoneymanagementAudioType == 1 ){
              
              MoneymanagementAudioType = '0';
            }

            

            if( MyMoneyAudioType == null ){

              MyMoneyAudioType = '0'; 
            }


            if( MyMoneyAudioType == 0 ){

              MyMoneyAudioType = '1';
            }else if( MyMoneyAudioType == 1 ){
              
              MyMoneyAudioType = '0';
            }


            if( ShoppingListAudioType == null ){

              ShoppingListAudioType = '0'; 
            }


            if( ShoppingListAudioType == 0 ){

              ShoppingListAudioType = '1';
            }else if( ShoppingListAudioType == 1 ){
              
              ShoppingListAudioType = '0';
            }

            if( GoShoppingAudioType == null ){

              GoShoppingAudioType = '0'; 
            }


            if( GoShoppingAudioType == 0 ){

              GoShoppingAudioType = '1';
            }else if( GoShoppingAudioType == 1 ){
              
              GoShoppingAudioType = '0';
            }

            if( ShoppingListType == null ){

              ShoppingListType = 'column';
            }

            if( PrimaryLanguage == null ){

              PrimaryLanguage = 'en';
            }

            if( SeconderyLanguage == null ){

              SeconderyLanguage = 'en';
            }

            if( LanguageType == null ){

              LanguageType = '"single"';
            } 

             
            if( LowmoneyValue == null ){

              LowmoneyValue = '0';
            }

            if( Selfietype == null ){

              Selfietype = 'same';
            }

            
            var formdataSetting = new FormData();

            


            formdataSetting.append('user_id', user_id );
            formdataSetting.append('language', JSON.parse(LanguageType) );
            formdataSetting.append('primary_language', PrimaryLanguage );
            formdataSetting.append('secondary_language', SeconderyLanguage);
            formdataSetting.append('lowmoney_value', LowmoneyValue );
            formdataSetting.append('lowmoney_status', LowmoneyStatus );
            formdataSetting.append('lowmoney_tune', LowmoneyTune );
            formdataSetting.append('lowmoney_alert', LowMoneyWarningType );
            formdataSetting.append('selfie_type', Selfietype );
            formdataSetting.append('audio_moneymanagement', MoneymanagementAudioType );
            formdataSetting.append('audio_shopping', ShoppingListAudioType );
            formdataSetting.append('audio_moneyihave', MyMoneyAudioType);
            formdataSetting.append('audio_goshopping', GoShoppingAudioType );
            formdataSetting.append('shoppinglist_type', ShoppingListType );

            console.log("formdataUpdateSetting", formdataSetting);



           
            try {
                let response = fetch( GVar.BaseUrl + GVar.UpdateSetting,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdataSetting,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            console.log(responseJson,"formdataUpdateSettingResponce");
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

// end update setting


            /**********************************************************************/          
          
            var GVar = GlobalVariables.getApiUrl();

            let formdataP = new FormData();
            
            if(PrimaryLanguage == null){
              PrimaryLanguage = 'en';
            }

            formdataP.append('language', PrimaryLanguage );

            try {
                  let response = fetch(GVar.Pantry,{
                                    method: 'POST',
                                    headers:{
                                                'token' : token
                                            },
                                    body: formdataP,   
                                    }).then((response) => response.json())
                                             .then((responseJson) => {
                                               
                                               AsyncStorage.setItem('PANTRYDATA',this.checknull(JSON.stringify(responseJson.pantry)));

                                             });

                
               }catch (error) {

                if( error.line == 18228 ){
                  //Alert.alert("Please connect to network");
                }

               
              }



            /**********************************************************************/


            this.setState({user_id: user_id});
            var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('name', name );


// update name 

            try {
                let response = fetch( GVar.BaseUrl + GVar.UpdateName,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                           // Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

//end update name






// get languages
           
            try {
                let response = fetch( GVar.BaseUrl + GVar.Languages,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            this.setState({LANGUAGES : responseJson})
                                            AsyncStorage.setItem('LANGUAGES',this.checknull(JSON.stringify(responseJson) ));
                                           
                                           });
              
                
            }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

            try {
                let response = fetch( GVar.BaseUrl + GVar.Languages,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            this.setState({ALL_SecondryLANGUAGES : responseJson})
                                            AsyncStorage.setItem('ALL_SecondryLANGUAGES',this.checknull(JSON.stringify(responseJson)));
                                           
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

// end languages


// get setting
            try {
                let response = fetch( GVar.BaseUrl + GVar.Setting,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            if( responseJson.status == '1'){

                                              var setting = responseJson.setting;

                                              console.log(setting,"setting")


                                              if( setting.lowmoney_status == 'tune' ){
                                                 
                                                AsyncStorage.setItem('LowMoneyWarningType', '0' );
                                                this.setState({LowMoneyWarningType: 0});
                                              }

                                              if( setting.lowmoney_status == 'alarm' ){
                                                 
                                                AsyncStorage.setItem('LowMoneyWarningType', '1' );
                                                this.setState({LowMoneyWarningType: 1});
                                              }


                                              if( setting.language == 'single' ){

                                                this.setState({ isSingle : true });
                                                AsyncStorage.setItem('isSingle', 'true' );

                                              }else{

                                                this.setState({ isSingle : false });
                                                AsyncStorage.setItem('isSingle', 'false' );
                                              }


                                              
                                              if( setting.audio_moneymanagement == 1 ){

                                                this.setState({ MMoneyAudioType : false });
                                                AsyncStorage.setItem('MoneymanagementAudioType', '1' );
                                              }else{

                                                this.setState({ MMoneyAudioType : true });
                                                AsyncStorage.setItem('MoneymanagementAudioType', '0' );
                                              }


                                              if( setting.audio_moneyihave == 1 ){

                                                this.setState({ MMoneyAudioType : true });
                                                AsyncStorage.setItem('MyMoneyAudioType', '0' );
                                              }else{

                                                this.setState({ MMoneyAudioType : true });
                                                AsyncStorage.setItem('MyMoneyAudioType', '1' );
                                              }
                                              
                                      
                                             


                                               AsyncStorage.multiSet([
                                                                       
                                                                        ["ShoppingListType",            this.checknull(setting.shoppinglist_type)],
                                                                        ["Selfietype",                  this.checknull(setting.selfie_type)],
                                                                        ["LowmoneyTune",                this.checknull(setting.lowmoney_tune)],
                                                                        ["LowmoneyValue",               this.checknull(setting.lowmoney_value)],
                                                                 //     ["LowmoneyValue",               LowmoneyValue],                                                                      
                                                                        ["PrimaryLanguage",             this.checknull(setting.primary_language)],
                                                                        ["SeconderyLanguage",           this.checknull(setting.secondary_language)],
                                                                        
                                                                        
                                                                    ]);



                                               this.setState({
                                                                ShoppingListType        : setting.shoppinglist_type,
                                                                Selfietype              : setting.selfie_type,
                                                                LowmoneyTune            : setting.lowmoney_tune,
                                                                LowmoneyValue           :  setting.lowmoney_value,
                                                                PrimaryLanguage         : setting.primary_language,
                                                                SeconderyLanguage       : setting.secondary_language,
                                                                //isLoading               : false
                                                          });


                                            }else{

                                              
                                            }
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }
// end get setting           
          
    });

    this.setState({ isLoading : false });
  }

  getIndex(value, arr, prop) {
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }

  

  onSelectPrimaryLanguage(item){   

      
      this.setState({PrimaryLanguage : item.language_code});
      this.setState({isSingle : true});
      this.setState({isLoading : true});
      AsyncStorage.setItem('PrimaryLanguage',this.checknull(item.language_code ));
      AsyncStorage.setItem('isSingle', 'true' );
     
      

      var ALL_SecondryLANGUAGES = this.state.ALL_SecondryLANGUAGES;
   
      var index = this.getIndex( item.language_code, ALL_SecondryLANGUAGES.languages, 'language_code'  );
      
      if( index > -1 ){

          AsyncStorage.getItem('LastPrimaryLanguage').then((data) => {
              


               
                ALL_SecondryLANGUAGES.languages.splice( index ,1);

                if( this.state.LastPrimaryLanguage ){

                  console.log(this.state.LastPrimaryLanguage,"LastPrimaryLanguage")

                  var datapush = this.state.LastPrimaryLanguage;
                  ALL_SecondryLANGUAGES.languages.push(datapush);
                  
                }

                AsyncStorage.setItem('LastPrimaryLanguage',this.checknull(item ));
                this.setState({LastPrimaryLanguage : item});
                
          });


        
      }else{

        Alert.alert('Try again');
      }

      this.setState({isLoading : false});
         
  }

  calledfortranslation(){

      this.setState({isLoading : true});

      AsyncStorage.getItem('PrimaryLanguage').then((P_LANG) => {

                              

          
                        AsyncStorage.getItem('WELCOME PAGE').then((data) => {

                                            var WECOME = JSON.parse(data);

                                            if( P_LANG == 'en'){

                                              this.setState({WECOME : WECOME.en})
                                            }

                                            if( P_LANG == 'el'){

                                                this.setState({WECOME : WECOME.el})
                                            }

                                            if( P_LANG == 'cmn'){

                                                this.setState({WECOME : WECOME.cmn})
                                            }

                                            if( P_LANG == 'zh'){

                                                this.setState({WECOME : WECOME.zh})
                                            }

                                            if( P_LANG == 'ar'){

                                                this.setState({WECOME : WECOME.ar})
                                            }

                                            if( P_LANG == 'vi'){

                                                this.setState({WECOME : WECOME.vi})
                                            }

                                            if( P_LANG == 'it'){

                                                this.setState({WECOME : WECOME.it})
                                            }

                                            if( P_LANG == 'es'){

                                                this.setState({WECOME : WECOME.es})
                                            }

                                            if( P_LANG == 'hi'){

                                                this.setState({WECOME : WECOME.hi})
                                            }

                                            if( P_LANG == 'pa'){

                                                this.setState({WECOME : WECOME.pa})
                                            }

                                            if( P_LANG == 'fil'){

                                               this.setState({WECOME : WECOME.fil})
                                            }

                        });

                        AsyncStorage.getItem('SHOPPING LIST').then((data) => {

                              var SHOPPINGLIST = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SHOPPINGLIST : SHOPPINGLIST.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SHOPPINGLIST : SHOPPINGLIST.fil})
                              }

                        });

                        AsyncStorage.getItem('LETS GO SHOPPING').then((data) => {

                              var GOSHOPPING = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({GOSHOPPING : GOSHOPPING.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({GOSHOPPING : GOSHOPPING.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({GOSHOPPING : GOSHOPPING.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({GOSHOPPING : GOSHOPPING.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({GOSHOPPING : GOSHOPPING.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({GOSHOPPING : GOSHOPPING.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({GOSHOPPING : GOSHOPPING.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({GOSHOPPING : GOSHOPPING.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({GOSHOPPING : GOSHOPPING.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({GOSHOPPING : GOSHOPPING.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({GOSHOPPING : GOSHOPPING.fil})
                              }

                        });

              
                        AsyncStorage.getItem('HOW MUCH MONEY DO I HAVE').then((data) => {

                              var MONEYIHAVE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({MONEYIHAVE : MONEYIHAVE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({MONEYIHAVE : MONEYIHAVE.fil})
                              }

                        });

                        AsyncStorage.getItem('MONEY MANAGEMENT').then((data) => {

                              var MONEYMANAGEMENT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.fil})
                              }

                        });

                        AsyncStorage.getItem('LOGOUT').then((data) => {

                              var LOGOUT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({LOGOUT : LOGOUT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({LOGOUT : LOGOUT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({LOGOUT : LOGOUT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({LOGOUT : LOGOUT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({LOGOUT : LOGOUT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({LOGOUT : LOGOUT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({LOGOUT : LOGOUT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({LOGOUT : LOGOUT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({LOGOUT : LOGOUT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({LOGOUT : LOGOUT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({LOGOUT : LOGOUT.fil})
                              }

                        });


                        AsyncStorage.getItem('NAME').then((data) => {

                              var NAME = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({NAME : NAME.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({NAME : NAME.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({NAME : NAME.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({NAME : NAME.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({NAME : NAME.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({NAME : NAME.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({NAME : NAME.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({NAME : NAME.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({NAME : NAME.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({NAME : NAME.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({NAME : NAME.fil})
                              }

                        });

                        AsyncStorage.getItem('LANGUAGE').then((data) => {

                              var LANGUAGE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({LANGUAGE : LANGUAGE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({LANGUAGE : LANGUAGE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({LANGUAGE : LANGUAGE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({LANGUAGE : LANGUAGE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({LANGUAGE : LANGUAGE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({LANGUAGE : LANGUAGE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({LANGUAGE : LANGUAGE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({LANGUAGE : LANGUAGE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({LANGUAGE : LANGUAGE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({LANGUAGE : LANGUAGE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({LANGUAGE : LANGUAGE.fil})
                              }

                        });

                        AsyncStorage.getItem('DUAL').then((data) => {

                              var DUAL = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({DUAL : DUAL.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({DUAL : DUAL.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({DUAL : DUAL.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({DUAL : DUAL.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({DUAL : DUAL.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({DUAL : DUAL.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({DUAL : DUAL.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({DUAL : DUAL.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({DUAL : DUAL.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({DUAL : DUAL.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({DUAL : DUAL.fil})
                              }

                        });

                        AsyncStorage.getItem('SINGLE').then((data) => {

                              var SINGLE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SINGLE : SINGLE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SINGLE : SINGLE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SINGLE : SINGLE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SINGLE : SINGLE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SINGLE : SINGLE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SINGLE : SINGLE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SINGLE : SINGLE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SINGLE : SINGLE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SINGLE : SINGLE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SINGLE : SINGLE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SINGLE : SINGLE.fil})
                              }

                        });

                        AsyncStorage.getItem('LOW MONEY WARNING').then((data) => {

                              var MONEYWARNING = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({MONEYWARNING : MONEYWARNING.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({MONEYWARNING : MONEYWARNING.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({MONEYWARNING : MONEYWARNING.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({MONEYWARNING : MONEYWARNING.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({MONEYWARNING : MONEYWARNING.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({MONEYWARNING : MONEYWARNING.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({MONEYWARNING : MONEYWARNING.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({MONEYWARNING : MONEYWARNING.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({MONEYWARNING : MONEYWARNING.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({MONEYWARNING : MONEYWARNING.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({MONEYWARNING : MONEYWARNING.fil})
                              }

                        });


                        AsyncStorage.getItem('PLAY A TUNE').then((data) => {

                              var PLAYATUNE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({PLAYATUNE : PLAYATUNE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({PLAYATUNE : PLAYATUNE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({PLAYATUNE : PLAYATUNE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({PLAYATUNE : PLAYATUNE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({PLAYATUNE : PLAYATUNE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({PLAYATUNE : PLAYATUNE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({PLAYATUNE : PLAYATUNE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({PLAYATUNE : PLAYATUNE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({PLAYATUNE : PLAYATUNE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({PLAYATUNE : PLAYATUNE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({PLAYATUNE : PLAYATUNE.fil})
                              }

                        });


                        AsyncStorage.getItem('ALARM').then((data) => {

                              var ALARM = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({ALARM : ALARM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({ALARM : ALARM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({ALARM : ALARM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({ALARM : ALARM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({ALARM : ALARM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({ALARM : ALARM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({ALARM : ALARM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({ALARM : ALARM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({ALARM : ALARM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({ALARM : ALARM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({ALARM : ALARM.fil})
                              }

                        });

                        AsyncStorage.getItem('ALL SELFIES INDEPENDENT').then((data) => {

                              var SELFIESINDEPENDENT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SELFIESINDEPENDENT : SELFIESINDEPENDENT.fil})
                              }

                        });

                        AsyncStorage.getItem('SELFIES ALL SAME').then((data) => {

                              var SELFIESALLSAME = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SELFIESALLSAME : SELFIESALLSAME.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SELFIESALLSAME : SELFIESALLSAME.fil})
                              }

                        });

                        AsyncStorage.getItem('SELFIES ALL SAME').then((data) => {

                              var SELFIESALLSAME = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SELFIESALLSAME : SELFIESALLSAME.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SELFIESALLSAME : SELFIESALLSAME.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SELFIESALLSAME : SELFIESALLSAME.fil})
                              }

                        });

                        AsyncStorage.getItem('OPTIONS').then((data) => {

                              var OPTIONS = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({OPTIONS : OPTIONS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({OPTIONS : OPTIONS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({OPTIONS : OPTIONS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({OPTIONS : OPTIONS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({OPTIONS : OPTIONS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({OPTIONS : OPTIONS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({OPTIONS : OPTIONS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({OPTIONS : OPTIONS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({OPTIONS : OPTIONS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({OPTIONS : OPTIONS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({OPTIONS : OPTIONS.fil})
                              }

                        });


                        AsyncStorage.getItem('AUDIO ON TAP').then((data) => {

                              var AUDIOONTAP = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({AUDIOONTAP : AUDIOONTAP.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({AUDIOONTAP : AUDIOONTAP.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({AUDIOONTAP : AUDIOONTAP.fil})
                              }

                        });

                        AsyncStorage.getItem('REPEATED IN LINE').then((data) => {

                              var REPEATEDINLINE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({REPEATEDINLINE : REPEATEDINLINE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({REPEATEDINLINE : REPEATEDINLINE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({REPEATEDINLINE : REPEATEDINLINE.fil})
                              }

                        });

                        AsyncStorage.getItem('NUMBERED COLUMN').then((data) => {

                              var NUMBEREDCOLUMN = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({NUMBEREDCOLUMN : NUMBEREDCOLUMN.fil})
                              }

                        });




                       


                        AsyncStorage.getItem('TOKEN').then((token) => {

     
                              var GVar = GlobalVariables.getApiUrl();

                              if(P_LANG == null){

                                PrimaryLanguage = 'en';
                              }else{

                                PrimaryLanguage = P_LANG;
                              }

                              let formdataP = new FormData();
                              formdataP.append('language', PrimaryLanguage );

                              console.log("formdataP",formdataP);
                              console.log("formdataP",GVar.Pantry);
                              console.log("token",token);

                              try {
                                    let response = fetch(GVar.Pantry,{
                                                      method: 'POST',
                                                      headers:{
                                                                  'token' : token
                                                              },
                                                      body: formdataP,   
                                                      }).then((response) => response.json())
                                                               .then((responseJson) => {
                                                                 
                                                                 console.log('response',responseJson)
                                                                 AsyncStorage.setItem('PANTRYDATA',this.checknull(JSON.stringify(responseJson.pantry)) );

                                                               });

                                  
                              }catch (error) {

                                  if( error.line == 18228 ){
                                    //Alert.alert("Please connect to network");
                                  }
                              }
                              

                        });

                        
      AsyncStorage.getItem('Your voice feature is active for money management').then((data) => {

                         

                              var voicefeatureMM = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureMM : voicefeatureMM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureMM : voicefeatureMM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureMM : voicefeatureMM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureMM : voicefeatureMM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureMM : voicefeatureMM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureMM : voicefeatureMM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureMM : voicefeatureMM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureMM : voicefeatureMM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureMM : voicefeatureMM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureMM : voicefeatureMM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureMM : voicefeatureMM.fil})
                              }

                        });

                        AsyncStorage.getItem('Your voice feature is active for my money').then((data) => {

                         

                              var voicefeatureMyM = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureMyM : voicefeatureMyM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureMyM : voicefeatureMyM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureMyM : voicefeatureMyM.fil})
                              }

                        });

                        AsyncStorage.getItem('Your voice feature is active for shopping list').then((data) => {

                         

                              var voicefeatureSL = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureSL : voicefeatureSL.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureSL : voicefeatureSL.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureSL : voicefeatureSL.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureSL : voicefeatureSL.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureSL : voicefeatureSL.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureSL : voicefeatureSL.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureSL : voicefeatureSL.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureSL : voicefeatureSL.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureSL : voicefeatureSL.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureSL : voicefeatureSL.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureSL : voicefeatureSL.fil})
                              }

                        });


                        AsyncStorage.getItem('Your voice feature is active for go shopping').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureGS : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureGS : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureGS : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureGS : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureGS : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureGS : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureGS : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureGS : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureGS : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureGS : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureGS : voicefeatureGS.fil})
                              }

                        });

                            
                          AsyncStorage.getItem('Are you sure to log out?').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforlogout : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforlogout : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('ok').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforok : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforok : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforok : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforok : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforok : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforok : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforok : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforok : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforok : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('cancel').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforcancel : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforcancel : voicefeatureGS.fil})
                              }

                        });



                        AsyncStorage.getItem('ON').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforON : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforON : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforON : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforON : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforON : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforON : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforON : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforON : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforON : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforON : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforON : voicefeatureGS.fil})
                              }

                        });

                          AsyncStorage.getItem('OFF').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforOff : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforOff : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforOff : voicefeatureGS.fil})
                              }

                        });
    });


      
    AsyncStorage.getItem('SeconderyLanguage').then((P_LANG) => {

        AsyncStorage.getItem('Your voice feature is active for money management').then((data) => {

                         

                              var SvoicefeatureMM = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({SvoicefeatureMM : SvoicefeatureMM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SvoicefeatureMM : SvoicefeatureMM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SvoicefeatureMM : SvoicefeatureMM.fil})
                              }

                        });

                        AsyncStorage.getItem('Your voice feature is active for my money').then((data) => {

                         

                              var SvoicefeatureMyM = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({SvoicefeatureMyM : SvoicefeatureMyM.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SvoicefeatureMyM : SvoicefeatureMyM.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SvoicefeatureMyM : SvoicefeatureMyM.fil})
                              }

                        });

                        AsyncStorage.getItem('Your voice feature is active for shopping list').then((data) => {

                         

                              var SvoicefeatureSL = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({SvoicefeatureSL : SvoicefeatureSL.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SvoicefeatureSL : SvoicefeatureSL.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SvoicefeatureSL : SvoicefeatureSL.fil})
                              }

                        });


                        AsyncStorage.getItem('Your voice feature is active for go shopping').then((data) => {

                         

                              var SvoicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({SvoicefeatureGS : SvoicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SvoicefeatureGS : SvoicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SvoicefeatureGS : SvoicefeatureGS.fil})
                              }

                        });

    });

    this.setState({isLoading : false});
    this.setState({animating : true});
    setTimeout(() => {
    this.setState({animating: false});
   }, 3000);
   
    
  }


  calledforprimarylanguagechange(){

    
    this.setModalVisiblelanguage(!this.state.setModalVisiblelanguage)
    this.calledfortranslation();
  }

  calledforlanguagechange(){

       var GVar = GlobalVariables.getApiUrl();

      
       console.log(this.state.PrimaryLanguage,"PrimaryLanguagePrimaryLanguage")
        
       this.setModalVisibleSlanguage(!this.state.setModalVisiblelanguage)
       this.calledfortranslation(); 
    
  }


  onSelectSecondryLanguage(item) {
  
  if(item.language_code==this.state.PrimaryLanguage){
     Alert.alert('Primary and Secondary Language are same')
  }

   else
    {

    this.setState({SeconderyLanguage : item.language_code});
    this.setState({isSingle : false});
    AsyncStorage.setItem('SeconderyLanguage',this.checknull(item.language_code ));
    AsyncStorage.setItem('isSingle', 'false' );
    }
}


  onSelectLowMoneyWaringAudioType(index, value){

    var type = null;

    if( index == 0 ){

        type  = 'tune';
    }else{

        type  = 'alarm';
    }

    if( type == 'alarm' ){

      this.setState({modalVisible : true});
    }else{

      this.setState({devicemusic : true});
    
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    this.setState({ LowMoneyWarningType : index });
    AsyncStorage.setItem('LowMoneyWarningType',this.checknull(JSON.stringify(index)) );
  
    console.log(formdata,"formdata");                                
   
    try {
        var response = fetch( GVar.BaseUrl + GVar.LowMoneyWarningType , {
                        method: 'POST',
                        headers:{
                                    'token' : this.state.token

                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            console.log(responseJson,"responseJson");
                            
                             if( responseJson.status == 1 ){

                                  this.setState({ LowMoneyWarningType : index });
                                  AsyncStorage.setItem('LowMoneyWarningType',this.checknull(JSON.stringify(index)));
                                  //Alert.alert(responseJson.message);
                             }else if( responseJson.status == 0 ){
                                  //Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }

  }


 /* onSelectMoneyManagement(index, value){

    var GVar = GlobalVariables.getApiUrl();

    var type = null;

    if( index == 0 ){

        type  = '1';
        //Tts.speak('Your Voice Feature is Active for Money management');

        
    }else{

        type  = '0';
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    var token = this.state.token;



    this.setState({ MoneymanagementAudioType : index });
    AsyncStorage.setItem('MoneymanagementAudioType', JSON.stringify(index) );
  
    try {
        var response = fetch(GVar.BaseUrl + GVar.MoneymanagementAudioType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(JSON.stringify(responseJson));
                            
                             if( responseJson.status == 1 ){
                                  this.setState({ MoneymanagementAudioType : index, });
                                  AsyncStorage.setItem('MoneymanagementAudioType', JSON.stringify(index) );
                             }else if( responseJson.status == 0 ){
                                  //Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }

  }


  onSelectShoppingListAudioType(index, value){

    var token   = this.state.token;
    var type    = null;

    if( index == 0 ){

        type  = '1';
        Tts.speak('Your Voice Feature is Active for Shopping List');
    }else{

        type  = '0';
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    this.setState({ ShoppingListAudioType : index });
    AsyncStorage.setItem('ShoppingListAudioType', JSON.stringify(index) );
                                  

   
    try {
        var response = fetch(GVar.BaseUrl + GVar.ShoppingListAudioType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(JSON.stringify(responseJson));
                            
                             if( responseJson.status == 1 ){
                                  this.setState({ ShoppingListAudioType : index });
                                  AsyncStorage.setItem('ShoppingListAudioType', JSON.stringify(index) );
                                  //Alert.alert(responseJson.message);
                             }else if( responseJson.status == 0 ){
                                 // Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }

  }



  onSelectMyMoneyAudioType(index, value){

    var token   = this.state.token;
    var type    = null;

    if( index == 0 ){

        type  = '1';
         Tts.speak('Your Voice Feature is Active for My Money');
    }else{

        type  = '0';
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    this.setState({ MyMoneyAudioType : index });
    AsyncStorage.setItem('MyMoneyAudioType',  JSON.stringify(index) );
                                 

   
    try {
        var response = fetch(GVar.BaseUrl + GVar.MyMoneyAudioType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(JSON.stringify(responseJson));
                            
                             if( responseJson.status == 1 ){
                                  this.setState({ MyMoneyAudioType : index });
                                  AsyncStorage.setItem('MyMoneyAudioType',  JSON.stringify(index) );
                                  //Alert.alert(responseJson.message);
                             }else if( responseJson.status == 0 ){
                                  //Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }

  }



  onSelectGoShoppingAudioType(index, value){


    var token   = this.state.token ;

    var type    = null;

    if( index == 0 ){

        type  = '1';
        Tts.speak('Your Voice Feature is Active for Go Shopping');
    }else{

        type  = '0';
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    this.setState({ GoShoppingAudioType : index });
    AsyncStorage.setItem('GoShoppingAudioType',  JSON.stringify(index) );
    
   
    try {
        var response = fetch(GVar.BaseUrl + GVar.GoShoppingAudioType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(JSON.stringify(responseJson));
                            
                             if( responseJson.status == 1 ){
                                  this.setState({ GoShoppingAudioType : index });
                                  AsyncStorage.setItem('GoShoppingAudioType',  JSON.stringify(index) );
                                  //Alert.alert(responseJson.message);
                             }else if( responseJson.status == 0 ){
                                  //Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }

  }*/


  onPressShoopingList(){
    Actions.ShoppingList();
  }

  onPressHome(){
    Actions.Home();
  }

  onPressPantry(){
    Actions.Pantry();
  }

  onPressMyMoney(){
     Actions.MyMoney();
  }

  onPressMoneyManagement(){
    Actions.MoneyManagement();
  }

  onPressGoShopping(){
    Actions.GoShopping();
  }

  onPressSetUp(){
    Actions.SetUp();
  }

  onPressShoopingListOption( value ){
    //Alert.alert(value);

    var GVar = GlobalVariables.getApiUrl();
    var token = this.state.token;

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', value);

    this.setState({ ShoppingListType : value });
    AsyncStorage.setItem('ShoppingListType',  value );

   
    try {
        var response = fetch(GVar.BaseUrl + GVar.ShoppingListType , {
                        method: 'POST',
                        headers:{
                                    'token' : token,
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(responseJson);
                            
                             if( responseJson.status == 1 ){

                                  //Alert.alert(responseJson.message);
                                  this.setState({ ShoppingListType : value });
                                  AsyncStorage.setItem('ShoppingListType', this.checknull(value ));
                                  
                             }else if( responseJson.status == 0 ){
                                 // Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }
  }



  // onPressLanguageOption( value ){

  //   //Alert.alert(value);

  //   var GVar = GlobalVariables.getApiUrl();

  //   let formdata = new FormData();

  //   formdata.append('user_id', this.state.user_id);
  //   formdata.append('type', value);

   
  //   try {
  //       var response = fetch(GVar.BaseUrl + GVar.LanguageType , {
  //                       method: 'POST',
  //                       headers:{
  //                                   'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiQW1pdCBLdW1hciIsImVtYWlsIjoiYW1pdC5rdW1hckBhcHBpZGVhc2luYy5jb20iLCJkb2IiOiIxNTMxNDc3NTk5In0.NOcppFh3ep4AEf7Gr7SfX3du82uuSTMonGIM5ZNJNvs'

  //                               },
  //                       body: formdata,
  //                     }).then((response) => response.json())
  //                        .then((responseJson) => {

  //                           //Alert.alert(responseJson);
                            
  //                            if( responseJson.status == 1 ){

  //                                 //Alert.alert(responseJson.message);
  //                                 this.setState({ LanguageType : value });
  //                                 AsyncStorage.setItem('LanguageType',  value );
                                  
  //                            }else if( responseJson.status == 0 ){
  //                                 Alert.alert(responseJson.message);
  //                            }
  //                        });

  //      }catch (error) {

  //     }
  // }


  onPressSelfietype( value ){

    //Alert.alert(this.state.token);
    var token     = this.state.token;
    var GVar      = GlobalVariables.getApiUrl();

    let formdata  = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', value);

    this.setState({ Selfietype : value });
    AsyncStorage.setItem('Selfietype', this.checknull(value ));

    
    try {
        var response = fetch(GVar.BaseUrl + GVar.Selfietype , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(responseJson);
                            
                             if( responseJson.status == 1 ){

                                  //Alert.alert(responseJson.message);
                                  this.setState({ Selfietype : value });
                                  AsyncStorage.setItem('Selfietype',this.checknull( value ) );
                                  
                             }else if( responseJson.status == 0 ){
                                 // Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }
  }

  onSelectLowMoney(  ){

    Alert.alert(this.state);

    
  }

  onchangeName( name ){

    AsyncStorage.multiSet([
                ["name", this.checknull(name)],
            ]);

    this.setState({ name : name});

    AsyncStorage.multiGet(["user_id", "TOKEN"]).then(response => {


                
            var user_id               = response[0][1];
            var token                = response[1][1];


   
           
            var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('name', name );

            try {
                let response = fetch( GVar.BaseUrl + GVar.UpdateName,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                           // Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }
            }

           

      });
  }

  

  onChangeLowmoneyValue(value){

    console.log(value,"value")

    if (value == parseInt(value, 10) ){

         
          AsyncStorage.multiGet(["user_id", "TOKEN"]).then(response => {

            AsyncStorage.multiSet([
                ["LowmoneyValue",this.checknull(value)],
            ]);

            this.setState({ LowmoneyValue : value});


                
            var user_id               = response[0][1];
            var token                = response[1][1];


   
           
            var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('money', value );

            try {
                let response = fetch( GVar.BaseUrl + GVar.LowMoneyValue,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                           // Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }
            }

            

      });
    }else{

          if( value == ''){

            value = 0;

            AsyncStorage.multiGet(["user_id", "TOKEN"]).then(response => {

                    AsyncStorage.multiSet([
                        ["LowmoneyValue",  this.checknull(JSON.stringify(value))],
                    ]);

                    this.setState({ LowmoneyValue : value});


                        
                    var user_id               = response[0][1];
                    var token                = response[1][1];


           
                   
                    var GVar = GlobalVariables.getApiUrl();

                    let formdata = new FormData();

                    formdata.append('user_id', user_id );
                    formdata.append('money', value );

                    try {
                        let response = fetch( GVar.BaseUrl + GVar.LowMoneyValue,{
                                          method: 'POST',
                                          headers:{
                                                      'token' : token
                                                  },
                                          body: formdata,        
                                          }).then((response) => response.json())
                                                   .then((responseJson) => {

                                                    //Alert.alert(JSON.stringify(responseJson));
                                                        
                                                   });
                      
                        
                     }catch (error) {

                      if( error.line == 18228 ){
                        


                      }
                    }

                    

              });

          }else{

            AsyncStorage.multiSet([
                ["LowmoneyValue", 0],
            ]);

            this.setState({ LowmoneyValue : 0});
            Alert.alert("Please enter valid money")
          }
          
       
    }


  }



     onPressLogout(){

        //Alert.alert("fsd");
        AsyncStorage.removeItem('TOKEN');
        Actions.Login();

    }

    getIndex(value, arr, prop) {
        for(var i = 0; i < arr.length; i++) {
            if(arr[i][prop] === value) {
                return i;
            }
        }
        return -1; //to handle the case where the value doesn't exist
    }


  
  createSelectItems(){

          var views = [];

          var PrimaryLanguage = this.state.PrimaryLanguage;
         
          var LANGUAGES       =this.state.LANGUAGES;

          if( LANGUAGES == undefined ){

            views.push(<Option>Language Loading</Option>)

            return views;
          }else{

            console.log(LANGUAGES.languages,"LANGUAGES");

            var languages  = LANGUAGES.languages;

            

            var index = this.getIndex(PrimaryLanguage, languages, 'language_code');
            languages.splice(index, 1);

            for (var i = 0; i < languages.length; i++) {
              
              views.push(<Option value = {languages[i].language_code}>{languages[i].language}</Option>)
            }

            

            return views;
          }

          
    }


  onChangeLanguage(value){

   

    var type = null;
    var isSingle = null;

    if( value ){

      type  = 'single';

      isSingle = 'true';

      this.setState({ showsinglelangage : true });
      this.setState({ showsduallangage : false });
      this.setState({ pre_lan : this.state.SeconderyLanguage});
      this.setState({ setModalVisibleLanguage : true });
    }else{

      type  = 'dual';
      isSingle  = 'false';

      this.setState({ showsinglelangage : false });
      this.setState({pre_lan: this.state.SeconderyLanguage});
      this.setState({ showsduallangage : true });
      this.setState({ setModalVisibleSLanguage : true });
    }

    this.setState({ isSingle: value});
   

    var GVar  = GlobalVariables.getApiUrl();
    var token = this.state.token; 

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

  

    this.setState({  LanguageType: type });
   
    AsyncStorage.multiSet([
        ["LanguageType", this.checknull(JSON.stringify(type) )],
        ["isSingle", this.checknull(JSON.stringify(isSingle))]
    ]);
    


   
    try {
        var response = fetch(GVar.BaseUrl + GVar.LanguageType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {
                                                            
                            //Alert.alert(JSON.stringify(responseJson));
                            
                             
                         });

       }catch (error) {

      }


    
  }


  onChangeMMAudioType( value ){

    var index;
    var MMoneyAudioType;

    if( value ){


      MMoneyAudioType = 'true';
      index = 0;
      
    }else{

      index = 1;
      MMoneyAudioType = 'false';
    }

    
    this.setState({MMoneyAudioType : value});


    var GVar = GlobalVariables.getApiUrl();

    var type = null;

    if( index == 0 ){

        type  = '1';

        if( this.state.voicefeatureMM ){

            Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

              console.log(setDefaultLanguage,"setDefaultLanguage")
              if( setDefaultLanguage == 'success'){
                
              }else{
                Tts.requestInstallData();
              }
          }).catch((error) => {

            Alert.alert("Please install all the voice packages for getting all voices from the application.");
            Tts.requestInstallData();
            console.log(error,"setDefaultLanguage41");
          });


          Tts.speak(this.state.voicefeatureMM);

            if( !this.state.isSingle && this.state.SvoicefeatureMM ){

              Tts.setDefaultLanguage(this.state.SeconderyLanguage);

              if( this.state.SeconderyLanguage != this.state.PrimaryLanguage){
                Tts.speak(this.state.SvoicefeatureMM);
              }
              
            } 
        
        }else{
          Tts.speak('Your Voice Feature is Active for Money Management');
        }
       
    }else{

        type  = '0';
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    var token = this.state.token;



    this.setState({ MoneymanagementAudioType : index });
    AsyncStorage.setItem('MoneymanagementAudioType',this.checknull(JSON.stringify(index)));

    AsyncStorage.setItem('MMoneyAudioType', MMoneyAudioType );
  
    try {
        var response = fetch(GVar.BaseUrl + GVar.MoneymanagementAudioType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(JSON.stringify(responseJson));
                            
                         });

       }catch (error) {

      }

  }

  onChangeHMMoneyAudioType( value ){

    var index;
    var HMMoneyAudioType;

    if( value ){


      HMMoneyAudioType = 'true';
      index = 0;
      
    }else{

      index = 1;
      HMMoneyAudioType = 'false';
    }

    
    this.setState({HMMoneyAudioType : value});

    var token   = this.state.token;
    var type    = null;

    Tts.voices().then(voices => console.log(voices,"ALLSUPPORTEDVOICE"));

    if( index == 0 ){

        type  = '1';

        if( this.state.voicefeatureMyM ){

          Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

              console.log(setDefaultLanguage,"setDefaultLanguage")
              if( setDefaultLanguage == 'success'){
                
              }else{
                Tts.requestInstallData();
              }
          }).catch((error) => {

            Alert.alert("Please install all the voice packages for getting all voices from the application.");
            Tts.requestInstallData();
            console.log(error,"setDefaultLanguage41");
          });

          Tts.speak(this.state.voicefeatureMyM);
          
          if( !this.state.isSingle && this.state.SvoicefeatureMyM ){

            Tts.setDefaultLanguage(this.state.SeconderyLanguage);
            if( this.state.SeconderyLanguage != this.state.PrimaryLanguage){
                Tts.speak(this.state.SvoicefeatureMyM);
            }
          } 
        }else{
          Tts.speak('Your Voice Feature is Active for My money');
        }
        
    }else{

        type  = '0';
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    this.setState({ MyMoneyAudioType : index });
    AsyncStorage.setItem('MyMoneyAudioType',this.checknull(JSON.stringify(index)) );


    AsyncStorage.setItem('HMMoneyAudioType', this.checknull(HMMoneyAudioType ));
                                 

   
    try {
        var response = fetch(GVar.BaseUrl + GVar.MyMoneyAudioType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                           // Alert.alert(JSON.stringify(responseJson));
                             
                         });

       }catch (error) {

      }

  }


  onChangeSLAudioType( value ){

    var index;
    var SLAudioType;

    if( value ){


      SLAudioType = 'true';
      index = 0;
      
    }else{

      index = 1;
      SLAudioType = 'false';
    }

    
    this.setState({SLAudioType : value});

    var token   = this.state.token;
    var type    = null;

    if( index == 0 ){

        if( this.state.voicefeatureSL ){

          type  = '1';
          Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

              console.log(setDefaultLanguage,"setDefaultLanguage")
              if( setDefaultLanguage == 'success'){
                
              }else{
                Tts.requestInstallData();
              }
          }).catch((error) => {

            Alert.alert("Please install all the voice packages for getting all voices from the application.");
            Tts.requestInstallData();
            console.log(error,"setDefaultLanguage41");
          });

          Tts.speak(this.state.voicefeatureSL);
          
          if( !this.state.isSingle && this.state.SvoicefeatureSL ){

            Tts.setDefaultLanguage(this.state.SeconderyLanguage);

            if( this.state.SeconderyLanguage != this.state.PrimaryLanguage){
                Tts.speak(this.state.SvoicefeatureSL);
            }
          } 
        }else{
          Tts.speak('Your Voice Feature is Active for Shopping List');
        }

        
    }else{

        type  = '0';
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    this.setState({ ShoppingListAudioType : index });
    AsyncStorage.setItem('ShoppingListAudioType',this.checknull(JSON.stringify(index) ));

    AsyncStorage.setItem('SLAudioType', this.checknull(SLAudioType ));
                                  

   
    try {
        var response = fetch(GVar.BaseUrl + GVar.ShoppingListAudioType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(JSON.stringify(responseJson));
                            
                             if( responseJson.status == 1 ){
                                  this.setState({ ShoppingListAudioType : index });
                                  AsyncStorage.setItem('ShoppingListAudioType', this.checknull(JSON.stringify(index) ));
                                  //Alert.alert(responseJson.message);
                             }else if( responseJson.status == 0 ){
                                 // Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }

  }

  onChangeLGSAudioType( value ){

    var index;
    var LGSAudioType;

    if( value ){


      LGSAudioType = 'true';
      index = 0;
      
    }else{

      index = 1;
      LGSAudioType = 'false';
    }

    
    this.setState({LGSAudioType : value});

    var token   = this.state.token ;

    var type    = null;

    if( index == 0 ){

        type  = '1';

        if( this.state.voicefeatureGS ){

          Tts.setDefaultLanguage(this.state.PrimaryLanguage).then(setDefaultLanguage => { 

              console.log(setDefaultLanguage,"setDefaultLanguage")
              if( setDefaultLanguage == 'success'){
                
              }else{
                Tts.requestInstallData();
              }
          }).catch((error) => {

            Alert.alert("Please install all the voice packages for getting all voices from the application.");
            Tts.requestInstallData();
            console.log(error,"setDefaultLanguage41");
          });

          Tts.speak(this.state.voicefeatureGS);

          if( !this.state.isSingle && this.state.SvoicefeatureGS ){

            Tts.setDefaultLanguage(this.state.SeconderyLanguage);

            if( this.state.SeconderyLanguage != this.state.PrimaryLanguage){

                Tts.speak(this.state.SvoicefeatureGS);
            }    
          } 
        }else{
          Tts.speak('Your Voice Feature is Active for Go Shopping');
        }
        
        
    }else{

        type  = '0';
    }

    var GVar = GlobalVariables.getApiUrl();

    let formdata = new FormData();

    formdata.append('user_id', this.state.user_id);
    formdata.append('type', type);

    this.setState({ GoShoppingAudioType : index });
    AsyncStorage.setItem('GoShoppingAudioType',  this.checknull(JSON.stringify(index) ));


    AsyncStorage.setItem('LGSAudioType',  this.checknull(LGSAudioType ));
    
   
    try {
        var response = fetch(GVar.BaseUrl + GVar.GoShoppingAudioType , {
                        method: 'POST',
                        headers:{
                                    'token' : token
                                },
                        body: formdata,
                      }).then((response) => response.json())
                         .then((responseJson) => {

                            //Alert.alert(JSON.stringify(responseJson));
                            
                             if( responseJson.status == 1 ){
                                  this.setState({ GoShoppingAudioType : index });
                                  AsyncStorage.setItem('GoShoppingAudioType',  this.checknull(JSON.stringify(index) ));
                                  //Alert.alert(responseJson.message);
                             }else if( responseJson.status == 0 ){
                                  //Alert.alert(responseJson.message);
                             }
                         });

       }catch (error) {

      }

  }

 
  playDeviceSoundMusic(testInfo) {

          AsyncStorage.setItem('currentAlert', this.checknull(JSON.stringify(testInfo) ));
          AsyncStorage.setItem('AlertType', 'path' );
          this.setState({currentAlert : testInfo});

          console.log(testInfo,"testInfo");
          var Sound = require('react-native-sound');

          Sound.setCategory('Playback');

          console.log("whoosh",whoosh);

          if (whoosh==null) {
           whoosh = new Sound(testInfo.path, Sound.MAIN_BUNDLE, (error) => {
                      if (error) {
                          console.log('failed to load the sound', error);
                      } else {
                          whoosh.play(); // have to put the call to play() in the onload callback
                      }
                  });
          }
          else
          {
            whoosh.release();
            whoosh = new Sound(testInfo.path, Sound.MAIN_BUNDLE, (error) => {
                      if (error) {
                          console.log('failed to load the sound', error);
                      } else {
                          whoosh.play(); // have to put the call to play() in the onload callback
                      }
                  });
          } 
  }


  playDeviceSoundAlert( testInfo ){

          AsyncStorage.setItem('currentAlert', this.checknull(JSON.stringify(testInfo) ));
          AsyncStorage.setItem('AlertType', 'url' );

          console.log(testInfo,"testInfo");
          var Sound = require('react-native-sound');

          Sound.setCategory('Playback');

         
          if (whoosh==null) {
           whoosh = new Sound(testInfo.url, Sound.MAIN_BUNDLE, (error) => {
                      if (error) {
                          console.log('failed to load the sound', error);
                      } else {
                          whoosh.play(); // have to put the call to play() in the onload callback
                      }
                  });
          }
          else
          {
            whoosh.release();
            whoosh = new Sound(testInfo.url, Sound.MAIN_BUNDLE, (error) => {
                      if (error) {
                          console.log('failed to load the sound', error);
                      } else {
                          whoosh.play(); // have to put the call to play() in the onload callback
                      }
                  });
          } 
  }

  render() {

    var repeated = 'repeated';
    var column   = 'column';

    

    var state = this.state;
    var selectrepeated  = null;
    var selectcolumn    = null;

    if( state.ShoppingListType == 'repeated' ){

        selectrepeated = true;
    }else{
        
        selectrepeated = false;
    }

    if( state.ShoppingListType == 'column' ){

        selectcolumn = true;
    }else{
        selectcolumn = false;
    }



    var selfiesame        = 'same';
    var selfieindependant = 'independent';

    if( state.Selfietype == 'same' ){

        selectsame = true;
    }else{
        
        selectsame = false;
    }

    if( state.Selfietype == 'independent' ){

        selectindependent = true;
    }else{
        selectindependent = false;
    }


    var singlelanguage  = 'single';
    var duallanguage    = 'dual';

    var selectsingle  = null;
    var selectdual    = null;

    if( state.LanguageType == 'single' ){

        selectsingle = true;
    }else{
        
        selectsingle = false;
    }

    if( state.LanguageType == 'dual' ){

        selectdual = true;
    }else{
        selectdual = false;
    }

    var showalertringtone = this.state.alertshow;

    var showsinglelangage = this.state.showsinglelangage;
    var showsduallangage = this.state.showsduallangage;

    var showmodel = this.state.modalVisible;

    var songs = this.state.songs;




   
    return (
          <View style={styles.setup_container} >
          <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/> 

            <ImageBackground source={require('../assets/images/shoppingimages/setupbk.jpg')}
                    style={[
                   styles.base,{
                       width: "100%"
                   }, {
                       height: "100%"
                   }
               ]}>  
           <ScrollView>



              <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>

                  <View style={{flex: 1,marginTop:15}}>
                          <Text style={styles.hometext}>SET UP </Text>
                          
                  </View>
                  

                  <View style={{flex: 4, flexDirection: 'row', width:wp('96%'),height:hp('65%'), justifyContent:'center', alignSelf:'center', backgroundColor:'#FFFFFF'}}>
                    <View style={{flex:4,alignSelf:'center', marginTop:10}} >
                        <View style={{flex: 3.5,flexDirection:'column'}} >

                            <View style={{flex: 1,flexDirection:'row', }}>
                                <View style={{flex:2, justifyContent:'center', marginTop:hp('5%')}}>
                                  <Text style={{alignSelf:'flex-end', fontSize:hp('3%'), fontWeight:'bold', color:'#000000'}}>{ this.state.PrimaryLanguage ? this.state.NAME : 'NAME' }</Text>
                                </View>
                                  
                              
                                <View style={{flex:4,}} >
                                    
                                 { 
                                      this.state.name != null
                                       ?
                                       <TextInput
                                          style={{height: hp('10%'),width:'95%', borderColor: '#000000', borderWidth: 2,fontSize: hp('3%'), alignSelf:'flex-end'}}
                                          value={this.state.name}
                                          onChangeText={this.onchangeName.bind(this)}
                                          underlineColorAndroid="transparent" />
                                       
                                        :
                                         <ActivityIndicator size="large"  transparent={true} animating={true} style={styles.indicator}/> 

                                  }

                                </View>
                                

                            </View>

                            <View style={{flex: 1, flexDirection:'row',marginTop:hp('7%')}} >

                                <View style={{flex:2.2, justifyContent:'center',}}>
                                  <Text style={{alignSelf:'flex-end', fontSize:hp('3%'), fontWeight:'bold', color:'#000000',}}>{ this.state.PrimaryLanguage ? this.state.LANGUAGE : 'LANGUAGE'}</Text>
                                </View>

                                <View style={{flex: 4, flexDirection:'row',justifyContent:'flex-start', marginLeft:wp('1%')}} >

                                      <View style={{flex:1, justifyContent:'flex-start', flexDirection:'row'}}>
                                          
                                          <View style={{justifyContent:'center',alignSelf:'center', marginRight:10}}>
                                            <Text style={{alignSelf:'flex-end', fontSize:hp('3%'), fontWeight:'bold', color:'#000000'}} onPress={this.onChangeLanguage.bind(this,false)}>{this.state.PrimaryLanguage ? this.state.DUAL : 'DUAL'}</Text>
                                          </View>

                                          <View style={{justifyContent:'center', alignSelf:'center',}}>
                                              <FlipToggle
                                                  value={this.state.isSingle}
                                                  buttonOffColor={'#666'}
                                                  sliderOffColor={'#fff'}
                                                  buttonOnColor={'#53bee6'}
                                                  sliderOnColor={'#fff'}
                                                  buttonWidth={wp('5%')}
                                                  buttonHeight={hp('5%')}
                                                  buttonRadius={wp('25%')}
                                                  onToggle={(value) => {this.onChangeLanguage(value)}}
                                                  onToggleLongPress={() => {
                                                    console.log('Long Press');
                                                  }}
                                                />
                                          </View>

                                          <View style={{justifyContent:'center',alignSelf:'center',marginLeft:10 }}>
                                            <Text style={{alignSelf:'flex-start', fontSize:hp('3%'), fontWeight:'bold', color:'#000000'}} onPress={this.onChangeLanguage.bind(this,true)}>{this.state.PrimaryLanguage ? this.state.SINGLE : 'SINGLE'}</Text>
                                          </View>
                                      </View>
                                        
                                </View>

                            </View>

                           

                            <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center', marginTop:'3%'}}>
                                
                                        <View style={{flex:4,justifyContent:'center'}}>
                                            <Text style={{marginLeft:'10%', fontSize: hp('3%'),justifyContent:'center',color: '#000000',fontWeight: 'bold',}}>{this.state.PrimaryLanguage ? this.state.MONEYWARNING : 'LOW MONEY WARNING'}</Text>
                                        </View>
                                        
                                        <View style={{flex:2,justifyContent:'center',justifyContent:'center', flexDirection:'row', borderWidth: 2,backgroundColor:'#FFFFFF',borderColor: '#000000',}}>

                                                  <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',}}>$</Text>
                                                
                                                  <TextInput style={{
                                                        alignSelf: 'center',
                                                        textAlign:'center',
                                                        fontSize:hp('3%'),
                                                        height: hp('10%')
                                                      }}
                                                      value={this.state.LowmoneyValue}
                                                      onChangeText={this.onChangeLowmoneyValue.bind(this)}
                                                      keyboardType='numeric'
                                                      placeholder={''}
                                                      underlineColorAndroid="transparent"
                                                  />
                                                
                                        </View>

                                        <View style={{justifyContent:'center', }}>

                                                    <RadioGroup 
                                                        size={20}
                                                        thickness={2}
                                                        color='#9575b2'
                                                        //highlightColor='#ccc8b9'
                                                        style={{ display: 'flex',flexDirection:'row',}} selectedIndex ={ this.state.LowMoneyWarningType } onSelect = {(index, value) => this.onSelectLowMoneyWaringAudioType(index, value)} >
                                                        <RadioButton value={this.state.LowMoneyWarningType} style={{alignItems:'center'}}>
                                                        <View style={{flexDirection:'row'}} >
                                                          <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.PLAYATUNE : 'PLAY A TUNE'}</Text>
                                                          <Image source={require('../assets/images/shoppingimages/music_new.png')} style={{height:30, width:30,}}/>
                                                        </View>
                                                        </RadioButton>
                                                 
                                                        <RadioButton value={this.state.LowMoneyWarningType} style={{alignItems:'center'}}>
                                                        <View style={{flexDirection:'row'}} >
                                                         <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.ALARM : 'ALARM'}</Text>
                                                         <Image source={require('../assets/images/alerticon.png')} style={{height:40, width:40,}}/>
                                                         
                                                        </View>
                                                        </RadioButton>
                                               
                                                    </RadioGroup> 

                                        </View>
                                
                                </View>
                            
                            <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',marginTop:'5%'}}>
                                
                                        <View style={{width: '35%', height: '80%'}}>
                                             
                                        </View>

                                        <View style={{width: '30%', height: '80%',marginRight:'2%', justifyContent:'center', borderWidth:2,backgroundColor: selectindependent ? '#267BC3' : '#1A74F5'}} >
                                              <TouchableOpacity onPress={this.onPressSelfietype.bind(this,selfieindependant)}>
                                              <Text style={styles.normaltext_small_white}>{this.state.PrimaryLanguage ? this.state.SELFIESINDEPENDENT : 'ALL SELFIES INDEPENDENT'}</Text>
                                              </TouchableOpacity>
                                        </View>
                                        

                                        <View style={{width: '30%', height: '80%',marginLeft:'2%', justifyContent:'center', borderWidth:2,backgroundColor: selectsame ? '#267BC3' : '#1A74F5'}} >
                                              <TouchableOpacity onPress={this.onPressSelfietype.bind(this,selfiesame)}>
                                              <Text style={styles.normaltext_small_white}>{this.state.PrimaryLanguage ? this.state.SELFIESALLSAME : 'SELFIES ALL SAME'}</Text>
                                              </TouchableOpacity>
                                        </View>
                                
                            </View>

                        </View>

                        <View style={{flex: 2.5, flexDirection:'row', marginTop:hp('5%')}} >
                            <View style={{flex:3}}>

                                  <View style={{flex: 5,flexDirection: 'column',justifyContent: 'center', borderWidth:2, width:'97%', alignSelf:'center',borderColor:'##808080', backgroundColor:'#e2fdfd'}}>
                                          <View style={{flex:1,marginTop:hp('2%')}} >
                                                <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.MONEYMANAGEMENT  : 'MONEY MANAGEMENT'} {this.state.PrimaryLanguage ? this.state.OPTIONS  : 'OPTIONS'}</Text> 
                                          </View>

                                          <View style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignSelf:'center',}}>
                                                  
                                                  <View style={{flex:1,alignSelf:'center' }} >
                                                        <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.AUDIOONTAP : 'AUDIO ON TAP'}</Text>
                                                  </View>

                                                  <View style={{flex:1,alignSelf:'center' }}>
                                                      <FlipToggle
                                                        value={this.state.MMoneyAudioType}
                                                        buttonRadius={5}
                                                        sliderRadius={wp('2.1%')}
                                                        buttonWidth={wp('9%')}
                                                        buttonHeight={hp('5%')}
                                                        buttonOffColor={'#666'}
                                                        sliderOffColor={'#fff'}
                                                        buttonOnColor={'#53bee6'}
                                                        sliderOnColor={'#fff'}
                                                        onLabel={this.state.PrimaryLanguage ? this.state.voicefeatureforON : 'ON'}
                                                        offLabel={this.state.PrimaryLanguage ? this.state.voicefeatureforOff : 'OFf'}
                                                        labelStyle={{ fontSize: hp('2%'), color: '#FFFFFF', alignSelf:'center', fontWeight:'bold' }}
                                                        onToggle={(value) => {this.onChangeMMAudioType(value)}}
                                                      />
                                                  </View>
                                                  
                                          </View>
                                  </View>   

                                  <View style={{flex:.1}}/>   
                            </View>

                            <View style={{flex:3, backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 5,flexDirection: 'column',justifyContent: 'center', borderWidth:2, width:'97%', alignSelf:'center',borderColor:'##808080', backgroundColor:'#deebfa'}}>
                                          <View style={{flex:1,marginTop:hp('2%')}} >
                                                <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE  : 'HOW MUCH MONEY I HAVE'} {this.state.PrimaryLanguage ? this.state.OPTIONS  : 'OPTIONS'}</Text> 
                                          </View>

                                          <View style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignSelf:'center',}}>
                                                  
                                                  <View style={{flex:1,alignSelf:'center' }} >
                                                        <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.AUDIOONTAP : 'AUDIO ON TAP'}</Text>
                                                  </View>

                                                  <View style={{flex:1,alignSelf:'center' ,marginLeft:'5%'}}>
                                                      <FlipToggle
                                                        value={this.state.HMMoneyAudioType}
                                                        buttonRadius={5}
                                                        sliderRadius={wp('2.1%')}
                                                        buttonWidth={wp('9%')}
                                                        buttonHeight={hp('5%')}
                                                        buttonOffColor={'#666'}
                                                        sliderOffColor={'#fff'}
                                                        buttonOnColor={'#53bee6'}
                                                        sliderOnColor={'#fff'}
                                                        onLabel={this.state.PrimaryLanguage ? this.state.voicefeatureforON : 'ON'}
                                                        offLabel={this.state.PrimaryLanguage ? this.state.voicefeatureforOff : 'OFf'}
                                                        labelStyle={{ fontSize: hp('2%'), color: '#FFFFFF', alignSelf:'center', fontWeight:'bold' }}
                                                        onToggle={(value) => {
                                                          this.onChangeHMMoneyAudioType(value)
                                                        }}
                                                      />
                                                  </View>

                                          </View>
                                </View> 

                                <View style={{flex:.1}}/> 

                            </View>

                        </View>

                    </View>


                    <View style={{flex:2, alignSelf:'center', justifyContent:'space-between', marginTop:10}} >
                        <View style={{flex: 3,}}>
                              <View style={{flex: 1, borderWidth:2,flexDirection: 'column',justifyContent: 'space-between',backgroundColor:'#dff8fc', width:'97%',alignSelf:'center'}}>

                                        <View style={{width: '100%',}} >
                                              <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.SHOPPINGLIST  : 'SHOPPING LIST'} {this.state.PrimaryLanguage ? this.state.OPTIONS  : 'OPTIONS'}</Text> 
                                        </View>

                                        <View style={{justifyContent:'center', width: '80%', height: '20%', borderWidth:2, marginTop:1,alignSelf:'center',backgroundColor: selectrepeated ? '#525252' : '#767676'}} >
                                              <TouchableOpacity onPress={this.onPressShoopingListOption.bind(this,repeated)}>
                                              <Text style={styles.normaltext_small_white}>{this.state.PrimaryLanguage ? this.state.REPEATEDINLINE : 'REPEATED IN LINE'}</Text>
                                              </TouchableOpacity>
                                        </View>

                                        <View style={{justifyContent:'center',width: '80%', height: '20%', borderWidth:2,alignSelf:'center', marginTop:hp('2%'), backgroundColor: selectcolumn ? '#525252' : '#767676'}} >
                                              <TouchableOpacity onPress={this.onPressShoopingListOption.bind(this,column)}>
                                              <Text  style={styles.normaltext_small_white}>{this.state.PrimaryLanguage ? this.state.NUMBEREDCOLUMN : 'NUMBERED COLUMN'}</Text>
                                              </TouchableOpacity>
                                        </View>

                                        
                                          <View style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignSelf:'center',}}>
                                                  
                                                  <View style={{flex:1,alignSelf:'center' }} >
                                                        <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.AUDIOONTAP : 'AUDIO ON TAP'}</Text>
                                                  </View>

                                                  <View style={{flex:1,alignSelf:'center' ,marginLeft:'5%'}}>
                                                      <FlipToggle
                                                        value={this.state.SLAudioType}
                                                        buttonRadius={5}
                                                        sliderRadius={wp('2.1%')}
                                                        buttonWidth={wp('9%')}
                                                        buttonHeight={hp('5%')}
                                                        buttonOffColor={'#666'}
                                                        sliderOffColor={'#fff'}
                                                        buttonOnColor={'#53bee6'}
                                                        sliderOnColor={'#fff'}
                                                        onLabel={this.state.PrimaryLanguage ? this.state.voicefeatureforON : 'ON'}
                                                        offLabel={this.state.PrimaryLanguage ? this.state.voicefeatureforOff : 'OFf'}
                                                        labelStyle={{ fontSize: hp('2%'), color: '#FFFFFF', alignSelf:'center', fontWeight:'bold' }}
                                                        onToggle={(value) => {
                                                          this.onChangeSLAudioType(value)
                                                        }}
                                                      />
                                                  </View>

                                                  
                                          </View>

                              </View>
                        </View>

                        <View style={{flex:.3}}/>
                        
                        <View style={{flex: 2,marginBottom:2}}>

                                <View style={{flex: 1,flexDirection: 'column',justifyContent: 'center', borderWidth:2, width:'97%', alignSelf:'center', backgroundColor:'#feebed'}}>
                                          <View style={{flex:1,marginTop:hp('3%')}} >
                                                <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.GOSHOPPING  : 'GO SHOPPING'} {this.state.PrimaryLanguage ? this.state.OPTIONS  : 'OPTIONS'}</Text> 
                                          </View>

                                          

                                         <View style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignSelf:'center',}}>
                                                  
                                                  <View style={{flex:3,alignSelf:'center' }} >
                                                        <Text style={styles.normaltext_small}>{this.state.PrimaryLanguage ? this.state.AUDIOONTAP : 'AUDIO ON TAP'}</Text>
                                                  </View>

                                                  <View style={{flex:3,alignSelf:'center' }}>
                                                      <FlipToggle
                                                        value={this.state.LGSAudioType}
                                                        buttonRadius={5}
                                                        sliderRadius={wp('2.1%')}
                                                        buttonWidth={wp('9%')}
                                                        buttonHeight={hp('5%')}
                                                        buttonOffColor={'#666'}
                                                        sliderOffColor={'#fff'}
                                                        buttonOnColor={'#53bee6'}
                                                        sliderOnColor={'#fff'}
                                                        onLabel={this.state.PrimaryLanguage ? this.state.voicefeatureforON : 'ON'}
                                                        offLabel={this.state.PrimaryLanguage ? this.state.voicefeatureforOff : 'OFf'}
                                                        labelStyle={{ fontSize: hp('2%'), color: '#FFFFFF', alignSelf:'center', fontWeight:'bold' }}
                                                        onToggle={(value) => {
                                                          this.onChangeLGSAudioType(value)
                                                        }}
                                                      />
                                                  </View>

                                          </View>
                                </View> 
                        </View>

                    </View>

                  </View>
                  

                 <View style={{flex: 1,flexDirection: 'row', alignSelf:'center', marginTop:hp('5%')}} >
                  
                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressHome.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/welcome.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center'}}>
                     <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.WECOME : 'WECOME PAGE'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressShoopingList.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/shopping-list.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center'}}>
                      <Text style={{alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5)}}>{this.state.PrimaryLanguage ? this.state.SHOPPINGLIST : 'SHOPPING LIST'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressGoShopping.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/letsgo.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center',}}>
                        <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.GOSHOPPING : 'LETS GO SHOPPING'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMyMoney.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/howmuch.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                        <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE : 'HOW MUCH MONEY I HAVE'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMoneyManagement.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/money-management.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                        <Text style={{marginLeft:'5%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYMANAGEMENT : 'MONEY MANAGEMENT'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={() => {
                                              Alert.alert(
                                               this.state.PrimaryLanguage ? this.state.voicefeatureforlogout : 'Are you sure to Logout',
                                                '',
                                                [
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforcancel : 'cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforok : 'Ok', onPress: () => {this.onPressLogout(this)}},
                                                ],
                                                { cancelable: false }
                                              )
                                    }}>
                    <ImageBackground source={require('../assets/images/shoppingimages/logoutmain.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                      <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.LOGOUT : 'LOGOUT'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>


                  </View>

              </View>


              {
                this.state.animating ? 
               <ActivityIndicator
                animating = {this.state.animating}
                size = "large"
                style = {styles.indicator}/>

                : <View/>}

            

              {this.state.isLoading ?  <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large" />

                   : <View/>}

              </ScrollView>

              {showmodel ?  <Modal
                    supportedOrientations={['portrait', 'landscape']}
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                      alert('Modal has been closed.');
                    }}>
                    

                                        

                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '60%', 
                        marginLeft:'20%',
                      }}>
                        <View style={{}} />

                        <View style={{flex: 1,flexDirection: 'column',height: '60%', backgroundColor: '#171717',borderWidth:5,borderRadius:5, borderColor:'#FFFFFF'}} >

                           <TouchableOpacity style={{marginTop:20,backgroundColor:'#FFFFFF', }} onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                              <Text style={{right:0, fontSize:hp('5%'),color:'#000000'}}>Close</Text>
                          </TouchableOpacity>
                          
                          <ScrollView style={styles.container} contentContainerStyle={styles.scrollContainer}>
                            {audioTests.map(testInfo => {
                              return (
                                <Feature
                                  status={this.state.tests[testInfo.title]}
                                  key={testInfo.title}
                                  title={testInfo.title}
                                   onPress={() => {this.playDeviceSoundAlert(testInfo)}}
                                />
                              );
                            })}
                           
                          </ScrollView>
                            

                        </View>

                        <View style={{}} />
                      </View>

                  </Modal> : <View/> }

                  {this.state.devicemusic ?  

                   <Modal
                    supportedOrientations={['portrait', 'landscape']}
                    animationType="slide"
                    transparent={true}
                    visible={this.state.devicemusic}
                    onRequestClose={() => {
                      alert('Modal has been closed.');
                    }}>
                    

                                        

                    <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',alignItems: 'center',width: '60%',  marginLeft:'20%', }}>
                        <View style={{}} />

                        <View style={{flex: 1,flexDirection: 'column',height: '60%', backgroundColor: '#171717',borderWidth:5,borderRadius:5, borderColor:'#FFFFFF'}} >

                          <TouchableOpacity style={{marginTop:20,backgroundColor:'#FFFFFF', }} onPress={() => {this.setModalVisibleDevice(!this.state.devicemusic);}}>
                              <Text style={{right:0, fontSize:hp('5%'),color:'#000000'}}>Close</Text>
                          </TouchableOpacity>
                         
                          <ScrollView style={styles.container} contentContainerStyle={styles.scrollContainer}>
      
                            { this.state.ismusic ? 

                              songs.map(testInfo => {
                              return (
                                <Feature
                                  status={this.state.tests[testInfo.title]}
                                  key={testInfo.title}
                                  title={testInfo.title}
                                  onPress={() => {this.playDeviceSoundMusic(testInfo)}}
                                />
                              );
                            }) : 

                            <View style={styles.feature}>
                              <Header style={{flex: 1, color:'#FFFFFF', fontSize: RF(2.5)}}>No music found in your device</Header>
                            </View>
                            }
                          </ScrollView>
                         

                        </View>

                        <View style={{}} />
                      </View>

                  </Modal>

                   : <View/> }


                  {showsinglelangage ?
                        <Modal animationType="slide" transparent={true} visible={this.state.setModalVisibleLanguage}
                        onRequestClose={() => {
                          Alert.alert('Modal has been closed.');
                        }}>
                        <View style={{marginTop: hp('10%'),height:hp('80%'), backgroundColor:'#9acecf', width:wp('40%'), alignSelf:'center', alignItems:'center', justifyContent:'center', borderRadius:wp('5%'), borderWidth:2, borderColor:'#000000'}}>
                          
                            <View style={{flex: 1, width:'80%', marginTop:5, justifyContent:'center'}}>
                              
                              <View style={{height:'10%',width:'20%', justifyContent:'center', borderWidth:2,backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', borderColor:'#000000'  }} >
                                  <TouchableOpacity style={{height:'100%',width:'100%'}}
                                    onPress={this.calledforprimarylanguagechange.bind(this)}>
                                    <Text style={{fontSize:RF(4), color:'#000000', textAlign:'center'}}>OK</Text>
                                  </TouchableOpacity>
                              </View>

                              <View style={{height:'10%', width:'80%', justifyContent:'center', borderWidth:2,backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', marginTop:2, borderColor:'#000000'}} >
                                    <Text style={{fontSize:RF(4), color:'#000000', textAlign:'center'}}>Primary Language</Text>
                              </View>

                              <View style={{height:'70%', width:'80%', justifyContent:'center', borderWidth:2,backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', marginTop:5, borderColor:'#000000'}} >

                                <ScrollView>
                                          <FlatList          
                                            data={this.state.LANGUAGES.languages}  
                                            extraData={this.state}        
                                            renderItem={({ item }) => ( 
                                              <View style={{backgroundColor: this.state.PrimaryLanguage == item.language_code ? 'gray' : 'white', width:'100%'}}> 
                                                    <TouchableOpacity onPress={this.onSelectPrimaryLanguage.bind(this, item)}>
                                                        <Text style={{fontSize:RF(4), color:'#000000', textAlign:'center'}}>{item.language}</Text>
                                                    </TouchableOpacity>
                                              </View>      
                                             )}          
                                             keyExtractor={item => item.language_code}  />
                 
                    {this.state.isLoading ?  <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large" />

                   : <View/>}  
                                </ScrollView>
                              </View>
                            </View>

                        </View>
                      </Modal>
                      : <View/> 
                  }


                  {showsduallangage ? 
                    <Modal supportedOrientations={['portrait', 'landscape']} animationType="slide" transparent={true} visible={this.state.setModalVisibleSLanguage}
                      onRequestClose={() => {
                        alert('Modal has been closed.');
                      }}>
                                    
                        <View style={{marginTop: hp('10%'),height:hp('80%'), backgroundColor:'#9acecf', width:wp('80%'), alignSelf:'center', alignItems:'center', justifyContent:'center', borderRadius:wp('5%'), borderWidth:2, borderColor:'#000000'}}>
                          
                            
                            <View style={{flex: 1, width:'80%', marginTop:5, justifyContent:'center'}}>
                                
                                <TouchableOpacity onPress={this.calledforlanguagechange.bind(this)} style={{height:'10%',width:'20%', justifyContent:'center', borderWidth:2,backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', borderColor:'#000000'  }}>
                                <View  >
                                      <Text style={{fontSize:RF(4), color:'#000000', textAlign:'center'}}>OK</Text>
                                </View>
                                </TouchableOpacity>

                                <View style={{flex: 1,flexDirection:'row', justifyContent:'space-between', alignItems:'center'}} >

                                  <View style={{height:'60%',width:'45%', justifyContent:'center', borderWidth:2,backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', borderColor:'#000000'  }} >
                                        <Text style={{fontSize:RF(4), color:'#000000', textAlign:'center'}}>Primary Language</Text>
                                  </View>

                                  <View style={{height:'60%',width:'45%', justifyContent:'center', borderWidth:2,backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', borderColor:'#000000'  }} >
                                        <Text style={{fontSize:RF(4), color:'#000000', textAlign:'center'}}>Secondery Language</Text>
                                  </View>
                                </View>

                                <View style={{flex: 4,flexDirection:'row', justifyContent:'space-between', alignItems:'center'}} >

                                  <View style={{height:'95%', width:'45%', justifyContent:'center', borderWidth:2,backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', marginTop:0, borderColor:'#000000'}} >

                                    <ScrollView>
                                              <FlatList          
                                                data={this.state.LANGUAGES.languages}  
                                                extraData={this.state}        
                                                renderItem={({ item }) => ( 
                                                  <View style={{backgroundColor: this.state.PrimaryLanguage == item.language_code ? 'gray' : 'white', width:'100%'}}> 
                                                        <TouchableOpacity onPress={this.onSelectPrimaryLanguage.bind(this, item)}>
                                                            <Text style={{fontSize:RF(4), color:'#000000', textAlign:'center'}}>{item.language}</Text>
                                                        </TouchableOpacity>
                                                  </View>      
                                                 )}          
                                                 keyExtractor={item => item.language_code}  />
                              {this.state.isLoading ?  <ActivityIndicator
                               animating     ={true}
                               transparent   ={true}
                               visible       ={false}
                               style         ={styles.indicator}
                               size          ="large" /> : <View/>}  

                                    </ScrollView>
                                  </View>


                                  <View style={{height:'95%', width:'45%', justifyContent:'center', borderWidth:2,backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', marginTop:0, borderColor:'#000000'}} >

                                    <ScrollView>
                                     {this.state.ALL_SecondryLANGUAGES.languages ? <FlatList          
                                                data={this.state.ALL_SecondryLANGUAGES.languages}  
                                                extraData={this.state}        
                                                renderItem={({ item }) => ( 
                                                  <View style={{backgroundColor: this.state.SeconderyLanguage == item.language_code ? 'gray' : 'white', width:'100%'}}> 
                                                        <TouchableOpacity onPress={this.onSelectSecondryLanguage.bind(this, item)}>
                                                            <Text style={{fontSize:RF(4), color:'#000000', textAlign:'center'}}>{item.language}</Text>
                                                        </TouchableOpacity>
                                                  </View>      
                                                 )}          
                                                 keyExtractor={item => item.language_code}  
                                              />  : <View/>} 

                                      {this.state.isLoading ?  <ActivityIndicator
                                       animating     ={true}
                                       transparent   ={true}
                                       visible       ={false}
                                       style         ={styles.indicator}
                                       size          ="large" /> : <View/>} 
                                              
                                    </ScrollView>
                                  </View>
                                </View>
                            </View>

                        </View>
                    </Modal>: <View/> }
                  
              </ImageBackground>
          </View> 
    );
  }
}






