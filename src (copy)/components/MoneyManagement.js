/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  Button,
  TouchableHighlight,
  Image,
  TextInput,
  Keyboard,
  Alert,
  FlatList,
  ScrollView,
  AsyncStorage,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';

import styles from './GlobalStyle.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import { Table, Row, Rows } from 'react-native-table-component';
import GlobalVariables from './GlobalVariables.js';
import ImagePicker from 'react-native-image-picker';
import Tts from 'react-native-tts';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import RF from "react-native-responsive-fontsize";


type Props = {};

export default class GoShopping extends Component<Props> {

  static navigationOptions = {
      header: null
  }


       constructor(props)
       {
            
            AsyncStorage.getItem('TOKEN').then((data) => { 

              if(!data){
                  Actions.Login();
              }
                    
            }); 

            super(props);

            this.state = { 
              isLoading   : true,
              totalmoney  : 0,
              AmountLeft  : 0,
              avatarSource: null,
              profileimage    : null,
              profileimage2   : null,
              subscribe       : null,
              Localprofileimage     : null,
              Localprofileimage2    : null,
              cinema                : null, 
              cinema1               : null, 
              dancing               : null, 
              dancing1              : null, 
              sports                : null, 
              sports1               : null, 
              nightout              : null, 
              nightout1             : null, 
            }



            
            AsyncStorage.getItem('name').then((data) => { 

              this.setState({name: data,});
            });


       AsyncStorage.multiGet(["profileimage", "profileimage2"]).then(response => {
            
            this.setState({
                                    profileimage     : response[0][1],
                                    profileimage2    : response[1][1],
                                   
                          });   
                          Alert.alert(JSON.parse(profileimage));       
           });

           //AsyncStorage.removeItem('AmountLeft');
             
           
            AsyncStorage.multiGet(["nightout1Name", "dancing1Name","sports1Name","cinema1Name"]).then(response => {
                
          
              this.setState({
                                      nightout1Name     : response[0][1],
                                      dancing1Name      : response[1][1],
                                      sports1Name       : response[2][1],
                                      cinema1Name       : response[3][1],
                            });          
            });



            AsyncStorage.getItem('PrimaryLanguage').then((data) => {
          
          

                  if( data == null ){

                    this.setState({PrimaryLanguage : 'en' });
                    
                  }else{

                    this.setState({PrimaryLanguage : data }); 
                  }
                  

            });


       }

      componentDidMount = async () => {

          this.setLanguages = this.setLanguages();

          AsyncStorage.multiGet(["Localprofileimage", "Localprofileimage2"]).then(response => {
                
            var Localprofileimage              = response[0][1];
            var Localprofileimage2             = response[1][1];
            //alert(Localprofileimage)


            this.setState({
                                    Localprofileimage     : JSON.parse(Localprofileimage),
                                    Localprofileimage2    : JSON.parse(Localprofileimage2),
                          });          
          });

  

          /*AsyncStorage.multiGet(["Localprofileimage", "Localprofileimage2","cinema", "cinema1", "dancing","dancing1", "sports", "sports1", "nightout", "nightout1","AmountLeft","totalmoney","pantryinbox","tableDatainPrice","pantryaddedPrice"]).then(response => {
                
            var Localprofileimage             = response[0][1];
            var Localprofileimage2            = response[1][1];
            var cinema                        = response[2][1];
            var cinema1                       = response[3][1];
            var dancing                       = response[4][1];
            var dancing1                      = response[5][1];
            var sports                        = response[6][1];
            var sports1                       = response[7][1];
            var nightout                      = response[8][1];
            var nightout1                     = response[9][1];
            var AmountLeft                    = response[10][1];
            var totalmoney                    = response[11][1];
            var pantryinbox                   = response[12][1];
            var tableDatainPrice              = response[13][1];
            var pantryaddedPrice              = response[14][1];

            console.log(JSON.parse(pantryinbox),"pantryinbox");

            this.setState({isLoading:false})

            if( pantryaddedPrice === null || pantryaddedPrice < 0){
              pantryaddedPrice = 0;
            }else{
              pantryaddedPrice = JSON.parse(pantryaddedPrice);
            }


            if( pantryinbox === null ){
              this.setState({pantryinbox: []});
            }else{
              this.setState({pantryinbox: JSON.parse(pantryinbox)});
            }

            console.log(pantryaddedPrice,"pantryaddedPricepantryaddedPrice");

            var c   = 0;
            var c1  = 0;
            var d   = 0;
            var d1  = 0;
            var s   = 0;
            var s1  = 0;
            var n   = 0;
            var n1  = 0;
            var amount      = 0;
            var lettotalmoney  = 0;



            if( JSON.parse(cinema) ){
              console.log(cinema,"cinema");
              this.setState({cinema : parseFloat(JSON.parse(cinema)).toFixed(0)});
              AsyncStorage.setItem('cinema', cinema);
              c = parseFloat(JSON.parse(cinema));

            }else{
              this.setState({cinema : 0 });
              AsyncStorage.setItem('cinema', 0);
              c =0;
            }

            if( JSON.parse(cinema1) ){
              this.setState({cinema1 : JSON.parse(cinema1)});
              c1 = parseFloat(JSON.parse(cinema1));
              AsyncStorage.setItem('cinema1', cinema1);
            }else{
              this.setState({cinema : 0});
              AsyncStorage.setItem('cinema1', 0);
              c1 =0;
            }

            if( JSON.parse(dancing) ){
              this.setState({dancing : JSON.parse(dancing)});
              AsyncStorage.setItem('dancing', dancing);
              d = parseFloat(JSON.parse(dancing));
            }else{
              this.setState({dancing : 0});
              AsyncStorage.setItem('dancing', 0);
              d = 0;
            }

            if( JSON.parse(dancing1) ){
              this.setState({dancing1 : JSON.parse(dancing1)});
              AsyncStorage.setItem('dancing1', dancing1);
              d1 = parseFloat(JSON.parse(dancing1));
            }else{
              this.setState({dancing : 0});
              d1 =0;
            }

            if( JSON.parse(sports) ){
              this.setState({sports : JSON.parse(sports)});
              AsyncStorage.setItem('sports', sports);
              s = parseFloat(JSON.parse(sports));
            }else{
              this.setState({sports : 0});
              AsyncStorage.setItem('sports', 0);
              s =0;
            }

            if( JSON.parse(sports1) ){
              this.setState({sports1 : JSON.parse(sports1)});
              AsyncStorage.setItem('sports1', sports1);
              s1 = parseFloat(JSON.parse(sports1));
            }else{
              this.setState({sports1 : 0});
              AsyncStorage.setItem('sports1', 0);
              s1 =0;
            }

            if( JSON.parse(nightout) ){
              this.setState({nightout : JSON.parse(nightout)});
              n = parseFloat(JSON.parse(nightout));
            }else{
              this.setState({nightout : 0});
              n = 0;
            }

            if( JSON.parse(nightout1) ){
              console.log(JSON.parse(nightout1),"nightout1")
              this.setState({nightout1 : JSON.parse(nightout1)});
              n1 = parseFloat(JSON.parse(nightout1));
            }else{
              this.setState({nightout1 : 0});
              n1 = 0;
            }

            if( JSON.parse(totalmoney) ){

              this.setState({totalmoney : JSON.parse(totalmoney)});
              lettotalmoney = parseFloat(JSON.parse(totalmoney));
              
            }else if( isNaN(totalmoney) ){

              this.setState({totalmoney : 0});
              lettotalmoney = 0;
            }

            if( JSON.parse(AmountLeft) ){
              amount = JSON.parse(AmountLeft);
            }else{
              amount = 0;
            }
           


            var total = c + c1 + d + d1 + n + n1 + s + s1 + pantryaddedPrice;
            var amountLeft = lettotalmoney - total;

            console.log(total,"TouchableHighlight");
            console.log(lettotalmoney,"lettotalmoney");
            console.log(amountLeft,"amountLeftamountLeft");
            console.log(pantryaddedPrice,"pantryaddedPrice");

             
            this.setState({AmountLeft : parseFloat(JSON.parse(amountLeft)).toFixed(2)});

            if( amountLeft < 0 ){

              this.setState({AmountLeft : JSON.parse(0)});

              Alert.alert("Warning message","Somthing went wrong please reset your money");
            }
           
            
          });*/

          AsyncStorage.getItem('totalmoney').then((data) => { 

              if( JSON.parse(data) ){

                  this.setState({totalmoney: parseFloat(JSON.parse(data)).toFixed(2),});            
                  this.setState({AmountLeft: parseFloat(0.00).toFixed(2)});

              }else{

                  this.setState({totalmoney: parseFloat(0.00).toFixed(2)});            
                  this.setState({AmountLeft: parseFloat(0.00).toFixed(2)});
              }
          });

          AsyncStorage.multiGet(["tableData", "tableDatainColumn","tableDataReplica","tableDatainPrice","tableDatainPriceLine","user_id","TOKEN","userdatasource","dataSource"]).then(response => {
              
              var tableData                     = response[0][1];
              var tableDatainColumn             = response[1][1];
              var tableDataReplica              = response[2][1];
              var tableDatainPrice              = response[3][1];
              var tableDatainPriceLine          = response[4][1];
              var user_id                       = response[5][1];
              var token                         = response[6][1];
              var userdatasource                = response[7][1];
              //var dataSource                    = response[8][1];

              
              this.setState({ isLoading              : false });

              console.log(tableData,"GoShoppingtableData");
              console.log(tableDatainColumn,"GoShoppingtableDatainColumn");
              console.log(tableDataReplica,"GoShoppingtableDataReplica");
              console.log(tableDatainPrice,"GoShoppingtableDatainPrice");
              console.log(tableDatainPriceLine,"GoShoppingtableDatainPriceLine");
              console.log(userdatasource,"GoShoppinguserdatasource");
             
             

             
              if( JSON.parse(tableData) ){
                this.setState({ tableData             : JSON.parse(tableData) });
               
              }else{
                this.setState({ tableData             : [] });
              }
             
              if(JSON.parse(tableDatainColumn) ){
                this.setState({ tableDatainColumn             : JSON.parse(tableDatainColumn) });
                
              }else{
                this.setState({ tableDatainColumn             : [] });
              }

              if( JSON.parse(tableDataReplica) ){
                this.setState({ tableDataReplica             : JSON.parse(tableDataReplica) });
              }else{
                this.setState({ tableDataReplica             : [] });
                
              }


              if( JSON.parse(tableDatainPrice) ){
                this.setState({ tableDatainPrice             : JSON.parse(tableDatainPrice) });
              }else{
                this.setState({ tableDatainPrice             : [] });
                
              }

              if( JSON.parse(tableDatainPriceLine) ){
                 this.setState({ tableDatainPriceLine             : JSON.parse(tableDatainPriceLine) });
              }else{
                this.setState({ tableDatainPriceLine             : [] });
               
              }
              this.setState({ user_id               : JSON.parse(user_id) });
              this.setState({ token                 : token });
              this.setState({ userdatasource        : JSON.parse(userdatasource) });
          });

          AsyncStorage.multiGet(["Localprofileimage", "Localprofileimage2","cinema", "cinema1", "dancing","dancing1", "sports", "sports1", "nightout", "nightout1","AmountLeft","totalmoney","pantryinbox","tableDatainPrice","pantryaddedPrice"]).then(response => {
                      
                  var Localprofileimage             = response[0][1];
                  var Localprofileimage2            = response[1][1];
                  var cinema                        = response[2][1];
                  var cinema1                       = response[3][1];
                  var dancing                       = response[4][1];
                  var dancing1                      = response[5][1];
                  var sports                        = response[6][1];
                  var sports1                       = response[7][1];
                  var nightout                      = response[8][1];
                  var nightout1                     = response[9][1];
                  var AmountLeft                    = response[10][1];
                  var totalmoney                    = response[11][1];
                  var pantryinbox                   = response[12][1];
                  var tableDatainPrice              = response[13][1];
                  var pantryaddedPrice              = response[14][1];
                  var tableDatainPrice              = this.state.tableDatainPrice;

                 

                  if( pantryaddedPrice === null || pantryaddedPrice < 0){
                    pantryaddedPrice = 0;
                  }else{
                    pantryaddedPrice = JSON.parse(pantryaddedPrice);
                  }


                  if( pantryinbox === null ){
                    this.setState({pantryinbox: []});
                  }else{
                    this.setState({pantryinbox: JSON.parse(pantryinbox)});
                  }

                  var pantryinboxData = this.state.pantryinbox;

                  var pantryaddedPriceForShopping = 0;           

                  for(var i = 0; i < pantryinboxData.length; i++) {
                      
                      var obj   = pantryinboxData[i];
                      var index = this.getIndexOfK(tableDatainPrice, obj);

                      if( index != -1 ){

                        cell1     = index[0];
                        cell2     = index[1];

                     
                        
                        pantryaddedPriceForShopping = pantryaddedPriceForShopping + (JSON.parse(tableDatainPrice[cell1][2]) * JSON.parse(tableDatainPrice[cell1][1]));
                        console.log(JSON.parse(tableDatainPrice[cell1][1]),"pantryaddedPricepantryaddedPrice1");
                      }else{

                        pantryaddedPriceForShopping = pantryaddedPriceForShopping + 0;
                        console.log("pantryaddedPricepantryaddedPrice2");
                      }

                      console.log("pantryaddedPricepantryaddedPrice3");
                                      
                  }

                  pantryaddedPrice = pantryaddedPriceForShopping; 


                  //console.log(pantryaddedPrice,"pantryaddedPricepantryaddedPrice");
                  //console.log(pantryaddedPriceForShopping,"pantryaddedPriceForShopping");

                  var c   = 0;
                  var c1  = 0;
                  var d   = 0;
                  var d1  = 0;
                  var s   = 0;
                  var s1  = 0;
                  var n   = 0;
                  var n1  = 0;
                  var amount      = 0;
                  var lettotalmoney  = 0;



                  if( JSON.parse(cinema) ){
                    console.log(cinema,"cinema");
                    this.setState({cinema : JSON.parse(cinema)});

                    c = parseFloat(JSON.parse(cinema));

                  }else{
                    this.setState({cinema : 0});
                    c =0;
                  }

                  if( JSON.parse(cinema1) ){
                    this.setState({cinema1 : JSON.parse(cinema1)});
                    c1 = parseFloat(JSON.parse(cinema1));
                  }else{
                    this.setState({cinema : 0});
                    c1 =0;
                  }

                  if( JSON.parse(dancing) ){
                    this.setState({dancing : JSON.parse(dancing)});
                    d = parseFloat(JSON.parse(dancing));
                  }else{
                    this.setState({dancing : 0});
                    d = 0;
                  }

                  if( JSON.parse(dancing1) ){
                    this.setState({dancing1 : JSON.parse(dancing1)});
                    d1 = parseFloat(JSON.parse(dancing1));
                  }else{
                    this.setState({dancing : 0});
                    d1 =0;
                  }

                  if( JSON.parse(sports) ){
                    this.setState({sports : JSON.parse(sports)});
                    s = parseFloat(JSON.parse(sports));
                  }else{
                    this.setState({sports : 0});
                    s =0;
                  }

                  if( JSON.parse(sports1) ){
                    this.setState({sports1 : JSON.parse(sports1)});
                    s1 = parseFloat(JSON.parse(sports1));
                  }else{
                    this.setState({sports1 : 0});
                    s1 =0;
                  }

                  if( JSON.parse(nightout) ){
                    this.setState({nightout : JSON.parse(nightout)});
                    n = parseFloat(JSON.parse(nightout));
                  }else{
                    this.setState({nightout : 0});
                    n = 0;
                  }

                  if( JSON.parse(nightout1) ){
                    console.log(JSON.parse(nightout1),"nightout1")
                    this.setState({nightout1 : JSON.parse(nightout1)});
                    n1 = parseFloat(JSON.parse(nightout1));
                  }else{
                    this.setState({nightout1 : 0});
                    n1 = 0;
                  }

                  if( JSON.parse(totalmoney) ){

                    this.setState({totalmoney : JSON.parse(totalmoney)});
                    lettotalmoney = parseFloat(JSON.parse(totalmoney));
                    
                  }else{

                    this.setState({totalmoney : 0});
                    lettotalmoney = 0;
                  }

                  if( JSON.parse(AmountLeft) ){
                    amount = JSON.parse(AmountLeft);
                  }else{
                    amount = 0;
                  }
                 


                  var total = c + c1 + d + d1 + n + n1 + s + s1 + pantryaddedPrice;
                  var amountLeft = lettotalmoney - total;
                  this.setState({AmountLeft : parseFloat(JSON.parse(amountLeft)).toFixed(2)});

                  if( amountLeft < 0 ){

                    this.setState({AmountLeft : JSON.parse(0)});

                    Alert.alert("Warning message","Somthing went wrong please reset your money");
                  }

                  console.log(total,"TouchableHighlight");
                  console.log(lettotalmoney,"lettotalmoney");
                  console.log(amountLeft,"amountLeftamountLeft");
                  console.log(pantryaddedPrice,"pantryaddedPrice");
          });


          AsyncStorage.multiGet(["name", "user_id","LowMoneyWarningType","MoneymanagementAudioType", "MyMoneyAudioType", "ShoppingListAudioType", "ShoppingListType", "LanguageType", "Selfietype","GoShoppingAudioType","TOKEN","GoShoppingAudioType","LowmoneyStatus","LowmoneyTune","LowmoneyValue","PrimaryLanguage","SeconderyLanguage","LanguageTypestatus","LanguageType"]).then(response => {
            

                    var name                          = response[0][1];
                    var user_id                       = response[1][1];
                    var LowMoneyWarningType           = response[2][1];
                    var MoneymanagementAudioType      = response[3][1];
                    var MyMoneyAudioType              = response[4][1];
                    var ShoppingListAudioType         = response[5][1];
                    var ShoppingListType              = response[6][1];
                    var LanguageType                  = response[7][1];
                    var Selfietype                    = response[8][1];
                    var GoShoppingAudioType           = response[9][1];
                    var token                         = response[10][1];
                    var GoShoppingAudioType           = response[11][1];
                    var LowmoneyStatus                = response[12][1];
                    var LowmoneyTune                  = response[13][1];
                    var LowmoneyValue                 = response[14][1];
                    var PrimaryLanguage               = response[15][1];
                    var SeconderyLanguage             = response[16][1];
                    var LanguageTypestatus            = response[17][1];
                    var LanguageType1                  = response[18][1];

                   
                   this.setState({
                                    name                    : name.toUpperCase(),
                                    LowMoneyWarningType     : LowMoneyWarningType,
                                    MoneymanagementAudioType: MoneymanagementAudioType,
                                    MyMoneyAudioType        : MyMoneyAudioType,
                                    ShoppingListAudioType   : ShoppingListAudioType,
                                    ShoppingListType        : ShoppingListType,
                                    LanguageType            : LanguageType,
                                    Selfietype              : Selfietype,
                                    GoShoppingAudioType     : GoShoppingAudioType,
                                    LowmoneyStatus          : LowmoneyStatus,
                                    LowmoneyTune            : LowmoneyTune,
                                    LowmoneyValue           : LowmoneyValue,
                                    PrimaryLanguage         : PrimaryLanguage,
                                    SeconderyLanguage       : SeconderyLanguage,
                                    token                   : token,
                                    LanguageTypestatus      : LanguageTypestatus
                              });
          });    
      }

      getIndexOfK(arr, k) {
        for (var i = 0; i < arr.length; i++) {
          var index = arr[i].indexOf(k);
          if (index > -1) {
            return [i, index];
          }
        }
      }


      setLanguages(){

  

                  AsyncStorage.getItem('PrimaryLanguage').then((P_LANG) => {
                        
                        

                        AsyncStorage.getItem('WELCOME PAGE').then((data) => {

                              var WECOME = JSON.parse(data);

                              if( P_LANG == 'en'){

                                this.setState({WECOME : WECOME.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({WECOME : WECOME.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({WECOME : WECOME.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({WECOME : WECOME.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({WECOME : WECOME.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({WECOME : WECOME.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({WECOME : WECOME.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({WECOME : WECOME.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({WECOME : WECOME.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({WECOME : WECOME.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({WECOME : WECOME.fil})
                              }

                        });

                        AsyncStorage.getItem('RESET ALL COUNTS').then((data) => {

                              var RESET = JSON.parse(data);

                             


                              if( P_LANG == 'en'){

                                this.setState({RESET : RESET.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({RESET : RESET.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({RESET : RESET.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({RESET : RESET.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({RESET : RESET.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({RESET : RESET.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({RESET : RESET.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({RESET : RESET.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({RESET : RESET.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({RESET : RESET.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({RESET : RESET.fil})
                              }

                        });

                        AsyncStorage.getItem('SHOPPING LIST').then((data) => {

                              var SHOPPINGLIST = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SHOPPINGLIST : SHOPPINGLIST.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SHOPPINGLIST : SHOPPINGLIST.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SHOPPINGLIST : SHOPPINGLIST.fil})
                              }

                        });

                        AsyncStorage.getItem('LETS GO SHOPPING').then((data) => {

                              var GOSHOPPING = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({GOSHOPPING : GOSHOPPING.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({GOSHOPPING : GOSHOPPING.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({GOSHOPPING : GOSHOPPING.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({GOSHOPPING : GOSHOPPING.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({GOSHOPPING : GOSHOPPING.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({GOSHOPPING : GOSHOPPING.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({GOSHOPPING : GOSHOPPING.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({GOSHOPPING : GOSHOPPING.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({GOSHOPPING : GOSHOPPING.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({GOSHOPPING : GOSHOPPING.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({GOSHOPPING : GOSHOPPING.fil})
                              }

                        });

                        AsyncStorage.getItem('PANTRY').then((data) => {

                              var PANTRY = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({PANTRY : PANTRY.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({PANTRY : PANTRY.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({PANTRY : PANTRY.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({PANTRY : PANTRY.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({PANTRY : PANTRY.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({PANTRY : PANTRY.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({PANTRY : PANTRY.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({PANTRY : PANTRY.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({PANTRY : PANTRY.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({PANTRY : PANTRY.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({PANTRY : PANTRY.fil})
                              }

                        });

                        AsyncStorage.getItem('HOW MUCH MONEY DO I HAVE').then((data) => {

                              var MONEYIHAVE = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({MONEYIHAVE : MONEYIHAVE.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({MONEYIHAVE : MONEYIHAVE.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({MONEYIHAVE : MONEYIHAVE.fil})
                              }

                        });

                        AsyncStorage.getItem('MONEY MANAGEMENT').then((data) => {

                              var MONEYMANAGEMENT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.fil})
                              }

                        });

                        AsyncStorage.getItem('SETUP').then((data) => {

                              var SETUP = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({SETUP : SETUP.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({SETUP : SETUP.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({SETUP : SETUP.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({SETUP : SETUP.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({SETUP : SETUP.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({SETUP : SETUP.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({SETUP : SETUP.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({SETUP : SETUP.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({SETUP : SETUP.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({SETUP : SETUP.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({SETUP : SETUP.fil})
                              }

                        });

                        AsyncStorage.getItem('LOGOUT').then((data) => {

                              var LOGOUT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({LOGOUT : LOGOUT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({LOGOUT : LOGOUT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({LOGOUT : LOGOUT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({LOGOUT : LOGOUT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({LOGOUT : LOGOUT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({LOGOUT : LOGOUT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({LOGOUT : LOGOUT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({LOGOUT : LOGOUT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({LOGOUT : LOGOUT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({LOGOUT : LOGOUT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({LOGOUT : LOGOUT.fil})
                              }

                        });

                        AsyncStorage.getItem('AMOUNT LEFT').then((data) => {

                              var AMOUNTLEFT = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({AMOUNTLEFT : AMOUNTLEFT.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({AMOUNTLEFT : AMOUNTLEFT.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({AMOUNTLEFT : AMOUNTLEFT.fil})
                              }

                        });

                        AsyncStorage.getItem('Write some thing here').then((data) => {

                              var WriteSomething = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({WriteSomething : WriteSomething.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({WriteSomething : WriteSomething.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({WriteSomething : WriteSomething.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({WriteSomething : WriteSomething.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({WriteSomething : WriteSomething.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({WriteSomething : WriteSomething.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({WriteSomething : WriteSomething.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({WriteSomething : WriteSomething.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({WriteSomething : WriteSomething.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({WriteSomething : WriteSomething.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({WriteSomething : WriteSomething.fil})
                              }

                        });

                        AsyncStorage.getItem('Night Outing').then((data) => {

                              var Nout = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({Nout : Nout.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({Nout : Nout.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({Nout : Nout.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({Nout : Nout.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({Nout : Nout.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({Nout : Nout.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({Nout : Nout.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({Nout : Nout.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({Nout : Nout.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({Nout : Nout.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({Nout : Nout.fil})
                              }

                        });

       AsyncStorage.getItem('Are you sure to log out?').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforlogout : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforlogout : voicefeatureGS.fil})
                              }

                        });
                AsyncStorage.getItem('ok').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforok : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforok : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforok : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforok : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforok : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforok : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforok : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforok : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforok : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('cancel').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforcancel : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforcancel : voicefeatureGS.fil})
                              }

                        });


                        AsyncStorage.getItem('Dancing Program').then((data) => {

                              var DancingP = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({DancingP : DancingP.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({DancingP : DancingP.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({DancingP : DancingP.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({DancingP : DancingP.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({DancingP : DancingP.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({DancingP : DancingP.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({DancingP : DancingP.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({DancingP : DancingP.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({DancingP : DancingP.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({DancingP : DancingP.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({DancingP : DancingP.fil})
                              }

                        });

                        AsyncStorage.getItem('Games').then((data) => {

                              var Games = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({Games : Games.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({Games : Games.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({Games : Games.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({Games : Games.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({Games : Games.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({Games : Games.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({Games : Games.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({Games : Games.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({Games : Games.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({Games : Games.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({Games : Games.fil})
                              }

                        });


                        AsyncStorage.getItem('Entertainment').then((data) => {

                              var Entertainment = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({Entertainment : Entertainment.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({Entertainment : Entertainment.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({Entertainment : Entertainment.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({Entertainment : Entertainment.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({Entertainment : Entertainment.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({Entertainment : Entertainment.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({Entertainment : Entertainment.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({Entertainment : Entertainment.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({Entertainment : Entertainment.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({Entertainment : Entertainment.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({Entertainment : Entertainment.fil})
                              }

                        });

                        AsyncStorage.getItem('Do you want to reset all money?').then((data) => {

                              var RESETALLMONEY = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({RESETALLMONEY : RESETALLMONEY.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({RESETALLMONEY : RESETALLMONEY.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({RESETALLMONEY : RESETALLMONEY.fil})
                              }

                        });


                        AsyncStorage.getItem('ok').then((data) => {

                              var ok = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({ok : ok.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({ok : ok.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({ok : ok.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({ok : ok.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({ok : ok.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({ok : ok.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({ok : ok.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({ok : ok.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({ok : ok.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({ok : ok.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({ok : ok.fil})
                              }

                        });


                        AsyncStorage.getItem('cancel').then((data) => {

                              var cancel = JSON.parse(data);


                              if( P_LANG == 'en'){

                                this.setState({cancel : cancel.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({cancel : cancel.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({cancel : cancel.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({cancel : cancel.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({cancel : cancel.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({cancel : cancel.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({cancel : cancel.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({cancel : cancel.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({cancel : cancel.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({cancel : cancel.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({cancel : cancel.fil})
                              }

                        });
                        
                  });
      
      }

     GetGridViewItem (item) {
      
     Alert.alert(item);

     }


    

      onPressShoopingList(){
        Actions.ShoppingList();
      }

      onPressHome(){
        Actions.Home();
      }

      onPressGoShopping(){
        Actions.GoShopping();
      }

      onPressMyMoney(){
         Actions.MyMoney();
      }

      onPressPantry(){
          Actions.Pantry();
      }

      _keyboardDidHide() {
       
        Keyboard.dismiss()
      }

      onPressResetAmount(){

        if( this.state.MoneymanagementAudioType == 0 ){
            // Tts.speak('Your all activities amount now reset please add again!');
        }

        this.setState({
            cinema      : 0,
            cinema1     : 0,
            dancing     : 0,
            dancing1    : 0,
            sports      : 0,
            sports1     : 0,
            nightout    : 0,
            nightout1   : 0,
            //AmountLeft  : this.state.totalmoney,
        });

        AsyncStorage.multiSet([
                                ["cinema",          JSON.stringify('0')],
                                ["cinema1",          JSON.stringify('0')],
                                ["dancing",          JSON.stringify('0')],
                                ["dancing1",          JSON.stringify('0')],
                                ["sports",          JSON.stringify('0')],
                                ["sports1",          JSON.stringify('0')],
                                ["nightout",          JSON.stringify('0')],
                                ["nightout1",          JSON.stringify('0')],
                                //["AmountLeft",          JSON.stringify(this.state.totalmoney)],
                            ]);

        AsyncStorage.getItem('pantryaddedPrice').then((data) => { 

            var pantryaddedPrice    = data;
            var totalmoney          = this.state.totalmoney;

            //console.log(JSON.parse(pantryinbox),"pantryinbox");

            if( pantryaddedPrice === null || pantryaddedPrice < 0){
              pantryaddedPrice = 0;
            }else{
              pantryaddedPrice = JSON.parse(pantryaddedPrice);
            }

            var AmountLeft = totalmoney-pantryaddedPrice;

            this.setState({AmountLeft:AmountLeft});
            AsyncStorage.setItem('AmountLeft', this.checknull(AmountLeft));

            //Alert.alert(JSON.stringify(totalmoney-pantryaddedPrice));

        });
             
        

      }

      setCamera1(){

        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true
          }
        };

        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled photo picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
          

          let source = { uri: response.uri };

          //alert(source)

          
          var token = this.state.token;
          var user_id = this.state.user_id;
          var Selfietype = this.state.Selfietype;


          
let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie1', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
            
            this.setState({ Localprofileimage : source});

            AsyncStorage.multiSet([
                                        ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                       
                                    ]);

            //Alert.alert(Selfietype)
            if( Selfietype === "same"){

              this.setState({ Localprofileimage : source, Localprofileimage2: source });
              AsyncStorage.multiSet([
                                        ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                        ["Localprofileimage2",         this.checknull(JSON.stringify(source))],
                                    ]);

            }

          }
        });
      }


checknull(value){
  if(value){
    return value;
  }
  else {
    return '';
  }
}

      setCamera(){

        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true
          }
        };



        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled photo picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            let source = { uri: response.uri };

            

          //alert(source)

          
          var token = this.state.token;
          var user_id = this.state.user_id;
          var Selfietype = this.state.Selfietype;


          

          let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie2', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
            
            this.setState({ Localprofileimage2 : source});

            AsyncStorage.multiSet([
                                        ["Localprofileimage2",          this.checknull(JSON.stringify(source))],
                                       
                                    ]);

            //Alert.alert(Selfietype)
            if( Selfietype === "same"){

              this.setState({ Localprofileimage : source, Localprofileimage2: source });
              AsyncStorage.multiSet([
                                        ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                        ["Localprofileimage2",         this.checknull(JSON.stringify(source))],
                                    ]);

            }
          }
        });
      }




   onChangeActivity = ( value , name) => {

     if (/^\d+(\.\d{0,4})?$/.test(name))
     {

      val = parseFloat(name);

      console.log(name,"validnumber")

      if( isNaN(val) ){

        if( name === null || name === ''){

              console.log("empty","validnumber")

              if( value === 'cinema' ){

                this.setState({cinema : ''});
              }

              if( value === 'cinema1' ){

                this.setState({cinema1 : ''});
              }

              if( value === 'dancing' ){

                this.setState({dancing : ''});
              }

              if( value === 'dancing1' ){

                this.setState({dancing1 : ''});
              }

              if( value === 'sports' ){

                this.setState({sports : ''});
              }

              if( value === 'sports1' ){

                this.setState({sports1 : ''});
              }

              if( value === 'nightout' ){

                this.setState({nightout : ''});
              }

              if( value === 'nightout1' ){

                this.setState({nightout1 : ''});
              }

        }else{

          alert("Invalid Money");
        }

      }else{

              var leftAmount = parseFloat(this.state.totalmoney).toFixed(2);



              if( value === 'cinema' ){
                
                var AmountLeft = this.state.AmountLeft;

                var cinemavalue = this.state.cinema;

                var cinema = AmountLeft - name ;

                var newAmountLeft   =  ( +cinemavalue +  + cinema);

                if( newAmountLeft < 0 ){

                  Alert.alert("No Amount Left Remaning");

                  if( this.state.MoneymanagementAudioType == 0 ){
                     //Tts.speak('Please enter money within range');
                  }

                }else{
                  this.setState({ AmountLeft: parseFloat(newAmountLeft).toFixed(2), cinema: name});

                  AsyncStorage.multiSet([
                                            ["AmountLeft",      this.checknull(JSON.stringify(newAmountLeft))],
                                            ["cinema",          this.checknull(JSON.stringify(name))],
                                        ]);

                  if( this.state.MoneymanagementAudioType == 0 ){
                    // Tts.speak('You have added'+name+'dollar for Activitie cinema and your remaning amount is'+parseFloat(newAmountLeft).toFixed(2));
                  }

                }
              }

              if( value === 'cinema1' ){

                var AmountLeft = this.state.AmountLeft;
                var cinemavalue = this.state.cinema1;

                var cinema1 = AmountLeft - name ;

                var newAmountLeft   =  ( +cinemavalue +  + cinema1);

                if( newAmountLeft < 0 ){

                  Alert.alert("No Amount Left Remaning");
                  if( this.state.MoneymanagementAudioType == 0 ){
                    // Tts.speak('Please enter money within range');
                  }

                }else{
                  this.setState({ AmountLeft: parseFloat(newAmountLeft).toFixed(2), cinema1: name});

                  AsyncStorage.multiSet([
                                            ["AmountLeft",      this.checknull(JSON.stringify(newAmountLeft))],
                                            ["cinema1",          this.checknull(JSON.stringify(name))],
                                        ]);

                  if( this.state.MoneymanagementAudioType == 0 ){
                     //Tts.speak('You have added'+name+'dollar for Activitie cinema1 and your remaning amount is'+parseFloat(newAmountLeft).toFixed(2));
                  }
                }
              }

              if( value === 'dancing' ){

                var AmountLeft = this.state.AmountLeft;
                var dancing = this.state.dancing;

                var newdancing = AmountLeft - name ;

                var newAmountLeft   =  parseFloat( +newdancing +  + dancing).toFixed(2);
         
                if( newAmountLeft < 0 ){

                  Alert.alert("No Amount Left Remaning");
                  if( this.state.MoneymanagementAudioType == 0 ){
                    // Tts.speak('Please enter money within range');
                  }
                }else{
                  this.setState({ AmountLeft: parseFloat(newAmountLeft).toFixed(2), dancing: name});

                   AsyncStorage.multiSet([
                                            ["AmountLeft",      this.checknull(JSON.stringify(newAmountLeft))],
                                            ["dancing",       this.checknull(JSON.stringify(name))],
                                        ]);

                   if( this.state.MoneymanagementAudioType == 0 ){
                    // Tts.speak('You have added'+name+'dollar for Activitie dancing and your remaning amount is'+parseFloat(newAmountLeft).toFixed(2));
                  }
                }
              }

              if( value === 'dancing1' ){

                
                var AmountLeft = this.state.AmountLeft;
                var dancing1 = this.state.dancing1;

                var newdancing = AmountLeft - name ;

                var newAmountLeft   =  parseFloat( +newdancing +  + dancing1).toFixed(2);

                if( newAmountLeft < 0 ){

                  Alert.alert("No Amount Left Remaning");
                  if( this.state.MoneymanagementAudioType == 0 ){
                     //Tts.speak('Please enter money within range');
                  }
                }else{
                  this.setState({ AmountLeft: parseFloat(newAmountLeft).toFixed(2), dancing1: name});

                   AsyncStorage.multiSet([
                                            ["AmountLeft",      this.checknull(JSON.stringify(newAmountLeft))],
                                            ["dancing1",          this.checknull(JSON.stringify(name))],
                                        ]);

                   if( this.state.MoneymanagementAudioType == 0 ){
                    // Tts.speak('You have added'+name+'dollar for Activitie dancing1 and your remaning amount is'+parseFloat(newAmountLeft).toFixed(2));
                  }
                }
              }

              if( value === 'sports' ){

                var AmountLeft = this.state.AmountLeft;
                var sports = this.state.sports;

                var newsports = AmountLeft - name ;

                var newAmountLeft   =  ( +newsports +  + sports);

                if( newAmountLeft < 0 ){

                    Alert.alert("No Amount Left Remaning");
                    if( this.state.MoneymanagementAudioType == 0 ){
                    // Tts.speak('Please enter money within range');
                  }
                }else{
                 
                    this.setState({ AmountLeft: parseFloat(newAmountLeft).toFixed(2), sports: name});

                     AsyncStorage.multiSet([
                                            ["AmountLeft",      this.checknull(JSON.stringify(newAmountLeft))],
                                            ["sports",          this.checknull(JSON.stringify(name))],
                                        ]);

                      if( this.state.MoneymanagementAudioType == 0 ){
                           // Tts.speak('You have added'+name+'dollar for Activitie sports and your remaning amount is'+parseFloat(newAmountLeft).toFixed(2));
                      }
                }
              }

              if( value === 'sports1' ){

                var AmountLeft = this.state.AmountLeft;
                var sports1 = this.state.sports1;

                var newsports1 = AmountLeft - name ;

                var newAmountLeft   =  ( +newsports1 +  + sports1);


                if( newAmountLeft < 0 ){

                    Alert.alert("No Amount Left Remaning");
                    if( this.state.MoneymanagementAudioType == 0 ){
                    // Tts.speak('Please enter money within range');
                  }
                }else{
                 
                    this.setState({ AmountLeft: parseFloat(newAmountLeft).toFixed(2), sports1: name});

                     AsyncStorage.multiSet([
                                            ["AmountLeft",      this.checknull(JSON.stringify(newAmountLeft))],
                                            ["sports1",          this.checknull(JSON.stringify(name))],
                                        ]);

                      if( this.state.MoneymanagementAudioType == 0 ){
                           // Tts.speak('You have added'+name+'dollar for Activitie sports1 and your remaning amount is'+parseFloat(newAmountLeft).toFixed(2));
                      }
                }
              }

              if( value === 'nightout' ){

                var AmountLeft = this.state.AmountLeft;
                var nightout = this.state.nightout;

                var newnightout = AmountLeft - name ;

                var newAmountLeft   =  ( +newnightout +  + nightout);

                

                if( newAmountLeft < 0 ){

                    Alert.alert("No Amount Left Remaning");

                    if( this.state.MoneymanagementAudioType == 0 ){
                       // Tts.speak('Please enter money within range');
                    }
                }else{
                 
                     this.setState({ AmountLeft: parseFloat(newAmountLeft).toFixed(2), nightout: name});

                     AsyncStorage.multiSet([
                                            ["AmountLeft",      this.checknull(JSON.stringify(parseFloat(newAmountLeft).toFixed(2)))],
                                            ["nightout",          this.checknull(JSON.stringify(name))],
                                        ]);

                      if( this.state.MoneymanagementAudioType == 0 ){
                           // Tts.speak('You have added'+name+'dollar for Activitie nightout and your remaning amount is'+parseFloat(newAmountLeft).toFixed(2));
                      }
                }
              }

              if( value === 'nightout1' ){

                var AmountLeft = this.state.AmountLeft;
                var nightout1 = this.state.nightout1;

                var newnightout1 = AmountLeft - name ;

                var newAmountLeft   =  ( +newnightout1 +  + nightout1);

                if( newAmountLeft < 0 ){

                    Alert.alert("No Amount Left Remaning");

                    if( this.state.MoneymanagementAudioType == 0 ){
                      //  Tts.speak('Please enter money within range');
                    }
                }else{
                 
                    this.setState({ AmountLeft: parseFloat(newAmountLeft).toFixed(2), nightout1: name});

                     AsyncStorage.multiSet([
                                            ["AmountLeft",      this.checknull(JSON.stringify(newAmountLeft))],
                                            ["nightout1",          this.checknull(JSON.stringify(name))],
                                        ]);

                      if( this.state.MoneymanagementAudioType == 0 ){
                          //  Tts.speak('You have added'+name+'dollar for Activitie nightout1 and your remaning amount is'+parseFloat(newAmountLeft).toFixed(2));
                      }
                }
              }
      }
    }
    else{
      Alert.alert('Please use a Valid number.')
    }




   }


   onChangeActivityName = ( value , name) => {


    
      if( value === 'nightout1Name' ){
        
        this.setState({nightout1Name : name })

        AsyncStorage.multiSet([["nightout1Name",this.checknull(name)]]);

          
      }

      
      if( value === 'dancing1Name' ){

        this.setState({dancing1Name : name })

        AsyncStorage.multiSet([["dancing1Name",this.checknull(name)]]);
      }

      
      if( value === 'sports1Name' ){

        this.setState({sports1Name : name })

        AsyncStorage.multiSet([["sports1Name",this.checknull(name)]]);
      }

      if( value === 'cinema1Name' ){

        this.setState({cinema1Name : name })

        AsyncStorage.multiSet([["cinema1Name",this.checknull(name)]]);
      }

   }

   onPressLogout(){

    //Alert.alert("fsd");
    AsyncStorage.removeItem('TOKEN');
    Actions.Login();

  }   



  render() {
    var state = this.state;
    var inline = 0;
   
    return (
          <View style={styles.moneymanagement_container} >
          <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>  
           <ImageBackground source={require('../assets/images/shoppingimages/welcome-bg.jpg')}
                    style={[
                   styles.base,{
                       width: "100%"
                   }, {
                       height: "100%"
                   }
               ]}> 
              <ScrollView>
              <View style={{flex: 1,marginTop:hp('1%')}} >
                  
                  <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between',marginLeft:hp('5%')}}>
                    
                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%', height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera1.bind(this)}>
                              <View style={{}}>
                               { 
                                this.state.Localprofileimage != null
                               ? <Image source={this.state.Localprofileimage} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>

                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>

                    <View style={{width: wp('62%'),}} >
                          <View style={{flex: 1, alignSelf:'center', }}>
                          <Text style = {styles.hometext}>{this.state.name}'S</Text>
                          <Text style = {styles.hometext}>{this.state.PrimaryLanguage ? this.state.MONEYMANAGEMENT : 'MONEY MANAGEMENT'}</Text>
                          </View>
                    </View>

                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%'), marginRight:hp('5%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%',height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera.bind(this)}>
                              <View style={{}}>
                              { 
                                this.state.Localprofileimage2 != null
                               ? <Image source={this.state.Localprofileimage2} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage2!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage2}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>


                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>
                  </View>

                  <View style={{flexDirection:'row', alignSelf:'center',marginTop:hp('.5%') }}>
                    <Text style={{color:'#FFFFFF', fontSize:RF(5), alignSelf:'center'}}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE : 'HOW MONEY DO I HAVE'}</Text>
                    <TextInput style={{
                            color: '#000000',
                            alignSelf: 'center',
                            textAlign:'center',
                            width: "15%",
                            height:50,
                            borderBottomWidth: 1,
                            marginTop:0,
                            borderWidth:1,
                            marginTop:-3,
                            borderRadius:2,
                            backgroundColor:'#FFFFFF',
                            borderColor:'#FFFFFF',
                            marginLeft:10,
                            fontSize: hp('4%')
                          }}
                          value={'$ '+ this.state.totalmoney}
                          onChangeText={(totalmoney) => this.setState({ totalmoney })}
                          keyboardType="numeric"
                          placeholderTextColor="#000000"
                          placeholder = "$ 0.00"
                          underlineColorAndroid="transparent"
                          editable={false}
                          />

                  </View>


                    <View style={{flex: 3.4,flexDirection: 'row',backgroundColor:'#FFFFFF' , alignSelf:'center', width:'96%',height:hp('45%'), marginTop:hp('4.5%')}}>
                        <View style={{flex: 2, backgroundColor: 'transparent'}} >

                            <View style={{flex: 1,flexDirection: 'row', backgroundColor: 'transparent',marginTop:'3%'}} >
                                                    <View style={{width: '50%', height: 30, marginLeft:30, justifyContent:'center'}} >
                                                      <Text style={{fontSize:hp('4%')}}>{this.state.PrimaryLanguage ? this.state.Nout : 'Night Outing'}</Text> 
                                                    </View>

                                                    <View style={{flexDirection: 'row', backgroundColor: 'transparent',borderWidth: 1, borderColor: '#C0C0C0', height: RF(10), width:RF(20)}} >
                                                        <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',marginLeft:5 }}>$</Text>
                                                        <TextInput style={{
                                                          alignSelf: 'center',
                                                          fontSize:hp('3%'),
                                                          width:'100%'
                                                        }}
                                                        value={this.state.nightout}
                                                        onChangeText={this.onChangeActivity.bind(this.state.nightout , 'nightout')}
                                                        keyboardType='numeric'
                                                      //  placeholder={'$'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  </View>

                            </View>

                            <View style={{flex: 1,flexDirection: 'row', backgroundColor: 'transparent'}} >
                                              <View style={{width: '50%', height: 30, marginLeft:30, justifyContent:'center'}} >
                                                  <Text style={{fontSize:hp('4%')}}>{this.state.PrimaryLanguage ? this.state.DancingP : 'Dancing Program'}</Text> 
                                              </View>

                                              <View style={{flexDirection: 'row', backgroundColor: 'transparent',borderWidth: 1, borderColor: '#C0C0C0', height: RF(10), width:RF(20)}} >
                                                    <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',marginLeft:5 }}>$</Text>
                                                    <TextInput style={{
                                                      alignSelf: 'center',
                                                      fontSize:hp('3%'),
                                                      width:'100%'
                                                    }}
                                                    value={this.state.dancing}
                                                    onChangeText={this.onChangeActivity.bind(this.state.dancing , 'dancing')}
                                                    keyboardType='numeric'
                                                   // placeholder={'$'}
                                                     underlineColorAndroid="transparent"
                                                />   
                                              </View>
                            </View>

                            <View style={{flex: 1,flexDirection: 'row', backgroundColor: 'transparent'}} >
                                                  <View style={{width: '50%', height: 30, marginLeft:30, justifyContent:'center' }} >
                                                      <Text style={{fontSize:hp('4%')}}>{this.state.PrimaryLanguage ? this.state.Games : 'Games'}</Text> 
                                                  </View>

                                                  <View style={{flexDirection: 'row', backgroundColor: 'transparent',borderWidth: 1, borderColor: '#C0C0C0', height: RF(10), width:RF(20)}} >
                                                      <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',marginLeft:5 }}>$</Text>
                                                        <TextInput style={{
                                                          alignSelf: 'center',
                                                          fontSize:hp('3%'),
                                                          width:'100%'
                                                        }}
                                                        value={this.state.sports}
                                                        onChangeText={this.onChangeActivity.bind(this.state.sports , 'sports')}
                                                        keyboardType='numeric'
                                                        //placeholder={'$'}
                                                         underlineColorAndroid="transparent"
                                                    />   
                                                  </View>
                            </View>


                            <View style={{flex: 1,flexDirection: 'row', backgroundColor: 'transparent'}} >
                                              <View style={{width: '50%', height: 30, marginLeft:30, justifyContent:'center'}} >
                                                  <Text style={{fontSize:hp('4%')}}>{this.state.PrimaryLanguage ? this.state.Entertainment : 'Entertainment'}</Text> 
                                              </View>

                                              <View style={{flexDirection: 'row', backgroundColor: 'transparent',borderWidth: 1, borderColor: '#C0C0C0', height: RF(10), width:RF(20)}} >
                                                  <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',marginLeft:5 }}>$</Text>

                                                   <TextInput style={{
                                                          alignSelf: 'center',
                                                          fontSize:hp('3%'),
                                                          width:'100%'
                                                        }}
                                                        value={this.state.cinema}
                                                        onChangeText={this.onChangeActivity.bind(this.state.cinema , 'cinema')}
                                                        keyboardType='numeric'
                                                       // placeholder={'$'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  
                                              </View>

                                              
                            </View>
                        </View>

                        <View style={{flex: 2.5, backgroundColor: 'transparent', justifyContent:'space-between'}} >
                              
                            <View style={{flex: 1,flexDirection: 'row', backgroundColor: 'transparent', justifyContent:'space-between', marginTop:'3%'}} >
                                
                                <View style={{width: '50%', height: RF(8), backgroundColor: '#FFFFFF', height: RF(8), justifyContent:'center' }} >
                                    <TextInput
                                        style={{height: RF(10),fontSize:RF(3), borderColor: 'gray', borderWidth: 1,}}
                                        onChangeText={this.onChangeActivityName.bind(this.state.nightout1Name , 'nightout1Name')}
                                        value={this.state.nightout1Name}
                                        placeholder={ this.state.PrimaryLanguage ? this.state.WriteSomething + '...': 'Write something here ...'}
                                      />
                                </View>

                                <View style={{flexDirection: 'row', backgroundColor: 'transparent',borderWidth: 1, borderColor: '#C0C0C0', height: RF(10), width:RF(20)}} >
                                      <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',marginLeft:5 }}>$</Text>
                                      <TextInput style={{
                                        alignSelf: 'center',
                                        fontSize:hp('3%'),
                                        width:'100%'
                                      }}
                                      value={this.state.nightout1}
                                      onChangeText={this.onChangeActivity.bind(this.state.nightout1 , 'nightout1')}
                                      keyboardType='numeric'
                                     // placeholder={'$'}
                                      underlineColorAndroid="transparent"
                                  />   
                                </View>
                            </View>

                            <View style={{flex: 1,flexDirection: 'row', backgroundColor: 'transparent', justifyContent:'space-between'}} >
                                              <View style={{width: '50%', height: RF(8), backgroundColor: '#FFFFFF',height: RF(8),justifyContent:'center' }} >
                                                  <TextInput
                                                      style={{height: RF(10),fontSize:RF(3), borderColor: 'gray', borderWidth: 1}}
                                                      onChangeText={this.onChangeActivityName.bind(this.state.dancing1Name , 'dancing1Name')}
                                                      value={this.state.dancing1Name}
                                                      placeholder={ this.state.PrimaryLanguage ? this.state.WriteSomething + '...': 'Write something here ...'}
                                                    />
                                              </View>

                                              <View style={{flexDirection: 'row', backgroundColor: 'transparent',borderWidth: 1, borderColor: '#C0C0C0', height: RF(10), width:RF(20)}} >
                                                    <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',marginLeft:5 }}>$</Text>
                                                    <TextInput style={{
                                                      alignSelf: 'center',
                                                      fontSize:hp('3%'),
                                                      width:'100%'
                                                    }}
                                                    value={this.state.dancing1}
                                                    onChangeText={this.onChangeActivity.bind(this.state.dancing1 , 'dancing1')}
                                                    keyboardType='numeric'
                                                    //placeholder={'$'}
                                                    underlineColorAndroid="transparent"
                                                />   
                                              </View>
                            </View>

                            <View style={{flex: 1,flexDirection: 'row', backgroundColor: 'transparent', justifyContent:'space-between'}} >
                                                  <View style={{width: '50%', height: RF(8), backgroundColor: '#FFFFFF', height: RF(8),justifyContent:'center' }} >
                                                      <TextInput
                                                          style={{height: RF(10),fontSize:RF(3), borderColor: 'gray', borderWidth: 1}}
                                                          onChangeText={this.onChangeActivityName.bind(this.state.sports1Name , 'sports1Name')}
                                                          value={this.state.sports1Name}
                                                          placeholder={ this.state.PrimaryLanguage ? this.state.WriteSomething + '...': 'Write something here ...'}
                                                        />
                                                  </View>

                                                  <View style={{flexDirection: 'row', backgroundColor: 'transparent',borderWidth: 1, borderColor: '#C0C0C0', height: RF(10), width:RF(20)}} >
                                                    <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',marginLeft:5 }}>$</Text>
                                                    <TextInput style={{
                                                          alignSelf: 'center',
                                                          fontSize:hp('3%'),
                                                          width:'100%'
                                                        }}
                                                        value={this.state.sports1}
                                                        onChangeText={this.onChangeActivity.bind(this.state.sports1 , 'sports1')}
                                                        keyboardType='numeric'
                                                        //placeholder={'$'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  </View>
                            </View>
                            
                            <View style={{flex: 1,flexDirection: 'row', backgroundColor: 'transparent', justifyContent:'space-between'}} >
                                              <View style={{width: '50%', height: RF(8), backgroundColor: '#FFFFFF', height: RF(8), justifyContent:'center' }} >
                                                    <TextInput
                                                        style={{height: RF(10),fontSize:RF(3), borderColor: 'gray', borderWidth: 1}}
                                                        onChangeText={this.onChangeActivityName.bind(this.state.cinema1Name , 'cinema1Name')}
                                                        value={this.state.cinema1Name}
                                                        placeholder={ this.state.PrimaryLanguage ? this.state.WriteSomething + '...': 'Write something here ...'}
                                                      />
                                                </View>

                                              <View style={{flexDirection: 'row', backgroundColor: 'transparent',borderWidth: 1, borderColor: '#C0C0C0', height: RF(10), width:RF(20)}} >
                                                    <Text style={{alignSelf:"center", fontSize:hp('4%'),color:'#45a09e',marginLeft:5 }}>$</Text>
                                                    <TextInput style={{
                                                      alignSelf: 'center',
                                                      fontSize:hp('3%'),
                                                      width:'100%',

                                                    }}
                                                    value={this.state.cinema1}
                                                    onChangeText={this.onChangeActivity.bind(this.state.cinema1 , 'cinema1')}
                                                    keyboardType='numeric'
                                                    //placeholder={'$'}
                                                     underlineColorAndroid="transparent"
                                                    />   
                                              </View>
                            </View>
                        </View>

                        <View style={{flex: 2, backgroundColor: 'transparent'}} >
                                <View style={{flex: 3,flexDirection:'column',justifyContent:'center', backgroundColor: 'transparent'}} >
                                            <Text style={{color:'#000000', fontSize:hp('3%'), alignSelf:'center'}}>{this.state.PrimaryLanguage ? this.state.AMOUNTLEFT : 'AMOUNT LEFT'}</Text>
                                             
                                             <View style={{width:'70%', height: 50, backgroundColor: '#FF4500',alignSelf:'center', marginTop:10, borderWidth: 1, borderColor: '#C0C0C0', borderRadius:5}} >
                                              <TextInput style={{
                                                  alignSelf: 'center',
                                                  textAlign:'center',
                                                  color: '#FFFFFF',
                                                  fontSize:hp('3%')
                                                }}
                                                value={this.state.AmountLeft}
                                                keyboardType='numeric'
                                                placeholder={'$ 0.00 '}
                                                underlineColorAndroid="transparent"
                                                editable={false}

                                            />   
                                          </View>
                                </View>
                                
                                <View style={{flex: 3,justifyContent:'center' }} >
                                    <View style={{width:'70%', height:'30%', backgroundColor:'#000000',alignSelf:'center'}}>
                                            <TouchableOpacity style={styles.homebutton_style,{width:'100%', height:'100%'}} onPress={() => {
                                                        Alert.alert(
                                                          this.state.PrimaryLanguage ? this.state.RESETALLMONEY : 'Do you want to reset all money ?',
                                                          '',
                                                          [
                                                            {text: this.state.cancel ? this.state.cancel : 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                            {text: this.state.ok ? this.state.ok : 'OK', onPress: () => {this.onPressResetAmount(this)}},
                                                          ],
                                                          { cancelable: false }
                                                        )
                                              }}>
                                            <Text style={{color:'#FFFFFF', fontSize:hp('3%'), alignSelf:'center'}}>{this.state.PrimaryLanguage ? this.state.RESET : 'RESET ALL AMOUNT'}</Text>
                                          </TouchableOpacity>
                                    </View>
                                </View>
        
                        </View>
                    </View> 



                    <View style={{flex: 1,flexDirection: 'row', alignSelf:'center', marginTop:hp('1%'), marginBottom:hp('.5%')}} >


                      <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressHome.bind(this)}  underlayColor='#232A40'>
                      <ImageBackground source={require('../assets/images/shoppingimages/welcome.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center'}}>
                       <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.WECOME : 'WECOME PAGE'}</Text>
                      </ImageBackground>
                      </TouchableOpacity>

                      <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressShoopingList.bind(this)}  underlayColor='#232A40'>
                      <ImageBackground source={require('../assets/images/shoppingimages/shopping-list.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center'}}>
                        <Text style={{alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5)}}>{this.state.PrimaryLanguage ? this.state.SHOPPINGLIST : 'SHOPPING LIST'}</Text>
                      </ImageBackground>
                      </TouchableOpacity>

                      <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressGoShopping.bind(this)}  underlayColor='#232A40'>
                      <ImageBackground source={require('../assets/images/shoppingimages/letsgo.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center',}}>
                          <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.GOSHOPPING : 'LETS GO SHOPPING'}</Text>
                      </ImageBackground>
                      </TouchableOpacity>

                      <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressPantry.bind(this)}  underlayColor='#232A40'>
                      <ImageBackground source={require('../assets/images/shoppingimages/pantry.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                          <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.PANTRY : 'PANTRY'}</Text>
                      </ImageBackground>
                      </TouchableOpacity>

                      <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMyMoney.bind(this)}  underlayColor='#232A40'>
                      <ImageBackground source={require('../assets/images/shoppingimages/howmuch.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                          <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE : 'HOW MUCH MONEY I HAVE'}</Text>
                      </ImageBackground>
                      </TouchableOpacity>

                      <TouchableOpacity style={styles.homebutton_style} onPress={() => {
                                              Alert.alert(
                                               this.state.PrimaryLanguage ? this.state.voicefeatureforlogout : 'Are you sure to Logout',
                                                '',
                                                [
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforcancel : 'cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforok : 'Ok', onPress: () => {this.onPressLogout(this)}},
                                                ],
                                                { cancelable: false }
                                              )
                                    }}>
                    <ImageBackground source={require('../assets/images/shoppingimages/logoutmain.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                      <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.LOGOUT : 'LOGOUT'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    </View>


              </View>
              </ScrollView>
              { this.state.isLoading ?<ActivityIndicator
                        animating     ={true}
                        transparent   ={true}
                        visible       ={false}
                        style         ={styles.indicator}
                        size          ="large"
                      />  : <View/>}   
          </ImageBackground>    
          </View> 
    );
  }
}


