/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  ActivityIndicator,
  AsyncStorage,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  TextInput
} from 'react-native';

import styles from './GlobalStyle.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import GlobalVariables from './GlobalVariables.js';
import ImagePicker from 'react-native-image-picker';


type Props = {};

export default class WelcomeScreen extends Component<Props> {

    static navigationOptions = {
          
        header: null
    }

    constructor(props){

        super(props);

        AsyncStorage.getItem('TOKEN').then((data) => { 

          if(data){
              Actions.Home();
          }else{
             Actions.Login();
          }
                
        });


    }

 
    render() {
        return (
          <View style={styles.container}>

          </View> 
    );
  }
}


