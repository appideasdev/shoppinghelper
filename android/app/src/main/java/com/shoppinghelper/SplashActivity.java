package com.shoppinghelper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 4000;
    SharedPreferences settings;
    boolean firstRun;
    //SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        StartAnimations();

        try {
            settings = getSharedPreferences("prefs", 0);
            firstRun = settings.getBoolean("firstRun", false);
        }catch (NullPointerException e){
            e.fillInStackTrace();
        }



    }

    /**
     * *********** *   Splash Time Loading  Method   ***************
     */

    private void StartAnimations() {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (firstRun == false) //if running for first time
                {
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("firstRun", true);
                    editor.commit();

                    Intent intent_mainpage = new Intent(SplashActivity.this, MainActivity.class);
                    intent_mainpage.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent_mainpage);
                   // overridePendingTransition(R.anim.translation2, R.anim.translation);

                    SplashActivity.this.finish();
                } else {

                    Intent intent_mainpage = new Intent(SplashActivity.this, MainActivity.class);
                    intent_mainpage.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent_mainpage);
                    //overridePendingTransition(R.anim.translation2, R.anim.translation);
                }
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }


}
