/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  Button,
  TouchableHighlight,
  Image,
  TextInput,
  Keyboard,
  Alert,
  FlatList,
  ActivityIndicator,
  AsyncStorage,
  ImageBackground,
  TouchableOpacity,
  Modal,
  ScrollView,
} from 'react-native';

import styles from './GlobalStyle.js';
import GlobalVariables from './GlobalVariables.js';

import {Actions, ActionConst} from 'react-native-router-flux';
import { Table, Row, Rows } from 'react-native-table-component';
import ImagePicker from 'react-native-image-picker';
import Tts from 'react-native-tts';
import renderIf from './renderIf'
import * as Animatable from 'react-native-animatable';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import RF from "react-native-responsive-fontsize";

type Props = {};

export default class GoShopping extends Component<Props> {

  static navigationOptions = {
      header: null
  }


 constructor(props)
 {
 
    AsyncStorage.getItem('TOKEN').then((data) => { 

      if(!data){
          Actions.Login();
      }
            
    });



   super(props);

   var GVar = GlobalVariables.getApiUrl();

   this.state = { 
      user_id                  : null,
      token                    : null,
      pantryaddedid            : null,
      GridViewItems: [
                        {key: 'One'},
                        {key: 'Two'},
                        {key: 'Three'},
                        {key: 'Four'},
                        {key: 'Five'},
                        {key: 'Six'},
                        {key: 'Seven'},
                        {key: 'Eight'},
                        {key: 'Nine'},
                        {key: 'Ten'},
                        {key: 'Eleven'},
                        {key: 'Twelve'},
                        {key: 'Thirteen'},
                        {key: 'Fourteen'},
                        {key: 'Fifteen'},
                        {key: 'Sixteen'},
                        {key: 'Seventeen'},
                        {key: 'Eighteen'},
                        {key: 'Nineteen'},
                        {key: 'Twenty'}
                    ],
   ImagePath                : GVar.ImagePath,
   isLoading                : true,
   isLoadingPantry          : true,
   name                     : null, 
   avatarSource             : null,
   dataSource               : null,
   userdatasource           : [],
   PrimaryPantryadded       : null,
   showmodel                : false,
   modalVisible             : false,
   Pantryuseradded          : null,
   cross                    : false,
   pantryweighttype         : false,
 }

  // Alert.alert(this.state.ImagePath);

  AsyncStorage.getItem('name').then((data) => { 
        
    this.setState({
               isLoading: false,
               name: data.toUpperCase(),
             });
  });


        AsyncStorage.getItem('PrimaryLanguage').then((data) => {
          
          

              if( data == null ){

                this.setState({PrimaryLanguage : 'en' });
                
              }else{

                this.setState({PrimaryLanguage : data }); 
              }
              

        });

  

    AsyncStorage.multiGet(["name", "user_id","LanguageType","TOKEN","PrimaryLanguage","SeconderyLanguage","userdatasource"]).then(response => {
    
            var name                          = response[0][1];
            var user_id                       = response[1][1];
            var LanguageType                  = response[2][1];
            var token                         = response[3][1];
            var PrimaryLanguage               = response[4][1];
            var SeconderyLanguage             = response[5][1];
            var userdatasource                = response[6][1];

            //AsyncStorage.removeItem('userdatasource')

            console.log("userdatasource"+JSON.stringify(userdatasource));

            //Alert.alert(LanguageType);

            /*if( LanguageType =="dual" ){

            
              this.setState({ShowtwoLanguageBox : true});
            }else{

             
              this.setState({ShowtwoLanguageBox : false});
            }*/

            this.setState({

                            pantryname          : PrimaryLanguage +'_pantry',
                            LanguageType        : LanguageType,
                            token               : token,
                            PrimaryLanguage     : PrimaryLanguage,
                            SeconderyLanguage   : SeconderyLanguage,
                            userdatasource      : JSON.parse(userdatasource),
                            user_id             : JSON.parse(user_id)
                          });

            /*AsyncStorage.setItem('pantryname', PrimaryLanguage +'_pantry' );

            var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();
            if(PrimaryLanguage == null){
              PrimaryLanguage = 'en';
            }
            formdata.append('language', PrimaryLanguage );


            
              try {
                    let response = fetch(GVar.Pantry,{
                                      method: 'POST',
                                      headers:{
                                                  'token' : token
                                              },
                                      body: formdata,   
                                      }).then((response) => response.json())
                                               .then((responseJson) => {
                                                 this.setState({
                                                   isLoadingPantry: false,
                                                   dataSource: responseJson.pantry,
                                                 }, function() {
                                                    AsyncStorage.setItem('dataSource', JSON.stringify(this.state.dataSource) );
                                                 });
                                               });


                        
                       }catch (error) {

                        if( error.line == 18228 ){
                          Alert.alert("Please connect to network");
                        }

                       
                      }*/


    }); 

    AsyncStorage.getItem('PANTRYDATA').then((data) => {

     
      var pantry = JSON.parse(data);

      if( Array.isArray(pantry) && pantry.length > 0 ){

          this.setState({
                     isLoading : false,
                     dataSource: JSON.parse(data),
                   });
      }else{

        this.setState({
                     isLoading : false,
                     dataSource: [],
                   });
      }
      

    });



   AsyncStorage.getItem('isSingle').then((data) => {
          
            

            if( data == 'true'){

              this.setState({isSingle : true});
              this.setState({ShowtwoLanguageBox : false});
            }

            if( data == 'false'){

              this.setState({isSingle : false});
              this.setState({ShowtwoLanguageBox : true});
            }

            if( data == null ){
              this.setState({ShowtwoLanguageBox : true});
            }

            

      });  






 }

 GetGridViewItem (item) {
  
 Alert.alert(item);

 }

 GetGridViewItem1 (item) {
  
 Alert.alert(item);

 }


  componentDidMount = async () => {

    this.setLanguages = this.setLanguages();
  }

  setLanguages(){

  

    AsyncStorage.getItem('PrimaryLanguage').then((P_LANG) => {
          
          

          AsyncStorage.getItem('WELCOME PAGE').then((data) => {

                var WECOME = JSON.parse(data);

                if( P_LANG == 'en'){

                  this.setState({WECOME : WECOME.en})
                }

                if( P_LANG == 'el'){

                    this.setState({WECOME : WECOME.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({WECOME : WECOME.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({WECOME : WECOME.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({WECOME : WECOME.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({WECOME : WECOME.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({WECOME : WECOME.it})
                }

                if( P_LANG == 'es'){

                    this.setState({WECOME : WECOME.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({WECOME : WECOME.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({WECOME : WECOME.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({WECOME : WECOME.fil})
                }

          });

          AsyncStorage.getItem('SHOPPING LIST').then((data) => {

                var SHOPPINGLIST = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({SHOPPINGLIST : SHOPPINGLIST.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SHOPPINGLIST : SHOPPINGLIST.fil})
                }

          });

          AsyncStorage.getItem('LETS GO SHOPPING').then((data) => {

                var GOSHOPPING = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({GOSHOPPING : GOSHOPPING.en})
                }

                if( P_LANG == 'el'){

                    this.setState({GOSHOPPING : GOSHOPPING.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({GOSHOPPING : GOSHOPPING.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({GOSHOPPING : GOSHOPPING.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({GOSHOPPING : GOSHOPPING.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({GOSHOPPING : GOSHOPPING.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({GOSHOPPING : GOSHOPPING.it})
                }

                if( P_LANG == 'es'){

                    this.setState({GOSHOPPING : GOSHOPPING.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({GOSHOPPING : GOSHOPPING.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({GOSHOPPING : GOSHOPPING.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({GOSHOPPING : GOSHOPPING.fil})
                }

          });

          AsyncStorage.getItem('PANTRY').then((data) => {

                var PANTRY = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({PANTRY : PANTRY.en})
                }

                if( P_LANG == 'el'){

                    this.setState({PANTRY : PANTRY.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({PANTRY : PANTRY.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({PANTRY : PANTRY.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({PANTRY : PANTRY.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({PANTRY : PANTRY.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({PANTRY : PANTRY.it})
                }

                if( P_LANG == 'es'){

                    this.setState({PANTRY : PANTRY.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({PANTRY : PANTRY.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({PANTRY : PANTRY.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({PANTRY : PANTRY.fil})
                }

          });

          AsyncStorage.getItem('HOW MUCH MONEY DO I HAVE').then((data) => {

                var MONEYIHAVE = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYIHAVE : MONEYIHAVE.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYIHAVE : MONEYIHAVE.fil})
                }

          });

  AsyncStorage.getItem('Are you sure to log out?').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforlogout : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforlogout : voicefeatureGS.fil})
                              }

                        });
                AsyncStorage.getItem('ok').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforok : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforok : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforok : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforok : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforok : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforok : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforok : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforok : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforok : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('cancel').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforcancel : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforcancel : voicefeatureGS.fil})
                              }

                        });

      
          AsyncStorage.getItem('MONEY MANAGEMENT').then((data) => {

                var MONEYMANAGEMENT = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.fil})
                }

          });

          AsyncStorage.getItem('SETUP').then((data) => {

                var SETUP = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({SETUP : SETUP.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SETUP : SETUP.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SETUP : SETUP.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SETUP : SETUP.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SETUP : SETUP.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SETUP : SETUP.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SETUP : SETUP.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SETUP : SETUP.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SETUP : SETUP.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SETUP : SETUP.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SETUP : SETUP.fil})
                }

          });

          AsyncStorage.getItem('LOGOUT').then((data) => {

                var LOGOUT = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({LOGOUT : LOGOUT.en})
                }

                if( P_LANG == 'el'){

                    this.setState({LOGOUT : LOGOUT.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({LOGOUT : LOGOUT.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({LOGOUT : LOGOUT.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({LOGOUT : LOGOUT.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({LOGOUT : LOGOUT.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({LOGOUT : LOGOUT.it})
                }

                if( P_LANG == 'es'){

                    this.setState({LOGOUT : LOGOUT.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({LOGOUT : LOGOUT.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({LOGOUT : LOGOUT.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({LOGOUT : LOGOUT.fil})
                }

          });

          AsyncStorage.getItem('ADD AN IMAGE INTO PANTRY').then((data) => {

                var ADDIMAGE = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({ADDIMAGE : ADDIMAGE.en})
                }

                if( P_LANG == 'el'){

                    this.setState({ADDIMAGE : ADDIMAGE.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({ADDIMAGE : ADDIMAGE.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({ADDIMAGE : ADDIMAGE.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({ADDIMAGE : ADDIMAGE.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({ADDIMAGE : ADDIMAGE.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({ADDIMAGE : ADDIMAGE.it})
                }

                if( P_LANG == 'es'){

                    this.setState({ADDIMAGE : ADDIMAGE.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({ADDIMAGE : ADDIMAGE.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({ADDIMAGE : ADDIMAGE.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({ADDIMAGE : ADDIMAGE.fil})
                }

          });

          
    });
  }


  onPressShoopingList(){
    Actions.ShoppingList();
  }

  onPressHome(){
    Actions.Home();
  }

  onPressGoShopping(){
    Actions.GoShopping();
  }

  onPressMyMoney(){
     Actions.MyMoney();
  }

  onPressMoneyManagement(){
      Actions.MoneyManagement();
  }

  onPressLearnSetUp(){

  }

  _keyboardDidHide() {
   
    Keyboard.dismiss()
  }

  GetPantryImage(){

    
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        Alert.alert("Please check Permition")
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        var arr   = [];

        
      var token = this.state.token;
      var user_id = this.state.user_id;
      var PrimaryLanguage = this.state.PrimaryLanguage;


      
      let formdata = new FormData();

      formdata.append('user_id', user_id );
      formdata.append('language_code', PrimaryLanguage );
      formdata.append('pantry_path', "" );
  
      formdata.append('pantry_path', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();

      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.AddPantryImage,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            this.setState({pantryaddedid: responseJson.id});
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }

        this.setState({
          //userdatasource    : arr,
          showmodel         : true,
          modalVisible      : true,
          latestpantry      : source,
        });


        // AsyncStorage.setItem('userdatasource', JSON.stringify(arr) );

       
      }
    });
  }


  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  closePantryModel(visible) {
            

            var GVar = GlobalVariables.getApiUrl();

            var user_id = this.state.user_id;
            var token = this.state.token;


            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('pantry_id', this.state.pantryaddedid );
            //formdata.append('pantry_id', '1' );
 
            try {
                  
                let response = fetch( GVar.BaseUrl + GVar.DeletePantry,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token,
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                             if( responseJson.status == 1 ){

                                                // Alert.alert(JSON.stringify(responseJson));
                                                             
                                             }
                                                
                                           });
                
               }catch (error) {

                if( error.line == 18228 ){
                  Alert.alert("Net is not connected when it will be conneted it will auto reconoige to secondary language");
                }

               
              }

              this.setState({modalVisible: visible});
  }

  onChangePantryName = ( value ) => {

    this.setState({changedPrimaryPantryadded : value });
    AsyncStorage.multiGet(["name", "user_id","LanguageType","TOKEN","PrimaryLanguage","SeconderyLanguage",]).then(response => {
    
            var name                          = response[0][1];
            var user_id                       = response[1][1];
            var LanguageType                  = response[2][1];
            var token                         = response[3][1];
            var PrimaryLanguage               = response[4][1];
            var SeconderyLanguage             = response[5][1];

            var GVar = GlobalVariables.getApiUrl();

            AsyncStorage.multiSet([
                                  ["PrimaryPantryadded",       this.checknull(JSON.stringify(value))],
                              ]);


            let formdata = new FormData();

            formdata.append('user_id', user_id );
            formdata.append('primary', PrimaryLanguage);
            formdata.append('secondary', SeconderyLanguage);
            formdata.append('text', value );
 
            try {
                  
                let response = fetch( GVar.BaseUrl + GVar.TextChange,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                             if( responseJson.status == 1 ){

                                                 //Alert.alert(JSON.stringify(responseJson));
                                                 this.setState({ Pantryuseradded: responseJson.text,});

                                                  AsyncStorage.multiSet([
                                                                            ["Pantryuseradded",             this.checknull(JSON.stringify(responseJson.text))],
                                                                            ["PrimaryPantryadded",          this.checknull(JSON.stringify(value))],
                                                                        ]);
                                             }
                                                
                                           });
                
               }catch (error) {

                if( error.line == 18228 ){
                  Alert.alert("Net is not connected when it will be conneted it will auto reconoige to secondary language");
                }

               
              }
    }); 
  } 

  checknull(value){
  if(value){
    return value;
  }
  else {
    return '';
  }
}



  onPressSavePantry(){

     AsyncStorage.multiGet(["Pantryuseradded", "PrimaryPantryadded","TOKEN","user_id", "PrimaryLanguage","dataSource"]).then(response => {

            var Pantryuseradded               = response[0][1];
            //var PrimaryPantryadded            = response[1][1];
            var token                         = response[2][1];
            var user_id                       = response[3][1];
            var PrimaryLanguage               = response[4][1];
            var dataSource                    = this.state.dataSource;
            var source                       = this.state.latestpantry;
            var userdatasource               = this.state.userdatasource;

          
            console.log(JSON.stringify(this.state.dataSource),"dataSource");

            var PrimaryPantryadded             = this.state.changedPrimaryPantryadded;

            if( !PrimaryPantryadded ){
              Alert.alert("Message", "Please enter pantry name also");
            }else{


                var userdatasource = this.state.userdatasource;

                if( userdatasource == null ){
                  userdatasource = [];
                }

                var pan = PrimaryPantryadded;
                var capspantry = pan.charAt(0).toUpperCase() + pan.substr(1);

                var index = this.getIndex(capspantry, userdatasource, 'pantry_name');
                var index1 = this.getIndex(capspantry, dataSource, 'en_pantry');

                console.log(index1,"index1");
            
                if( index < 0 && index1 < 0 ){


                var arr = [];

                if( JSON.stringify(userdatasource) != 'null'){

                  arr = userdatasource;
                  id  = userdatasource.length + 1; 
                }

                var pan = PrimaryPantryadded;
                var capspantry = pan.charAt(0).toUpperCase() + pan.substr(1);

                arr.push({
                    useraddedpantry_id    : this.state.pantryaddedid, 
                    pantry_image          : source, 
                    pantry_name           : capspantry,
                    pantry_secondryname   : JSON.parse(Pantryuseradded),
                    pantry_isweighted     : this.state.pantryweighttype,
                });

                console.log(JSON.stringify(arr),"newpantryadded");

                AsyncStorage.setItem('userdatasource', this.checknull(JSON.stringify(arr) ));

                
               

                this.setState({
                  userdatasource    : arr,
                });


                var token = this.state.token;
                var user_id = this.state.user_id;
                var PrimaryLanguage = this.state.PrimaryLanguage;


                
                let formdata = new FormData();

                formdata.append('user_id', user_id );
                formdata.append('language_code', PrimaryLanguage );
                formdata.append('id', this.state.pantryaddedid );
                formdata.append('pantry_name', capspantry );
                

                var GVar = GlobalVariables.getApiUrl();

                  try {
                        

                      try {
                            let response = fetch( GVar.BaseUrl + GVar.UpdatePantry,{
                                              method: 'POST',
                                              headers:{
                                                          'Accept': 'application/json',
                                                          'token' : token,
                                                          'Content-Type' : 'multipart/form-data',
                                                      },
                                              body: formdata,        
                                              }).then((response) => response.json())
                                                       .then((responseJson) => {

                                                        //Alert.alert(JSON.stringify(responseJson));
                                                            
                                                       });
                          
                            
                         }catch (error) {

                          if( error.line == 18228 ){
                            


                          }

                        }

                  }catch (error) {

                      if( error.line == 18228 ){
                        //Alert.alert("Please connect to network");
                      }

                           
                  }


                  Alert.alert("Message","Ok" );
                  this.setState({modalVisible: false});
                  this.setState({showmodel:false});

                }else{
                  Alert.alert("Message", "Pantry allready added please add another");
                }
   
            }

     });

  } 

  onPressLogout(){

    AsyncStorage.removeItem('TOKEN');
    Actions.Login();

  }

  _onLongPressButton( item ) {

    this.setState({cross: !this.state.cross});
   
  }

   getIndex(value, arr, prop) {
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }
 
  handleViewRef = ref => this.view = ref;

  bounce = () =>{ 

    //this.view.rubberBand(800).then(endState => this.setState({ cross    : true }) );
     this.setState({ cross    : false })
   

  }



  deletePantry( pantry_id ) {

    var userdatasource = this.state.userdatasource;
    //alert(JSON.stringify(userdatasource));
    var index = this.getIndex(pantry_id, userdatasource, 'useraddedpantry_id');
    //Alert.alert(JSON.stringify(index))
    //delete userdatasource[index];

    if( index > -1 ){

      userdatasource.splice(index, 1);
      //alert(JSON.stringify(userdatasource));

      AsyncStorage.setItem('userdatasource', this.checknull(JSON.stringify(userdatasource) ));

      this.setState({
        userdatasource    : userdatasource,
      }); 
    }
    

    this.setState({cross : false})
  }


  deleteAdminPantry( pantry_id ) {


    AsyncStorage.getItem('PANTRYDATA').then((data) => {

     
            var pantry = JSON.parse(data);

            var index = this.getIndex(pantry_id, pantry, 'id');

            pantry.splice(index, 1);
   

            AsyncStorage.setItem('PANTRY', this.checknull(JSON.stringify(pantry) ));

            this.setState({
              dataSource    : pantry,
            });


    });

    var token = this.state.token;
    var user_id = this.state.user_id;


    let formdata = new FormData();

    formdata.append('user_id', user_id );
    formdata.append('pantry_id', pantry_id );

    console.log(formdata,"FormData");
    console.log(token,"token");

    var GVar = GlobalVariables.getApiUrl();

 

    try {
          let response = fetch( GVar.BaseUrl + GVar.DeleteAdminPantry,{
                            method: 'POST',
                            headers:{
                                        'token' : token,
                                    },
                            body: formdata,        
                            }).then((response) => response.json())
                            .then((responseJson) => {

                              console.log(responseJson,"responseJson")
                              if( responseJson.status == 1 ){

                                Alert.alert("Deleted");
                              }else{

                                Alert.alert("Somthing went wrong try again.");

                              }       
                                          
                            });
        
          
       }catch (error) {

        if( error.line == 18228 ){
          


        }

      }

                


    

   this.setState({cross : false})
  }

  render() {
    var state       = this.state;
    var inline      = 0;
    var showmodel     = this.state.showmodel; 

    var crossshowing  = this.state.cross;

    var language = this.state.PrimaryLanguage;

    

    var pantry = 'en_pantry';
/*
    if( language == 'en'){

      pantry = 'en_pantry';
    }

    if( language == 'el'){
        pantry = 'el_pantry';
    }

    if( language == 'cmn'){

        pantry = 'cmn_pantry';
    }

    if( language == 'zh'){

        pantry = 'zh_pantry';
    }

    if( language == 'ar'){

        pantry = 'ar_pantry';
    }

    if( language == 'vi'){

        pantry = 'vi_pantry';
    }

    if( language == 'it'){

        pantry = 'it_pantry';
    }

    if( language == 'es'){

        pantry = 'es_pantry';
    }

    if( language == 'hi'){



        pantry = 'hi_pantry';
    }

    if( language == 'pa'){

        pantry = 'pa_pantry';
    }

    if( language == 'fil'){

        pantry = 'fil_pantry';
    }*/

   // Alert.alert(language)
    //Alert.alert(pantry)

    return (
          <View style={styles.pantry_container} >
          <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/> 
           <ImageBackground source={require('../assets/images/shoppingimages/pantry-bg.jpg')}
                    style={[
                   styles.base,{
                       width: "100%"
                   }, {
                       height: "100%"
                   }
               ]}> 

              <View style={{flex: 1,marginTop:hp('2%')}} >
                  
                    <View style={{flex: 1,flexDirection: 'row',alignSelf:'center'}}>


                        <View style={{flex: 1,flexDirection: 'column',alignSelf:'center'}}>
                            <TouchableOpacity onPress={this.GetPantryImage.bind(this)}>
                            <Image source={require('../assets/images/shoppingimages/camera-btn2.png')} style={{alignSelf:'center',width: wp('20%'), height: hp('10%'), marginBottom:-20}}/>
                            <Text style={styles.addpantrytext}>{this.state.PrimaryLanguage ? this.state.ADDIMAGE : "ADD AN IMAGE INTO PANTRY"}</Text> 
                            </TouchableOpacity>
                        </View>
                        
                        <View style={{flex: 1,flexDirection: 'column',alignSelf:'center', }}>
                          <Text style= {styles.hometext}>{this.state.name}'S {this.state.PrimaryLanguage ? this.state.PANTRY : 'PANTRY'}</Text> 
                        </View>

                        <View style={{flex: 1,flexDirection: 'column',alignSelf:'center',}}>
                          
                        </View>

                    </View>

                    <View style={{flex: 4,width:wp('96%'), flexDirection: 'row',justifyContent: 'center',backgroundColor:'#FFFFFF',alignSelf:'center', alignItems:'center', borderWidth:1, borderColor:'#CCCCCC'}}>
                          <ScrollView style={{marginTop:'1%', marginBottom:'1%', alignSelf:'flex-start'}}> 
                                <View style={{flex:1,alignSelf:'flex-start'}} >

                                          <FlatList
                                                 data={ this.state.dataSource }
                                                 renderItem={({item}) =>
                                                 <View style={{flexDirection:'row',width: wp('24%'),  borderWidth:1, borderColor:'#CCCCCC'}}>
                                                    <View style={{flex:1,height: hp('15%'), flexDirection:'row', justifyContent:'center', justifyContent:'space-between'}}>

                                                        <TouchableOpacity onLongPress={this._onLongPressButton.bind(this, item.pantry_image)} onPress={this.bounce.bind()}  style={{flex:1,height: hp('15%'), flexDirection:'row', justifyContent:'center', }}>
                                                          <Animatable.View ref={this.handleViewRef}>
                                                             <ImageBackground style={{width:wp('6%'), height: hp('10%'), alignSelf:'center', marginTop:'10%', marginLeft:wp('-8%')}} source={{uri: this.state.ImagePath +item.pantry_image}} >


                                                                { renderIf(this.state.cross)(
                                                                   <Text style={{marginTop:-2, color: 'red', fontSize: RF(3) }} onPress={() => {
                                                                  Alert.alert(
                                                                    'Are you sure to delete pantry item',
                                                                    '',
                                                                    [
                                                                      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                                      {text: 'OK', onPress: () => {this.deleteAdminPantry(item.id)}},
                                                                    ],
                                                                    { cancelable: false }
                                                                  )
                                                                  }}>X</Text> 
                                                                  ) }
                                                                  
                                                                  
                                                            </ImageBackground>
                                                          </Animatable.View>
                                                      </TouchableOpacity>
                                                      <Text style={{alignSelf:'center', fontWeight:'bold', fontSize: RF(3), marginRight:wp('1%')}} onPress={this.GetGridViewItem.bind(this, item.en_pantry)} > {item.pantry} </Text>
                                                        

                                                    </View>
                                                    </View>}
                                                 numColumns={4}
                                                 extraData={this.state}
                                            />
                                    
                                </View>

                                <View style={{flex:1,alignSelf:'flex-start'}} >

                                          <FlatList
                                                 data={ this.state.userdatasource }
                                                 renderItem={({item}) =>
                                                 <View style={{flexDirection:'row',width: wp('24%'),  borderWidth:1, borderColor:'#CCCCCC'}}>
                                                    <View style={{flex:1,height: hp('15%'), flexDirection:'row', justifyContent:'space-between'}}>

                                                      <TouchableOpacity onLongPress={this._onLongPressButton.bind(this, item.pantry_image)} onPress={this.bounce.bind()}  style={{flex:1,height: hp('15%'), flexDirection:'row', justifyContent:'center', }}>
                                                          <Animatable.View ref={this.handleViewRef}>
                                                             <ImageBackground style={{width:wp('6%'), height: hp('10%'), alignSelf:'center', marginTop:'10%', marginLeft:wp('-8%')}} source={item.pantry_image} >


                                                                { renderIf(this.state.cross)(
                                                                   <Text style={{marginTop:-2, color: 'red',fontSize: RF(3) }} onPress={() => {
                                                                  Alert.alert(
                                                                    'Are you sure to delete pantry item',
                                                                    '',
                                                                    [
                                                                      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                                      {text: 'OK', onPress: () => {this.deletePantry(item.useraddedpantry_id)}},
                                                                    ],
                                                                    { cancelable: false }
                                                                  )
                                                                  }}>X</Text> 
                                                                  ) }
                                                                  
                                                                  
                                                            </ImageBackground>
                                                          </Animatable.View>
                                                      </TouchableOpacity>
                                                      <Text style={{alignSelf:'center', fontWeight:'bold', fontSize: RF(3),marginRight:wp('1%')}} onPress={this.GetGridViewItem1.bind(this, item.pantry_name)} onLongPress={this._onLongPressButton.bind(this, item.pantry_name)}> {item.pantry_name} </Text>
                                              
                                                    </View>
                                                    </View>}
                                                 numColumns={4}
                                                 extraData={this.state}
                                            />
                                    
                                </View>

                          </ScrollView>  
                    </View>

                          

                    <View style={{flex: 1,flexDirection: 'row', alignSelf:'center', marginTop:hp('5%'), marginBottom:hp('.5%')}} >
                
                        <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressHome.bind(this)}  underlayColor='#232A40'>
                        <ImageBackground source={require('../assets/images/shoppingimages/welcome.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center'}}>
                         <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.WECOME : 'WECOME PAGE'}</Text>
                        </ImageBackground>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressShoopingList.bind(this)}  underlayColor='#232A40'>
                          <ImageBackground source={require('../assets/images/shoppingimages/shopping-list.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center'}}>
                            <Text style={{alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5)}}>{this.state.PrimaryLanguage ? this.state.SHOPPINGLIST : 'SHOPPING LIST'}</Text>
                          </ImageBackground>
                          </TouchableOpacity>

                        <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressGoShopping.bind(this)}  underlayColor='#232A40'>
                        <ImageBackground source={require('../assets/images/shoppingimages/letsgo.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center',}}>
                            <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.GOSHOPPING : 'LETS GO SHOPPING'}</Text>
                        </ImageBackground>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMyMoney.bind(this)}  underlayColor='#232A40'>
                        <ImageBackground source={require('../assets/images/shoppingimages/howmuch.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                            <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE : 'HOW MUCH MONEY I HAVE'}</Text>
                        </ImageBackground>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMoneyManagement.bind(this)}  underlayColor='#232A40'>
                        <ImageBackground source={require('../assets/images/shoppingimages/money-management.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                            <Text style={{marginLeft:'5%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYMANAGEMENT : 'MONEY MANAGEMENT'}</Text>
                        </ImageBackground>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homebutton_style} onPress={() => {
                                              Alert.alert(
                                               this.state.PrimaryLanguage ? this.state.voicefeatureforlogout : 'Are you sure to Logout',
                                                '',
                                                [
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforcancel : 'cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforok : 'Ok', onPress: () => {this.onPressLogout(this)}},
                                                ],
                                                { cancelable: false }
                                              )
                                    }}>
                    <ImageBackground source={require('../assets/images/shoppingimages/logoutmain.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                      <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.LOGOUT : 'LOGOUT'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>


                    </View>


              </View>
              </ImageBackground>
              {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
               /> : <View/>
              }

          {this.state.showmodel ?  <Modal
                      supportedOrientations={['landscape']}
                      animationType="slide"
                      transparent={true}
                      visible={this.state.modalVisible}
                      //visible={true}
                      onRequestClose={() => {
                        alert('Modal has been closed.');
                      }}>
                      <ScrollView>
                     
                      <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',alignItems: 'center', width: wp('80%'), marginTop:hp('25%'), marginLeft: wp('10%')}}>
                            

                            <View style={{flex: 1,flexDirection: 'column',height: hp('50%'), backgroundColor: '#00ffff',borderWidth:5,borderRadius:5, borderColor:'#FFFFFF'}} >

                               <TouchableOpacity style={{marginTop:hp('5%'),backgroundColor:'#FFFFFF', }} onPress={() => {this.closePantryModel(!this.state.modalVisible);}}>
                                  <Text style={{marginRight:0,color:'red', fontSize:hp('3%')}}>Close</Text>
                              </TouchableOpacity>


                              <View style={{flex: 1,flexDirection  : 'row', justifyContent : 'space-between',borderWidth : 1,}}>
                               
                                <View style={{width: wp('20%'), height: hp('20%'), marginLeft:-5,}} >
                                    <Image style={{height:wp('20%'), width:hp('25%'),alignSelf:'center', marginTop:hp('2%'),}} source={this.state.latestpantry} />
                                </View>

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between',marginTop:hp('10%'),right:12}}>
                                
                                        <View>
                                            <Text style={{fontSize:hp('3'), fontWeight: 'bold',color:'#000000'}}>Pantry Weighted</Text>
                                        </View>
                                        
                                        <View style={{width: '70%', height: '20%',marginTop:-25 }}>

                                                  <RadioGroup
                                                        size={24}
                                                        thickness={2}
                                                        color='#9575b2'
                                                       // highlightColor='#ccc8b9'
                                                        selectedIndex ={ 0 } onSelect = {(index, value) => this.setState({pantryweighttype:value})} >
                                                        <RadioButton value={false} >
                                                        <View style={{flexDirection:'row'}}>
                                                          <Text style={styles.normaltext_small}>No</Text>
                                                          </View>
                                                        </RadioButton>
                                                 
                                                        <RadioButton value={true}>
                                                        <View style={{flexDirection:'row'}}>
                                                         <Text style={styles.normaltext_small}>Yes</Text>
                                                        </View>
                                                        </RadioButton>
                                               
                                                    </RadioGroup> 

                                        </View>

                                </View>

                                <View style={{flex:1, flexDirection:"column", width: 100, height: 100, right:30, marginTop:-5}} >

                                  {this.state.ShowtwoLanguageBox ? <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',marginLeft:5}}>
                                        <View style={{flex: 1,flexDirection: 'column', width: '100%', height: 50,}} >
                                                  <View style={{width: '100%', height: hp('10%'), borderWidth: 1, borderColor: '#000000',marginTop:10,justifyContent:'center'}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                          //backgroundColor: '#FFFFFF'
                                                        }}
                                                        value={this.state.PrimaryPantryadded}
                                                        onChangeText={this.onChangePantryName.bind(this.state.PrimaryPantryadded)}
                                                        keyboardType=''
                                                        placeholder={'Primary Language'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  </View>
                                                  
                                                  <View style={{width: '100%', height: hp('10%'),marginTop:5, borderWidth: 1, borderColor: '#000000',}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                          //backgroundColor: '#FFFFFF'
                                                        }}
                                                        value={this.state.Pantryuseradded}
                                                        keyboardType=''
                                                        placeholder={'Secondary Language'}
                                                        underlineColorAndroid="transparent"
                                                        editable={false}
                                                  />   
                                                  </View>
                                        </View>
                                        <View style={{width: '100%', height: hp('10%'), marginTop:hp('25%'),borderWidth: 1, borderColor: '#000000',backgroundColor:'#FF0000',justifyContent:'center'}} >
                                              <TouchableOpacity onPress={this.onPressSavePantry.bind(this)}>
                                                  <Text style = {{alignSelf:'center',fontWeight: 'bold',color:'#FFFFFF',fontSize:hp('3%')}}>ADD PANTRY</Text> 
                                              </TouchableOpacity>
                                        </View>
                                      </View>:
                                      <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',marginLeft:20}}>
                                        <View style={{flex: 1,flexDirection: 'row', width: '100%', height: 50,marginTop:hp('5%'),alignSelf:'center'  }} >
                                                  <View style={{width: wp('25%'), height: hp('10%'), borderWidth: 1, borderColor: '#000000',justifyContent:'center'}} >
                                                      <TextInput style={{
                                                          alignSelf: 'center',
                                                          textAlign:'center',
                                                          width: "100%",
                                                          height:hp('10%'),
                                                          fontSize:hp('3%')
                                                        }}
                                                        value={this.state.PrimaryPantryadded}
                                                        onChangeText={this.onChangePantryName.bind(this.state.PrimaryPantryadded)}
                                                        keyboardType=''
                                                        placeholder={'Primary Language'}
                                                        underlineColorAndroid="transparent"
                                                    />   
                                                  </View>
                                                  
                                        </View>
                                        <View style={{width: '85%', height: hp('10%'),marginTop:hp('20%'),borderWidth: 1, borderColor: '#000000',backgroundColor:'#FF0000',justifyContent:'center'}} >
                                              <TouchableOpacity onPress={this.onPressSavePantry.bind(this)}>
                                                  <Text style = {{alignSelf:'center',fontWeight: 'bold',color:'#FFFFFF',fontSize:hp('3%')}}>ADD PANTRY</Text> 
                                              </TouchableOpacity>
                                        </View>
                                      </View>

                                      
                                  }

                                    </View>

                              </View>
                             
                            </View>

                            <View style={{}} />
                          </View>
                          </ScrollView>
                      </Modal> : <View/> }        
          </View> 
          
          
          
    );
  }
}

