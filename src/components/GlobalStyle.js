
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PixelRatio,
  TouchableOpacity,
  Image,
  Alert,
  Dimensions
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


//const deviceHeight = Dimensions.get('window').height;
//const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#000000',
  },
  modelcontainer: {
    flex: 1,
    width:'50%',
    backgroundColor: 'gray',
    alignSelf:'center',
  },
  shoppingList_container: {
    flex: 1,
    backgroundColor: '#fd996b',
  },
  goshopping_container: {
    flex: 1,
    backgroundColor: '#6efecd',
    position:'relative'
  },
  pantry_container: {
    flex: 1,
    backgroundColor: '#fffece',
  },
  mymoney_container: {
    flex: 1,
    backgroundColor: '#fd9bcb',
  },

  moneymanagement_container: {
    flex: 1,
    backgroundColor: '#cccccc',
  },

  setup_container: {
    flex: 1,
    backgroundColor: '#6efffe',
  },
  
  /*********** Buttons/ Text Homepage *******************/
  button_Home: {
    backgroundColor: '#FFFF00',
    borderColor: '#000000',
    height: 60,
    width: 90,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:5,
  },
  button_shopping: {
    backgroundColor: '#fd996b',
    borderColor: '#000000',
    height: 60,
    width: 90,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:5,
  },
  button_goShopping: {
    backgroundColor: '#6efecd',
    borderColor: '#000000',
    height: 60,
    width: 90,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:5,
  },
  button_pantry: {
    backgroundColor: '#fffece',
    borderColor: '#000000',
    height: 60,
    width: 90,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:5,
  },
  button_moneyIhave: {
    backgroundColor: '#fd9bcb',
    borderColor: '#000000',
    height: 60,
    width: 90,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:5,
  },
  button_maoneymanagement: {
    backgroundColor: '#cccccc',
    borderColor: '#000000',
    height: 60,
    width: 90,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:5,
  },
  button_set_up: {
    backgroundColor: '#6efffe',
    borderColor: '#000000',
    height: 60,
    width: 90,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:5,
  }, 

  // Text

  button_text: {
    fontSize: 10,
    textAlign: 'center',
    color: '#000000',
    margin: 10,
    fontWeight: 'bold',
  },

  /*********** end buttons/text Home Page ****************/

  /**************Buttons / Text Shopping List  **************************/
  button_Home_c: {
    backgroundColor: '#FFFF00',
    borderColor: '#000000',
    height: 40,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    flexDirection: 'row'
  },
  button_shopping_c: {
    backgroundColor: '#fd996b',
    borderColor: '#000000',
    height: 40,
    width: 100,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    flexDirection: 'row',
  },
  button_goShopping_c: {
    backgroundColor: '#6efecd',
    borderColor: '#000000',
    height: 40,
    width: 100,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    flexDirection: 'row',
  },
  button_pantry_c: {
    backgroundColor: '#fffece',
    borderColor: '#000000',
    height: 40,
    width: 100,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    flexDirection: 'row',
  },
  button_moneyIhave_c: {
    backgroundColor: '#fd9bcb',
    borderColor: '#000000',
    height: 40,
    width: 100,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    flexDirection: 'row',
  },
  button_maoneymanagement_c: {
    backgroundColor: '#cccccc',
    borderColor: '#000000',
    height: 40,
    width: 100,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    flexDirection: 'row',
    flex:1,
  },
  button_set_up_c: {
    backgroundColor: '#6efffe',
    borderColor: '#000000',
    height: 40,
    width: 100,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    flexDirection: 'row',
    flex:1,
  }, 


  /*********** end buttons Shopping list ****************/

  button_ResetMoney: {
    backgroundColor: '#6efffe',
    borderColor: '#000000',
    height: 40,
    width: 100,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    flexDirection: 'row',
    flex:1,
  }, 
  
  
  selfie_imageRight: {
    height: 50,
    width: 50,
    left: '150%'
  },
  selfie_imageLeft: {
    height: 50,
    width: 50,
    right: '150%'
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    color: '#000000',
    fontWeight: 'bold',
  },
  hometext: {
    fontSize: hp('5%'),
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontFamily: 'Helvetica'
  },
  normaltext: {
    fontSize: 15,
    textAlign: 'center',
    color: '#000000',
    fontWeight: 'bold',
  },
  normaltext_small: {
    fontSize: hp('3%'),
    textAlign: 'center',
    color: '#000000',
    fontWeight: 'bold',
    alignSelf:'center',
    fontFamily:'Courier'
  },
  normaltext_small_white: {
    fontSize: hp('3%'),
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: 'bold',
  },

   normaltext_small_fromstart: {
    fontSize: hp('3%'),
    textAlign: 'center',
    color: '#000000',
    fontWeight: 'bold',

  },

  // Home Buttons Style
    homebutton_style: {
      justifyContent: 'center',
      alignSelf: 'center',
      marginLeft:1,
      marginRight:1,
      marginBottom:10,
      flexDirection: 'row',
      flex:1,
    },
    resetmoney_style: {
      justifyContent: 'center',
      alignSelf: 'center',
      right:0,
      marginBottom:-20,
      flexDirection: 'row',
      flex:1,
    },
    balance:{
      height: wp('10%') ,
      width: hp('30%'), 
      alignSelf:'center'},

    shoppingcamra:{height:30 ,width:100, alignSelf:'center',marginTop:10},
    moneybag:{height:110 ,width:85, alignSelf:'center'},
    spendmoney:{height:44 ,width:80, alignSelf:'center',marginTop:10},



    MainContainer :{

    justifyContent: 'center',
    flex:1,
    margin: 10,
    //paddingTop: (Platform.OS) === 'ios' ? 20 : 0

    },

    GridViewBlockStyle: {

      justifyContent: 'center',
      flex:1,
      alignItems: 'center',
      height: 50,
      margin: 5,
      backgroundColor: '#00BCD4'

    }
    ,

    GridViewInsideTextItemStyle: {

       color: '#fff',
       padding: 10,
       fontSize: 18,
       justifyContent: 'center',
       
     },

     addpantryimage: {
        height:60,
        width:80,
        alignSelf:'center'
     },

     addpantrytext: {
        fontSize:hp('3%'),
        alignSelf:'center',
        fontWeight: 'bold',
        color: '#FFFFFF',
        marginTop:20
     },

     button_stylemoney: {
      justifyContent: 'space-between',
      //alignSelf: 'center',
      marginLeft:10,
      marginRight:10,
      marginBottom:10,
      flexDirection: 'row',
      //flex:1,
    },


    // Activity Indicater

    indicator: {
      alignItems: 'center',
      height: '100%',
      position: 'absolute',
      backgroundColor: 'rgba(192,192,192,0.3)',
      width: '100%'
    },




    // camara

    
    base: {},
    authorizationContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    notAuthorizedText: {
      textAlign: 'center',
      fontSize: 16,
    },


    row: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      fontSize: hp('3%')
      //backgroundColor: 'rgba(192,192,192,0.9)', opacity:0.7
    },

    column: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'space-between',
      fontSize: hp('3%')
    },



    containerx: {
    flex: 1,
    backgroundColor: 'orange',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropzone: {
    zIndex: 0,
    margin: 5,
    width: 106,
    height: 106,
    borderColor: 'green',
    borderWidth: 3
  },
  draggable: {
    zIndex: 0,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: 'black'
  },
  image: {
    width: 75,
    height: 75
  },


  flexdirRowcenter: {

    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop:10,
  },
  
  title: {
    fontSize: 15,
    fontWeight: 'bold',
    paddingTop: 30,
    padding: 20,
    textAlign: 'center',
    backgroundColor: 'rgba(240,240,240,1)',
  },
  button: {
    fontSize: 20,
    backgroundColor: 'rgba(220,220,220,1)',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'rgba(80,80,80,0.5)',
    overflow: 'hidden',
    padding: 7,
  },
  header: {
    textAlign: 'left',
  },
  feature: {
    flexDirection: 'row',
    padding: 10,
    alignSelf: 'stretch',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'rgb(180,180,180)',
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(230,230,230)',
  },

  containerimage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },

  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150
  },

  draggableContainer :{
    //position    : 'absolute',
   
  },
  item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 30,
      margin: 2,
      borderColor: '#2a4944',
      borderWidth: 1,
      backgroundColor: '#d2f7f1'
   },
   Hline : {
      borderBottomColor: '#CCCCCC',
      borderBottomWidth: 2,
      width:'90%',
      alignSelf:'center'
    }
});
