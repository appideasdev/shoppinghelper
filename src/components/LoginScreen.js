/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  ActivityIndicator,
  AsyncStorage,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  TextInput
} from 'react-native';

import styles from './GlobalStyle.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import GlobalVariables from './GlobalVariables.js';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";


type Props = {};

export default class Login extends Component<Props> {

    static navigationOptions = {
          
        header: null
    }

    constructor(props){
        
        super(props);

        var GVar = GlobalVariables.getApiUrl();

        this.state = {
            email               : null,
            Password            : null,
            isloading           : false,
            
        }

        //AsyncStorage.removeItem('TOKEN')

        AsyncStorage.getItem('TOKEN').then((data) => { 

          if(data){
              Actions.Home();
          }
                
        });


    }

    Login(){


        this.setState({isloading:true});

        AsyncStorage.clear();

        var email = this.state.email;
        var Password = this.state.Password;

        //alert(email);
        //alert(Password);

        var GVar = GlobalVariables.getApiUrl();

        let formdata = new FormData();

        formdata.append('email',        email );
        formdata.append('password',  Password );


       /* fetch( GVar.BaseUrl + GVar.Login,{
                                        method: 'POST',
                                        body: formdata,        
                                        }).then((response) => response.json())
                                       .then((responseJson) => {
                                       
                                            if( responseJson.status == '1'){

                                                AsyncStorage.setItem('TOKEN', responseJson.token);
                                                Actions.SetUp();
                                              
                                            }else if( responseJson.status == '0'){

                                                Alert.alert(responseJson.message);
                                                Actions.LoginScreen();
                                              
                                            }
                                            
                                       });*/

        try {
                      let response = fetch( GVar.BaseUrl + GVar.Login,{
                                        method: 'POST',
                                        body: formdata,        
                                        }).then((response) => response.json())
                                       .then((responseJson) => {

                                            this.setState({isloading:false});
                                       
                                            if( responseJson.status == 1){

                                                AsyncStorage.multiSet([
                                                                    ["TOKEN",                responseJson.token],
                                                                    ["PrimaryLanguage",     'en'],
                                                                ]);

                                                //AsyncStorage.setItem('TOKEN', responseJson.token);
                                               
                                                Actions.SetUp();
                                              
                                            }else if( responseJson.status == 0){



                                                Alert.alert(responseJson.message);
                                                //Actions.LoginScreen();
                                              
                                            }
                                            
                                       });
            }catch (error) {

                    if( error.line == 18228 ){
                      
                      Alert.alert("Message","Please to internet");
                      
                    }

            }
    }

    Register(){
        Actions.Signup();
    }

    ForgotPassword(){
       Actions.ForgetPassword();
    }

   
   
    render() {

        var email = this.state.email;
        var Password = this.state.Password;
        return (

          <View style={styles.container}>
          <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>
           
              <ImageBackground  blurRadius={0} source={require('../assets/images/shopper.jpeg')}
                    style={[
                   styles.base,{
                       width: "100%"
                   }, {
                       height: "100%"
                   }
               ]}
               >
              <ScrollView>

                <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}} >
                    <View style={{flex: 2,marginTop:hp('10%')}} >
                             <Image style={{height:RF(15), width:RF(40),alignSelf:'center', marginTop:10}} source={require('../assets/images/site_logo.png')} />
                    </View>
                    
                    <View style={{flex: 4,flexDirection: 'column',justifyContent: 'space-between',marginTop:hp('5%')}} >
                            <View style={{width: wp('50%'), height: hp('12%'), alignSelf:'center',borderWidth: 1, borderColor: '#FFFFFF',borderRadius:wp('25%'),marginTop:10, justifyContent:'center'}} >
                            <TextInput style={{
                                          textAlign:'center',
                                          TextColor: 'FFFFFF',
                                          fontSize: hp('4%')
                                      }}
                                    onChangeText={(email) => this.setState({email})}
                                    value={this.state.email}
                                    keyboardType='email-address'
                                    placeholder={'Email Address'}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor= '#000000'
                                    />   
                            </View>

                            <View style={{width: wp('50%'), height: hp('12%'), alignSelf:'center',borderWidth: 1, borderColor: '#FFFFFF',borderRadius:wp('25%'),marginTop:hp('4%'),justifyContent:'center'}} >
                            <TextInput style={{
                                        textAlign:'center',
                                        TextColor: 'FFFFFF',
                                        fontSize: hp('4%')
                                      }}
                                    onChangeText={(Password) => this.setState({Password})}
                                    value={this.state.Password}
                                    keyboardType='numeric'
                                    placeholder={'Password'}
                                    underlineColorAndroid="transparent"
                                    secureTextEntry={true}
                                    maxLength={4}
                                    placeholderTextColor= '#000000'
                                    />   
                            </View>

                            {this.state.isloading ? <ActivityIndicator size="large" color="#0000ff" /> : <TouchableOpacity onPress={this.Login.bind(this)}>
                            <View style={{width: wp('50%'), height: hp('12%'),justifyContent: 'center', alignSelf:'center',borderWidth: 1, borderColor: 'green',borderRadius:wp('25%'),marginTop:hp('5%'), backgroundColor:'green'}} >
                                <Text style={{alignSelf:'center',fontSize: hp('4%'),textAlign: 'center',color: '#FFFFFF',}}>Login</Text> 
                            </View>
                            </TouchableOpacity>} 

                            <View style={{width: '100%', flex: 1,flexDirection: 'row',justifyContent: 'center',marginTop:20, marginLeft:15}} >
                                <View style={{width: wp('40%'), height: hp('10%'),}} >
                                    <TouchableOpacity onPress={this.ForgotPassword.bind(this)}>
                                        <Text style={{fontSize: hp('4%'),color:'#000000',fontWeight: 'bold',alignSelf:'flex-end', marginRight:'8%'}}>Forgot Password</Text>
                                    </TouchableOpacity>
                                </View>
                                
                                <View style={{width: wp('5%'), height: hp('10%'),}} >
                                    <Text style={{fontSize: hp('4%'),fontWeight: 'bold',color:'#000000'}}>|</Text>
                                </View>
                                
                                <View style={{width: wp('40%'), height: hp('10%'),}} >
                                    <TouchableOpacity onPress={this.Register.bind(this)}>
                                        <Text style={{fontSize: hp('4%'),fontWeight: 'bold',color:'#000000'}}>Register Now</Text>
                                    </TouchableOpacity>
                                </View>
      
                            </View>
                    </View>
                </View>
           
              </ScrollView> 

              </ImageBackground>
                
          </View> 
    );
  }
}


