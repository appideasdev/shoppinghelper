/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  ActivityIndicator,
  AsyncStorage,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  PermissionsAndroid
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";


 async function requestCameraPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        'title': 'Cool Photo App Camera Permission',
        'message': 'Cool Photo App needs access to your camera ' +
                   'so you can take awesome pictures.'
      }
    )


    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera")
    } else {
      console.log("Camera permission denied")
    }
  } catch (err) {
    console.warn(err)
  }
}

import styles from './GlobalStyle.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import GlobalVariables from './GlobalVariables.js';
import ImagePicker from 'react-native-image-picker';


type Props = {};

export default class ShoppingList extends Component<Props> {

  static navigationOptions = {
      header: null
  }

 


  constructor(props){
   super(props);

   var GVar = GlobalVariables.getApiUrl();

   this.state = {
    isLoading       : false,
    name            : null,
    user_id         : null,
    avatarSource    : null,
    profileimage    : null,
    profileimage2   : null,
    subscribe       : null,
    Localprofileimage     : null,
    Localprofileimage2    : null,
   }

   
    AsyncStorage.getItem('TOKEN').then((data) => { 

      if(!data){
          Actions.Login();
      }
            
    });

    AsyncStorage.multiGet(["coin5cent", "coin10cent","coin20cent","coin50cent", "coin1dollar", "coin2dollar", "note5dollar", "note10dollar", "note20dollar", "note50dollar", "note100dollar", "totalmoney","user_id", "TOKEN","MyMoneyAudioType"]).then(response => {
            
            var coin5cent       = response[0][1];
            var coin10cent      = response[1][1];
            var coin20cent      = response[2][1];
            var coin50cent      = response[3][1];
            var coin1dollar     = response[4][1];
            var coin2dollar     = response[5][1];
            var note5dollar     = response[6][1];
            var note10dollar    = response[7][1];
            var note20dollar    = response[8][1];
            var note50dollar    = response[9][1];
            var note100dollar   = response[10][1];
            var totalmoney      = response[11][1];
            var user_id         = response[12][1];
            var token           = response[13][1];
            var MyMoneyAudioType           = response[14][1];


            var GVar = GlobalVariables.getApiUrl();

            let formdata = new FormData();

            formdata.append('user_id',        JSON.parse(user_id) );
            formdata.append('money',          JSON.parse(totalmoney) );
            formdata.append('5centcoin',      JSON.parse(coin5cent) );
            formdata.append('10centcoin',     JSON.parse(coin10cent) );
            formdata.append('20centcoin',     JSON.parse(coin20cent) );
            formdata.append('50centcoin',     JSON.parse(coin50cent) );
            formdata.append('1dollarcoin',    JSON.parse(coin1dollar) );
            formdata.append('2dollarcoin',    JSON.parse(coin2dollar) );
            formdata.append('5dollarnote',    JSON.parse(note5dollar) );
            formdata.append('10dollarnote',   JSON.parse(note10dollar) );
            formdata.append('20dollarnote',   JSON.parse(note20dollar) );
            formdata.append('50dollarnote',   JSON.parse(note50dollar) );
            formdata.append('100dollarnote',  JSON.parse(note100dollar) );

          // Alert.alert( coin5cent +" "+  coin10cent  +" "+coin20cent  +" "+coin50cent +" "+coin1dollar+" "+coin2dollar+" "+note5dollar +" "+note10dollar+" "+note20dollar+" "+note50dollar+" "+note100dollar +" "+ totalmoney)


           console.log(formdata,"formdata");       

            try {
              let response = fetch( GVar.BaseUrl + GVar.UpdateMoney,{
                                method: 'POST',
                                headers:{
                                            'token' : token
                                        },
                                body: formdata,        
                                }).then((response) => response.json())
                               .then((responseJson) => {

                               //Alert.alert(JSON.stringify(responseJson));

                                if( responseJson.status == '1'){

                                
                                  
                                }else if( responseJson.status == '0'){

                                 //Alert.alert(responseJson.message);
                                  
                                }
                                    
                               });
           }catch (error) {

            if( error.line == 18228 ){
              
              
            }

          }

      }); 

  AsyncStorage.multiGet(["profileimage", "profileimage2"]).then(response => {
    
           this.setState({
                                    profileimage     : response[0][1],
                                    profileimage2    : response[1][1],
                                   
                          });   
                          
                           
    });

    AsyncStorage.getItem('PrimaryLanguage').then((data) => {
          
          

          if( data == null ){

            this.setState({PrimaryLanguage : 'en' });
            
          }else{

            this.setState({PrimaryLanguage : data }); 
          }
          

    });   

    AsyncStorage.multiGet(["name", "user_id","LowMoneyWarningType","MoneymanagementAudioType", "MyMoneyAudioType", "ShoppingListAudioType", "ShoppingListType", "LanguageType", "Selfietype","GoShoppingAudioType","TOKEN","GoShoppingAudioType","LowmoneyStatus","LowmoneyTune","LowmoneyValue","PrimaryLanguage","SeconderyLanguage","LanguageTypestatus","LanguageType"]).then(response => {
    

            var name                          = response[0][1];
            var user_id                       = response[1][1];
            var LowMoneyWarningType           = response[2][1];
            var MoneymanagementAudioType      = response[3][1];
            var MyMoneyAudioType              = response[4][1];
            var ShoppingListAudioType         = response[5][1];
            var ShoppingListType              = response[6][1];
            var LanguageType                  = response[7][1];
            var Selfietype                    = response[8][1];
            var GoShoppingAudioType           = response[9][1];
            var token                         = response[10][1];
            var GoShoppingAudioType           = response[11][1];
            var LowmoneyStatus                = response[12][1];
            var LowmoneyTune                  = response[13][1];
            var LowmoneyValue                 = response[14][1];
            var PrimaryLanguage               = response[15][1];
            var SeconderyLanguage             = response[16][1];
            var LanguageTypestatus            = response[17][1];
            var LanguageType1                  = response[18][1];

            this.setState({
                              name                    : name,
                              LowMoneyWarningType     : LowMoneyWarningType,
                              MoneymanagementAudioType: MoneymanagementAudioType,
                              MyMoneyAudioType        : MyMoneyAudioType,
                              ShoppingListAudioType   : ShoppingListAudioType,
                              ShoppingListType        : ShoppingListType,
                              LanguageType            : LanguageType,
                              Selfietype              : Selfietype,
                              GoShoppingAudioType     : GoShoppingAudioType,
                              LowmoneyStatus          : LowmoneyStatus,
                              LowmoneyTune            : LowmoneyTune,
                              LowmoneyValue           : LowmoneyValue,
                              PrimaryLanguage         : PrimaryLanguage,
                              SeconderyLanguage       : SeconderyLanguage,
                              token                   : token,
                              LanguageTypestatus      : LanguageTypestatus,
                              isLoading               : false,
                        });
       });  


      AsyncStorage.multiGet(["coin5cent", "coin10cent","coin20cent","coin50cent", "coin1dollar", "coin2dollar", "note5dollar", "note10dollar", "note20dollar", "note50dollar", "note100dollar", "totalmoney","user_id", "TOKEN", "name"]).then(response => {
              

              var coin5cent       = response[0][1];
              var coin10cent      = response[1][1];
              var coin20cent      = response[2][1];
              var coin50cent      = response[3][1];
              var coin1dollar     = response[4][1];
              var coin2dollar     = response[5][1];
              var note5dollar     = response[6][1];
              var note10dollar    = response[7][1];
              var note20dollar    = response[8][1];
              var note50dollar    = response[9][1];
              var note100dollar   = response[10][1];
              var totalmoney      = response[11][1];
              var user_id         = response[12][1];
              var token           = response[13][1];
              var name           = response[14][1];

              this.setState({token, token, isLoading: true, name: name,user_id: user_id,totalmoney: JSON.parse(totalmoney)});
            

              var GVar = GlobalVariables.getApiUrl();
      
              try {
                  let response = fetch(GVar.BaseUrl + GVar.Profile,{
                                    method: 'GET',
                                    headers:{
                                                'token' : token
                                            },
                                    }).then((response) => response.json())
                                             .then((responseJson) => {

                                              if( responseJson.status == '1'){

                                                  var profile = responseJson.profile;

                                                 

                                                  AsyncStorage.setItem('USER',this.checknull(JSON.stringify(profile) ));

                                                  var name = profile.name;

                                                  this.setState({
                                                     isLoading      : true,
                                                     name           : name.toUpperCase(),
                                                     user_id        : profile.user_id,
                                                     profileimage   : profile.profile_image,
                                                     profileimage2  : profile.profile_image1,
                                                     password       : profile.password,
                                                     subscribe      : profile.subscribe,
                                                     dob            : profile.dob,
                                                     email          : profile.email,
                                                     created_on     : profile.created_on,
                                                  });
                                                    
                                                 AsyncStorage.multiSet([
                                                                    ["name",                this.checknull(this.state.name)],
                                                                    ["user_id",             this.checknull(this.state.user_id)],
                                                                    ["profileimage",        this.checknull(profile.profile_image)],
                                                                    ["profileimage2",       this.checknull(profile.profile_image1)],
                                                                    ["password",            this.checknull(this.state.password)],
                                                                    ["status",              this.checknull(JSON.stringify(profile.status))],
                                                                    ["subscribe",           this.checknull(JSON.stringify(profile.subscribe))],
                                                                    ["dob",                 this.checknull(this.state.dob)],
                                                                    ["email",               this.checknull(this.state.email)],
                                                                    ["created_on",          this.checknull(this.state.created_on)],
                                                                ]);


                                              let Moneyformdata = new FormData();

                                              Moneyformdata.append('user_id', profile.user_id );



                                              try {
                                                      let response = fetch( GVar.BaseUrl + GVar.UserMoney,{
                                                                method: 'POST',
                                                                headers:{
                                                                            'token' : token
                                                                        },
                                                                body: Moneyformdata,        
                                                                }).then((response) => response.json())
                                                               .then((responseJson) => {
                                                            
                                                                if( responseJson.status == '1'){

                                                                  var money = responseJson.money;

                                                                  console.log(money,"money");

                                                                  AsyncStorage.multiSet([
                                                                      ["coin5cent",       this.checknull(money.total_5cent_coin)],
                                                                      ["coin10cent",      this.checknull(money.total_10cent_coin)],
                                                                      ["coin20cent",      this.checknull(money.total_20cent_coin)],
                                                                      ["coin50cent",      this.checknull(money.total_50cent_coin)],
                                                                      ["coin1dollar",     this.checknull(money.total_1dollar_coin)],
                                                                      ["coin2dollar",     this.checknull(money.total_2dollar_coin)],
                                                                      ["note5dollar",     this.checknull(money.total_5dollar_note)],
                                                                      ["note10dollar",    this.checknull(money.total_10dollar_note)],
                                                                      ["note20dollar",    this.checknull(money.total_20dollar_note)],
                                                                      ["note50dollar",    this.checknull(money.total_50dollar_note)],
                                                                      ["note100dollar",   this.checknull(money.total_100dollar_note)],
                                                                      ["totalmoney",      this.checknull(money.total_amount)],
                                                                  ]);
                                                                  
                                                                }else{

                                                                  AsyncStorage.multiSet([
                                                                      ["coin5cent",       this.checknull(JSON.stringify(0))],
                                                                      ["coin10cent",      this.checknull(JSON.stringify(0))],
                                                                      ["coin20cent",      this.checknull(JSON.stringify(0))],
                                                                      ["coin50cent",      this.checknull(JSON.stringify(0))],
                                                                      ["coin1dollar",     this.checknull(JSON.stringify(0))],
                                                                      ["coin2dollar",     this.checknull(JSON.stringify(0))],
                                                                      ["note5dollar",     this.checknull(JSON.stringify(0))],
                                                                      ["note10dollar",    this.checknull(JSON.stringify(0))],
                                                                      ["note20dollar",    this.checknull(JSON.stringify(0))],
                                                                      ["note50dollar",    this.checknull(JSON.stringify(0))],
                                                                      ["note100dollar",   this.checknull(JSON.stringify(0))],
                                                                      ["totalmoney",      this.checknull(JSON.stringify(0))],
                                                                  ]);
                                                                  
                                                                }
                                                                    
                                                               });
                                              
                                                }catch (error) {

                                                      if( error.line == 18228 ){
                                                        
                                                      
                                                      }

                                                }
                                                

                                              }else{
                                                
                                              }
                                                  
                                             });

              
                  
               }catch (error) {

                if( error.line == 18228 ){
                      
                    
                }

              }



          AsyncStorage.multiGet(["created_on","subscribe"]).then(response => {

              var created_on                          = response[0][1];
              var subscribe                           = response[1][1];

              

              var theDate   = new Date();
              today         = theDate.toGMTString();
              Atoday        = today.split(" ");
              var Dnow      = Atoday[1] + Atoday[2] + Atoday[3]
             

              var date0 = new Date(new Date(created_on * 1000).getTime());
              newonedays = date0.toGMTString();

              //Alert.alert(newonedays)


              var date1       = new Date(new Date(created_on * 1000).getTime()+(16*24*60*60*1000));
              newsixteendays  = date1.toGMTString();

              Anewsixteendays = newsixteendays.split(" ");
              var D16         = Anewsixteendays[1] + Anewsixteendays[2] + Anewsixteendays[3];
              



              var date2       = new Date(new Date(created_on * 1000).getTime()+(21*24*60*60*1000));
              newtwentryone   = date2.toGMTString();

              Anewtwentryone  = newtwentryone.split(" ");
              var D21         = Anewtwentryone[1] + Anewtwentryone[2] + Anewtwentryone[3];




              var date3       = new Date(new Date(created_on * 1000).getTime()+(30*24*60*60*1000));
              newThirtydays   = date3.toGMTString();

              AnewThirtydays  = newThirtydays.split(" ");
              var D30         = AnewThirtydays[1] + AnewThirtydays[2] + AnewThirtydays[3];




              var date4           = new Date(new Date(created_on * 1000).getTime()+(335*24*60*60*1000));
              newellevenMonths    = date4.toGMTString();

              AnewellevenMonths   = newellevenMonths.split(" ");
              var D11m             = AnewellevenMonths[1] + AnewellevenMonths[2] + AnewellevenMonths[3];
              //alert(D11m)

              var date5                     = new Date(new Date(created_on * 1000).getTime()+(349*24*60*60*1000));
              newellevenMonthsfourteendays  = date5.toGMTString();

              AnewellevenMonthsfourteendays = newellevenMonthsfourteendays.split(" ");
              var D11m14d                   = AnewellevenMonthsfourteendays[1] + AnewellevenMonthsfourteendays[2] + AnewellevenMonthsfourteendays[3];
              
              //Alert.alert(newonedays)


              var date6                       = new Date(new Date(created_on * 1000).getTime()+(356*24*60*60*1000));
              newellevenMonthstwentyonedays   = date6.toGMTString();

              AnewellevenMonthstwentyonedays  = newellevenMonthstwentyonedays.split(" ");
              var D11m21d                     = AnewellevenMonthstwentyonedays[1] + AnewellevenMonthstwentyonedays[2] + AnewellevenMonthstwentyonedays[3];
              //Alert.alert(D11m21d)


              var date7         = new Date(new Date(created_on * 1000).getTime()+(365*24*60*60*1000));
              newtwelveMonths   = date7.toGMTString();

              AnewtwelveMonths  = newtwelveMonths.split(" ");
              var D12m          = AnewtwelveMonths[1] + AnewtwelveMonths[2] + AnewtwelveMonths[3];
              


              //Alert.alert(date7)

             if( JSON.parse(subscribe) == 0 ){

                console.log(Dnow,"DnowDnow");
                console.log(D16,"D16");
                console.log(D21,"D21");
                console.log(D30,"D30");
                console.log(D11m,"D11m");
                console.log(D11m14d,"D11m14d");
                console.log(D11m21d,"D11m21d");
                console.log(D12m,"D12m");
                  

                  if( Dnow == D16 ){
                      Alert.alert(
                                    this.state.expire14days ? this.state.expire14days : "Your free trial period will expire in 14 days",
                                    this.state.extendnow ? this.state.extendnow : 'If you extend now, the new start date will commence in 30 days’ time. You will not lose any of your current period. Would you like to extend, only $5.99 for another 12 months ?',
                                    [
                                      {text: this.state.noIwillwait ? this.state.noIwillwait : 'No, I will wait', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                      {text: this.state.Yes ? this.state.Yes : 'Yes', onPress: () => {}},
                                    ],
                                    { cancelable: false }
                                  )
                  }

                  if( Dnow == D21 ){
                      Alert.alert(
                                    this.state.expire7days ? this.state.expire7days :"Your free trial period will expire in 7 days.",
                                    this.state.extendnow ? this.state.extendnow : 'If you extend now, the new start date will commence in 30 days’ time. You will not lose any of your current period. Would you like to extend, only $5.99 for another 12 months ?',
                                    [
                                      {text: this.state.noIwillwait ? this.state.noIwillwait : 'No, I will wait', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                      {text: this.state.Yes ? this.state.Yes : 'Yes', onPress: () => {}},
                                    ],
                                    { cancelable: false }
                                  )
                  }

                  if( Dnow == D30 ){
                    Alert.alert(
                                    this.state.hasexpiredtoday ? this.state.hasexpiredtoday : "Your free trial period has expired. Would you like to extend 'Shopping Helper', only $5.99 for 12 months ?",
                                    '',
                                    [
                                      {text: this.state.Yes ? this.state.Yes : 'Yes', onPress: () => {}},
                                    ],
                                    { cancelable: false }
                                  )
                  }

                 
                   if( Dnow == D11m ){
                    Alert.alert(
                                    this.state.extenddue30days ? this.state.extenddue30days : "Your ‘shopping helper’ is due to expire in 30 days.",
                                    this.state.extendnow ? this.state.extendnow : 'If you extend now, the new start date will commence in 30 days’ time. You will not lose any of your current period. Would you like to extend, only $5.99 for another 12 months ?',
                                    [
                                      {text: this.state.Yes ? this.state.Yes : 'Yes', onPress: () => {}},
                                    ],
                                    { cancelable: false }
                                  )
                  }


                  if( Dnow == D11m14d ){
                    Alert.alert(
                                    this.state.extenddue14days ? this.state.extenddue14days : "Your 'Shopping Helper' is due to expire in 14 days.",
                                    this.state.extendnow ? this.state.extendnow : 'If you extend now, the new start date will commence in 30 days’ time. You will not lose any of your current period. Would you like to extend, only $5.99 for another 12 months ?',
                                    [
                                      {text: this.state.Yes ? this.state.Yes : 'Yes', onPress: () => {}},
                                    ],
                                    { cancelable: false }
                                  )
                  }


                  if( Dnow == D11m21d ){
                    Alert.alert(
                                    this.state.expire7days ? this.state.expire7days : "Your 'Shopping Helper' is due to expire in 7 days.",
                                    this.state.extendnow ? this.state.extendnow : 'If you extend now, the new start date will commence in 30 days’ time. You will not lose any of your current period. Would you like to extend, only $5.99 for another 12 months ?',
                                    [
                                      {text:  this.state.Yes ? this.state.Yes : 'Yes', onPress: () => {}},
                                    ],
                                    { cancelable: false }
                                  )
                  }

             }

             if( Dnow == D12m ){
                    Alert.alert(
                                    this.state.hasexpired ? this.state.hasexpired : 'Your ‘shopping helper’ has expired.',
                                    this.state.Wouldyouliketoextend ? this.state.Wouldyouliketoextend : 'Would you like to extend, only $5.99 for another 12 months',
                                    [
                                      {text: 'Yes', onPress: () => {}},
                                    ],
                                    { cancelable: false }
                                  )
              }
   
          });

          


            let formdata = new FormData();

            formdata.append('user_id',        user_id );
            formdata.append('money',          totalmoney );
            formdata.append('5centcoin',      coin5cent );
            formdata.append('10centcoin',     coin10cent );
            formdata.append('20centcoin',     coin20cent );
            formdata.append('50centcoin',     coin50cent );
            formdata.append('1dollarcoin',    coin1dollar );
            formdata.append('2dollarcoin',    coin2dollar );
            formdata.append('5dollarnote',    note5dollar );
            formdata.append('10dollarnote',   note10dollar );
            formdata.append('20dollarnote',   note20dollar );
            formdata.append('50dollarnote',   note50dollar );
            formdata.append('100dollarnote',  note100dollar );

           // Alert.alert( coin5cent +" "+  coin10cent  +" "+coin20cent  +" "+coin50cent +" "+coin1dollar+" "+coin2dollar+" "+note5dollar +" "+note10dollar+" "+note20dollar+" "+note50dollar+" "+note100dollar +" "+ totalmoney)


            try {
                let response = fetch( GVar.BaseUrl + GVar.UpdateMoney,{
                                  method: 'POST',
                                  headers:{
                                              'token' : token
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                 .then((responseJson) => {

                                  //Alert.alert(JSON.stringify(responseJson));

                                      
                                 });
             }catch (error) {


            }


            

           
          });
 }

  componentDidMount = async () => {

    AsyncStorage.multiGet(["Localprofileimage", "Localprofileimage2"]).then(response => {
        
        var Localprofileimage              = response[0][1];
        var Localprofileimage2             = response[1][1];
      
        this.setState({
                                Localprofileimage     : JSON.parse(Localprofileimage),
                                Localprofileimage2    : JSON.parse(Localprofileimage2)
                      });          
    });

   
    this.setLanguages = this.setLanguages();
  }


checknull(value){
  if(value){
    return value;
  }
  else {
    return '';
  }
}

  setLanguages(){

  

    AsyncStorage.getItem('PrimaryLanguage').then((P_LANG) => {
          
          

          AsyncStorage.getItem('SHOPPING HELPER').then((data) => {

                var SHOPPING = JSON.parse(data);




                if( P_LANG == 'en'){

                  this.setState({SHOPPING : SHOPPING.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SHOPPING : SHOPPING.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SHOPPING : SHOPPING.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SHOPPING : SHOPPING.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SHOPPING : SHOPPING.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SHOPPING : SHOPPING.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SHOPPING : SHOPPING.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SHOPPING : SHOPPING.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SHOPPING : SHOPPING.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SHOPPING : SHOPPING.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SHOPPING : SHOPPING.fil})
                }

          });

          AsyncStorage.getItem('SHOPPING LIST').then((data) => {

                var SHOPPINGLIST = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({SHOPPINGLIST : SHOPPINGLIST.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SHOPPINGLIST : SHOPPINGLIST.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SHOPPINGLIST : SHOPPINGLIST.fil})
                }

          });

          AsyncStorage.getItem('LETS GO SHOPPING').then((data) => {

                var GOSHOPPING = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({GOSHOPPING : GOSHOPPING.en})
                }

                if( P_LANG == 'el'){

                    this.setState({GOSHOPPING : GOSHOPPING.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({GOSHOPPING : GOSHOPPING.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({GOSHOPPING : GOSHOPPING.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({GOSHOPPING : GOSHOPPING.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({GOSHOPPING : GOSHOPPING.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({GOSHOPPING : GOSHOPPING.it})
                }

                if( P_LANG == 'es'){

                    this.setState({GOSHOPPING : GOSHOPPING.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({GOSHOPPING : GOSHOPPING.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({GOSHOPPING : GOSHOPPING.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({GOSHOPPING : GOSHOPPING.fil})
                }

          });
AsyncStorage.getItem('Are you sure to log out?').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforlogout : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforlogout : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforlogout : voicefeatureGS.fil})
                              }

                        });
                AsyncStorage.getItem('ok').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforok : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforok : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforok : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforok : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforok : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforok : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforok : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforok : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforok : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforok : voicefeatureGS.fil})
                              }

                        });
                          AsyncStorage.getItem('cancel').then((data) => {

                         

                              var voicefeatureGS = JSON.parse(data);
                              

                              if( P_LANG == 'en'){

                                this.setState({voicefeatureforcancel : voicefeatureGS.en})
                              }

                              if( P_LANG == 'el'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.el})
                              }

                              if( P_LANG == 'cmn'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.cmn})
                              }

                              if( P_LANG == 'zh'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.zh})
                              }

                              if( P_LANG == 'ar'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.ar})
                              }

                              if( P_LANG == 'vi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.vi})
                              }

                              if( P_LANG == 'it'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.it})
                              }

                              if( P_LANG == 'es'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.es})
                              }

                              if( P_LANG == 'hi'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.hi})
                              }

                              if( P_LANG == 'pa'){

                                  this.setState({voicefeatureforcancel : voicefeatureGS.pa})
                              }

                              if( P_LANG == 'fil'){

                                 this.setState({voicefeatureforcancel : voicefeatureGS.fil})
                              }

                        });


          AsyncStorage.getItem('PANTRY').then((data) => {

                var PANTRY = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({PANTRY : PANTRY.en})
                }

                if( P_LANG == 'el'){

                    this.setState({PANTRY : PANTRY.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({PANTRY : PANTRY.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({PANTRY : PANTRY.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({PANTRY : PANTRY.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({PANTRY : PANTRY.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({PANTRY : PANTRY.it})
                }

                if( P_LANG == 'es'){

                    this.setState({PANTRY : PANTRY.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({PANTRY : PANTRY.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({PANTRY : PANTRY.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({PANTRY : PANTRY.fil})
                }

          });

          AsyncStorage.getItem('HOW MUCH MONEY DO I HAVE').then((data) => {

                var MONEYIHAVE = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYIHAVE : MONEYIHAVE.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYIHAVE : MONEYIHAVE.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYIHAVE : MONEYIHAVE.fil})
                }

          });

          AsyncStorage.getItem('MONEY MANAGEMENT').then((data) => {

                var MONEYMANAGEMENT = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.en})
                }

                if( P_LANG == 'el'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.it})
                }

                if( P_LANG == 'es'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({MONEYMANAGEMENT : MONEYMANAGEMENT.fil})
                }

          });

          AsyncStorage.getItem('SETUP').then((data) => {

                var SETUP = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({SETUP : SETUP.en})
                }

                if( P_LANG == 'el'){

                    this.setState({SETUP : SETUP.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({SETUP : SETUP.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({SETUP : SETUP.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({SETUP : SETUP.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({SETUP : SETUP.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({SETUP : SETUP.it})
                }

                if( P_LANG == 'es'){

                    this.setState({SETUP : SETUP.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({SETUP : SETUP.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({SETUP : SETUP.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({SETUP : SETUP.fil})
                }

          });

          AsyncStorage.getItem('LOGOUT').then((data) => {

                var LOGOUT = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({LOGOUT : LOGOUT.en})
                }

                if( P_LANG == 'el'){

                    this.setState({LOGOUT : LOGOUT.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({LOGOUT : LOGOUT.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({LOGOUT : LOGOUT.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({LOGOUT : LOGOUT.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({LOGOUT : LOGOUT.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({LOGOUT : LOGOUT.it})
                }

                if( P_LANG == 'es'){

                    this.setState({LOGOUT : LOGOUT.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({LOGOUT : LOGOUT.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({LOGOUT : LOGOUT.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({LOGOUT : LOGOUT.fil})
                }

          });

          AsyncStorage.getItem('Your ‘shopping helper’ has expired.').then((data) => {

                var hasexpired = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({hasexpired : hasexpired.en})
                }

                if( P_LANG == 'el'){

                    this.setState({hasexpired : hasexpired.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({hasexpired : hasexpired.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({hasexpired : hasexpired.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({hasexpired : hasexpired.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({hasexpired : hasexpired.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({hasexpired : hasexpired.it})
                }

                if( P_LANG == 'es'){

                    this.setState({hasexpired : hasexpired.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({hasexpired : hasexpired.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({hasexpired : hasexpired.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({hasexpired : hasexpired.fil})
                }

          });

          AsyncStorage.getItem('Would you like to extend, only $5.99 for another 12 months').then((data) => {

                var Wouldyouliketoextend = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({Wouldyouliketoextend : Wouldyouliketoextend.en})
                }

                if( P_LANG == 'el'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.it})
                }

                if( P_LANG == 'es'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({Wouldyouliketoextend : Wouldyouliketoextend.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({Wouldyouliketoextend : Wouldyouliketoextend.fil})
                }

          });

          AsyncStorage.getItem("Your Shopping Helper is due to expire in 7 days.").then((data) => {

                var expire7days = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({expire7days : expire7days.en})
                }

                if( P_LANG == 'el'){

                    this.setState({expire7days : expire7days.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({expire7days : expire7days.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({expire7days : expire7days.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({expire7days : expire7days.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({expire7days : expire7days.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({expire7days : expire7days.it})
                }

                if( P_LANG == 'es'){

                    this.setState({expire7days : expire7days.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({expire7days : expire7days.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({expire7days : expire7days.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({expire7days : expire7days.fil})
                }

          });

          AsyncStorage.getItem("If you extend now, the new start date will commence in 30 days’ time. You will not lose any of your current period. Would you like to extend, only $5.99 for another 12 months ?").then((data) => {

                var extendnow = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({extendnow : extendnow.en})
                }

                if( P_LANG == 'el'){

                    this.setState({extendnow : extendnow.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({extendnow : extendnow.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({extendnow : extendnow.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({extendnow : extendnow.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({extendnow : extendnow.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({extendnow : extendnow.it})
                }

                if( P_LANG == 'es'){

                    this.setState({extendnow : extendnow.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({extendnow : extendnow.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({extendnow : extendnow.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({extendnow : extendnow.fil})
                }

          });

          AsyncStorage.getItem("Your free trial period will expire in 14 days").then((data) => {

                var expire14days = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({expire14days : expire14days.en})
                }

                if( P_LANG == 'el'){

                    this.setState({expire14days : expire14days.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({expire14days : expire14days.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({expire14days : expire14days.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({expire14days : expire14days.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({expire14days : expire14days.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({expire14days : expire14days.it})
                }

                if( P_LANG == 'es'){

                    this.setState({expire14days : expire14days.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({expire14days : expire14days.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({expire14days : expire14days.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({expire14days : expire14days.fil})
                }

          });

          AsyncStorage.getItem("Your free trial period will expire in 7 days").then((data) => {

                var expire7days = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({expire7days : expire7days.en})
                }

                if( P_LANG == 'el'){

                    this.setState({expire7days : expire7days.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({expire7days : expire7days.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({expire7days : expire7days.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({expire7days : expire7days.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({expire7days : expire7days.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({expire7days : expire7days.it})
                }

                if( P_LANG == 'es'){

                    this.setState({expire7days : expire7days.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({expire7days : expire7days.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({expire7days : expire7days.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({expire7days : expire7days.fil})
                }

          });


          AsyncStorage.getItem("Your ‘shopping helper’ is due to expire in 30 days.").then((data) => {

                var extenddue30days = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({extenddue30days : extenddue30days.en})
                }

                if( P_LANG == 'el'){

                    this.setState({extenddue30days : extenddue30days.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({extenddue30days : extenddue30days.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({extenddue30days : extenddue30days.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({extenddue30days : extenddue30days.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({extenddue30days : extenddue30days.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({extenddue30days : extenddue30days.it})
                }

                if( P_LANG == 'es'){

                    this.setState({extenddue30days : extenddue30days.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({extenddue30days : extenddue30days.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({extenddue30days : extenddue30days.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({extenddue30days : extenddue30days.fil})
                }

          });

          AsyncStorage.getItem("Your Shopping Helper is due to expire in 14 days.").then((data) => {

                var extenddue14days = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({extenddue14days : extenddue14days.en})
                }

                if( P_LANG == 'el'){

                    this.setState({extenddue14days : extenddue14days.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({extenddue14days : extenddue14days.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({extenddue14days : extenddue14days.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({extenddue14days : extenddue14days.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({extenddue14days : extenddue14days.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({extenddue14days : extenddue14days.it})
                }

                if( P_LANG == 'es'){

                    this.setState({extenddue14days : extenddue14days.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({extenddue14days : extenddue14days.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({extenddue14days : extenddue14days.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({extenddue14days : extenddue14days.fil})
                }

          });

          AsyncStorage.getItem("Your free trial period has expired. Would you like to extend Shopping Helper, only $5.99 for 12 months ?").then((data) => {

                var hasexpiredtoday = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({hasexpiredtoday : hasexpiredtoday.en})
                }

                if( P_LANG == 'el'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.it})
                }

                if( P_LANG == 'es'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({hasexpiredtoday : hasexpiredtoday.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({hasexpiredtoday : hasexpiredtoday.fil})
                }

          });

          AsyncStorage.getItem("No, I will wait").then((data) => {

                var noIwillwait = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({noIwillwait : noIwillwait.en})
                }

                if( P_LANG == 'el'){

                    this.setState({noIwillwait : noIwillwait.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({noIwillwait : noIwillwait.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({noIwillwait : noIwillwait.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({noIwillwait : noIwillwait.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({noIwillwait : noIwillwait.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({noIwillwait : noIwillwait.it})
                }

                if( P_LANG == 'es'){

                    this.setState({noIwillwait : noIwillwait.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({noIwillwait : noIwillwait.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({noIwillwait : noIwillwait.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({noIwillwait : noIwillwait.fil})
                }

          });

          AsyncStorage.getItem("Yes").then((data) => {

                var Yes = JSON.parse(data);


                if( P_LANG == 'en'){

                  this.setState({Yes : Yes.en})
                }

                if( P_LANG == 'el'){

                    this.setState({Yes : Yes.el})
                }

                if( P_LANG == 'cmn'){

                    this.setState({Yes : Yes.cmn})
                }

                if( P_LANG == 'zh'){

                    this.setState({Yes : Yes.zh})
                }

                if( P_LANG == 'ar'){

                    this.setState({Yes : Yes.ar})
                }

                if( P_LANG == 'vi'){

                    this.setState({Yes : Yes.vi})
                }

                if( P_LANG == 'it'){

                    this.setState({Yes : Yes.it})
                }

                if( P_LANG == 'es'){

                    this.setState({Yes : Yes.es})
                }

                if( P_LANG == 'hi'){

                    this.setState({Yes : Yes.hi})
                }

                if( P_LANG == 'pa'){

                    this.setState({Yes : Yes.pa})
                }

                if( P_LANG == 'fil'){

                   this.setState({Yes : Yes.fil})
                }

          });
    });
  }


  onPressShoopingList(){
    Actions.ShoppingList();
    //Actions.Login();

  }

  onPressGoShopping(){
    Actions.GoShopping();
  }

  onPressPantry(){
    Actions.Pantry();
  }

  onPressMyMoney(){
     Actions.MyMoney();
  }

  onPressMoneyManagement(){
      Actions.MoneyManagement();
  }

  onPressSetUp(){
      Actions.SetUp();
      //Actions.Login();
  }


  setCamera1(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);

        Alert.alert("Please check Permition")
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
      

      let source = { uri: response.uri };

      //alert(source)

      
      var token = this.state.token;
      var user_id = this.state.user_id;
      var Selfietype = this.state.Selfietype;


      let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie1', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
        
        this.setState({ Localprofileimage : source});

        AsyncStorage.multiSet([
                                    ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                   
                                ]);

        //Alert.alert(Selfietype)
        if( Selfietype === "same"){

          this.setState({ Localprofileimage : source, Localprofileimage2: source });
          AsyncStorage.multiSet([
                                    ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                    ["Localprofileimage2",         this.checknull(JSON.stringify(source))],
                                ]);

        }

      }
    });
  }

 /* export function setimage(){
     var value=null;
     var GVar = GlobalVariables.getApiUrl();
    if(this.state.Localprofileimage === null){
      if(this.state.profileimage===null){
       value='../assets/images/shoppingimages/avtar.jpg';
        return value;
      }
      else{
          value=GVar.BaseImageURl+this.state.profileimage;
          return value;
      }
    else{
          value=this.state.Localprofileimage;
          return value;
      }
    }



    if(this.state.Localprofileimage == null)
                                {
                                  if(this.state.profileimage==null)
                                  {
                                       <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                  }
                                  else{

                                    let Image_Http_URL ={ uri:GlobalVariables.getApiUrl().BaseImageURl};

                                      <Image source={Image_Http_URL} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  }
                                }
                                else{
                                  <Image source={this.state.Localprofileimage} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                }
  }*/


  setCamera(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        

     // alert(source)

      
      var token = this.state.token;
      var user_id = this.state.user_id;
      var Selfietype = this.state.Selfietype;


      

      let formdata = new FormData();

      formdata.append('user_id', user_id );
  
      formdata.append('selfie2', {
        uri: response.uri,
        type: 'image/jpeg', // or photo.type
        name: 'testPhotoName'
      });

      var GVar = GlobalVariables.getApiUrl();




      
      try {
            

          try {
                let response = fetch( GVar.BaseUrl + GVar.UploadSelfie,{
                                  method: 'POST',
                                  headers:{
                                              'Accept': 'application/json',
                                              'token' : token,
                                              'Content-Type' : 'multipart/form-data',
                                          },
                                  body: formdata,        
                                  }).then((response) => response.json())
                                           .then((responseJson) => {

                                            //Alert.alert(JSON.stringify(responseJson));
                                                
                                           });
              
                
             }catch (error) {

              if( error.line == 18228 ){
                


              }

            }

                
      }catch (error) {

          if( error.line == 18228 ){
            //Alert.alert("Please connect to network");
          }

               
      }
        
        this.setState({ Localprofileimage2 : source});

        AsyncStorage.multiSet([
                                    ["Localprofileimage2",          this.checknull(JSON.stringify(source))],
                                   
                                ]);

        //Alert.alert(Selfietype)
        if( Selfietype === "same"){

          this.setState({ Localprofileimage : source, Localprofileimage2: source });
          AsyncStorage.multiSet([
                                    ["Localprofileimage",          this.checknull(JSON.stringify(source))],
                                    ["Localprofileimage2",         this.checknull(JSON.stringify(source))],
                                ]);

        }
      }
    });
  }


  onPressLogout(){

    //Alert.alert("fsd");
    AsyncStorage.removeItem('TOKEN');
    Actions.Login();

  }




  render() {
    return (
          <View style={styles.container}>
          <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>
           
              <ImageBackground source={require('../assets/images/shopper.jpeg')}
                    style={[
                   styles.base,{
                       width: "100%"
                   }, {
                       height: "100%"
                   }
               ]}>
              
              <View style={{flex: 1,}}>

                  <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between',marginLeft:hp('5%')}}>
                    
                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%', height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera1.bind(this)}>
                              <View style={{}}>
                              { 
                                this.state.Localprofileimage != null
                               ? <Image source={this.state.Localprofileimage} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              
                              </View>
                            </TouchableOpacity>
                            </View>

                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>

                    <View style={{width: wp('62%'), height: hp('10%'),marginTop:hp('2%'),}} >
                          <View style={{flex: 1, alignSelf:'center', marginTop:hp('5%')}}>
                          <Text style = {styles.hometext}>{this.state.name}'S</Text>
                          <Text style = {styles.hometext}>{this.state.PrimaryLanguage != '' ? this.state.SHOPPING : 'SHOPPING HELPER'}</Text>
                          </View>
                    </View>

                    <View style={{width: wp('18%'), height: hp('20%'),marginTop:hp('2%'), marginRight:hp('5%')}} >
                          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'space-between',}}>
                            
                            <View style={{width: '70%',height: hp('25%'), backgroundColor: '#FFFFFF'}} >
                            <TouchableOpacity onPress={this.setCamera.bind(this)}>
                              <View style={{}}>
                              { 
                                this.state.Localprofileimage2 != null
                               ? <Image source={this.state.Localprofileimage2} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                               : [
                               (this.state.profileimage2!=null
                               ?
                                <Image source={{ uri: GlobalVariables.getApiUrl().BaseImageURl+this.state.profileimage2}} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                              : 
                               <Image source={require('../assets/images/shoppingimages/avtar.jpg')} style={{height:'100%', width:'75%', alignSelf:'center', marginTop:5}}/>
                                ),]
                              }
                              </View>
                            </TouchableOpacity>
                            </View>


                            <View style={{width: '70%', height: hp('5%'), backgroundColor: '#FFFFFF'}} >

                                <View style={{flex: 1,flexDirection: 'row',justifyContent: 'center',}}>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF',marginTop:0}} >
                                       <Image source={require('../assets/images/shoppingimages/selfie-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center', marginLeft:10}}/>
                                  </View>
                                  
                                  <View style={{width: wp('5%'), height: hp('5%'), backgroundColor: '#FFFFFF', marginTop:0}}>
                                      <Image source={require('../assets/images/shoppingimages/upload-icon.png')} style={{width: wp('3%'), height: hp('4%'),alignSelf:'center',marginLeft:-10}}/>
                                  </View>

                                </View>

                            </View>

                          </View>
                    </View>
                  </View>


                  <View style={{flex: 4,flexDirection: 'row',alignSelf:'center',width:wp('100%'),width: hp('60%') }}>
                      
                  </View>

                  <View style={{flex: 1,flexDirection: 'row', alignSelf:'center',marginTop: hp('4%')}} >
                    
                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressShoopingList.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/shopping-list.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center'}}>
                      <Text style={{alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5)}}>{this.state.PrimaryLanguage ? this.state.SHOPPINGLIST : 'SHOPPING LIST'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressGoShopping.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/letsgo.png')} style={{width: wp('13.5%'), height: hp('16.5%'), justifyContent:'center',}}>
                        <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('8%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.GOSHOPPING : 'LETS GO SHOPPING'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressPantry.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/pantry.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                        <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.PANTRY : 'PANTRY'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMyMoney.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/howmuch.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                        <Text style={{marginLeft:'8%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYIHAVE : 'HOW MUCH MONEY I HAVE'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressMoneyManagement.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/money-management.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                        <Text style={{marginLeft:'5%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.MONEYMANAGEMENT : 'MONEY MANAGEMENT'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={this.onPressSetUp.bind(this)}  underlayColor='#232A40'>
                    <ImageBackground source={require('../assets/images/shoppingimages/setup.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                        <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.SETUP : 'SETUP'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homebutton_style} onPress={() => {
                                              Alert.alert(
                                               this.state.PrimaryLanguage ? this.state.voicefeatureforlogout : 'Are you sure to Logout',
                                                '',
                                                [
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforcancel : 'cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                  {text: this.state.PrimaryLanguage ? this.state.voicefeatureforok : 'Ok', onPress: () => {this.onPressLogout(this)}},
                                                ],
                                                { cancelable: false }
                                              )
                                    }}>
                    <ImageBackground source={require('../assets/images/shoppingimages/logoutmain.png')} style={{width: wp('13.5%'), height: hp('16.5%'),justifyContent:'center',}}>
                      <Text style={{marginLeft:'0%', alignSelf:'center', marginTop:hp('10%'), color: '#FFFFFF', fontWeight: 'bold', fontSize: RF(2.5),}}>{this.state.PrimaryLanguage ? this.state.LOGOUT : 'LOGOUT'}</Text>
                    </ImageBackground>
                    </TouchableOpacity>

                  </View>
              </View>
              {this.state.isLoading ? <View/> : <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> }
              </ImageBackground>
                 
          </View> 
    );
  }
}


